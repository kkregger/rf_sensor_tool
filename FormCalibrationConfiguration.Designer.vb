﻿Namespace CTS_RF_Sensor_Tool
	Partial Public Class FormCalibrationConfiguration
		''' <summary>
		''' Required designer variable.
		''' </summary>
		Private components As System.ComponentModel.IContainer = Nothing

		''' <summary>
		''' Clean up any resources being used.
		''' </summary>
		''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		Protected Overrides Sub Dispose(ByVal disposing As Boolean)
			If disposing AndAlso (components IsNot Nothing) Then
				components.Dispose()
			End If
			MyBase.Dispose(disposing)
		End Sub

		#Region "Windows Form Designer generated code"

		''' <summary>
		''' Required method for Designer support - do not modify
		''' the contents of this method with the code editor.
		''' </summary>
		Private Sub InitializeComponent()
			Dim resources As New System.ComponentModel.ComponentResourceManager(GetType(FormCalibrationConfiguration))
			Me.ListBoxCalibrations = New System.Windows.Forms.ListBox()
			Me.panel1 = New System.Windows.Forms.Panel()
			Me.label1 = New System.Windows.Forms.Label()
			Me.PanelDataFields = New System.Windows.Forms.Panel()
			Me.CheckBoxBinaryFunction = New System.Windows.Forms.CheckBox()
			Me.TextBoxDescription = New System.Windows.Forms.TextBox()
			Me.label6 = New System.Windows.Forms.Label()
			Me.TextBoxFunction = New System.Windows.Forms.TextBox()
			Me.label5 = New System.Windows.Forms.Label()
			Me.TextBoxCoefficents = New System.Windows.Forms.TextBox()
			Me.label4 = New System.Windows.Forms.Label()
			Me.label3 = New System.Windows.Forms.Label()
			Me.NumericUpDownSootLoad = New System.Windows.Forms.NumericUpDown()
			Me.label2 = New System.Windows.Forms.Label()
			Me.panel3 = New System.Windows.Forms.Panel()
			Me.ButtonCancel = New System.Windows.Forms.Button()
			Me.ButtonAdd = New System.Windows.Forms.Button()
			Me.ButtonEdit = New System.Windows.Forms.Button()
			Me.ButtonRemove = New System.Windows.Forms.Button()
			Me.ButtonUpdate = New System.Windows.Forms.Button()
			Me.ButtonClose = New System.Windows.Forms.Button()
			Me.CopyThenEditButton = New System.Windows.Forms.Button()
			Me.panel1.SuspendLayout()
			Me.PanelDataFields.SuspendLayout()
			CType(Me.NumericUpDownSootLoad, System.ComponentModel.ISupportInitialize).BeginInit()
			Me.panel3.SuspendLayout()
			Me.SuspendLayout()
			' 
			' ListBoxCalibrations
			' 
			Me.ListBoxCalibrations.Dock = System.Windows.Forms.DockStyle.Fill
			Me.ListBoxCalibrations.FormatString = "N2"
			Me.ListBoxCalibrations.FormattingEnabled = True
			Me.ListBoxCalibrations.Items.AddRange(New Object() { ""})
			Me.ListBoxCalibrations.Location = New System.Drawing.Point(0, 23)
			Me.ListBoxCalibrations.Name = "ListBoxCalibrations"
			Me.ListBoxCalibrations.Size = New System.Drawing.Size(196, 225)
			Me.ListBoxCalibrations.TabIndex = 0
'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
'ORIGINAL LINE: this.ListBoxCalibrations.SelectedIndexChanged += new System.EventHandler(this.ListBoxCalibrations_SelectedIndexChanged);
			' 
			' panel1
			' 
			Me.panel1.Controls.Add(Me.ListBoxCalibrations)
			Me.panel1.Controls.Add(Me.label1)
			Me.panel1.Dock = System.Windows.Forms.DockStyle.Left
			Me.panel1.Location = New System.Drawing.Point(0, 0)
			Me.panel1.Name = "panel1"
			Me.panel1.Size = New System.Drawing.Size(196, 248)
			Me.panel1.TabIndex = 3
			' 
			' label1
			' 
			Me.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
			Me.label1.Dock = System.Windows.Forms.DockStyle.Top
			Me.label1.Location = New System.Drawing.Point(0, 0)
			Me.label1.Name = "label1"
			Me.label1.Size = New System.Drawing.Size(196, 23)
			Me.label1.TabIndex = 0
			Me.label1.Text = "Calibration Function"
			Me.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
			' 
			' PanelDataFields
			' 
			Me.PanelDataFields.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
			Me.PanelDataFields.Controls.Add(Me.CheckBoxBinaryFunction)
			Me.PanelDataFields.Controls.Add(Me.TextBoxDescription)
			Me.PanelDataFields.Controls.Add(Me.label6)
			Me.PanelDataFields.Controls.Add(Me.TextBoxFunction)
			Me.PanelDataFields.Controls.Add(Me.label5)
			Me.PanelDataFields.Controls.Add(Me.TextBoxCoefficents)
			Me.PanelDataFields.Controls.Add(Me.label4)
			Me.PanelDataFields.Controls.Add(Me.label3)
			Me.PanelDataFields.Controls.Add(Me.NumericUpDownSootLoad)
			Me.PanelDataFields.Controls.Add(Me.label2)
			Me.PanelDataFields.Dock = System.Windows.Forms.DockStyle.Top
			Me.PanelDataFields.Enabled = False
			Me.PanelDataFields.Location = New System.Drawing.Point(196, 0)
			Me.PanelDataFields.Name = "PanelDataFields"
			Me.PanelDataFields.Size = New System.Drawing.Size(391, 165)
			Me.PanelDataFields.TabIndex = 4
			' 
			' CheckBoxBinaryFunction
			' 
			Me.CheckBoxBinaryFunction.AutoSize = True
			Me.CheckBoxBinaryFunction.Location = New System.Drawing.Point(192, 38)
			Me.CheckBoxBinaryFunction.Name = "CheckBoxBinaryFunction"
			Me.CheckBoxBinaryFunction.Size = New System.Drawing.Size(99, 17)
			Me.CheckBoxBinaryFunction.TabIndex = 2
			Me.CheckBoxBinaryFunction.Text = "Binary Function"
			Me.CheckBoxBinaryFunction.UseVisualStyleBackColor = True
'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
'ORIGINAL LINE: this.CheckBoxBinaryFunction.CheckedChanged += new System.EventHandler(this.CheckBoxBinaryFunction_CheckedChanged);
			' 
			' TextBoxDescription
			' 
			Me.TextBoxDescription.Location = New System.Drawing.Point(75, 10)
			Me.TextBoxDescription.Name = "TextBoxDescription"
			Me.TextBoxDescription.Size = New System.Drawing.Size(302, 20)
			Me.TextBoxDescription.TabIndex = 0
'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
'ORIGINAL LINE: this.TextBoxDescription.TextChanged += new System.EventHandler(this.TextBoxDescription_TextChanged);
			' 
			' label6
			' 
			Me.label6.AutoSize = True
			Me.label6.Location = New System.Drawing.Point(6, 13)
			Me.label6.Name = "label6"
			Me.label6.Size = New System.Drawing.Size(63, 13)
			Me.label6.TabIndex = 7
			Me.label6.Text = "Description:"
			' 
			' TextBoxFunction
			' 
			Me.TextBoxFunction.Anchor = (CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
			Me.TextBoxFunction.BackColor = System.Drawing.Color.White
			Me.TextBoxFunction.Location = New System.Drawing.Point(75, 88)
			Me.TextBoxFunction.Multiline = True
			Me.TextBoxFunction.Name = "TextBoxFunction"
			Me.TextBoxFunction.ReadOnly = True
			Me.TextBoxFunction.Size = New System.Drawing.Size(302, 69)
			Me.TextBoxFunction.TabIndex = 4
			Me.TextBoxFunction.TabStop = False
			' 
			' label5
			' 
			Me.label5.AutoSize = True
			Me.label5.Location = New System.Drawing.Point(18, 91)
			Me.label5.Name = "label5"
			Me.label5.Size = New System.Drawing.Size(51, 13)
			Me.label5.TabIndex = 5
			Me.label5.Text = "Function:"
			' 
			' TextBoxCoefficents
			' 
			Me.TextBoxCoefficents.Anchor = (CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
			Me.TextBoxCoefficents.Location = New System.Drawing.Point(75, 62)
			Me.TextBoxCoefficents.Name = "TextBoxCoefficents"
			Me.TextBoxCoefficents.Size = New System.Drawing.Size(302, 20)
			Me.TextBoxCoefficents.TabIndex = 3
'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
'ORIGINAL LINE: this.TextBoxCoefficents.TextChanged += new System.EventHandler(this.TextBoxCoefficents_TextChanged);
			' 
			' label4
			' 
			Me.label4.AutoSize = True
			Me.label4.Location = New System.Drawing.Point(4, 65)
			Me.label4.Name = "label4"
			Me.label4.Size = New System.Drawing.Size(65, 13)
			Me.label4.TabIndex = 3
			Me.label4.Text = "Coefficients:"
			' 
			' label3
			' 
			Me.label3.AutoSize = True
			Me.label3.Location = New System.Drawing.Point(152, 40)
			Me.label3.Name = "label3"
			Me.label3.Size = New System.Drawing.Size(30, 13)
			Me.label3.TabIndex = 2
			Me.label3.Text = "(g/L)"
			' 
			' NumericUpDownSootLoad
			' 
			Me.NumericUpDownSootLoad.DecimalPlaces = 3
			Me.NumericUpDownSootLoad.Location = New System.Drawing.Point(75, 36)
			Me.NumericUpDownSootLoad.Name = "NumericUpDownSootLoad"
			Me.NumericUpDownSootLoad.Size = New System.Drawing.Size(75, 20)
			Me.NumericUpDownSootLoad.TabIndex = 1
			Me.NumericUpDownSootLoad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
			Me.NumericUpDownSootLoad.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left
'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
'ORIGINAL LINE: this.NumericUpDownSootLoad.ValueChanged += new System.EventHandler(this.NumericUpDownSootLoad_ValueChanged);
			' 
			' label2
			' 
			Me.label2.AutoSize = True
			Me.label2.Location = New System.Drawing.Point(10, 38)
			Me.label2.Name = "label2"
			Me.label2.Size = New System.Drawing.Size(59, 13)
			Me.label2.TabIndex = 0
			Me.label2.Text = "Soot Load:"
			' 
			' panel3
			' 
			Me.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
			Me.panel3.Controls.Add(Me.CopyThenEditButton)
			Me.panel3.Controls.Add(Me.ButtonCancel)
			Me.panel3.Controls.Add(Me.ButtonAdd)
			Me.panel3.Controls.Add(Me.ButtonEdit)
			Me.panel3.Controls.Add(Me.ButtonRemove)
			Me.panel3.Controls.Add(Me.ButtonUpdate)
			Me.panel3.Controls.Add(Me.ButtonClose)
			Me.panel3.Dock = System.Windows.Forms.DockStyle.Fill
			Me.panel3.Location = New System.Drawing.Point(196, 165)
			Me.panel3.Name = "panel3"
			Me.panel3.Size = New System.Drawing.Size(391, 83)
			Me.panel3.TabIndex = 5
			' 
			' ButtonCancel
			' 
			Me.ButtonCancel.Location = New System.Drawing.Point(116, 46)
			Me.ButtonCancel.Name = "ButtonCancel"
			Me.ButtonCancel.Size = New System.Drawing.Size(75, 23)
			Me.ButtonCancel.TabIndex = 3
			Me.ButtonCancel.Text = "Cancel"
			Me.ButtonCancel.UseVisualStyleBackColor = True
'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
'ORIGINAL LINE: this.ButtonCancel.Click += new System.EventHandler(this.ButtonCancel_Click);
			' 
			' ButtonAdd
			' 
			Me.ButtonAdd.Location = New System.Drawing.Point(72, 17)
			Me.ButtonAdd.Name = "ButtonAdd"
			Me.ButtonAdd.Size = New System.Drawing.Size(75, 23)
			Me.ButtonAdd.TabIndex = 0
			Me.ButtonAdd.Text = "Add"
			Me.ButtonAdd.UseVisualStyleBackColor = True
'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
'ORIGINAL LINE: this.ButtonAdd.Click += new System.EventHandler(this.ButtonAdd_Click);
			' 
			' ButtonEdit
			' 
			Me.ButtonEdit.Location = New System.Drawing.Point(247, 17)
			Me.ButtonEdit.Name = "ButtonEdit"
			Me.ButtonEdit.Size = New System.Drawing.Size(75, 23)
			Me.ButtonEdit.TabIndex = 1
			Me.ButtonEdit.Text = "Edit"
			Me.ButtonEdit.UseVisualStyleBackColor = True
'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
'ORIGINAL LINE: this.ButtonEdit.Click += new System.EventHandler(this.ButtonEdit_Click);
			' 
			' ButtonRemove
			' 
			Me.ButtonRemove.Location = New System.Drawing.Point(204, 46)
			Me.ButtonRemove.Name = "ButtonRemove"
			Me.ButtonRemove.Size = New System.Drawing.Size(75, 23)
			Me.ButtonRemove.TabIndex = 4
			Me.ButtonRemove.Text = "Remove"
			Me.ButtonRemove.UseVisualStyleBackColor = True
'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
'ORIGINAL LINE: this.ButtonRemove.Click += new System.EventHandler(this.ButtonRemove_Click);
			' 
			' ButtonUpdate
			' 
			Me.ButtonUpdate.Location = New System.Drawing.Point(28, 46)
			Me.ButtonUpdate.Name = "ButtonUpdate"
			Me.ButtonUpdate.Size = New System.Drawing.Size(75, 23)
			Me.ButtonUpdate.TabIndex = 2
			Me.ButtonUpdate.Text = "Update"
			Me.ButtonUpdate.UseVisualStyleBackColor = True
'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
'ORIGINAL LINE: this.ButtonUpdate.Click += new System.EventHandler(this.ButtonUpdate_Click);
			' 
			' ButtonClose
			' 
			Me.ButtonClose.Location = New System.Drawing.Point(292, 46)
			Me.ButtonClose.Name = "ButtonClose"
			Me.ButtonClose.Size = New System.Drawing.Size(75, 23)
			Me.ButtonClose.TabIndex = 5
			Me.ButtonClose.Text = "Close"
			Me.ButtonClose.UseVisualStyleBackColor = True
'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
'ORIGINAL LINE: this.ButtonClose.Click += new System.EventHandler(this.ButtonClose_Click);
			' 
			' CopyThenEditButton
			' 
			Me.CopyThenEditButton.Location = New System.Drawing.Point(160, 17)
			Me.CopyThenEditButton.Name = "CopyThenEditButton"
			Me.CopyThenEditButton.Size = New System.Drawing.Size(75, 23)
			Me.CopyThenEditButton.TabIndex = 6
			Me.CopyThenEditButton.Text = "Copy/Edit"
			Me.CopyThenEditButton.UseVisualStyleBackColor = True
'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
'ORIGINAL LINE: this.CopyThenEditButton.Click += new System.EventHandler(this.CopyThenEditButton_Click);
			' 
			' FormCalibrationConfiguration
			' 
			Me.AutoScaleDimensions = New System.Drawing.SizeF(6F, 13F)
			Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
			Me.ClientSize = New System.Drawing.Size(587, 248)
			Me.Controls.Add(Me.panel3)
			Me.Controls.Add(Me.PanelDataFields)
			Me.Controls.Add(Me.panel1)
			Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
			Me.Icon = (CType(resources.GetObject("$this.Icon"), System.Drawing.Icon))
			Me.Name = "FormCalibrationConfiguration"
			Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
			Me.Text = "Calibration Configuration"
'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
'ORIGINAL LINE: this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormCalibrationConfiguration_FormClosing);
'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
'ORIGINAL LINE: this.Load += new System.EventHandler(this.FormCalibrationConfiguration_Load);
			Me.panel1.ResumeLayout(False)
			Me.PanelDataFields.ResumeLayout(False)
			Me.PanelDataFields.PerformLayout()
			CType(Me.NumericUpDownSootLoad, System.ComponentModel.ISupportInitialize).EndInit()
			Me.panel3.ResumeLayout(False)
			Me.ResumeLayout(False)

		End Sub

		#End Region

		Private WithEvents ListBoxCalibrations As System.Windows.Forms.ListBox
		Private panel1 As System.Windows.Forms.Panel
		Private label1 As System.Windows.Forms.Label
		Private PanelDataFields As System.Windows.Forms.Panel
		Private TextBoxFunction As System.Windows.Forms.TextBox
		Private label5 As System.Windows.Forms.Label
		Private WithEvents TextBoxCoefficents As System.Windows.Forms.TextBox
		Private label4 As System.Windows.Forms.Label
		Private label3 As System.Windows.Forms.Label
		Private WithEvents NumericUpDownSootLoad As System.Windows.Forms.NumericUpDown
		Private label2 As System.Windows.Forms.Label
		Private panel3 As System.Windows.Forms.Panel
		Private WithEvents ButtonCancel As System.Windows.Forms.Button
		Private WithEvents ButtonAdd As System.Windows.Forms.Button
		Private WithEvents ButtonEdit As System.Windows.Forms.Button
		Private WithEvents ButtonRemove As System.Windows.Forms.Button
		Private WithEvents ButtonUpdate As System.Windows.Forms.Button
		Private WithEvents ButtonClose As System.Windows.Forms.Button
		Private WithEvents TextBoxDescription As System.Windows.Forms.TextBox
		Private label6 As System.Windows.Forms.Label
		Private WithEvents CheckBoxBinaryFunction As System.Windows.Forms.CheckBox
		Private WithEvents CopyThenEditButton As System.Windows.Forms.Button
	End Class
End Namespace