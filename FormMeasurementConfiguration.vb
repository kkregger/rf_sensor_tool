﻿Imports System.Collections.Concurrent
Imports System.IO

    Public Class FormMeasurementConfiguration
        Inherits Form

        Private Const FileHeaderBaseAddress As UInt32 = &H20000
        Private _currentMeasurement As Measurement
        Private _copiedRegionCal As RegionCalibration
        Private ReadOnly _main As FormRFSensorMain
        ' Indicates the current add/edit state of this Form.
        Private _state As FormState = FormState.Ready
        ' May be changed to Yes to indicate to the caller that Measurement,
        ' and possibly RegionCalibration, configuration(s) have changed.
        Private _configChanged As DialogResult = DialogResult.No
        ' Working copy of the master FormRFDPF.RegionCalibrations dictionary.
        ' Will contain RegionCalibration modifications accumulated during add
        ' or modification of an existing Measurement configuration. Applied 
        ' when the user presses Update.
        Private ReadOnly _regionCalCopy As New ConcurrentDictionary(Of Integer, RegionCalibration)()
        ' Event to raise when there is an error to be logged.
        Private Shared Event LogErrorEvent As ActivityLog.ErrorLogDelegate

        Public Sub New(ByVal main As FormRFSensorMain)
            InitializeComponent()
            _main = main
        End Sub

        Private Sub FormMeasurementConfiguration_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
            ' Register error logging event handlers.
            AddHandler LogErrorEvent, AddressOf ActivityLog.LogError
            ' Get a copy of all of the available RegionCalibrations.
            CopyRegionCalibrations()
            ' Load Descriptions of all currently available measurement configurations.
            ListBoxMeasurements.Items.Clear()
            For Each measure As KeyValuePair(Of String, Measurement) In FormRFSensorMain.Measurements
                ListBoxMeasurements.Items.Add(String.Copy(measure.Value.Description))
            Next measure
            ' By default, display the first  measurement in the control.
            If ListBoxMeasurements.Items.Count > 0 Then
                ListBoxMeasurements.SelectedIndex = 0
                DisplaySelectedMeasurement()
            Else
                ' No Measurements available so display default values.
                ListBoxMeasurements.SelectedIndex = -1
                InitializeForm()
            End If
        End Sub

        Private Sub FormMeasurementConfiguration_FormClosing(ByVal sender As Object, ByVal e As FormClosingEventArgs) Handles Me.FormClosing
            DialogResult = _configChanged
            RemoveHandler LogErrorEvent, AddressOf ActivityLog.LogError
        End Sub

        Private Sub CopyRegionCalibrations()
            _regionCalCopy.Clear()
            ' Make a working copy of all of the available RegionCalibrations.
            For Each kvpRegiongCal As KeyValuePair(Of Integer, RegionCalibration) In FormRFSensorMain.RegionCalibrations
                _regionCalCopy.TryAdd(kvpRegiongCal.Key, New RegionCalibration(kvpRegiongCal.Value))
            Next kvpRegiongCal
        End Sub

        Private Class FlashDataHeader
            Public MeasurementAddress As UInt32
            Public SweepCount As UInt16
            Public SweepAddress As UInt32
            Public RegionCount As UInt16
            Public RegionsAddress As UInt32
            Public CalibrationCount As UInt16
            Public CalibrationAddress As UInt32
            Public FrequencyCount As UInt16
            Public FrequencyAddress As UInt32
            Public OSLCalibrationAddress As UInt32
            Public NormalizationAddress As UInt32

            Public Function WriteFlashDataHeader(ByVal Buffer() As Byte) As UInt32
                Array.Copy(BitConverter.GetBytes(MeasurementAddress), 0, Buffer, 0, 4)
                Array.Copy(BitConverter.GetBytes(SweepCount), 0, Buffer, 4, 2)
                Array.Copy(BitConverter.GetBytes(SweepAddress), 0, Buffer, 6, 4)
                Array.Copy(BitConverter.GetBytes(RegionCount), 0, Buffer, 10, 2)
                Array.Copy(BitConverter.GetBytes(RegionsAddress), 0, Buffer, 12, 4)
                Array.Copy(BitConverter.GetBytes(CalibrationCount), 0, Buffer, 16, 2)
                Array.Copy(BitConverter.GetBytes(CalibrationAddress), 0, Buffer, 18, 4)
                Array.Copy(BitConverter.GetBytes(FrequencyCount), 0, Buffer, 22, 2)
                Array.Copy(BitConverter.GetBytes(FrequencyAddress), 0, Buffer, 24, 4)
                Array.Copy(BitConverter.GetBytes(OSLCalibrationAddress), 0, Buffer, 28, 4)
                Array.Copy(BitConverter.GetBytes(NormalizationAddress), 0, Buffer, 32, 4)

                Return 36
            End Function
        End Class

        Private Function WriteToBuffer(ByVal Buffer() As Byte, ByVal BufferIndex As UInt32, ByVal Value As Integer, ByVal Length As Integer) As UInt32
            Dim Source() As Byte = BitConverter.GetBytes(Value)
            For Index As Integer = 0 To Length - 1
                If Index >= Source.Length Then
                    ' WriteLogByte(0)
                    Buffer(BufferIndex) = 0
                    BufferIndex += 1
                Else
                    ' WriteLogByte(Source(Index))
                    Buffer(BufferIndex) = Source(Index)
                    BufferIndex += 1
                End If
            Next Index
            Return BufferIndex
        End Function

        Private Function WriteToBuffer(ByVal Buffer() As Byte, ByVal BufferIndex As UInt32, ByVal Value As UInteger, ByVal Length As Integer) As UInt32
            Write(Value)
            Dim Source() As Byte = BitConverter.GetBytes(Value)
            For Index As Integer = 0 To Length - 1
                If Index >= Source.Length Then
                    Buffer(BufferIndex) = 0
                    BufferIndex += 1
                Else
                    Buffer(BufferIndex) = Source(Index)
                    BufferIndex += 1
                End If
            Next Index
            Return BufferIndex
        End Function

        Private Function WriteToBuffer(ByVal Buffer() As Byte, ByVal BufferIndex As UInt32, ByVal Value As Double, ByVal Length As Integer) As UInt32
            Write(Value)
            Dim Source() As Byte
            If Length < 8 Then
                Source = BitConverter.GetBytes(CSng(Value))
            Else
                Source = BitConverter.GetBytes(Value)
            End If
            For Index As Integer = 0 To Length - 1
                If Index >= Source.Length Then
                    Buffer(BufferIndex) = 0
                    BufferIndex += 1
                Else
                    Buffer(BufferIndex) = Source(Index)
                    BufferIndex += 1
                End If
            Next Index
            Return BufferIndex
        End Function

        Private Function WriteToBuffer(ByVal Buffer() As Byte, ByVal BufferIndex As UInt32, ByVal Source As String, ByVal Length As Integer) As UInt32
            Write(Source)
            For Index As Integer = 0 To Length - 1
                If Index >= Source.Length Then
                    Buffer(BufferIndex) = 0
                    BufferIndex += 1
                Else
                    Buffer(BufferIndex) = AscW(Source.Chars(Index))
                    BufferIndex += 1
                End If
            Next Index
            Return BufferIndex
        End Function

        Private Function WriteToBuffer(ByVal Buffer() As Byte, ByVal BufferIndex As UInt32, ByVal Source As Boolean, ByVal Length As Integer) As UInt32
            Write(Source.ToString())
            Return WriteToBuffer(Buffer, BufferIndex, If(Source, 1, 0), Length)
        End Function

        Private Function WriteMeasurement(ByVal bufferIndex As UInteger, ByVal buffer() As Byte,
                                          ByVal calibrations As List(Of Calibration),
                                          ByVal regions As List(Of RegionOfInterest),
                                          ByVal currentSweep As Sweep) As UInt32

            Dim activeFrequencies(259) As Byte
            Dim currentMeasurement As New Measurement(_currentMeasurement)

            Write("Description: ")
            bufferIndex = WriteToBuffer(buffer, bufferIndex, currentMeasurement.Description, 80)
            Write(ControlChars.CrLf & "Operating Mode: ")
            bufferIndex = WriteToBuffer(buffer, bufferIndex, CByte(Measurement.TransmissionMode(currentMeasurement.OperatingMode)), 2)
            Write(ControlChars.CrLf & "Final Value Statistic: ")
            bufferIndex = WriteToBuffer(buffer, bufferIndex, ComboBoxFinalValueStatistic.SelectedIndex, 2)
            Write(ControlChars.CrLf & "Inverse: ")
            bufferIndex = WriteToBuffer(buffer, bufferIndex, CheckBoxInverse.Checked, 2)
            Write(ControlChars.CrLf & "Statistic Divisor: ")
            bufferIndex = WriteToBuffer(buffer, bufferIndex, currentMeasurement.StatisticDivisor, 4)
            Write(ControlChars.CrLf & "Sweep Count: ")
            bufferIndex = WriteToBuffer(buffer, bufferIndex, currentMeasurement.SweepCount, 2)
            Write(ControlChars.CrLf & "Maximum Temperature: ")
            bufferIndex = WriteToBuffer(buffer, bufferIndex, currentMeasurement.MaximumTemperature, 2)
            Write(ControlChars.CrLf & "Minimum Temperature: ")
            bufferIndex = WriteToBuffer(buffer, bufferIndex, currentMeasurement.MinimumTemperature, 2)
            Write(ControlChars.CrLf & "Maximum Soot: ")
            bufferIndex = WriteToBuffer(buffer, bufferIndex, currentMeasurement.MaximumSoot, 4)
            Write(ControlChars.CrLf & "Minimum Soot: ")
            bufferIndex = WriteToBuffer(buffer, bufferIndex, currentMeasurement.MinimumSoot, 4)
            Write(ControlChars.CrLf & "Measure Phase: ")
            bufferIndex = WriteToBuffer(buffer, bufferIndex, currentMeasurement.MeasurePhase, 2)
            Write(ControlChars.CrLf & "Board Calibration: ")
            bufferIndex = WriteToBuffer(buffer, bufferIndex, FindCalibration(currentMeasurement.BoardCalibration, calibrations), 2) ' Maybe this should be a Calibration value or name instead of an index?? XXX KTK
            Write(ControlChars.CrLf & "Cold Junction Calibration: ") ' FindCalibration() just returns the index of the Calibration config.
            bufferIndex = WriteToBuffer(buffer, bufferIndex, FindCalibration(currentMeasurement.ColdJunctionCalibration, calibrations), 2) ' that contains the matching Calibration value?????
            Write(ControlChars.CrLf & "Thermocouple Calibration: ") ' Calibrations are presented to the user by name.
            bufferIndex = WriteToBuffer(buffer, bufferIndex, FindCalibration(currentMeasurement.ColdJunctionCalibration, calibrations), 2)
            Write(ControlChars.CrLf & "RTD Calibration: ")
            bufferIndex = WriteToBuffer(buffer, bufferIndex, FindCalibration(currentMeasurement.RTDCalibration, calibrations), 2)
            Write(ControlChars.CrLf & "Signal 1 Calibration: ")
            bufferIndex = WriteToBuffer(buffer, bufferIndex, FindCalibration(currentMeasurement.Signal1Calibration, calibrations), 2)
            Write(ControlChars.CrLf & "Signal 2 Calibration: ")
            bufferIndex = WriteToBuffer(buffer, bufferIndex, FindCalibration(currentMeasurement.Signal2Calibration, calibrations), 2)
            Write(ControlChars.CrLf & "Signal 3 Calibration: ")
            bufferIndex = WriteToBuffer(buffer, bufferIndex, FindCalibration(currentMeasurement.Signal3Calibration, calibrations), 2)
            Write(ControlChars.CrLf & "Signal 4 Calibration: ")
            bufferIndex = WriteToBuffer(buffer, bufferIndex, FindCalibration(currentMeasurement.Signal4Calibration, calibrations), 2)
            Write(ControlChars.CrLf & "Temperature Source: ")
            bufferIndex = WriteToBuffer(buffer, bufferIndex, ComboBoxTemperatureSource.SelectedIndex, 2)
            Write(ControlChars.CrLf & "Normalize VIA Bypass: ")
            bufferIndex = WriteToBuffer(buffer, bufferIndex, currentMeasurement.NormalizeViaBypass, 2)
            Write(ControlChars.CrLf & "Phase Bias: ")
            bufferIndex = WriteToBuffer(buffer, bufferIndex, currentMeasurement.PhaseBias, 2)
            Write(ControlChars.CrLf & "Magnitude Bias: ")
            bufferIndex = WriteToBuffer(buffer, bufferIndex, currentMeasurement.MagnitudeBias, 2)
            Write(ControlChars.CrLf & "Generate Raw Output Files: ")
            bufferIndex = WriteToBuffer(buffer, bufferIndex, False, 2)
            Write(ControlChars.CrLf & "Generate RO Files: ")
            bufferIndex = WriteToBuffer(buffer, bufferIndex, currentMeasurement.GenerateROFiles, 2)
            Write(ControlChars.CrLf & "Generate PO File: ")
            bufferIndex = WriteToBuffer(buffer, bufferIndex, currentMeasurement.GeneratePOFile, 2)
            Write(ControlChars.CrLf & "Auto Start: ")
            bufferIndex = WriteToBuffer(buffer, bufferIndex, currentMeasurement.AutoStart, 2)
            Write(ControlChars.CrLf & "Sweep Delay: ")
            bufferIndex = WriteToBuffer(buffer, bufferIndex, FormRFSensorMain.RunConfigurations.SweepInterval, 2)
            Write(ControlChars.CrLf & "Calibration On: ")
            bufferIndex = WriteToBuffer(buffer, bufferIndex, FormRFSensorMain.RunConfigurations.CalibrationOn, 2)

            For Index As Integer = 0 To activeFrequencies.Length - 1
                activeFrequencies(Index) = 0
            Next Index

            Dim frequencyCount = CInt(Math.Truncate((currentSweep.RFEndFrequency - currentSweep.RFStartFrequency) / currentSweep.RFStepSize + 1))

            For Index As Integer = 0 To frequencyCount - 1
                Dim Frequency As Double

                If FormRFSensorMain.ActiveSweep.FixedDifference Then
                    Frequency = currentSweep.RFStartFrequency + Index * currentSweep.RFStepSize
                Else
                    Frequency = currentSweep.RFEndFrequency - Index * currentSweep.RFStepSize
                End If

                For Each kvpregion As KeyValuePair(Of String, RegionOfInterest) In FormRFSensorMain.RegionsOfInterest
                    If kvpregion.Value.StartFrequency <= Frequency AndAlso kvpregion.Value.EndFrequency >= Frequency Then
                        activeFrequencies(Index >> 3) = activeFrequencies(Index >> 3) Or CByte(1 << (Index And 7))
                        Exit For
                    End If
                Next kvpregion
            Next Index

            Write(ControlChars.CrLf & "Active Frequencies: ")

            For Index As Integer = 0 To activeFrequencies.Length - 1
                bufferIndex = WriteToBuffer(buffer, bufferIndex, activeFrequencies(Index), 1)
            Next Index

            Return bufferIndex
        End Function

        Private Function WriteSweep(ByVal BufferIndex As UInteger, ByVal Buffer() As Byte, ByVal Sweep As Sweep) As UInteger
            Write(ControlChars.CrLf & "Sweep: ")
            BufferIndex = WriteToBuffer(Buffer, BufferIndex, Sweep.Description, 80)
            Write(ControlChars.CrLf & "Input Frequency: ")
            BufferIndex = WriteToBuffer(Buffer, BufferIndex, CUInt(Sweep.InputFrequency * 10), 4)
            Write(ControlChars.CrLf & "RF Start Frequency: ")
            BufferIndex = WriteToBuffer(Buffer, BufferIndex, CUInt(Sweep.RFStartFrequency * 10), 4)
            Write(ControlChars.CrLf & "RF End Frequency: ")
            BufferIndex = WriteToBuffer(Buffer, BufferIndex, CUInt(Sweep.RFEndFrequency * 10), 4)
            Write(ControlChars.CrLf & "RF Step Size: ")
            BufferIndex = WriteToBuffer(Buffer, BufferIndex, CUInt(Sweep.RFStepSize * 10), 4)
            Write(ControlChars.CrLf & "LO Start Frequency: ")
            BufferIndex = WriteToBuffer(Buffer, BufferIndex, CUInt(Sweep.LOStartFrequency * 10), 4)
            Write(ControlChars.CrLf & "LO End Frequency: ")
            BufferIndex = WriteToBuffer(Buffer, BufferIndex, CUInt(Sweep.LOEndFrequency * 10), 4)
            Write(ControlChars.CrLf & "LO Step Size: ")
            BufferIndex = WriteToBuffer(Buffer, BufferIndex, CUInt(Sweep.LOStepSize * 10), 4)
            Write(ControlChars.CrLf & "Fixed Difference: ")
            BufferIndex = WriteToBuffer(Buffer, BufferIndex, Sweep.FixedDifference, 2)
            Write(ControlChars.CrLf & "Step Delay: ")
            BufferIndex = WriteToBuffer(Buffer, BufferIndex, Sweep.StepDelay, 2)

            Return BufferIndex
        End Function

        Private Function FindCalibration(ByVal calibrationID As Integer, ByVal calibrations As List(Of Calibration)) As UShort
            For Index As UShort = 0 To calibrations.Count - 1
                If calibrations(Index).CalibrationID.Equals(calibrationID) Then
                    Return Index
                End If
            Next Index
            Return 0
        End Function

        Private Function WriteRegionOfInterest(ByVal BufferIndex As UInteger,
                                               ByVal Buffer() As Byte,
                                               ByVal roi As RegionOfInterest,
                                               ByVal regionCal As RegionCalibration,
                                               ByVal calibrations As List(Of Calibration)) As UInteger

            Write(ControlChars.CrLf & "Region of Interest: ")
            BufferIndex = WriteToBuffer(Buffer, BufferIndex, roi.Description, 80)
            Write(ControlChars.CrLf & "Start Frequency: ")
            BufferIndex = WriteToBuffer(Buffer, BufferIndex, CUInt(roi.StartFrequency * 10), 4)
            Write(ControlChars.CrLf & "End Frequency: ")
            BufferIndex = WriteToBuffer(Buffer, BufferIndex, CUInt(roi.EndFrequency * 10), 4)
            Write(ControlChars.CrLf & "Include In Calculation: ")
            BufferIndex = WriteToBuffer(Buffer, BufferIndex, regionCal.IncludeInCalculation, 2)
            Write(ControlChars.CrLf & "Use for Status: ")
            BufferIndex = WriteToBuffer(Buffer, BufferIndex, regionCal.UseForStatus, 2)
            Write(ControlChars.CrLf & "Calibration Count: ")
            BufferIndex = WriteToBuffer(Buffer, BufferIndex, CUShort(regionCal.CalibrationID.Count), 2)
            For Each calID As Integer In regionCal.CalibrationID
                For Each cal As Calibration In calibrations
                    If cal.CalibrationID.Equals(calID) Then
                        Write(ControlChars.CrLf & "Calibration Used: ")
                        BufferIndex = WriteToBuffer(Buffer, BufferIndex, calID, 2)
                    End If
                Next cal
            Next calID
            Return BufferIndex
        End Function

        Private Function WriteCalibration(ByVal BufferIndex As UInteger, ByVal Buffer() As Byte,
                                          ByVal cal As Calibration) As UInteger
            Write(ControlChars.CrLf & "Calibration: ")
            BufferIndex = WriteToBuffer(Buffer, BufferIndex, cal.Description, 80)
            Write(ControlChars.CrLf & "Soot Load: ")
            BufferIndex = WriteToBuffer(Buffer, BufferIndex, cal.SootLoad, 4)
            Write(ControlChars.CrLf & "Binary: ")
            BufferIndex = WriteToBuffer(Buffer, BufferIndex, cal.Binary, 2)
            Dim Coefficients() As String = cal.Coefficients.Split(New Char() {","c, " "c}, StringSplitOptions.RemoveEmptyEntries)
            Write(ControlChars.CrLf & "Coefficient Count: ")
            BufferIndex = WriteToBuffer(Buffer, BufferIndex, CUInt(Coefficients.Count()), 2)
            Write(ControlChars.CrLf & "Coefficients: ")
            For Index As Integer = 0 To 9
                If Index < Coefficients.Count() Then
                    BufferIndex = WriteToBuffer(Buffer, BufferIndex, Double.Parse(Coefficients(Index)), 4)
                Else
                    BufferIndex = WriteToBuffer(Buffer, BufferIndex, 0.0, 4)
                End If
            Next Index
            Return BufferIndex
        End Function

        Private Function WriteLineHeaderToHexFile(ByVal HexFile As StreamWriter, ByVal Address As UInt32, ByVal RecordType As Byte, ByVal DataLength As Byte) As Byte
            Dim CheckSum As Byte

            CheckSum = DataLength
            HexFile.Write(":" & DataLength.ToString("X2"))
            CheckSum += CByte((Address >> 8) And &HFF)
            HexFile.Write(((Address >> 8) And &HFF).ToString("X2"))
            CheckSum += CByte(Address And &HFF)
            HexFile.Write((Address And &HFF).ToString("X2"))
            CheckSum += RecordType
            HexFile.Write(RecordType.ToString("X2"))

            Return CheckSum
        End Function

        Private Sub WriteESALineToHexFile(ByVal HexFile As StreamWriter, ByVal Address As UInt32)
            Dim CheckSum As Byte = WriteLineHeaderToHexFile(HexFile, 0, &H4, 2)

            CheckSum += CByte((Address >> 8) And &HFF)
            HexFile.Write(((Address >> 8) And &HFF).ToString("X2"))

            CheckSum += CByte(Address And &HFF)
            HexFile.Write((Address And &HFF).ToString("X2"))

            HexFile.WriteLine(((&H100 - CheckSum) And &HFF).ToString("X2"))
        End Sub

        Private Sub WriteDataLineToHexFile(ByVal Buffer() As Byte, ByVal BufferIndex As UInt32, ByVal HexFile As StreamWriter, ByVal Address As UInt32, ByVal DataLength As Byte)
            Dim CheckSum As Byte = WriteLineHeaderToHexFile(HexFile, Address, &H0, DataLength)

            Dim DataIndex As UInt32 = 0

            Do While DataIndex < DataLength \ 2
                Dim DataAddress As UInt32 = Address + DataIndex

                CheckSum += Buffer(BufferIndex + DataIndex)
                HexFile.Write(Buffer(BufferIndex + DataIndex).ToString("X2"))
                If (DataAddress And &H1) = &H1 Then
                    HexFile.Write("0000")
                End If
                DataIndex += 1
            Loop
            HexFile.WriteLine(((&H100 - CheckSum) And &HFF).ToString("X2"))
        End Sub

        Private Sub WriteEndOfFileLineToHexFile(ByVal HexFile As StreamWriter, ByVal Address As UInt32)
            Dim CheckSum As Byte = WriteLineHeaderToHexFile(HexFile, 0, &H1, 0)

            HexFile.WriteLine(((&H100 - CheckSum) And &HFF).ToString("X2"))
        End Sub

        Private Sub WriteBufferToHexFile(ByVal Buffer() As Byte, ByVal BufferIndex As UInt32, ByVal HexFile As StreamWriter, ByVal StartingAddress As UInt32)
            For Index As UInt32 = 0 To BufferIndex - 1 Step 8
                If (Index And &H7FFF) = 0 Then
                    WriteESALineToHexFile(HexFile, (Index + StartingAddress) >> 15)
                End If
                WriteDataLineToHexFile(Buffer, Index, HexFile, (Index And &HFFFF) * 2, CByte(If(BufferIndex - Index > 8, 16, (BufferIndex - Index) * 2)))
            Next Index
            WriteEndOfFileLineToHexFile(HexFile, 0)
        End Sub

        Private Function MakePSVAddress(ByVal LinearAddress As UInt32) As UInt32
            Dim Page As UInt32 = CUInt(&H100 + CByte((LinearAddress >> 16) And &HFF))
            Dim Offset As UInt32 = (LinearAddress And &H7FFF)

            Return (Page << 16) + Offset
        End Function

        Private Sub DisplaySelectedMeasurement()
            ' A different measurement configuration has been selected.
            ' Reconfigure the controls on this form accordingly.
            Dim sweepID As Integer = 0

            PanelDataFields.Enabled = False
            CheckedListBoxRegions.Items.Clear()
            CheckedListBoxCalibrations.Items.Clear()
            CheckedListBoxRegions.Enabled = True
            CheckedListBoxCalibrations.Enabled = False
            ComboBoxBoardTempCalibration.Items.Clear()
            ComboBoxColdJunctionCalibration.Items.Clear()
            ComboBoxThermocoupleCalibration.Items.Clear()
            ComboBoxRTDCalibration.Items.Clear()
            ComboBoxSignalCalibration.Items.Clear()

            If ListBoxMeasurements.SelectedItem IsNot Nothing Then
                ' Update the contents of all form controls.

                ' Get a copy of the selected Measurement. Constructor does deep copy.
                _currentMeasurement = New Measurement(FormRFSensorMain.Measurements(DirectCast(ListBoxMeasurements.SelectedItem, String)))
                MeasurementIdTextBox.Text = _currentMeasurement.MeasurementID.ToString()
                ' Display the selected measurement description.
                TextBoxDescription.Text = String.Copy(_currentMeasurement.Description)
                ' Load sweep combobox with all available sweep descriptions.
                ComboBoxSweep.Items.Clear()
                For Each currentSweep As KeyValuePair(Of Integer, Sweep) In FormRFSensorMain.Sweeps
                    ComboBoxSweep.Items.Add(String.Copy(currentSweep.Value.Description))
                    If _currentMeasurement.SweepID = currentSweep.Value.SweepID Then
                        sweepID = CInt(currentSweep.Value.SweepID)
                    End If
                Next currentSweep
                If FormRFSensorMain.Sweeps.ContainsKey(sweepID) Then
                    ' Set the selected item to the current Measurement Sweep Description.
                    ComboBoxSweep.SelectedIndex = ComboBoxSweep.FindString(FormRFSensorMain.Sweeps(sweepID).Description)
                End If
                ComboBoxOperatingMode.SelectedItem = String.Copy(_currentMeasurement.OperatingMode)
                ComboBoxFinalValueStatistic.SelectedItem = String.Copy(_currentMeasurement.FinalValueStatistic)
                NumericUpDownMinimumTemperature.Value = _currentMeasurement.MinimumTemperature
                NumericUpDownMaximumTemperature.Value = _currentMeasurement.MaximumTemperature
                NumericUpDownMinimumSoot.Value = CDec(_currentMeasurement.MinimumSoot)
                NumericUpDownMaximumSoot.Value = CDec(_currentMeasurement.MaximumSoot)
                CheckBoxNormalizeViaBypass.Checked = _currentMeasurement.NormalizeViaBypass
                CheckBoxActive.Checked = _currentMeasurement.Active
                CheckBoxInverse.Checked = _currentMeasurement.Inverse
                CheckBoxMeasurePhase.Checked = _currentMeasurement.MeasurePhase
                NumericUpDownSweepCount.Value = _currentMeasurement.SweepCount
                NumericUpDownStatisticScale.Value = CDec(_currentMeasurement.StatisticDivisor)
                ComboBoxTemperatureSource.SelectedItem = String.Copy(_currentMeasurement.TemperatureSource)
                ' Display all available region calibration/region of interest descriptions.
                For Each kvpROI As KeyValuePair(Of String, RegionOfInterest) In FormRFSensorMain.RegionsOfInterest
                    CheckedListBoxRegions.Items.Add(String.Copy(kvpROI.Value.Description), False)
                Next kvpROI
                ' Display all available region calibration/calibration descriptions.
                For Each kvpCal As KeyValuePair(Of Integer, Calibration) In FormRFSensorMain.Calibrations
                    CheckedListBoxCalibrations.Items.Add(String.Copy(kvpCal.Value.Description), False)
                Next kvpCal
                ' Display associated region calibration/region of interest descriptions.
                Dim regionCal As RegionCalibration = GetRegionCalibration(If(_currentMeasurement.MeasurementID, 0))
                If regionCal IsNot Nothing Then
                    For Each kvpROI As KeyValuePair(Of String, RegionOfInterest) In FormRFSensorMain.RegionsOfInterest
                        Dim found As Boolean = False
                        For Each regionID As Integer In regionCal.RegionID
                            If regionID = kvpROI.Value.RegionID Then
                                found = True
                                Exit For
                            End If
                        Next regionID
                        For idx As Integer = 0 To CheckedListBoxRegions.Items.Count - 1
                            If DirectCast(CheckedListBoxRegions.Items(idx), String) = kvpROI.Value.Description Then
                                ' Display a check mark for all associated Regions Of Interest.
                                CheckedListBoxRegions.SetItemChecked(idx, found)
                                CheckBoxIncludeRegionInSootLoadCalculation.Checked = regionCal.IncludeInCalculation
                                CheckBoxUseRegionForStatus.Checked = regionCal.UseForStatus
                                CheckBoxIncludeRegionInSootLoadCalculation.Enabled = False
                                CheckBoxUseRegionForStatus.Enabled = False
                                Exit For
                            End If
                        Next idx
                    Next kvpROI
                    ' Display associated region calibration/calibration descriptions.
                    For Each kvpCal As KeyValuePair(Of Integer, Calibration) In FormRFSensorMain.Calibrations
                        Dim found As Boolean = False
                        For Each calID As Integer In regionCal.CalibrationID
                            If calID = CInt(kvpCal.Value.CalibrationID) Then
                                found = True
                                Exit For
                            End If
                        Next calID
                        For idx As Integer = 0 To CheckedListBoxCalibrations.Items.Count - 1
                            If DirectCast(CheckedListBoxCalibrations.Items(idx), String) = kvpCal.Value.Description Then
                                ' Display a check mark for all associated Calibrations.
                                CheckedListBoxCalibrations.SetItemChecked(idx, found)
                                Exit For
                            End If
                        Next idx
                    Next kvpCal
                End If
                ' Update file generation controls.
                CheckBoxGenerateRawOutputFiles.Checked = _currentMeasurement.GenerateRawOutputFiles
                CheckBoxAutoStart.Checked = _currentMeasurement.AutoStart
                CheckBoxGenerateROFiles.Checked = _currentMeasurement.GenerateROFiles
                CheckBoxGeneratePOFile.Checked = _currentMeasurement.GeneratePOFile
                ' Set up the calibration related controls.
                For Each kvpCal As KeyValuePair(Of Integer, Calibration) In FormRFSensorMain.Calibrations
                    ComboBoxBoardTempCalibration.Items.Add(String.Copy(kvpCal.Value.Description))
                    ComboBoxColdJunctionCalibration.Items.Add(String.Copy(kvpCal.Value.Description))
                    ComboBoxThermocoupleCalibration.Items.Add(String.Copy(kvpCal.Value.Description))
                    ComboBoxRTDCalibration.Items.Add(String.Copy(kvpCal.Value.Description))
                    ComboBoxSignalCalibration.Items.Add(String.Copy(kvpCal.Value.Description))
                Next kvpCal
                If FormRFSensorMain.Calibrations.ContainsKey(_currentMeasurement.BoardCalibration) Then
                    ComboBoxBoardTempCalibration.SelectedItem = FormRFSensorMain.Calibrations(_currentMeasurement.BoardCalibration).Description
                End If
                If FormRFSensorMain.Calibrations.ContainsKey(_currentMeasurement.ColdJunctionCalibration) Then
                    ComboBoxColdJunctionCalibration.SelectedItem = FormRFSensorMain.Calibrations(_currentMeasurement.ColdJunctionCalibration).Description
                End If
                If FormRFSensorMain.Calibrations.ContainsKey(_currentMeasurement.ThermocoupleCalibration) Then
                    ComboBoxThermocoupleCalibration.SelectedItem = FormRFSensorMain.Calibrations(_currentMeasurement.ThermocoupleCalibration).Description
                End If
                If FormRFSensorMain.Calibrations.ContainsKey(_currentMeasurement.RTDCalibration) Then
                    ComboBoxRTDCalibration.SelectedItem = FormRFSensorMain.Calibrations(_currentMeasurement.RTDCalibration).Description
                End If
                TextBoxSignalName.Text = _currentMeasurement.Signal1Name
                If FormRFSensorMain.Calibrations.ContainsKey(_currentMeasurement.Signal1Calibration) Then
                    ComboBoxSignalCalibration.SelectedItem = FormRFSensorMain.Calibrations(_currentMeasurement.Signal1Calibration).Description
                End If
                TextBoxDescription.Focus()
            Else
                _currentMeasurement = Nothing
            End If
        End Sub

        Public Function GetRegionCalibration(ByVal measurementID As Integer) As RegionCalibration
            ' Find RegionCalibration with MeasurementID that
            ' matches measurementID parameter.
            Dim regionCalibrations = (
                From regionCal In _regionCalCopy
                Where regionCal.Value.MeasurementID = measurementID
                Select regionCal.Value).ToList()

            If regionCalibrations.Count > 0 Then
                ' Currently expect only one.
                Return regionCalibrations.First()
            End If
            Return Nothing
        End Function

        Private Sub InitializeForm()
            ' A new measurement is beinmg configured so
            ' reinitialize the controls on this form.
            PanelDataFields.Enabled = False
            CheckedListBoxRegions.Items.Clear()
            CheckedListBoxCalibrations.Items.Clear()
            ComboBoxBoardTempCalibration.Items.Clear()
            ComboBoxColdJunctionCalibration.Items.Clear()
            ComboBoxThermocoupleCalibration.Items.Clear()
            ComboBoxRTDCalibration.Items.Clear()
            ComboBoxSignalCalibration.Items.Clear()
            CheckedListBoxRegions.Enabled = FormRFSensorMain.RegionsOfInterest.Count > 0
            CheckedListBoxCalibrations.Enabled = FormRFSensorMain.Calibrations.Count > 0
            ' Default the selected measurement description.
            TextBoxDescription.Text = "Description"
            TextBoxDescription.Focus()
            ' Load sweep combobox with all available sweep descriptions.
            ComboBoxSweep.Items.Clear()
            For Each currentSweep As KeyValuePair(Of Integer, Sweep) In FormRFSensorMain.Sweeps
                ComboBoxSweep.Items.Add(String.Copy(currentSweep.Value.Description))
            Next currentSweep

            If ComboBoxSweep.Items.Count > 0 Then
                ComboBoxSweep.SelectedIndex = 0
            End If

            ComboBoxOperatingMode.SelectedIndex = 0
            ComboBoxFinalValueStatistic.SelectedIndex = 0
            NumericUpDownMinimumTemperature.Value = NumericUpDownMinimumTemperature.Minimum
            NumericUpDownMaximumTemperature.Value = NumericUpDownMaximumTemperature.Minimum
            NumericUpDownMinimumSoot.Value = NumericUpDownMinimumSoot.Minimum
            NumericUpDownMaximumSoot.Value = NumericUpDownMaximumSoot.Minimum
            CheckBoxNormalizeViaBypass.Checked = False
            CheckBoxActive.Checked = False
            CheckBoxInverse.Checked = False
            CheckBoxMeasurePhase.Checked = False
            NumericUpDownSweepCount.Value = NumericUpDownSweepCount.Minimum
            NumericUpDownStatisticScale.Value = NumericUpDownStatisticScale.Minimum
            ComboBoxTemperatureSource.SelectedIndex = 0
            ' Display all available region calibration/region of interest descriptions.
            For Each kvpROI As KeyValuePair(Of String, RegionOfInterest) In FormRFSensorMain.RegionsOfInterest
                CheckedListBoxRegions.Items.Add(kvpROI.Value.Description, False)
            Next kvpROI
            ' Display all available region calibration/calibration descriptions.
            For Each kvpCal As KeyValuePair(Of Integer, Calibration) In FormRFSensorMain.Calibrations
                CheckedListBoxCalibrations.Items.Add(kvpCal.Value.Description, False)
            Next kvpCal
            ' Update region using controls.
            CheckBoxIncludeRegionInSootLoadCalculation.Checked = False
            CheckBoxUseRegionForStatus.Checked = False
            ' Update file generation controls.
            CheckBoxGenerateRawOutputFiles.Checked = False
            CheckBoxAutoStart.Checked = False
            CheckBoxGenerateROFiles.Checked = False
            CheckBoxGeneratePOFile.Checked = False
            ' Set up the calibration related controls.
            For Each kvpCal As KeyValuePair(Of Integer, Calibration) In FormRFSensorMain.Calibrations
                ComboBoxBoardTempCalibration.Items.Add(kvpCal.Value.Description)
                ComboBoxColdJunctionCalibration.Items.Add(kvpCal.Value.Description)
                ComboBoxThermocoupleCalibration.Items.Add(kvpCal.Value.Description)
                ComboBoxRTDCalibration.Items.Add(kvpCal.Value.Description)
                ComboBoxSignalCalibration.Items.Add(kvpCal.Value.Description)
            Next kvpCal
            ComboBoxBoardTempCalibration.SelectedIndex = ComboBoxBoardTempCalibration.FindString("Board Temp")
            ComboBoxColdJunctionCalibration.SelectedIndex = ComboBoxColdJunctionCalibration.FindString("Cold Junction")
            ComboBoxThermocoupleCalibration.SelectedIndex = ComboBoxThermocoupleCalibration.FindString("Thermocouple")
            ComboBoxRTDCalibration.SelectedIndex = ComboBoxRTDCalibration.FindString("RTD")
            TextBoxSignalName.Text = ""
            ComboBoxSignalCalibration.SelectedIndex = ComboBoxSignalCalibration.FindString("Unity")
            MeasurementIdTextBox.Text = ""
        End Sub

        Private Sub GetRegionCalibration(ByVal region As RegionOfInterest, ByRef regionCal As RegionCalibration)
            ' Get the RegionCalibration, from the working copy, that contains
            ' the current MeasurementID.
            regionCal = Nothing
            For Each kvpRegionCal As KeyValuePair(Of Integer, RegionCalibration) In _regionCalCopy.Where(Function(regCal) _currentMeasurement.MeasurementID.Equals(regCal.Value.MeasurementID))
                If kvpRegionCal.Value.RegionID.Any(Function(regID) regID = (If(region.RegionID, 0))) Then
                    regionCal = kvpRegionCal.Value
                    Exit For ' Only expecting one.
                End If
            Next kvpRegionCal
        End Sub

        Private Sub ConfigurationChangeEnable(ByVal enable As Boolean)
            ' Enabled/Disable controls based on whether were in the process
            ' of adding or changing Measurement configurations.
            ButtonAdd.Enabled = enable
            CopyThenEditButton.Enabled = enable
            ButtonEdit.Enabled = enable
            ButtonRemove.Enabled = enable
            ButtonGenerate.Enabled = enable
            ButtonUpdate.Enabled = Not enable
            ButtonCancel.Enabled = Not enable
            ListBoxMeasurements.Enabled = enable
        End Sub

        Private Sub DisableEdit()
            CopyThenEditButton.Enabled = False
            ButtonEdit.Enabled = False
        End Sub

        Private Sub ListBoxMeasurements_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ListBoxMeasurements.SelectedIndexChanged
            If _state = FormState.Ready Then
                ConfigurationChangeEnable(True)
            End If
            DisplaySelectedMeasurement()
        End Sub

        Private Sub ComboBoxOperatingMode_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ComboBoxOperatingMode.SelectedIndexChanged
            If _currentMeasurement Is Nothing Then
                Return
            End If
            _currentMeasurement.OperatingMode = DirectCast(ComboBoxOperatingMode.SelectedItem, String)
        End Sub

        Private Sub ComboBoxFinalValueStatistic_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ComboBoxFinalValueStatistic.SelectedIndexChanged
            If _currentMeasurement Is Nothing Then
                Return
            End If
            _currentMeasurement.FinalValueStatistic = DirectCast(ComboBoxFinalValueStatistic.SelectedItem, String)
        End Sub

        Private Sub NumericUpDownMinimumTemperature_ValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles NumericUpDownMinimumTemperature.ValueChanged
            If _currentMeasurement Is Nothing Then
                Return
            End If
            _currentMeasurement.MinimumTemperature = CShort(Math.Truncate(NumericUpDownMinimumTemperature.Value))
        End Sub

        Private Sub NumericUpDownMaximumTemperature_ValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles NumericUpDownMaximumTemperature.ValueChanged
            If _currentMeasurement Is Nothing Then
                Return
            End If
            _currentMeasurement.MaximumTemperature = CShort(Math.Truncate(NumericUpDownMaximumTemperature.Value))
        End Sub

        Private Sub NumericUpDownMinimumSoot_ValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles NumericUpDownMinimumSoot.ValueChanged
            _currentMeasurement.MinimumSoot = CDbl(NumericUpDownMinimumSoot.Value)
        End Sub

        Private Sub NumericUpDownMaximumSoot_ValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles NumericUpDownMaximumSoot.ValueChanged
            If _currentMeasurement Is Nothing Then
                Return
            End If
            _currentMeasurement.MaximumSoot = CDbl(NumericUpDownMaximumSoot.Value)
        End Sub

        Private Sub CheckBoxNormalizeViaBypass_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CheckBoxNormalizeViaBypass.CheckedChanged
            If _currentMeasurement Is Nothing Then
                Return
            End If
            _currentMeasurement.NormalizeViaBypass = CheckBoxNormalizeViaBypass.Checked
        End Sub

        Private Sub CheckBoxActive_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CheckBoxActive.CheckedChanged
            If _currentMeasurement Is Nothing Then
                Return
            End If
            _currentMeasurement.Active = CheckBoxActive.Checked
        End Sub

        Private Sub CheckBoxInverse_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CheckBoxInverse.CheckedChanged
            If _currentMeasurement Is Nothing Then
                Return
            End If
            _currentMeasurement.Inverse = CheckBoxInverse.Checked
        End Sub

        Private Sub CheckBoxMeasurePhase_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CheckBoxMeasurePhase.CheckedChanged
            If _currentMeasurement Is Nothing Then
                Return
            End If
            If CheckBoxMeasurePhase.Checked AndAlso NumericUpDownSweepCount.Value = 1 Then
                NumericUpDownSweepCount.Value = 5
            ElseIf Not CheckBoxMeasurePhase.Checked AndAlso NumericUpDownSweepCount.Value = 5 Then
                NumericUpDownSweepCount.Value = 1
            End If
            _currentMeasurement.MeasurePhase = CheckBoxMeasurePhase.Checked
        End Sub

        Private Sub NumericUpDownSweepCount_ValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles NumericUpDownSweepCount.ValueChanged
            If _currentMeasurement Is Nothing Then
                Return
            End If
            _currentMeasurement.SweepCount = CInt(Math.Truncate(NumericUpDownSweepCount.Value))
        End Sub

        Private Sub NumericUpDownPhaseBias_ValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles NumericUpDownPhaseBias.ValueChanged
            If _currentMeasurement Is Nothing Then
                Return
            End If
            _currentMeasurement.PhaseBias = CInt(Math.Truncate(NumericUpDownPhaseBias.Value))
        End Sub

        Private Sub NumericUpDownMagnitudeBias_ValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles NumericUpDownMagnitudeBias.ValueChanged
            If _currentMeasurement Is Nothing Then
                Return
            End If
            _currentMeasurement.MagnitudeBias = CInt(Math.Truncate(NumericUpDownMagnitudeBias.Value))
        End Sub

        Private Sub NumericUpDownStatisticScale_ValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles NumericUpDownStatisticScale.ValueChanged
            If _currentMeasurement Is Nothing Then
                Return
            End If
            _currentMeasurement.StatisticDivisor = CDbl(NumericUpDownStatisticScale.Value)
        End Sub

        Private Sub ComboBoxTemperatureSource_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ComboBoxTemperatureSource.SelectedIndexChanged
            If _currentMeasurement Is Nothing Then
                Return
            End If
            _currentMeasurement.TemperatureSource = DirectCast(ComboBoxTemperatureSource.SelectedItem, String)
        End Sub

        Private Sub CheckedListBoxRegions_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CheckedListBoxRegions.SelectedIndexChanged

            Dim selectedRegion As RegionOfInterest
            Dim regionCal As RegionCalibration = Nothing

            CheckedListBoxRegions.Enabled = False
            If CheckedListBoxRegions.CheckedItems.Count > 0 Then
                CheckedListBoxCalibrations.Items.Clear()
                ' Display all available region calibration/calibration descriptions.
                For Each kvpCal As KeyValuePair(Of Integer, Calibration) In FormRFSensorMain.Calibrations
                    CheckedListBoxCalibrations.Items.Add(kvpCal.Value.Description, False)
                Next kvpCal
                ' If there is no RegionOfInterest selected then disable related checkboxes.
                If CheckedListBoxRegions.SelectedItem Is Nothing Then
                    CheckBoxIncludeRegionInSootLoadCalculation.Enabled = False
                    CheckBoxUseRegionForStatus.Enabled = False
                    Return
                End If
                selectedRegion = FormRFSensorMain.RegionsOfInterest(DirectCast(CheckedListBoxRegions.SelectedItem, String))
                ' Get the RegionCalibration that contains the current MeasurementID.
                GetRegionCalibration(selectedRegion, regionCal)
                If regionCal IsNot Nothing Then
                    CheckBoxIncludeRegionInSootLoadCalculation.Checked = regionCal.IncludeInCalculation
                    CheckBoxIncludeRegionInSootLoadCalculation.Enabled = True
                    CheckBoxUseRegionForStatus.Checked = regionCal.UseForStatus
                    CheckBoxUseRegionForStatus.Enabled = True
                    For Each calID As Integer In regionCal.CalibrationID
                        For idx As Integer = 0 To CheckedListBoxCalibrations.Items.Count - 2
                            Dim cal As New Calibration()

                            FormRFSensorMain.Calibrations.TryGetValue(calID, cal)
                            If DirectCast(CheckedListBoxCalibrations.Items(idx), String) = cal?.Description Then
                                ' Display a check mark for all associated Calibrations.
                                CheckedListBoxCalibrations.SetItemChecked(idx, True)
                                Exit For
                            End If
                        Next idx
                    Next calID
                    CheckedListBoxCalibrations.Enabled = True
                End If
            End If
            CheckedListBoxRegions.Enabled = True
        End Sub

        Private Sub CheckedListBoxRegions_ItemCheck(ByVal sender As Object, ByVal e As ItemCheckEventArgs) Handles CheckedListBoxRegions.ItemCheck

            Dim selectedRegion As RegionOfInterest
            Dim regionCal As RegionCalibration = Nothing

            If CheckedListBoxRegions.SelectedItem Is Nothing Then
                Return
            End If
            ' The check state is not updated until after this event handler is called.
            If e.NewValue = CheckState.Checked Then
                If CheckedListBoxRegions.SelectedItem Is Nothing Then
                    Return
                End If
                selectedRegion = FormRFSensorMain.RegionsOfInterest(DirectCast(CheckedListBoxRegions.SelectedItem, String))
                If (selectedRegion?.RegionID Is Nothing) OrElse (_currentMeasurement?.MeasurementID Is Nothing) Then
                    ' There is no Region Of Interest selected or the current
                    ' Measurement/MeasurementID is null, this shouldn't happen,
                    ' we shouldn't be here. Leave a breadcrumb.
                    LogErrorEventEvent?.Invoke("CheckedListBoxRegions_ItemCheck(): region/RegionID or " & "_currentMeasurment/MeasurementID are null")
                    Return
                End If
                _regionCalCopy.TryGetValue(CInt(_currentMeasurement.MeasurementID), regionCal)
                ' A Region Of Interest is being checked.
                If regionCal Is Nothing Then
                    ' Create a RegionCalibration to associate the Measurement and Region IDs.
                    regionCal = New RegionCalibration()
                    regionCal.MeasurementID = CInt(_currentMeasurement.MeasurementID)
                End If
                regionCal.RegionID.Add(CInt(selectedRegion.RegionID))
                _regionCalCopy.TryAdd(CInt(_currentMeasurement.MeasurementID), regionCal)
                ' Uncheck and enable Region related checkboxes.
                CheckBoxIncludeRegionInSootLoadCalculation.Checked = False
                CheckBoxIncludeRegionInSootLoadCalculation.Enabled = True
                CheckBoxUseRegionForStatus.Checked = False
                CheckBoxUseRegionForStatus.Enabled = True
                ' Display all available region calibration/calibration descriptions.
                For Each kvpCal As KeyValuePair(Of Integer, Calibration) In FormRFSensorMain.Calibrations
                    CheckedListBoxCalibrations.Items.Add(kvpCal.Value.Description, False)
                Next kvpCal
                For Each kvpCal As KeyValuePair(Of Integer, Calibration) In FormRFSensorMain.Calibrations
                    Dim found As Boolean = regionCal.CalibrationID.Any(Function(calID) CBool(calID = kvpCal.Value.CalibrationID))
                    For idx As Integer = 0 To CheckedListBoxCalibrations.Items.Count - 1
                        If DirectCast(CheckedListBoxCalibrations.Items(idx), String) = kvpCal.Value.Description Then
                            ' Display a check mark for all associated Calibrations.
                            CheckedListBoxCalibrations.SetItemChecked(idx, found)
                            Exit For
                        End If
                    Next idx
                Next kvpCal
                ' Enable Calibration selection.
                CheckedListBoxCalibrations.Enabled = True
            ElseIf e.NewValue = CheckState.Unchecked Then
                If _currentMeasurement?.MeasurementID Is Nothing Then
                    ' Measurement/MeasurementID is null, this shouldn't happen,
                    ' we shouldn't be here. Leave a breadcrumb.
                    LogErrorEventEvent?.Invoke("CheckedListBoxRegions_ItemCheck(): _currentMeasurement" & vbcrlf & "or _currentMeasurement.MeasurementID is null.")
                    Return
                End If
                ' A Region Of Interest is being unchecked.
                ' Remove the associated RegionCalibration.
                _regionCalCopy.TryRemove(CInt(_currentMeasurement?.MeasurementID), regionCal)
                ' Uncheck and disable Region related checkboxes.
                CheckBoxIncludeRegionInSootLoadCalculation.Checked = False
                CheckBoxIncludeRegionInSootLoadCalculation.Enabled = False
                CheckBoxUseRegionForStatus.Checked = False
                CheckBoxUseRegionForStatus.Enabled = False
                ' Uncheck the selected Calibration in the List Box.
                For idx As Integer = 0 To CheckedListBoxCalibrations.Items.Count - 2
                    ' Uncheck the calibration checkbox.
                    CheckedListBoxCalibrations.SetItemCheckState(idx, CheckState.Unchecked)
                Next idx
                CheckedListBoxCalibrations.ClearSelected()
                ' Disable Calibration selection.
                CheckedListBoxCalibrations.Enabled = False
            End If
        End Sub

        Private Sub CheckedListBoxCalibrations_ItemCheck(ByVal sender As Object, ByVal e As ItemCheckEventArgs) Handles CheckedListBoxCalibrations.ItemCheck

            Dim selectedRegion As RegionOfInterest
            Dim regionCal As RegionCalibration = Nothing

            ' The check state is not updated until after this event handler is called.
            If CheckedListBoxCalibrations.SelectedItem Is Nothing Then
                ' Nothings selected, shouldn't be here.
                Return
            End If
            selectedRegion = FormRFSensorMain.RegionsOfInterest(DirectCast(CheckedListBoxRegions.SelectedItem, String))
            If selectedRegion Is Nothing Then
                ' There is no Region Of Interest selected, shouldn't be here.
                Return
            End If
            ' Get the RegionCalibration that contains the current MeasurementID.
            GetRegionCalibration(selectedRegion, regionCal)
            If regionCal Is Nothing Then
                ' There is no corresponding RegionCalibration, so uncheck
                ' this selection.
                e.NewValue = CheckState.Unchecked
                Return
            End If
            If e.CurrentValue = CheckState.Checked Then
                ' This Calibration is being unchecked (deselected).
                Dim idx As Integer = 0
                Do While idx < regionCal.CalibrationID.Count
                    ' Remove the corresponding CalibrationID from the RegionCalibration.
                    If FormRFSensorMain.Calibrations(regionCal.CalibrationID(idx)).Description = DirectCast(CheckedListBoxCalibrations.Items(e.Index), String) Then
                        regionCal.CalibrationID.RemoveAt(idx)
                        Exit Do
                    End If
                    idx += 1
                Loop
            Else
                ' This Calibration is being checked (selected).
                For Each kvpCal As KeyValuePair(Of Integer, Calibration) In FormRFSensorMain.Calibrations
                    If kvpCal.Value.Description = DirectCast(CheckedListBoxCalibrations.Items(e.Index), String) Then
                        ' Add the corresponding CalibrationID to the RegionCalibration.
                        If kvpCal.Value.CalibrationID IsNot Nothing Then
                            regionCal.CalibrationID.Add(CInt(kvpCal.Value.CalibrationID))
                        End If
                        Exit For
                    End If
                Next kvpCal
            End If
        End Sub

        Private Sub CheckBoxIncludeRegionInSootLoadCalculation_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CheckBoxIncludeRegionInSootLoadCalculation.CheckedChanged

            Dim selectedRegion As RegionOfInterest
            Dim regionCal As RegionCalibration = Nothing

            If CheckedListBoxRegions.SelectedItem Is Nothing Then
                Return
            End If

            selectedRegion = FormRFSensorMain.RegionsOfInterest(DirectCast(CheckedListBoxRegions.SelectedItem, String))
            If selectedRegion Is Nothing Then
                ' There is no Region Of Interest selected, shoiuldn't be here.
                Return
            End If
            ' Get the RegionCalibration that contains the current MeasurementID.
            GetRegionCalibration(selectedRegion, regionCal)
            If regionCal IsNot Nothing Then
                regionCal.IncludeInCalculation = CheckBoxIncludeRegionInSootLoadCalculation.Checked
            End If
        End Sub

        Private Sub CheckBoxUseRegionForStatus_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CheckBoxUseRegionForStatus.CheckedChanged

            Dim selectedRegion As RegionOfInterest
            Dim regionCal As RegionCalibration = Nothing

            If CheckedListBoxRegions.SelectedItem Is Nothing Then
                Return
            End If

            selectedRegion = FormRFSensorMain.RegionsOfInterest(DirectCast(CheckedListBoxRegions.SelectedItem, String))
            If selectedRegion Is Nothing Then
                ' There is no Region Of Interest selected, shouldn't be here.
                Return
            End If
            For Each kvpRegionCal As KeyValuePair(Of Integer, RegionCalibration) In _regionCalCopy.Where(Function(regCal) _currentMeasurement.MeasurementID.Equals(regCal.Value.MeasurementID))
                If kvpRegionCal.Value.RegionID.Any(Function(regID) regID = (If(selectedRegion.RegionID, 0))) Then
                    regionCal = kvpRegionCal.Value
                    Exit For ' Only expecting one.
                End If
            Next kvpRegionCal
            If regionCal IsNot Nothing Then
                regionCal.UseForStatus = CheckBoxUseRegionForStatus.Checked
            End If
        End Sub

        Private Sub ButtonAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonAdd.Click
            Dim lastMeasureID As Integer = -1

            If _state <> FormState.Ready Then
                Select Case _state
                    Case FormState.CopyEditing
                        MessageBox.Show("Copy/Edit is in progress.", "Invalid Operation!")
                    Case FormState.Editing
                        MessageBox.Show("Edit is in progress.", "Invalid Operation!")
                End Select
                Return ' Operation invalid;
            End If
            _currentMeasurement = New Measurement()
            For Each measureID As Integer In
                From measure In FormRFSensorMain.Measurements
                Where measure.Value.MeasurementID IsNot Nothing
                Select CInt(measure.Value.MeasurementID)
                ' Find the last Measurement ID.
                If measureID > lastMeasureID Then
                    lastMeasureID = measureID
                End If
            Next measureID
            If lastMeasureID > 0 Then
                ' New Measurement ID = last Measurement ID + 1.
                _currentMeasurement.MeasurementID = lastMeasureID + 1
            Else
                ' This is the first Measurement configuration so use the
                ' default Measurement ID value.
                _currentMeasurement.MeasurementID = Common.DefaultID
            End If
            _state = FormState.Adding
            ' During an Add disable all change initiating controls.
            ConfigurationChangeEnable(False)
            InitializeForm()
            PanelDataFields.Enabled = True
            TextBoxDescription.Focus()
        End Sub

        Private Sub CopyThenEditButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles CopyThenEditButton.Click
            Dim lastMeasureID As Integer = -1


            If _state <> FormState.Ready Then
                Select Case _state
                    Case FormState.Adding
                        MessageBox.Show("Add is in progress.", "Invalid Operation!")
                    Case FormState.Editing
                        MessageBox.Show("Edit is in progress.", "Invalid Operation!")
                End Select
                Return ' Operation invalid;
            End If
            ' Editing a copy of an existing MeasurementID.
            If _currentMeasurement IsNot Nothing Then
                For Each sourceRegionCal As RegionCalibration In
                    From regCal In FormRFSensorMain.RegionCalibrations
                    Where _currentMeasurement.MeasurementID.Equals(regCal.Value.MeasurementID)
                    Select regCal.Value
                    ' Get a copy of the currently selected RegionCalibration,
                    ' if there is one.
                    _copiedRegionCal = New RegionCalibration(sourceRegionCal)
                Next sourceRegionCal
                For Each measureID As Integer In
                    From measure In FormRFSensorMain.Measurements
                    Where measure.Value.MeasurementID IsNot Nothing
                    Select CInt(measure.Value.MeasurementID)
                    ' Find the last Measurement ID.
                    If measureID > lastMeasureID Then
                        lastMeasureID = measureID
                    End If
                Next measureID
                ' Assign a new Measurement ID value to a copy of the current
                ' Measurement configuration.
                If lastMeasureID <= 0 Then
                    MessageBox.Show("Copy/Edit operation failed." & vbcrlf & "No Measurement ID found.", "")
                    Return
                Else
                    _currentMeasurement.MeasurementID = lastMeasureID + 1
                End If
                _state = FormState.CopyEditing
                ' Change the Measurement Description to the defualt value.
                TextBoxDescription.Text = "Description"
                _currentMeasurement.Description = ""
                ' During an Copy/Edit disable all change initiating controls.
                ConfigurationChangeEnable(False)
                PanelDataFields.Enabled = True
                TextBoxDescription.Focus()
            Else
                MessageBox.Show("Please select a Measurement configuration first.", "")
            End If
        End Sub

        Private Sub ButtonEdit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonEdit.Click
            If _state <> FormState.Ready Then
                Select Case _state
                    Case FormState.Adding
                        MessageBox.Show("Add is in progress.", "Invalid Operation!")
                    Case FormState.CopyEditing
                        MessageBox.Show("Copy/Edit is in progress.", "Invalid Operation!")
                End Select
                Return ' Operation invalid;
            End If
            If _currentMeasurement IsNot Nothing Then
                _state = FormState.Editing
                ' During an Edit disable all change initiating controls.
                ConfigurationChangeEnable(False)
                PanelDataFields.Enabled = True
                TextBoxDescription.Focus()
            Else
                MessageBox.Show("Please select a Measurement configuration first.", "")
            End If
        End Sub

        Private Sub ButtonUpdate_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonUpdate.Click
            Dim removed As Measurement = Nothing

            If (PanelDataFields.Enabled <> True) OrElse (_state = FormState.Ready) Then
                Return ' Shouldn't be here.
            End If

            If (_currentMeasurement?.MeasurementID) Is Nothing Then
                ' Shouldn't happen, get ready to try again.
                _state = FormState.Ready
                _currentMeasurement = Nothing
                ' Re-enable all change initiating controls.
                ConfigurationChangeEnable(True)
                ' Display the first Measurement in the control.
                If ListBoxMeasurements.Items.Count > 0 Then
                    ListBoxMeasurements.SelectedIndex = 0
                    DisplaySelectedMeasurement()
                Else
                    ' No existing Measurements so display default values.
                    ListBoxMeasurements.SelectedIndex = -1

                    ' Disable the edit buttons.
                    DisableEdit()
                    InitializeForm()
                End If
                MessageBox.Show("A failure prevented the new Measurement" & vbcrlf & "configuration from being created." & vbcrlf & "Please try again.", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return
            End If
            If Not String.IsNullOrWhiteSpace(TextBoxDescription.Text) AndAlso TextBoxDescription.Text <> "Description" Then
                If ComboBoxSweep.SelectedIndex <> -1 Then
                    If _state <> FormState.Editing Then
                        For idx As Integer = 0 To ListBoxMeasurements.Items.Count - 1
                            If _currentMeasurement.Description.ToLower() = DirectCast(ListBoxMeasurements.Items(idx), String).ToLower() Then
                                ' The new or copy/edited Measurement description
                                ' is a duplicate of an existing Measurement
                                ' configuration description. Comparison is
                                ' case insensitive.
                                MessageBox.Show("First enter a unique Description string." & vbcrlf & "The comparison is case insensitive.", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                Return
                            End If
                        Next idx
                    End If
                    ' If a new Measurement was created or an existing one was modified
                    ' then save the current measurement.
                    ' First find the current active measurement configuration,
                    ' if there is one.
                    FormRFSensorMain.LoadActiveMeasurement()
                    If _state = FormState.Adding Then
                        ' This is a new Measurement configuration.
                        ' Save the new Measurement configuration.
                        If Not FormRFSensorMain.Measurements.TryAdd(String.Copy(_currentMeasurement.Description), _currentMeasurement) Then
                            MessageBox.Show("A failure prevented the new Measurement" & vbcrlf & "configuration from being created." & vbcrlf & "Please try again or press Cancel.", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            Return
                        End If
                    Else
                        For Each kvpMeasure In (
                            From measure In FormRFSensorMain.Measurements
                            Where _currentMeasurement.MeasurementID.Equals(measure.Value.MeasurementID)
                            Select measure).Where(Function(kvp) kvp.Key <> _currentMeasurement.Description)
                            ' The Measuremment Description has changed so add
                            ' the current Measurment configuration to the
                            ' Master Measurement configuration dictionary.
                            Dim key As String = String.Copy(_currentMeasurement.Description)
                            If FormRFSensorMain.Measurements.TryAdd(_currentMeasurement.Description, New Measurement(_currentMeasurement)) Then
                                ListBoxMeasurements.Items.Add(String.Copy(_currentMeasurement.Description))
                            Else
                                MessageBox.Show("Failure to update the Measurements ListBox" & vbcrlf & "prevented the edited Measurement" & vbcrlf & "configuration from being saved." & vbcrlf & "Please try again or press Cancel.", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                Return
                            End If
                            ' Now remove the previous Measurement configuration
                            ' from the Master Measurement configuration dictionary.
                            If FormRFSensorMain.Measurements.TryRemove(kvpMeasure.Key, removed) Then
                                ListBoxMeasurements.Items.RemoveAt(ListBoxMeasurements.FindString(String.Copy(kvpMeasure.Key)))
                                ' I don't know how, but ListBoxMeasurements seems
                                ' to be holding a reference to the _currentMeasurement
                                ' object. Even though I only place copied strings
                                ' into ListBoxMeasurements. As a result _currentMeasurement
                                ' is now null. Doh!!! So reload _currentMeasurement
                                ' with a copy of the Measurement configuration
                                ' that was just added to the master Measurements
                                ' dictionary.
                                _currentMeasurement = New Measurement(FormRFSensorMain.Measurements(key))
                                Exit For
                            Else
                                MessageBox.Show("A failure prevented the edited Measurement" & vbcrlf & "configuration from being saved." & vbcrlf & "Please try again or press Cancel.", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                Return
                            End If
                        Next kvpMeasure
                        If removed Is Nothing Then
                            If _state = FormState.Editing Then
                                ' This is an edited Measurement configuration,
                                ' so replace the previous in the Master Measurement
                                ' configuration dictionary.
                                FormRFSensorMain.Measurements(_currentMeasurement.Description) = _currentMeasurement
                            ElseIf _state = FormState.CopyEditing Then
                                If (_copiedRegionCal?.MeasurementID IsNot Nothing) AndAlso (_currentMeasurement.MeasurementID IsNot Nothing) Then
                                    ' Copy of the RegionCalibration associated with
                                    ' the copied Measurement Configuration that
                                    ' Was edited to become the Measurement configuration
                                    ' that is being stored.
                                    _copiedRegionCal.MeasurementID = CInt(_currentMeasurement.MeasurementID)
                                    If Not _regionCalCopy.TryAdd(If((_copiedRegionCal.MeasurementID), 0), _copiedRegionCal) Then

                                        MessageBox.Show("A failure prevented the RegionCalibration Configuration associated with" & vbcrlf & "the copied and edited Measurement configuration from being saved." & vbcrlf & "Please try again or press Cancel.", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                        Return
                                    End If
                                End If
                                ' This is a Measurement configuration that has
                                ' copied and edited. It is new, so add it to the
                                ' Master Measurement configuration dictionary.
                                If Not FormRFSensorMain.Measurements.TryAdd(_currentMeasurement.Description, _currentMeasurement) Then
                                    MessageBox.Show("A failure prevented the copied and edited" & vbcrlf & "Measurement configuration from being saved." & vbcrlf & "Please try again or press Cancel.", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                    Return
                                End If
                            End If
                        End If
                    End If
                    If (_state = FormState.Adding) OrElse (_state = FormState.CopyEditing) Then
                        ListBoxMeasurements.Items.Add(String.Copy(_currentMeasurement.Description))
                    End If
                    ListBoxMeasurements.SelectedIndex = ListBoxMeasurements.FindString(_currentMeasurement.Description)
                    If (FormRFSensorMain.ActiveMeasurement.Description IsNot Nothing) AndAlso (_currentMeasurement.Active) Then
                        ' Allow only one active measurement configuration at a time.
                        FormRFSensorMain.Measurements(FormRFSensorMain.ActiveMeasurement.Description).Active = False
                    End If
                    FormRFSensorMain.LoadActiveMeasurement()
                    ' Indicate to the caller, on return, that the Measurement
                    ' configurations have changed.
                    _configChanged = System.Windows.Forms.DialogResult.Yes
                    _currentMeasurement = Nothing
                    ' Update the master RegionCalibration dictionary.
                    FormRFSensorMain.RegionCalibrations.Clear()
                    For Each kvpRegionCal As KeyValuePair(Of Integer, RegionCalibration) In _regionCalCopy
                        FormRFSensorMain.RegionCalibrations.TryAdd(kvpRegionCal.Key, kvpRegionCal.Value)
                    Next kvpRegionCal
                    _state = FormState.Ready
                    ' Re-initialize change initiating controls.
                    ConfigurationChangeEnable(True)
                    ' Display the currently selected Measurement configuration
                    ' and reload _currentMeasurement.
                    DisplaySelectedMeasurement()
                    ' Disable form controls.
                    PanelDataFields.Enabled = False
                Else
                    MessageBox.Show("You must select a Sweep first." & vbcrlf & "Or press Cancel to discard changes.", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            Else
                MessageBox.Show("You must enter a Description first." & vbcrlf & "Or press Cancel to discard changes.", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        End Sub

        Private Sub ButtonRemove_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonRemove.Click
            If _state <> FormState.Ready Then
                Select Case _state
                    Case FormState.Adding
                        MessageBox.Show("Add is in progress.", "Invalid Operation!")
                    Case FormState.CopyEditing
                        MessageBox.Show("Copy/Edit is in progress.", "Invalid Operation!")
                    Case FormState.Editing
                        MessageBox.Show("Edit is in progress.", "Invalid Operation!")
                End Select
                Return ' Operation invalid;
            End If
            If _currentMeasurement IsNot Nothing Then
                If FormRFSensorMain.Measurements.ContainsKey(_currentMeasurement.Description) Then
                    Dim removed As Measurement = Nothing
                    If FormRFSensorMain.Measurements.TryRemove(_currentMeasurement.Description, removed) Then
                        PanelDataFields.Enabled = False
                        ' Remove RegionCalibrations that are associated with
                        ' the Measurement being removed.
                        For Each kvpRegionCal As KeyValuePair(Of Integer, RegionCalibration) In _regionCalCopy.Where(Function(regCal) _currentMeasurement.MeasurementID.Equals(regCal.Key))
                            Dim removedCal As RegionCalibration = Nothing
                            _regionCalCopy.TryRemove(kvpRegionCal.Key, removedCal)
                        Next kvpRegionCal
                        ' Remove the current measurement from the Measurements List Box.
                        ListBoxMeasurements.Items.RemoveAt(ListBoxMeasurements.FindString(_currentMeasurement.Description))
                        _currentMeasurement = Nothing
                        If ListBoxMeasurements.Items.Count > 0 Then
                            ' The previous selection has been removed, so select and
                            ' display the first measurement in the control.
                            ListBoxMeasurements.SelectedIndex = 0
                            DisplaySelectedMeasurement()
                        Else
                            ' Display default values.
                            InitializeForm()
                        End If
                        ' In case the removed measurement configuration was the
                        ' active one.
                        FormRFSensorMain.LoadActiveMeasurement()
                        ' Indicate to the caller, on return, that the Measurement
                        ' configurations have changed.
                        _configChanged = System.Windows.Forms.DialogResult.Yes
                        ' Update the master RegionCalibration dictionary.
                        FormRFSensorMain.RegionCalibrations.Clear()
                        For Each kvpRegionCal As KeyValuePair(Of Integer, RegionCalibration) In _regionCalCopy
                            FormRFSensorMain.RegionCalibrations.TryAdd(kvpRegionCal.Key, kvpRegionCal.Value)
                        Next kvpRegionCal
                    Else
                        MessageBox.Show("A failure prevented the Measurement" & vbcrlf & "configuration from being removed." & vbcrlf & "Please try again.", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End If
                    ' Re-enable all change initiating controls.
                    ConfigurationChangeEnable(True)
                    If ListBoxMeasurements.Items.Count = 0 Then
                        ' There are no more Measurement configurations, so disable
                        ' the edit buttons.
                        DisableEdit()
                    End If
                Else
                    ' It looks like an Add might have been in progress when the
                    ' user pressed remove so discard it.
                    ' Disable controls.
                    PanelDataFields.Enabled = False
                    _currentMeasurement = Nothing
                    ' Re-enable all change initiating controls.
                    ConfigurationChangeEnable(True)
                    ' Display the first Measurement in the control.
                    If ListBoxMeasurements.Items.Count > 0 Then
                        ListBoxMeasurements.SelectedIndex = 0
                        DisplaySelectedMeasurement()
                    Else
                        ' No existing Measurements so display default values.
                        ListBoxMeasurements.SelectedIndex = -1
                        InitializeForm()
                    End If
                End If
            Else
                MessageBox.Show("Please select a Measurement configuration first.", "")
            End If
        End Sub

        Private Sub ButtonCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonCancel.Click
            If PanelDataFields.Enabled = True Then
                ' Do not change the dialog result here. You may lose
                ' previous changes.
                ' Disable controls.
                PanelDataFields.Enabled = False
                _currentMeasurement = Nothing
                ' Through away any RegionCalibration changes by getting fresh
                ' copies.
                CopyRegionCalibrations()
                ' Re-enable all change initiating controls.
                ConfigurationChangeEnable(True)
                If ListBoxMeasurements.Items.Count > 0 Then
                    ' The current edit or add has been canceled, so
                    ' redisplay the currently selected measurement,
                    ' or the first Measurement if the form.
                    If ListBoxMeasurements.SelectedItem Is Nothing Then
                        ListBoxMeasurements.SelectedIndex = 0
                    End If
                    DisplaySelectedMeasurement()
                Else
                    ' No Measurements available so display default values.
                    ListBoxMeasurements.SelectedIndex = -1
                    ' Disable the edit buttons.
                    DisableEdit()
                    InitializeForm()
                End If
                _state = FormState.Ready
            End If
        End Sub

        Private Sub ButtonGenerate_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonGenerate.Click
            Dim buffer(99999) As Byte
            Dim bufferIndex As UInt32 = 0
            Dim fileHeader As FlashDataHeader = Nothing

            If ListBoxMeasurements.Items.Count = 0 Then
                MessageBox.Show("Please create a Measurement configuration first.")
                Return
            ElseIf _currentMeasurement Is Nothing Then
                MessageBox.Show("Please select a Measurement configuration first.")
                Return
            End If
            SaveGeneratedFileDialog.FileName = TextBoxDescription.Text
            If SaveGeneratedFileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
                Try
                    fileHeader = New FlashDataHeader()
                    bufferIndex = fileHeader.WriteFlashDataHeader(buffer)

                    Dim regionConfigurations As Tuple(Of List(Of Calibration), List(Of RegionOfInterest), List(Of RegionCalibration)) = FormRFSensorMain.GetRegionCalibrations(If(_currentMeasurement.MeasurementID, 0))

                    fileHeader.MeasurementAddress = MakePSVAddress(bufferIndex + FileHeaderBaseAddress)
                    bufferIndex = WriteMeasurement(bufferIndex, buffer, regionConfigurations.Item1, regionConfigurations.Item2, FormRFSensorMain.Sweeps(_currentMeasurement.SweepID))

                    fileHeader.SweepCount = 1
                    fileHeader.SweepAddress = MakePSVAddress(bufferIndex + FileHeaderBaseAddress)

                    bufferIndex = WriteSweep(bufferIndex, buffer, FormRFSensorMain.Sweeps(_currentMeasurement.SweepID))
                    fileHeader.RegionsAddress = MakePSVAddress(bufferIndex + FileHeaderBaseAddress)


                    fileHeader.RegionCount = CUShort(regionConfigurations.Item2.Count)

                    For Each sourceRegion As RegionOfInterest In regionConfigurations.Item2
                        Dim sourceRegionCal As RegionCalibration = CType(
                            (From regionCal In regionConfigurations.Item3
                             Where regionCal.RegionID.Any(Function(regID) sourceRegion.RegionID.Equals(regID))
                             Select regionCal), RegionCalibration)
                        bufferIndex = WriteRegionOfInterest(bufferIndex, buffer, sourceRegion, sourceRegionCal, regionConfigurations.Item1)
                    Next sourceRegion

                    fileHeader.CalibrationAddress = MakePSVAddress(bufferIndex + FileHeaderBaseAddress)
                    fileHeader.CalibrationCount = CUShort(FormRFSensorMain.Calibrations.Count)

                    For Each kvpCal As KeyValuePair(Of Integer, Calibration) In FormRFSensorMain.Calibrations
                        bufferIndex = WriteCalibration(bufferIndex, buffer, kvpCal.Value)
                    Next kvpCal

                    fileHeader.FrequencyAddress = MakePSVAddress(bufferIndex + FileHeaderBaseAddress)

                Catch ex As Exception
                    LogErrorEventEvent?.Invoke(ex.Message & vbcrlf & ex.StackTrace)
                    MessageBox.Show("ButtonGenerate_Click(): File header generation" & vbcrlf & "failed due to an exception:" & vbcrlf & ex.Message)
                End Try
                Try
                    If fileHeader IsNot Nothing Then
                        fileHeader.WriteFlashDataHeader(buffer)
                        MessageBox.Show("Output File Generated. Data Area Size: " & bufferIndex.ToString() & ", (0x" & bufferIndex.ToString("X4") & ")")
                        Dim Output As New StreamWriter(SaveGeneratedFileDialog.FileName)
                        WriteBufferToHexFile(buffer, bufferIndex, Output, FileHeaderBaseAddress)
                        Output.Close()
                    End If
                Catch ex As Exception
                    LogErrorEventEvent?.Invoke(ex.Message & vbcrlf & ex.StackTrace)
                    MessageBox.Show("ButtonGenerate_Click(): File save failed due to an exception:" & vbcrlf & ex.Message)
                End Try
            End If
        End Sub

        Private Sub ButtonClose_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonClose.Click
            Close()
        End Sub

        Private Sub TextBoxDescription_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TextBoxDescription.TextChanged
            If _currentMeasurement Is Nothing Then
                Return
            End If
            _currentMeasurement.Description = TextBoxDescription.Text
        End Sub

        Private Sub ComboBoxSweep_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ComboBoxSweep.SelectedIndexChanged
            If _currentMeasurement Is Nothing Then
                Return
            End If
            For Each selectedSweep As KeyValuePair(Of Integer, Sweep) In FormRFSensorMain.Sweeps
                If selectedSweep.Value.Description = DirectCast(ComboBoxSweep.SelectedItem, String) Then
                    _currentMeasurement.SweepID = If(selectedSweep.Value.SweepID, 0)
                End If
            Next selectedSweep
        End Sub

        Private Sub ComboBoxBoardTempCalibration_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ComboBoxBoardTempCalibration.SelectedIndexChanged
            If _currentMeasurement Is Nothing Then
                Return
            End If
            For Each kvpCal As KeyValuePair(Of Integer, Calibration) In FormRFSensorMain.Calibrations
                If kvpCal.Value.Description = DirectCast(ComboBoxBoardTempCalibration.SelectedItem, String) Then
                    _currentMeasurement.BoardCalibration = If(kvpCal.Value.CalibrationID, 0)
                End If
            Next kvpCal
        End Sub

        Private Sub ComboBoxColdJunctionCalibration_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ComboBoxColdJunctionCalibration.SelectedIndexChanged
            If _currentMeasurement Is Nothing Then
                Return
            End If
            For Each kvpCal As KeyValuePair(Of Integer, Calibration) In FormRFSensorMain.Calibrations
                If kvpCal.Value.Description = DirectCast(ComboBoxColdJunctionCalibration.SelectedItem, String) Then
                    _currentMeasurement.ColdJunctionCalibration = If(kvpCal.Value.CalibrationID, 0)
                End If
            Next kvpCal
        End Sub

        Private Sub ComboBoxThermocoupleCalibration_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ComboBoxThermocoupleCalibration.SelectedIndexChanged
            If _currentMeasurement Is Nothing Then
                Return
            End If
            For Each kvpCal As KeyValuePair(Of Integer, Calibration) In FormRFSensorMain.Calibrations
                If kvpCal.Value.Description = DirectCast(ComboBoxThermocoupleCalibration.SelectedItem, String) Then
                    _currentMeasurement.ThermocoupleCalibration = If(kvpCal.Value.CalibrationID, 0)
                End If
            Next kvpCal
        End Sub

        Private Sub ComboBoxRTDCalibration_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ComboBoxRTDCalibration.SelectedIndexChanged
            If _currentMeasurement Is Nothing Then
                Return
            End If
            For Each kvpCal As KeyValuePair(Of Integer, Calibration) In FormRFSensorMain.Calibrations
                If kvpCal.Value.Description = DirectCast(ComboBoxRTDCalibration.SelectedItem, String) Then
                    _currentMeasurement.RTDCalibration = If(kvpCal.Value.CalibrationID, 0)
                End If
            Next kvpCal
        End Sub

        Private Sub ComboBoxSignal1Calibration_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ComboBoxSignalCalibration.SelectedIndexChanged
            If _currentMeasurement Is Nothing Then
                Return
            End If
            For Each kvpCal As KeyValuePair(Of Integer, Calibration) In FormRFSensorMain.Calibrations
                If kvpCal.Value.Description = DirectCast(ComboBoxSignalCalibration.SelectedItem, String) Then
                    _currentMeasurement.Signal1Calibration = If(kvpCal.Value.CalibrationID, 0)
                End If
            Next kvpCal
        End Sub

        Private Sub CheckBoxAutoStart_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CheckBoxAutoStart.CheckedChanged
            If _currentMeasurement Is Nothing Then
                Return
            End If
            _currentMeasurement.AutoStart = CheckBoxAutoStart.Checked
        End Sub

        Private Sub CheckBoxGenerateRawOutputFiles_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CheckBoxGenerateRawOutputFiles.CheckedChanged
            If _currentMeasurement Is Nothing Then
                Return
            End If
            _currentMeasurement.GenerateRawOutputFiles = CheckBoxGenerateRawOutputFiles.Checked
        End Sub

        Private Sub CheckBoxGenerateROFiles_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CheckBoxGenerateROFiles.CheckedChanged
            If _currentMeasurement Is Nothing Then
                Return
            End If
            _currentMeasurement.GenerateROFiles = CheckBoxGenerateROFiles.Checked
        End Sub

        Private Sub CheckBoxGeneratePOFile_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CheckBoxGeneratePOFile.CheckedChanged
            If _currentMeasurement Is Nothing Then
                Return
            End If
            _currentMeasurement.GeneratePOFile = CheckBoxGeneratePOFile.Checked
        End Sub

        Private Sub TextBoxSignalName_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TextBoxSignalName.TextChanged
            If _currentMeasurement Is Nothing Then
                Return
            End If
            _currentMeasurement.Signal1Name = TextBoxSignalName.Text
        End Sub
    End Class
