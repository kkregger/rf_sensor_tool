﻿Imports System.Globalization

' Calibration Classes

    Public Class AppCalibration
        Inherits ConfigurationBase

        Public Sub New()
        End Sub

        Public Sub New(calNum As XElement)
            Dim name As String

            For Each ele As Object In calNum.Descendants()
                name = ele.Name.ToString().ToUpper()
                If name = "CALIDNUM" Then
                    Dim caliID As UShort
                    If UShort.TryParse(ele.Value, caliID) Then
                        CalIdNum = caliID
                    End If
                ElseIf name = "CALVARIABLE" Then
                    VarName = ele.Value.Trim(ControlChars.Tab, " "C, Chr(34))
                ElseIf name = "BYTES" Then
                    Dim cnt As Byte
                    If Byte.TryParse(ele.Value, cnt) Then
                        Bytes = cnt
                    End If
                ElseIf name = "ID" Then
                    Dim canAccID As Byte
                    If Byte.TryParse(ele.Value, canAccID) Then
                        CanAccessIndex = canAccID
                    End If
                ElseIf name = "RAWVALUE" Then
                    Dim rawVal As Long
                    If Long.TryParse(ele.Value, rawVal) Then
                        RawValue = rawVal
                    Else
                        RawValue = Long.MinValue
                    End If
                ElseIf name = "SCALEDVALUE" Then
                    Dim scaledVal As Long
                    If Long.TryParse(ele.Value, scaledVal) Then
                        ScaledValue = scaledVal
                    Else
                        ScaledValue = Long.MinValue
                    End If
                ElseIf name = "DISPMODE" Then
                    Dim dispMode As Byte
                    If Byte.TryParse(ele.Value, dispMode) Then
                        DisplayMode = dispMode
                    End If
                ElseIf name = "MIN" Then
                    Dim mini As Long
                    If Long.TryParse(ele.Value, mini) Then
                        Min = mini
                    End If
                ElseIf name = "MAX" Then
                    Dim maxi As Long
                    If Long.TryParse(ele.Value, maxi) Then
                        Max = maxi
                    End If
                ElseIf name = "UNITS" Then
                    Units = ele.Value.Trim(ControlChars.Tab, " "C, Chr(34))
                ElseIf name = "DESCRIPTIVE" Then
                    Description = ele.Value.Trim(ControlChars.Tab, " "C, Chr(34))
                ElseIf name = "TYPE" Then
                    CalType = ele.Value.Trim(ControlChars.Tab, " "C, Chr(34))
                End If
            Next
        End Sub

        Public Sub New(previous As AppCalibration)
            Me.CalIdNum       = previous.CalIdNum
            Me.Bytes          = previous.Bytes
            Me.VarName        = StringCopy(previous.VarName)
            Me.CanAccessIndex = previous.CanAccessIndex
            Me.RawValue       = previous.RawValue
            Me.ScaledValue    = previous.ScaledValue
            Me.DisplayMode    = previous.DisplayMode
            Me.Min            = previous.Min
            Me.Max            = previous.Max
            Me.Units          = StringCopy(previous.Units)
            Me.Description    = StringCopy(previous.Description)
            Me.CalType        = StringCopy(previous.CalType)
        End Sub

        Public ReadOnly Property CalIdNum()       As UShort

        Public ReadOnly Property VarName()        As String

        Public ReadOnly Property Bytes()          As Byte

        Public ReadOnly Property CanAccessIndex() As UShort

        Public ReadOnly Property RawValue()       As Long

        Public ReadOnly Property ScaledValue()    As Long

        Public ReadOnly Property DisplayMode()    As Byte

        Public ReadOnly Property Min()            As Long

        Public ReadOnly Property Max()            As Long

        Public ReadOnly Property Units()          As String

        Public ReadOnly Property Description()    As String

        Public ReadOnly Property CalType()        As String
    End Class

    Public Class UsageData
        Inherits ConfigurationBase

        Public Sub New()
        End Sub

        Public Sub New(item As XElement)
            Dim name As String

            For Each ele As Object In item.Descendants()
                name = ele.Name.ToString().ToUpper()
                If name = "VARNAME" Then
                    VarName = ele.Value.Trim(ControlChars.Tab, " "C, Chr(34))
                ElseIf name = "READADDR" Then
                    Dim readAddress As UShort
                    If UShort.TryParse(ele.Value, NumberStyles.HexNumber,
                                       CultureInfo.InvariantCulture, readAddress) Then
                        ReadAddr = readAddress
                    End If
                ElseIf name = "BYTES" Then
                    Dim cnt As Byte
                    If Byte.TryParse(ele.Value, cnt) Then
                        Bytes = cnt
                    End If
                ElseIf name = "OFFSET" Then
                    Dim oSet As Byte
                    If Byte.TryParse(ele.Value, oSet) Then
                        Offset = oSet
                    End If
                ElseIf name = "DISPMODE" Then
                    Dim dispMode As Byte
                    If Byte.TryParse(ele.Value, dispMode) Then
                        DisplayMode = dispMode
                    End If
                ElseIf name = "TOOLTYPE" Then
                    Dim tType As Byte
                    If Byte.TryParse(ele.Value, NumberStyles.HexNumber,
                                     CultureInfo.InvariantCulture, tType) Then
                        ToolType = tType
                    End If
                ElseIf name = "ESUM" Then
                    Dim sum As Byte
                    If Byte.TryParse(ele.Value, sum) Then
                        ESum = sum
                    End If
                End If
            Next
        End Sub

        Public Sub New(previous As UsageData)
            Me.VarName     = StringCopy(previous.VarName)
            Me.ReadAddr    = previous.ReadAddr
            Me.Bytes       = previous.Bytes
            Me.Offset      = previous.Offset
            Me.DisplayMode = previous.DisplayMode
            Me.ToolType    = previous.ToolType
            Me.ESum        = previous.ESum
        End Sub

        Public ReadOnly Property VarName()     As String

        Public ReadOnly Property ReadAddr()    As UShort

        Public ReadOnly Property Bytes()       As Byte

        Public ReadOnly Property Offset()      As Byte

        Public ReadOnly Property DisplayMode() As Byte

        Public ReadOnly Property ToolType()    As Byte

        Public ReadOnly Property ESum()        As Byte
    End Class

    Public Class E2MapEntry
        Inherits ConfigurationBase

        Public Sub New()
        End Sub

        Public Sub New(entry As XElement)
            Dim name As String

            For Each ele As Object In entry.Descendants()
                name = ele.Name.ToString().ToUpper()
                If name = "E2ADDR" Then
                    Dim e2Address As UShort
                    If UShort.TryParse(ele.Value, NumberStyles.HexNumber, CultureInfo.InvariantCulture, e2Address) Then
                        E2Addr = e2Address
                    End If
                ElseIf name = "VNAME" Then
                    VarName = ele.Value.Trim(ControlChars.Tab, " "C, Chr(34))
                ElseIf name = "BYTES" Then
                    Dim cnt As Byte
                    If Byte.TryParse(ele.Value, cnt) Then
                        Bytes = cnt
                    End If
                ElseIf name = "DISPMODE" Then
                    Dim dispMode As Byte
                    If Byte.TryParse(ele.Value, dispMode) Then
                        DisplayMode = dispMode
                    End If
                ElseIf name = "ESUM" Then
                    Dim sum As Byte
                    If Byte.TryParse(ele.Value, sum) Then
                        ESum = sum
                    End If
                End If
            Next
        End Sub

        Public Sub New(previous As E2MapEntry)
            Me.E2Addr      = previous.E2Addr
            Me.VarName     = StringCopy(previous.VarName)
            Me.Bytes       = previous.Bytes
            Me.DisplayMode = previous.DisplayMode
            Me.ESum        = previous.ESum
        End Sub

        Public ReadOnly Property E2Addr()      As UShort

        Public ReadOnly Property VarName()     As String

        Public ReadOnly Property Bytes()       As Byte

        Public ReadOnly Property DisplayMode() As Byte

        Public ReadOnly Property ESum()        As Byte
    End Class


Public Class ParamTblEntry
    Inherits ConfigurationBase

    Public Sub New()
    End Sub

    Public Sub New(entry As XElement)
        Dim name As String

        For Each ele As Object In entry.Descendants()
            name = ele.Name.ToString().ToUpper()
            If name = "VARIABLENAME" Then
                VarName = ele.Value.Trim(ControlChars.Tab, " "c, Chr(34))
            End If
            If name = "TOOLNAME" Then
                ToolName = ele.Value.Trim(ControlChars.Tab, " "c, Chr(34))
            ElseIf name = "BYTES" Then
                Dim cnt As Byte
                If Byte.TryParse(ele.Value, cnt) Then
                    Bytes = cnt
                End If
            ElseIf name = "PID" Then
                Dim pid As UShort
                If UShort.TryParse(ele.Value, pid) Then
                    ParamID = pid
                End If
            ElseIf name = "RAMADDRESS" Then
                Dim ramAddress As UShort
                If UShort.TryParse(ele.Value, NumberStyles.HexNumber, CultureInfo.InvariantCulture, ramAddress) Then
                    RamAddr = ramAddress
                End If
            ElseIf name = "MIN" Then
                Dim mini As Long
                If Long.TryParse(ele.Value, mini) Then
                    Min = mini
                End If
            ElseIf name = "MAX" Then
                Dim maxi As Long
                If Long.TryParse(ele.Value, maxi) Then
                    Max = maxi
                End If
            ElseIf name = "DISPLAYMODE" Then
                Dim dispMode As Byte
                If Byte.TryParse(ele.Value, dispMode) Then
                    DisplayMode = dispMode
                End If
            ElseIf name = "UNITS" Then
                Units = ele.Value.Trim(ControlChars.Tab, " "c, Chr(34))
            End If
        Next
    End Sub

    Public Sub New(previous As ParamTblEntry)
        Me.VarName     = StringCopy(previous.VarName)
        Me.ToolName    = StringCopy(previous.ToolName)
        Me.Bytes       = previous.Bytes
        Me.ParamID     = previous.ParamID
        Me.RamAddr     = previous.RamAddr
        Me.Min         = previous.Min
        Me.Max         = previous.Max
        Me.DisplayMode = previous.DisplayMode
        Me.Units       = StringCopy(previous.Units)
    End Sub

    Public ReadOnly Property VarName()     As String

    Public ReadOnly Property ToolName()    As String

    Public ReadOnly Property Bytes()       As Byte

    Public ReadOnly Property ParamID()     As UShort

    Public ReadOnly Property RamAddr()     As UShort

    Public ReadOnly Property Min()         As Long

    Public ReadOnly Property Max()         As Long

    Public ReadOnly Property DisplayMode() As Byte

    Public ReadOnly Property Units()       As String
End Class
