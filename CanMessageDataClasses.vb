﻿Imports System.Linq
Imports CTS_CAN_Services
Imports CTS_CAN_Services.Peak.Can.Basic

    Public Class SweepDataPoint

        Public Property Index() As UInteger
        Public Property ScanNumber() As UInteger
        Public Property MagnitudeAtoD() As UInteger
        Public Property PhaseAtoD() As UInteger
        
    End Class

    Public Class SweepStartData
        Public Property StartInletTemp() As Integer
        Public Property StartOutletTemp() As Integer
        Public Property StartFrequency() As UInteger

        Public Sub New()
        End Sub

        Public Sub New(ByVal previous As SweepStartData)
            StartInletTemp  = previous.StartInletTemp
            StartOutletTemp = previous.StartOutletTemp
            StartFrequency  = previous.StartFrequency
        End Sub
    End Class

    Public Class SweepEndData
        Public Property EndInletTemp() As Integer
        Public Property EndOutletTemp() As Integer
        Public Property EndFrequency() As UInteger

        Public Sub New()
        End Sub

        Public Sub New(ByVal previous As SweepEndData)
            EndInletTemp  = previous.EndInletTemp
            EndOutletTemp = previous.EndOutletTemp
            EndFrequency  = previous.EndFrequency
        End Sub
    End Class

    Public Class SweepStepData
        Public Property StepSize() As UInteger
        Public Property ReadDelay() As UInteger

        Public Sub New()
        End Sub

        Public Sub New(ByVal previous As SweepStepData)
            StepSize = previous.StepSize
            ReadDelay = previous.ReadDelay
        End Sub
    End Class

    Public Class SweepCmdData
        Public Property Entries() As UInteger
        Public Property OperatingMode() As Byte
        Public Property BoardTemp() As Short
        Public Property StepDelay() As Byte

        Public Sub New()
        End Sub

        Public Sub New(ByVal previous As SweepCmdData)
            Entries        = previous.Entries
            OperatingMode  = previous.OperatingMode
            BoardTemp      = previous.BoardTemp
            StepDelay      = previous.StepDelay
        End Sub
    End Class

    ''' <summary>
    ''' Class used to store received CAN Messages for processing.
    ''' </summary>
    Public Class CanMsgRcvd
        ' Contains received CAN message, corresponding time-stamp
        ' and corresponding properties.
        Private _canMsg As TPCANMsg
        Private _timeStamp As CAN_Services.CAN_MsgTimestamp

        Public Shared Operator =(ByVal a As CanMsgRcvd, ByVal b As CanMsgRcvd) As Boolean
            Return a.CanMsg.ID                                    = b.CanMsg.ID AndAlso
                   a.CanMsg.LEN                                   = b.CanMsg.LEN AndAlso
                   a.CanMsg.MSGTYPE                               = b.CanMsg.MSGTYPE AndAlso
                   Enumerable.SequenceEqual(a.CanMsg.DATA, b.CanMsg.DATA) AndAlso
                   a._timeStamp.PCAN_MsgTimeStamp.millis          = b._timeStamp.PCAN_MsgTimeStamp.millis AndAlso
                   a._timeStamp.PCAN_MsgTimeStamp.millis_overflow = b._timeStamp.PCAN_MsgTimeStamp.millis_overflow AndAlso
                   a._timeStamp.PCAN_MsgTimeStamp.micros          = b._timeStamp.PCAN_MsgTimeStamp.micros AndAlso
                   a._timeStamp.NiCAN_MsgTimeStampOvflw           = b._timeStamp.NiCAN_MsgTimeStampOvflw
        End Operator

        Public Shared Operator <>(ByVal a As CanMsgRcvd, ByVal b As CanMsgRcvd) As Boolean
            Return Not (a Is b)
        End Operator

        Public Overloads Function Equals(ByVal x As CanMsgRcvd) As Boolean
            Return (Me Is x)
        End Function

        Public Overrides Function Equals(ByVal x As Object) As Boolean
            If x Is Nothing Then
                Return False
            End If
            ' Can x be cast as a CanMsgRcvd?
            Dim y = TryCast(x, CanMsgRcvd)
            If y Is Nothing Then
                ' Could not be cast as a CanMsgRcvd.
                Return False
            Else
                Return (Me Is y)
            End If
        End Function

        Public Sub New(ByVal msg As TPCANMsg, ByVal timestamp As CAN_Services.CAN_MsgTimestamp)
            _canMsg = msg
            _timeStamp = timestamp
        End Sub

        Public ReadOnly Property CanMsg() As TPCANMsg
            Get
                Return _canMsg
            End Get
        End Property

        Public ReadOnly Property Timestamp() As CAN_Services.CAN_MsgTimestamp
            Get
                Return _timeStamp
            End Get
        End Property
    End Class

