﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Public Class FormMeasurementConfiguration
        Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
        Dim label7 As System.Windows.Forms.Label
        Dim label5 As System.Windows.Forms.Label
        Dim label3 As System.Windows.Forms.Label
        Dim label4 As System.Windows.Forms.Label
        Dim label8 As System.Windows.Forms.Label
        Dim label6 As System.Windows.Forms.Label
        Dim label18 As System.Windows.Forms.Label
        Dim label25 As System.Windows.Forms.Label
        Dim label26 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormMeasurementConfiguration))
        Me.panel1 = New System.Windows.Forms.Panel()
        Me.ListBoxMeasurements = New System.Windows.Forms.ListBox()
        Me.label1 = New System.Windows.Forms.Label()
        Me.PanelDataFields = New System.Windows.Forms.Panel()
        Me.MeasurementIDLabel = New System.Windows.Forms.Label()
        Me.MeasurementIdTextBox = New System.Windows.Forms.TextBox()
        Me.CheckBoxGeneratePOFile = New System.Windows.Forms.CheckBox()
        Me.CheckBoxGenerateROFiles = New System.Windows.Forms.CheckBox()
        Me.CheckBoxAutoStart = New System.Windows.Forms.CheckBox()
        Me.CheckBoxGenerateRawOutputFiles = New System.Windows.Forms.CheckBox()
        Me.NumericUpDownMagnitudeBias = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDownPhaseBias = New System.Windows.Forms.NumericUpDown()
        Me.CheckBoxUseRegionForStatus = New System.Windows.Forms.CheckBox()
        Me.CheckBoxIncludeRegionInSootLoadCalculation = New System.Windows.Forms.CheckBox()
        Me.CheckBoxNormalizeViaBypass = New System.Windows.Forms.CheckBox()
        Me.label24 = New System.Windows.Forms.Label()
        Me.NumericUpDownSweepCount = New System.Windows.Forms.NumericUpDown()
        Me.label23 = New System.Windows.Forms.Label()
        Me.TextBoxSignalName = New System.Windows.Forms.TextBox()
        Me.label22 = New System.Windows.Forms.Label()
        Me.CheckBoxInverse = New System.Windows.Forms.CheckBox()
        Me.ComboBoxTemperatureSource = New System.Windows.Forms.ComboBox()
        Me.label19 = New System.Windows.Forms.Label()
        Me.NumericUpDownStatisticScale = New System.Windows.Forms.NumericUpDown()
        Me.ComboBoxSignalCalibration = New System.Windows.Forms.ComboBox()
        Me.label1Signal = New System.Windows.Forms.Label()
        Me.ComboBoxRTDCalibration = New System.Windows.Forms.ComboBox()
        Me.label15 = New System.Windows.Forms.Label()
        Me.ComboBoxThermocoupleCalibration = New System.Windows.Forms.ComboBox()
        Me.label14 = New System.Windows.Forms.Label()
        Me.ComboBoxColdJunctionCalibration = New System.Windows.Forms.ComboBox()
        Me.label13 = New System.Windows.Forms.Label()
        Me.ComboBoxBoardTempCalibration = New System.Windows.Forms.ComboBox()
        Me.label12 = New System.Windows.Forms.Label()
        Me.label10 = New System.Windows.Forms.Label()
        Me.label9 = New System.Windows.Forms.Label()
        Me.CheckedListBoxCalibrations = New System.Windows.Forms.CheckedListBox()
        Me.CheckedListBoxRegions = New System.Windows.Forms.CheckedListBox()
        Me.CheckBoxMeasurePhase = New System.Windows.Forms.CheckBox()
        Me.NumericUpDownMinimumSoot = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDownMinimumTemperature = New System.Windows.Forms.NumericUpDown()
        Me.CheckBoxActive = New System.Windows.Forms.CheckBox()
        Me.NumericUpDownMaximumSoot = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDownMaximumTemperature = New System.Windows.Forms.NumericUpDown()
        Me.ComboBoxFinalValueStatistic = New System.Windows.Forms.ComboBox()
        Me.ComboBoxOperatingMode = New System.Windows.Forms.ComboBox()
        Me.label11 = New System.Windows.Forms.Label()
        Me.label2 = New System.Windows.Forms.Label()
        Me.ComboBoxSweep = New System.Windows.Forms.ComboBox()
        Me.TextBoxDescription = New System.Windows.Forms.TextBox()
        Me.ButtonCancel = New System.Windows.Forms.Button()
        Me.ButtonAdd = New System.Windows.Forms.Button()
        Me.ButtonEdit = New System.Windows.Forms.Button()
        Me.ButtonRemove = New System.Windows.Forms.Button()
        Me.ButtonUpdate = New System.Windows.Forms.Button()
        Me.ButtonClose = New System.Windows.Forms.Button()
        Me.panelButons = New System.Windows.Forms.Panel()
        Me.CopyThenEditButton = New System.Windows.Forms.Button()
        Me.ButtonGenerate = New System.Windows.Forms.Button()
        Me.SaveGeneratedFileDialog = New System.Windows.Forms.SaveFileDialog()
        label7 = New System.Windows.Forms.Label()
        label5 = New System.Windows.Forms.Label()
        label3 = New System.Windows.Forms.Label()
        label4 = New System.Windows.Forms.Label()
        label8 = New System.Windows.Forms.Label()
        label6 = New System.Windows.Forms.Label()
        label18 = New System.Windows.Forms.Label()
        label25 = New System.Windows.Forms.Label()
        label26 = New System.Windows.Forms.Label()
        Me.panel1.SuspendLayout
        Me.PanelDataFields.SuspendLayout
        CType(Me.NumericUpDownMagnitudeBias,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.NumericUpDownPhaseBias,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.NumericUpDownSweepCount,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.NumericUpDownStatisticScale,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.NumericUpDownMinimumSoot,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.NumericUpDownMinimumTemperature,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.NumericUpDownMaximumSoot,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.NumericUpDownMaximumTemperature,System.ComponentModel.ISupportInitialize).BeginInit
        Me.panelButons.SuspendLayout
        Me.SuspendLayout
        '
        'label7
        '
        label7.AutoSize = true
        label7.BackColor = System.Drawing.Color.DimGray
        label7.ForeColor = System.Drawing.Color.White
        label7.Location = New System.Drawing.Point(16, 119)
        label7.Name = "label7"
        label7.Size = New System.Drawing.Size(76, 13)
        label7.TabIndex = 42
        label7.Text = "Minimum Soot:"
        '
        'label5
        '
        label5.AutoSize = true
        label5.BackColor = System.Drawing.Color.DimGray
        label5.ForeColor = System.Drawing.Color.White
        label5.Location = New System.Drawing.Point(11, 93)
        label5.Name = "label5"
        label5.Size = New System.Drawing.Size(81, 13)
        label5.TabIndex = 41
        label5.Text = "Minimum Temp:"
        '
        'label3
        '
        label3.AutoSize = true
        label3.BackColor = System.Drawing.Color.DimGray
        label3.ForeColor = System.Drawing.Color.White
        label3.Location = New System.Drawing.Point(6, 67)
        label3.Name = "label3"
        label3.Size = New System.Drawing.Size(86, 13)
        label3.TabIndex = 40
        label3.Text = "Operating Mode:"
        '
        'label4
        '
        label4.AutoSize = true
        label4.BackColor = System.Drawing.Color.DimGray
        label4.ForeColor = System.Drawing.Color.White
        label4.Location = New System.Drawing.Point(209, 68)
        label4.Name = "label4"
        label4.Size = New System.Drawing.Size(102, 13)
        label4.TabIndex = 46
        label4.Text = "Final Value Statistic:"
        '
        'label8
        '
        label8.AutoSize = true
        label8.BackColor = System.Drawing.Color.DimGray
        label8.ForeColor = System.Drawing.Color.White
        label8.Location = New System.Drawing.Point(232, 120)
        label8.Name = "label8"
        label8.Size = New System.Drawing.Size(79, 13)
        label8.TabIndex = 51
        label8.Text = "Maximum Soot:"
        '
        'label6
        '
        label6.AutoSize = true
        label6.BackColor = System.Drawing.Color.DimGray
        label6.ForeColor = System.Drawing.Color.White
        label6.Location = New System.Drawing.Point(227, 94)
        label6.Name = "label6"
        label6.Size = New System.Drawing.Size(84, 13)
        label6.TabIndex = 50
        label6.Text = "Maximum Temp:"
        '
        'label18
        '
        label18.AutoSize = true
        label18.BackColor = System.Drawing.Color.DimGray
        label18.ForeColor = System.Drawing.Color.White
        label18.Location = New System.Drawing.Point(15, 217)
        label18.Name = "label18"
        label18.Size = New System.Drawing.Size(77, 13)
        label18.TabIndex = 71
        label18.Text = "Statistic Scale:"
        label18.Visible = false
        '
        'label25
        '
        label25.AutoSize = true
        label25.BackColor = System.Drawing.Color.DimGray
        label25.ForeColor = System.Drawing.Color.White
        label25.Location = New System.Drawing.Point(29, 191)
        label25.Name = "label25"
        label25.Size = New System.Drawing.Size(63, 13)
        label25.TabIndex = 89
        label25.Text = "Phase Bias:"
        '
        'label26
        '
        label26.AutoSize = true
        label26.BackColor = System.Drawing.Color.DimGray
        label26.ForeColor = System.Drawing.Color.White
        label26.Location = New System.Drawing.Point(228, 194)
        label26.Name = "label26"
        label26.Size = New System.Drawing.Size(83, 13)
        label26.TabIndex = 91
        label26.Text = "Magnitude Bias:"
        '
        'panel1
        '
        Me.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.panel1.Controls.Add(Me.ListBoxMeasurements)
        Me.panel1.Controls.Add(Me.label1)
        Me.panel1.Dock = System.Windows.Forms.DockStyle.Left
        Me.panel1.Location = New System.Drawing.Point(0, 0)
        Me.panel1.Name = "panel1"
        Me.panel1.Size = New System.Drawing.Size(166, 403)
        Me.panel1.TabIndex = 0
        '
        'ListBoxMeasurements
        '
        Me.ListBoxMeasurements.BackColor = System.Drawing.Color.DimGray
        Me.ListBoxMeasurements.Dock = System.Windows.Forms.DockStyle.Top
        Me.ListBoxMeasurements.ForeColor = System.Drawing.Color.White
        Me.ListBoxMeasurements.FormattingEnabled = true
        Me.ListBoxMeasurements.IntegralHeight = false
        Me.ListBoxMeasurements.Items.AddRange(New Object() {"MeasurementOne"})
        Me.ListBoxMeasurements.Location = New System.Drawing.Point(0, 24)
        Me.ListBoxMeasurements.Name = "ListBoxMeasurements"
        Me.ListBoxMeasurements.Size = New System.Drawing.Size(162, 376)
        Me.ListBoxMeasurements.TabIndex = 0
        '
        'label1
        '
        Me.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.label1.Dock = System.Windows.Forms.DockStyle.Top
        Me.label1.Location = New System.Drawing.Point(0, 0)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(162, 24)
        Me.label1.TabIndex = 0
        Me.label1.Text = "Measurements"
        Me.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PanelDataFields
        '
        Me.PanelDataFields.BackColor = System.Drawing.Color.DimGray
        Me.PanelDataFields.Controls.Add(Me.MeasurementIDLabel)
        Me.PanelDataFields.Controls.Add(Me.MeasurementIdTextBox)
        Me.PanelDataFields.Controls.Add(Me.CheckBoxGeneratePOFile)
        Me.PanelDataFields.Controls.Add(Me.CheckBoxGenerateROFiles)
        Me.PanelDataFields.Controls.Add(Me.CheckBoxAutoStart)
        Me.PanelDataFields.Controls.Add(Me.CheckBoxGenerateRawOutputFiles)
        Me.PanelDataFields.Controls.Add(label26)
        Me.PanelDataFields.Controls.Add(Me.NumericUpDownMagnitudeBias)
        Me.PanelDataFields.Controls.Add(label25)
        Me.PanelDataFields.Controls.Add(Me.NumericUpDownPhaseBias)
        Me.PanelDataFields.Controls.Add(Me.CheckBoxUseRegionForStatus)
        Me.PanelDataFields.Controls.Add(Me.CheckBoxIncludeRegionInSootLoadCalculation)
        Me.PanelDataFields.Controls.Add(Me.CheckBoxNormalizeViaBypass)
        Me.PanelDataFields.Controls.Add(Me.label24)
        Me.PanelDataFields.Controls.Add(Me.NumericUpDownSweepCount)
        Me.PanelDataFields.Controls.Add(Me.label23)
        Me.PanelDataFields.Controls.Add(Me.TextBoxSignalName)
        Me.PanelDataFields.Controls.Add(Me.label22)
        Me.PanelDataFields.Controls.Add(Me.CheckBoxInverse)
        Me.PanelDataFields.Controls.Add(Me.ComboBoxTemperatureSource)
        Me.PanelDataFields.Controls.Add(Me.label19)
        Me.PanelDataFields.Controls.Add(label18)
        Me.PanelDataFields.Controls.Add(Me.NumericUpDownStatisticScale)
        Me.PanelDataFields.Controls.Add(Me.ComboBoxSignalCalibration)
        Me.PanelDataFields.Controls.Add(Me.label1Signal)
        Me.PanelDataFields.Controls.Add(Me.ComboBoxRTDCalibration)
        Me.PanelDataFields.Controls.Add(Me.label15)
        Me.PanelDataFields.Controls.Add(Me.ComboBoxThermocoupleCalibration)
        Me.PanelDataFields.Controls.Add(Me.label14)
        Me.PanelDataFields.Controls.Add(Me.ComboBoxColdJunctionCalibration)
        Me.PanelDataFields.Controls.Add(Me.label13)
        Me.PanelDataFields.Controls.Add(Me.ComboBoxBoardTempCalibration)
        Me.PanelDataFields.Controls.Add(Me.label12)
        Me.PanelDataFields.Controls.Add(Me.label10)
        Me.PanelDataFields.Controls.Add(Me.label9)
        Me.PanelDataFields.Controls.Add(Me.CheckedListBoxCalibrations)
        Me.PanelDataFields.Controls.Add(Me.CheckedListBoxRegions)
        Me.PanelDataFields.Controls.Add(Me.CheckBoxMeasurePhase)
        Me.PanelDataFields.Controls.Add(Me.NumericUpDownMinimumSoot)
        Me.PanelDataFields.Controls.Add(Me.NumericUpDownMinimumTemperature)
        Me.PanelDataFields.Controls.Add(Me.CheckBoxActive)
        Me.PanelDataFields.Controls.Add(label8)
        Me.PanelDataFields.Controls.Add(Me.NumericUpDownMaximumSoot)
        Me.PanelDataFields.Controls.Add(label6)
        Me.PanelDataFields.Controls.Add(Me.NumericUpDownMaximumTemperature)
        Me.PanelDataFields.Controls.Add(Me.ComboBoxFinalValueStatistic)
        Me.PanelDataFields.Controls.Add(label4)
        Me.PanelDataFields.Controls.Add(Me.ComboBoxOperatingMode)
        Me.PanelDataFields.Controls.Add(Me.label11)
        Me.PanelDataFields.Controls.Add(label7)
        Me.PanelDataFields.Controls.Add(label5)
        Me.PanelDataFields.Controls.Add(label3)
        Me.PanelDataFields.Controls.Add(Me.label2)
        Me.PanelDataFields.Controls.Add(Me.ComboBoxSweep)
        Me.PanelDataFields.Controls.Add(Me.TextBoxDescription)
        Me.PanelDataFields.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelDataFields.Enabled = false
        Me.PanelDataFields.ForeColor = System.Drawing.Color.White
        Me.PanelDataFields.Location = New System.Drawing.Point(166, 0)
        Me.PanelDataFields.Name = "PanelDataFields"
        Me.PanelDataFields.Size = New System.Drawing.Size(785, 369)
        Me.PanelDataFields.TabIndex = 0
        '
        'MeasurementIDLabel
        '
        Me.MeasurementIDLabel.AutoSize = true
        Me.MeasurementIDLabel.Location = New System.Drawing.Point(397, 14)
        Me.MeasurementIDLabel.Name = "MeasurementIDLabel"
        Me.MeasurementIDLabel.Size = New System.Drawing.Size(21, 13)
        Me.MeasurementIDLabel.TabIndex = 103
        Me.MeasurementIDLabel.Text = "ID:"
        '
        'MeasurementIdTextBox
        '
        Me.MeasurementIdTextBox.BackColor = System.Drawing.Color.White
        Me.MeasurementIdTextBox.Location = New System.Drawing.Point(420, 11)
        Me.MeasurementIdTextBox.Name = "MeasurementIdTextBox"
        Me.MeasurementIdTextBox.ReadOnly = true
        Me.MeasurementIdTextBox.Size = New System.Drawing.Size(50, 20)
        Me.MeasurementIdTextBox.TabIndex = 2
        Me.MeasurementIdTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CheckBoxGeneratePOFile
        '
        Me.CheckBoxGeneratePOFile.AutoSize = true
        Me.CheckBoxGeneratePOFile.BackColor = System.Drawing.Color.DimGray
        Me.CheckBoxGeneratePOFile.ForeColor = System.Drawing.Color.White
        Me.CheckBoxGeneratePOFile.Location = New System.Drawing.Point(408, 294)
        Me.CheckBoxGeneratePOFile.Name = "CheckBoxGeneratePOFile"
        Me.CheckBoxGeneratePOFile.Size = New System.Drawing.Size(107, 17)
        Me.CheckBoxGeneratePOFile.TabIndex = 95
        Me.CheckBoxGeneratePOFile.Text = "Generate PO File"
        Me.CheckBoxGeneratePOFile.UseVisualStyleBackColor = false
        '
        'CheckBoxGenerateROFiles
        '
        Me.CheckBoxGenerateROFiles.AutoSize = true
        Me.CheckBoxGenerateROFiles.BackColor = System.Drawing.Color.DimGray
        Me.CheckBoxGenerateROFiles.ForeColor = System.Drawing.Color.White
        Me.CheckBoxGenerateROFiles.Location = New System.Drawing.Point(408, 272)
        Me.CheckBoxGenerateROFiles.Name = "CheckBoxGenerateROFiles"
        Me.CheckBoxGenerateROFiles.Size = New System.Drawing.Size(113, 17)
        Me.CheckBoxGenerateROFiles.TabIndex = 94
        Me.CheckBoxGenerateROFiles.Text = "Generate RO Files"
        Me.CheckBoxGenerateROFiles.UseVisualStyleBackColor = false
        '
        'CheckBoxAutoStart
        '
        Me.CheckBoxAutoStart.AutoSize = true
        Me.CheckBoxAutoStart.BackColor = System.Drawing.Color.DimGray
        Me.CheckBoxAutoStart.ForeColor = System.Drawing.Color.White
        Me.CheckBoxAutoStart.Location = New System.Drawing.Point(408, 228)
        Me.CheckBoxAutoStart.Name = "CheckBoxAutoStart"
        Me.CheckBoxAutoStart.Size = New System.Drawing.Size(73, 17)
        Me.CheckBoxAutoStart.TabIndex = 93
        Me.CheckBoxAutoStart.Text = "Auto Start"
        Me.CheckBoxAutoStart.UseVisualStyleBackColor = false
        '
        'CheckBoxGenerateRawOutputFiles
        '
        Me.CheckBoxGenerateRawOutputFiles.AutoSize = true
        Me.CheckBoxGenerateRawOutputFiles.BackColor = System.Drawing.Color.DimGray
        Me.CheckBoxGenerateRawOutputFiles.ForeColor = System.Drawing.Color.White
        Me.CheckBoxGenerateRawOutputFiles.Location = New System.Drawing.Point(408, 250)
        Me.CheckBoxGenerateRawOutputFiles.Name = "CheckBoxGenerateRawOutputFiles"
        Me.CheckBoxGenerateRawOutputFiles.Size = New System.Drawing.Size(154, 17)
        Me.CheckBoxGenerateRawOutputFiles.TabIndex = 21
        Me.CheckBoxGenerateRawOutputFiles.Text = "Generate Raw Output Files"
        Me.CheckBoxGenerateRawOutputFiles.UseVisualStyleBackColor = false
        '
        'NumericUpDownMagnitudeBias
        '
        Me.NumericUpDownMagnitudeBias.Location = New System.Drawing.Point(314, 189)
        Me.NumericUpDownMagnitudeBias.Minimum = New Decimal(New Integer() {100, 0, 0, -2147483648})
        Me.NumericUpDownMagnitudeBias.Name = "NumericUpDownMagnitudeBias"
        Me.NumericUpDownMagnitudeBias.Size = New System.Drawing.Size(69, 20)
        Me.NumericUpDownMagnitudeBias.TabIndex = 14
        Me.NumericUpDownMagnitudeBias.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.NumericUpDownMagnitudeBias.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left
        '
        'NumericUpDownPhaseBias
        '
        Me.NumericUpDownPhaseBias.Location = New System.Drawing.Point(98, 189)
        Me.NumericUpDownPhaseBias.Maximum = New Decimal(New Integer() {360, 0, 0, 0})
        Me.NumericUpDownPhaseBias.Minimum = New Decimal(New Integer() {100, 0, 0, -2147483648})
        Me.NumericUpDownPhaseBias.Name = "NumericUpDownPhaseBias"
        Me.NumericUpDownPhaseBias.Size = New System.Drawing.Size(69, 20)
        Me.NumericUpDownPhaseBias.TabIndex = 13
        Me.NumericUpDownPhaseBias.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.NumericUpDownPhaseBias.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left
        '
        'CheckBoxUseRegionForStatus
        '
        Me.CheckBoxUseRegionForStatus.AutoSize = true
        Me.CheckBoxUseRegionForStatus.BackColor = System.Drawing.Color.DimGray
        Me.CheckBoxUseRegionForStatus.Enabled = false
        Me.CheckBoxUseRegionForStatus.ForeColor = System.Drawing.Color.White
        Me.CheckBoxUseRegionForStatus.Location = New System.Drawing.Point(408, 344)
        Me.CheckBoxUseRegionForStatus.Name = "CheckBoxUseRegionForStatus"
        Me.CheckBoxUseRegionForStatus.Size = New System.Drawing.Size(133, 17)
        Me.CheckBoxUseRegionForStatus.TabIndex = 20
        Me.CheckBoxUseRegionForStatus.Text = "Use Region For Status"
        Me.CheckBoxUseRegionForStatus.UseVisualStyleBackColor = false
        '
        'CheckBoxIncludeRegionInSootLoadCalculation
        '
        Me.CheckBoxIncludeRegionInSootLoadCalculation.AutoSize = true
        Me.CheckBoxIncludeRegionInSootLoadCalculation.BackColor = System.Drawing.Color.DimGray
        Me.CheckBoxIncludeRegionInSootLoadCalculation.Enabled = false
        Me.CheckBoxIncludeRegionInSootLoadCalculation.ForeColor = System.Drawing.Color.White
        Me.CheckBoxIncludeRegionInSootLoadCalculation.Location = New System.Drawing.Point(408, 322)
        Me.CheckBoxIncludeRegionInSootLoadCalculation.Name = "CheckBoxIncludeRegionInSootLoadCalculation"
        Me.CheckBoxIncludeRegionInSootLoadCalculation.Size = New System.Drawing.Size(217, 17)
        Me.CheckBoxIncludeRegionInSootLoadCalculation.TabIndex = 19
        Me.CheckBoxIncludeRegionInSootLoadCalculation.Text = "Include Region In Soot Load Calculation"
        Me.CheckBoxIncludeRegionInSootLoadCalculation.UseVisualStyleBackColor = false
        '
        'CheckBoxNormalizeViaBypass
        '
        Me.CheckBoxNormalizeViaBypass.AutoSize = true
        Me.CheckBoxNormalizeViaBypass.BackColor = System.Drawing.Color.DimGray
        Me.CheckBoxNormalizeViaBypass.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxNormalizeViaBypass.ForeColor = System.Drawing.Color.White
        Me.CheckBoxNormalizeViaBypass.Location = New System.Drawing.Point(37, 143)
        Me.CheckBoxNormalizeViaBypass.Name = "CheckBoxNormalizeViaBypass"
        Me.CheckBoxNormalizeViaBypass.Size = New System.Drawing.Size(130, 17)
        Me.CheckBoxNormalizeViaBypass.TabIndex = 8
        Me.CheckBoxNormalizeViaBypass.Text = "Normalize Via Bypass:"
        Me.CheckBoxNormalizeViaBypass.UseVisualStyleBackColor = false
        '
        'label24
        '
        Me.label24.AutoSize = true
        Me.label24.BackColor = System.Drawing.Color.DimGray
        Me.label24.ForeColor = System.Drawing.Color.White
        Me.label24.Location = New System.Drawing.Point(233, 162)
        Me.label24.Name = "label24"
        Me.label24.Size = New System.Drawing.Size(78, 26)
        Me.label24.TabIndex = 84
        Me.label24.Text = "A/D Readings "&Global.Microsoft.VisualBasic.ChrW(13)&Global.Microsoft.VisualBasic.ChrW(10)&"for Average:"
        Me.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'NumericUpDownSweepCount
        '
        Me.NumericUpDownSweepCount.Location = New System.Drawing.Point(314, 165)
        Me.NumericUpDownSweepCount.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.NumericUpDownSweepCount.Name = "NumericUpDownSweepCount"
        Me.NumericUpDownSweepCount.Size = New System.Drawing.Size(69, 20)
        Me.NumericUpDownSweepCount.TabIndex = 12
        Me.NumericUpDownSweepCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.NumericUpDownSweepCount.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left
        Me.NumericUpDownSweepCount.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'label23
        '
        Me.label23.AutoSize = true
        Me.label23.BackColor = System.Drawing.Color.DimGray
        Me.label23.ForeColor = System.Drawing.Color.White
        Me.label23.Location = New System.Drawing.Point(448, 177)
        Me.label23.Name = "label23"
        Me.label23.Size = New System.Drawing.Size(35, 13)
        Me.label23.TabIndex = 82
        Me.label23.Text = "Name"
        '
        'TextBoxSignalName
        '
        Me.TextBoxSignalName.Location = New System.Drawing.Point(447, 194)
        Me.TextBoxSignalName.Name = "TextBoxSignalName"
        Me.TextBoxSignalName.Size = New System.Drawing.Size(81, 20)
        Me.TextBoxSignalName.TabIndex = 26
        '
        'label22
        '
        Me.label22.AutoSize = true
        Me.label22.BackColor = System.Drawing.Color.DimGray
        Me.label22.ForeColor = System.Drawing.Color.White
        Me.label22.Location = New System.Drawing.Point(532, 177)
        Me.label22.Name = "label22"
        Me.label22.Size = New System.Drawing.Size(56, 13)
        Me.label22.TabIndex = 77
        Me.label22.Text = "Calibration"
        '
        'CheckBoxInverse
        '
        Me.CheckBoxInverse.AutoSize = true
        Me.CheckBoxInverse.BackColor = System.Drawing.Color.DimGray
        Me.CheckBoxInverse.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxInverse.ForeColor = System.Drawing.Color.White
        Me.CheckBoxInverse.Location = New System.Drawing.Point(292, 143)
        Me.CheckBoxInverse.Name = "CheckBoxInverse"
        Me.CheckBoxInverse.Size = New System.Drawing.Size(91, 17)
        Me.CheckBoxInverse.TabIndex = 10
        Me.CheckBoxInverse.Text = "Inverse (1/X):"
        Me.CheckBoxInverse.UseVisualStyleBackColor = false
        '
        'ComboBoxTemperatureSource
        '
        Me.ComboBoxTemperatureSource.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxTemperatureSource.FormattingEnabled = true
        Me.ComboBoxTemperatureSource.Items.AddRange(New Object() {"Thermo", "RTD"})
        Me.ComboBoxTemperatureSource.Location = New System.Drawing.Point(314, 214)
        Me.ComboBoxTemperatureSource.Name = "ComboBoxTemperatureSource"
        Me.ComboBoxTemperatureSource.Size = New System.Drawing.Size(69, 21)
        Me.ComboBoxTemperatureSource.TabIndex = 16
        '
        'label19
        '
        Me.label19.AutoSize = true
        Me.label19.BackColor = System.Drawing.Color.DimGray
        Me.label19.ForeColor = System.Drawing.Color.White
        Me.label19.Location = New System.Drawing.Point(204, 218)
        Me.label19.Name = "label19"
        Me.label19.Size = New System.Drawing.Size(107, 13)
        Me.label19.TabIndex = 72
        Me.label19.Text = "Temperature Source:"
        '
        'NumericUpDownStatisticScale
        '
        Me.NumericUpDownStatisticScale.Increment = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.NumericUpDownStatisticScale.Location = New System.Drawing.Point(98, 215)
        Me.NumericUpDownStatisticScale.Maximum = New Decimal(New Integer() {1000000000, 0, 0, 0})
        Me.NumericUpDownStatisticScale.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.NumericUpDownStatisticScale.Name = "NumericUpDownStatisticScale"
        Me.NumericUpDownStatisticScale.Size = New System.Drawing.Size(69, 20)
        Me.NumericUpDownStatisticScale.TabIndex = 15
        Me.NumericUpDownStatisticScale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.NumericUpDownStatisticScale.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left
        Me.NumericUpDownStatisticScale.Value = New Decimal(New Integer() {1, 0, 0, 0})
        Me.NumericUpDownStatisticScale.Visible = false
        '
        'ComboBoxSignalCalibration
        '
        Me.ComboBoxSignalCalibration.FormattingEnabled = true
        Me.ComboBoxSignalCalibration.Location = New System.Drawing.Point(534, 194)
        Me.ComboBoxSignalCalibration.Name = "ComboBoxSignalCalibration"
        Me.ComboBoxSignalCalibration.Size = New System.Drawing.Size(236, 21)
        Me.ComboBoxSignalCalibration.TabIndex = 27
        '
        'label1Signal
        '
        Me.label1Signal.AutoSize = true
        Me.label1Signal.BackColor = System.Drawing.Color.DimGray
        Me.label1Signal.ForeColor = System.Drawing.Color.White
        Me.label1Signal.Location = New System.Drawing.Point(404, 197)
        Me.label1Signal.Name = "label1Signal"
        Me.label1Signal.Size = New System.Drawing.Size(39, 13)
        Me.label1Signal.TabIndex = 67
        Me.label1Signal.Text = "Signal:"
        '
        'ComboBoxRTDCalibration
        '
        Me.ComboBoxRTDCalibration.FormattingEnabled = true
        Me.ComboBoxRTDCalibration.Location = New System.Drawing.Point(534, 135)
        Me.ComboBoxRTDCalibration.Name = "ComboBoxRTDCalibration"
        Me.ComboBoxRTDCalibration.Size = New System.Drawing.Size(236, 21)
        Me.ComboBoxRTDCalibration.TabIndex = 25
        '
        'label15
        '
        Me.label15.AutoSize = true
        Me.label15.BackColor = System.Drawing.Color.DimGray
        Me.label15.ForeColor = System.Drawing.Color.White
        Me.label15.Location = New System.Drawing.Point(447, 139)
        Me.label15.Name = "label15"
        Me.label15.Size = New System.Drawing.Size(85, 13)
        Me.label15.TabIndex = 65
        Me.label15.Text = "RTD Calibration:"
        '
        'ComboBoxThermocoupleCalibration
        '
        Me.ComboBoxThermocoupleCalibration.FormattingEnabled = true
        Me.ComboBoxThermocoupleCalibration.Location = New System.Drawing.Point(534, 111)
        Me.ComboBoxThermocoupleCalibration.Name = "ComboBoxThermocoupleCalibration"
        Me.ComboBoxThermocoupleCalibration.Size = New System.Drawing.Size(236, 21)
        Me.ComboBoxThermocoupleCalibration.TabIndex = 24
        '
        'label14
        '
        Me.label14.AutoSize = true
        Me.label14.BackColor = System.Drawing.Color.DimGray
        Me.label14.ForeColor = System.Drawing.Color.White
        Me.label14.Location = New System.Drawing.Point(402, 115)
        Me.label14.Name = "label14"
        Me.label14.Size = New System.Drawing.Size(130, 13)
        Me.label14.TabIndex = 63
        Me.label14.Text = "Thermocouple Calibration:"
        '
        'ComboBoxColdJunctionCalibration
        '
        Me.ComboBoxColdJunctionCalibration.FormattingEnabled = true
        Me.ComboBoxColdJunctionCalibration.Location = New System.Drawing.Point(534, 87)
        Me.ComboBoxColdJunctionCalibration.Name = "ComboBoxColdJunctionCalibration"
        Me.ComboBoxColdJunctionCalibration.Size = New System.Drawing.Size(236, 21)
        Me.ComboBoxColdJunctionCalibration.TabIndex = 23
        '
        'label13
        '
        Me.label13.AutoSize = true
        Me.label13.BackColor = System.Drawing.Color.DimGray
        Me.label13.ForeColor = System.Drawing.Color.White
        Me.label13.Location = New System.Drawing.Point(406, 91)
        Me.label13.Name = "label13"
        Me.label13.Size = New System.Drawing.Size(126, 13)
        Me.label13.TabIndex = 61
        Me.label13.Text = "Cold Junction Calibration:"
        '
        'ComboBoxBoardTempCalibration
        '
        Me.ComboBoxBoardTempCalibration.FormattingEnabled = true
        Me.ComboBoxBoardTempCalibration.Location = New System.Drawing.Point(534, 63)
        Me.ComboBoxBoardTempCalibration.Name = "ComboBoxBoardTempCalibration"
        Me.ComboBoxBoardTempCalibration.Size = New System.Drawing.Size(236, 21)
        Me.ComboBoxBoardTempCalibration.TabIndex = 22
        '
        'label12
        '
        Me.label12.AutoSize = true
        Me.label12.BackColor = System.Drawing.Color.DimGray
        Me.label12.ForeColor = System.Drawing.Color.White
        Me.label12.Location = New System.Drawing.Point(412, 67)
        Me.label12.Name = "label12"
        Me.label12.Size = New System.Drawing.Size(120, 13)
        Me.label12.TabIndex = 59
        Me.label12.Text = "Board Temp Calibration:"
        '
        'label10
        '
        Me.label10.AutoSize = true
        Me.label10.BackColor = System.Drawing.Color.DimGray
        Me.label10.ForeColor = System.Drawing.Color.White
        Me.label10.Location = New System.Drawing.Point(206, 248)
        Me.label10.Name = "label10"
        Me.label10.Size = New System.Drawing.Size(64, 13)
        Me.label10.TabIndex = 58
        Me.label10.Text = "Calibrations:"
        '
        'label9
        '
        Me.label9.AutoSize = true
        Me.label9.BackColor = System.Drawing.Color.DimGray
        Me.label9.ForeColor = System.Drawing.Color.White
        Me.label9.Location = New System.Drawing.Point(14, 248)
        Me.label9.Name = "label9"
        Me.label9.Size = New System.Drawing.Size(49, 13)
        Me.label9.TabIndex = 57
        Me.label9.Text = "Regions:"
        '
        'CheckedListBoxCalibrations
        '
        Me.CheckedListBoxCalibrations.Enabled = false
        Me.CheckedListBoxCalibrations.FormattingEnabled = true
        Me.CheckedListBoxCalibrations.Location = New System.Drawing.Point(204, 264)
        Me.CheckedListBoxCalibrations.Name = "CheckedListBoxCalibrations"
        Me.CheckedListBoxCalibrations.Size = New System.Drawing.Size(179, 94)
        Me.CheckedListBoxCalibrations.TabIndex = 18
        '
        'CheckedListBoxRegions
        '
        Me.CheckedListBoxRegions.Enabled = false
        Me.CheckedListBoxRegions.FormattingEnabled = true
        Me.CheckedListBoxRegions.Location = New System.Drawing.Point(14, 264)
        Me.CheckedListBoxRegions.Name = "CheckedListBoxRegions"
        Me.CheckedListBoxRegions.Size = New System.Drawing.Size(179, 94)
        Me.CheckedListBoxRegions.TabIndex = 17
        '
        'CheckBoxMeasurePhase
        '
        Me.CheckBoxMeasurePhase.AutoSize = true
        Me.CheckBoxMeasurePhase.BackColor = System.Drawing.Color.DimGray
        Me.CheckBoxMeasurePhase.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxMeasurePhase.ForeColor = System.Drawing.Color.White
        Me.CheckBoxMeasurePhase.Location = New System.Drawing.Point(64, 166)
        Me.CheckBoxMeasurePhase.Name = "CheckBoxMeasurePhase"
        Me.CheckBoxMeasurePhase.Size = New System.Drawing.Size(103, 17)
        Me.CheckBoxMeasurePhase.TabIndex = 11
        Me.CheckBoxMeasurePhase.Text = "Measure Phase:"
        Me.CheckBoxMeasurePhase.UseVisualStyleBackColor = false
        '
        'NumericUpDownMinimumSoot
        '
        Me.NumericUpDownMinimumSoot.DecimalPlaces = 3
        Me.NumericUpDownMinimumSoot.Increment = New Decimal(New Integer() {1, 0, 0, 65536})
        Me.NumericUpDownMinimumSoot.Location = New System.Drawing.Point(98, 117)
        Me.NumericUpDownMinimumSoot.Name = "NumericUpDownMinimumSoot"
        Me.NumericUpDownMinimumSoot.Size = New System.Drawing.Size(69, 20)
        Me.NumericUpDownMinimumSoot.TabIndex = 6
        Me.NumericUpDownMinimumSoot.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.NumericUpDownMinimumSoot.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left
        '
        'NumericUpDownMinimumTemperature
        '
        Me.NumericUpDownMinimumTemperature.Location = New System.Drawing.Point(98, 91)
        Me.NumericUpDownMinimumTemperature.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.NumericUpDownMinimumTemperature.Minimum = New Decimal(New Integer() {40, 0, 0, -2147483648})
        Me.NumericUpDownMinimumTemperature.Name = "NumericUpDownMinimumTemperature"
        Me.NumericUpDownMinimumTemperature.Size = New System.Drawing.Size(69, 20)
        Me.NumericUpDownMinimumTemperature.TabIndex = 4
        Me.NumericUpDownMinimumTemperature.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.NumericUpDownMinimumTemperature.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left
        '
        'CheckBoxActive
        '
        Me.CheckBoxActive.AutoSize = true
        Me.CheckBoxActive.BackColor = System.Drawing.Color.DimGray
        Me.CheckBoxActive.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBoxActive.ForeColor = System.Drawing.Color.White
        Me.CheckBoxActive.Location = New System.Drawing.Point(200, 143)
        Me.CheckBoxActive.Name = "CheckBoxActive"
        Me.CheckBoxActive.Size = New System.Drawing.Size(59, 17)
        Me.CheckBoxActive.TabIndex = 9
        Me.CheckBoxActive.Text = "Active:"
        Me.CheckBoxActive.UseVisualStyleBackColor = false
        '
        'NumericUpDownMaximumSoot
        '
        Me.NumericUpDownMaximumSoot.DecimalPlaces = 3
        Me.NumericUpDownMaximumSoot.Increment = New Decimal(New Integer() {1, 0, 0, 65536})
        Me.NumericUpDownMaximumSoot.Location = New System.Drawing.Point(314, 117)
        Me.NumericUpDownMaximumSoot.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.NumericUpDownMaximumSoot.Name = "NumericUpDownMaximumSoot"
        Me.NumericUpDownMaximumSoot.Size = New System.Drawing.Size(69, 20)
        Me.NumericUpDownMaximumSoot.TabIndex = 7
        Me.NumericUpDownMaximumSoot.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.NumericUpDownMaximumSoot.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left
        '
        'NumericUpDownMaximumTemperature
        '
        Me.NumericUpDownMaximumTemperature.Location = New System.Drawing.Point(314, 91)
        Me.NumericUpDownMaximumTemperature.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.NumericUpDownMaximumTemperature.Name = "NumericUpDownMaximumTemperature"
        Me.NumericUpDownMaximumTemperature.Size = New System.Drawing.Size(69, 20)
        Me.NumericUpDownMaximumTemperature.TabIndex = 5
        Me.NumericUpDownMaximumTemperature.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.NumericUpDownMaximumTemperature.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left
        '
        'ComboBoxFinalValueStatistic
        '
        Me.ComboBoxFinalValueStatistic.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxFinalValueStatistic.FormattingEnabled = true
        Me.ComboBoxFinalValueStatistic.Items.AddRange(New Object() {"Area", "Max", "Min", "Q", "FMax", "FMin", "AveMag", "AvePha"})
        Me.ComboBoxFinalValueStatistic.Location = New System.Drawing.Point(314, 64)
        Me.ComboBoxFinalValueStatistic.Name = "ComboBoxFinalValueStatistic"
        Me.ComboBoxFinalValueStatistic.Size = New System.Drawing.Size(69, 21)
        Me.ComboBoxFinalValueStatistic.TabIndex = 3
        '
        'ComboBoxOperatingMode
        '
        Me.ComboBoxOperatingMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxOperatingMode.FormattingEnabled = true
        Me.ComboBoxOperatingMode.Items.AddRange(New Object() {"S12", "S21", "S11", "S22", "S12 Bypass", "S21 Bypass"})
        Me.ComboBoxOperatingMode.Location = New System.Drawing.Point(98, 64)
        Me.ComboBoxOperatingMode.Name = "ComboBoxOperatingMode"
        Me.ComboBoxOperatingMode.Size = New System.Drawing.Size(69, 21)
        Me.ComboBoxOperatingMode.TabIndex = 2
        '
        'label11
        '
        Me.label11.AutoSize = true
        Me.label11.BackColor = System.Drawing.Color.DimGray
        Me.label11.ForeColor = System.Drawing.Color.White
        Me.label11.Location = New System.Drawing.Point(49, 40)
        Me.label11.Name = "label11"
        Me.label11.Size = New System.Drawing.Size(43, 13)
        Me.label11.TabIndex = 43
        Me.label11.Text = "Sweep:"
        '
        'label2
        '
        Me.label2.AutoSize = true
        Me.label2.BackColor = System.Drawing.Color.DimGray
        Me.label2.ForeColor = System.Drawing.Color.White
        Me.label2.Location = New System.Drawing.Point(29, 14)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(63, 13)
        Me.label2.TabIndex = 39
        Me.label2.Text = "Description:"
        '
        'ComboBoxSweep
        '
        Me.ComboBoxSweep.FormattingEnabled = true
        Me.ComboBoxSweep.Location = New System.Drawing.Point(98, 37)
        Me.ComboBoxSweep.Name = "ComboBoxSweep"
        Me.ComboBoxSweep.Size = New System.Drawing.Size(285, 21)
        Me.ComboBoxSweep.TabIndex = 1
        '
        'TextBoxDescription
        '
        Me.TextBoxDescription.Location = New System.Drawing.Point(98, 11)
        Me.TextBoxDescription.Name = "TextBoxDescription"
        Me.TextBoxDescription.Size = New System.Drawing.Size(285, 20)
        Me.TextBoxDescription.TabIndex = 0
        '
        'ButtonCancel
        '
        Me.ButtonCancel.BackColor = System.Drawing.Color.Cyan
        Me.ButtonCancel.Enabled = false
        Me.ButtonCancel.ForeColor = System.Drawing.Color.Black
        Me.ButtonCancel.Location = New System.Drawing.Point(394, 6)
        Me.ButtonCancel.Name = "ButtonCancel"
        Me.ButtonCancel.Size = New System.Drawing.Size(75, 23)
        Me.ButtonCancel.TabIndex = 3
        Me.ButtonCancel.Text = "Cancel"
        Me.ButtonCancel.UseVisualStyleBackColor = false
        '
        'ButtonAdd
        '
        Me.ButtonAdd.BackColor = System.Drawing.Color.Cyan
        Me.ButtonAdd.ForeColor = System.Drawing.Color.Black
        Me.ButtonAdd.Location = New System.Drawing.Point(70, 6)
        Me.ButtonAdd.Name = "ButtonAdd"
        Me.ButtonAdd.Size = New System.Drawing.Size(75, 23)
        Me.ButtonAdd.TabIndex = 0
        Me.ButtonAdd.Text = "Add"
        Me.ButtonAdd.UseVisualStyleBackColor = false
        '
        'ButtonEdit
        '
        Me.ButtonEdit.BackColor = System.Drawing.Color.Cyan
        Me.ButtonEdit.Enabled = false
        Me.ButtonEdit.ForeColor = System.Drawing.Color.Black
        Me.ButtonEdit.Location = New System.Drawing.Point(232, 6)
        Me.ButtonEdit.Name = "ButtonEdit"
        Me.ButtonEdit.Size = New System.Drawing.Size(75, 23)
        Me.ButtonEdit.TabIndex = 1
        Me.ButtonEdit.Text = "Edit"
        Me.ButtonEdit.UseVisualStyleBackColor = false
        '
        'ButtonRemove
        '
        Me.ButtonRemove.BackColor = System.Drawing.Color.Cyan
        Me.ButtonRemove.Enabled = false
        Me.ButtonRemove.ForeColor = System.Drawing.Color.Black
        Me.ButtonRemove.Location = New System.Drawing.Point(475, 6)
        Me.ButtonRemove.Name = "ButtonRemove"
        Me.ButtonRemove.Size = New System.Drawing.Size(75, 23)
        Me.ButtonRemove.TabIndex = 4
        Me.ButtonRemove.Text = "Remove"
        Me.ButtonRemove.UseVisualStyleBackColor = false
        '
        'ButtonUpdate
        '
        Me.ButtonUpdate.BackColor = System.Drawing.Color.Cyan
        Me.ButtonUpdate.Enabled = false
        Me.ButtonUpdate.ForeColor = System.Drawing.Color.Black
        Me.ButtonUpdate.Location = New System.Drawing.Point(313, 6)
        Me.ButtonUpdate.Name = "ButtonUpdate"
        Me.ButtonUpdate.Size = New System.Drawing.Size(75, 23)
        Me.ButtonUpdate.TabIndex = 2
        Me.ButtonUpdate.Text = "Update"
        Me.ButtonUpdate.UseVisualStyleBackColor = false
        '
        'ButtonClose
        '
        Me.ButtonClose.BackColor = System.Drawing.Color.Gold
        Me.ButtonClose.ForeColor = System.Drawing.Color.Black
        Me.ButtonClose.Location = New System.Drawing.Point(637, 6)
        Me.ButtonClose.Name = "ButtonClose"
        Me.ButtonClose.Size = New System.Drawing.Size(75, 23)
        Me.ButtonClose.TabIndex = 5
        Me.ButtonClose.Text = "Close"
        Me.ButtonClose.UseVisualStyleBackColor = false
        '
        'panelButons
        '
        Me.panelButons.BackColor = System.Drawing.Color.DimGray
        Me.panelButons.Controls.Add(Me.CopyThenEditButton)
        Me.panelButons.Controls.Add(Me.ButtonGenerate)
        Me.panelButons.Controls.Add(Me.ButtonAdd)
        Me.panelButons.Controls.Add(Me.ButtonCancel)
        Me.panelButons.Controls.Add(Me.ButtonClose)
        Me.panelButons.Controls.Add(Me.ButtonUpdate)
        Me.panelButons.Controls.Add(Me.ButtonRemove)
        Me.panelButons.Controls.Add(Me.ButtonEdit)
        Me.panelButons.ForeColor = System.Drawing.Color.White
        Me.panelButons.Location = New System.Drawing.Point(166, 368)
        Me.panelButons.Name = "panelButons"
        Me.panelButons.Size = New System.Drawing.Size(785, 34)
        Me.panelButons.TabIndex = 1
        '
        'CopyThenEditButton
        '
        Me.CopyThenEditButton.BackColor = System.Drawing.Color.Cyan
        Me.CopyThenEditButton.Enabled = false
        Me.CopyThenEditButton.ForeColor = System.Drawing.Color.Black
        Me.CopyThenEditButton.Location = New System.Drawing.Point(151, 6)
        Me.CopyThenEditButton.Name = "CopyThenEditButton"
        Me.CopyThenEditButton.Size = New System.Drawing.Size(75, 23)
        Me.CopyThenEditButton.TabIndex = 7
        Me.CopyThenEditButton.Text = "Copy/Edit"
        Me.CopyThenEditButton.UseVisualStyleBackColor = false
        '
        'ButtonGenerate
        '
        Me.ButtonGenerate.BackColor = System.Drawing.Color.Cyan
        Me.ButtonGenerate.Enabled = false
        Me.ButtonGenerate.ForeColor = System.Drawing.Color.Black
        Me.ButtonGenerate.Location = New System.Drawing.Point(556, 6)
        Me.ButtonGenerate.Name = "ButtonGenerate"
        Me.ButtonGenerate.Size = New System.Drawing.Size(75, 23)
        Me.ButtonGenerate.TabIndex = 6
        Me.ButtonGenerate.Text = "Generate"
        Me.ButtonGenerate.UseVisualStyleBackColor = false
        '
        'SaveGeneratedFileDialog
        '
        Me.SaveGeneratedFileDialog.DefaultExt = "hex"
        Me.SaveGeneratedFileDialog.Filter = "hex|*.hex|All Files|*.*"
        Me.SaveGeneratedFileDialog.Title = "Generate Configuration File"
        '
        'FormMeasurementConfiguration
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(951, 403)
        Me.Controls.Add(Me.panelButons)
        Me.Controls.Add(Me.PanelDataFields)
        Me.Controls.Add(Me.panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"),System.Drawing.Icon)
        Me.Name = "FormMeasurementConfiguration"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Measurement Configuration"
        Me.panel1.ResumeLayout(false)
        Me.PanelDataFields.ResumeLayout(false)
        Me.PanelDataFields.PerformLayout
        CType(Me.NumericUpDownMagnitudeBias,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.NumericUpDownPhaseBias,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.NumericUpDownSweepCount,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.NumericUpDownStatisticScale,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.NumericUpDownMinimumSoot,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.NumericUpDownMinimumTemperature,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.NumericUpDownMaximumSoot,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.NumericUpDownMaximumTemperature,System.ComponentModel.ISupportInitialize).EndInit
        Me.panelButons.ResumeLayout(false)
        Me.ResumeLayout(false)

End Sub

        #End Region
        Private panel1 As System.Windows.Forms.Panel
        Private WithEvents ListBoxMeasurements As System.Windows.Forms.ListBox
        Private label1 As System.Windows.Forms.Label
        Private PanelDataFields As System.Windows.Forms.Panel
        Private WithEvents TextBoxDescription As System.Windows.Forms.TextBox
        Private WithEvents ButtonCancel As System.Windows.Forms.Button
        Private WithEvents ButtonAdd As System.Windows.Forms.Button
        Private WithEvents ButtonEdit As System.Windows.Forms.Button
        Private WithEvents ButtonRemove As System.Windows.Forms.Button
        Private WithEvents ButtonUpdate As System.Windows.Forms.Button
        Private WithEvents ButtonClose As System.Windows.Forms.Button
        Private WithEvents ComboBoxSweep As System.Windows.Forms.ComboBox
        Private label11 As System.Windows.Forms.Label
        Private label2 As System.Windows.Forms.Label
        Private WithEvents ComboBoxFinalValueStatistic As System.Windows.Forms.ComboBox
        Private WithEvents ComboBoxOperatingMode As System.Windows.Forms.ComboBox
        Private WithEvents CheckBoxActive As System.Windows.Forms.CheckBox
        Private WithEvents NumericUpDownMaximumSoot As System.Windows.Forms.NumericUpDown
        Private WithEvents NumericUpDownMaximumTemperature As System.Windows.Forms.NumericUpDown
        Private WithEvents CheckBoxMeasurePhase As System.Windows.Forms.CheckBox
        Private WithEvents NumericUpDownMinimumSoot As System.Windows.Forms.NumericUpDown
        Private WithEvents NumericUpDownMinimumTemperature As System.Windows.Forms.NumericUpDown
        Private label10 As System.Windows.Forms.Label
        Private label9 As System.Windows.Forms.Label
        Private WithEvents CheckedListBoxCalibrations As System.Windows.Forms.CheckedListBox
        Private WithEvents CheckedListBoxRegions As System.Windows.Forms.CheckedListBox
        Private WithEvents ComboBoxBoardTempCalibration As System.Windows.Forms.ComboBox
        Private label12 As System.Windows.Forms.Label
        Private WithEvents ComboBoxColdJunctionCalibration As System.Windows.Forms.ComboBox
        Private label13 As System.Windows.Forms.Label
        Private WithEvents ComboBoxSignalCalibration As System.Windows.Forms.ComboBox
        Private label1Signal As System.Windows.Forms.Label
        Private WithEvents ComboBoxRTDCalibration As System.Windows.Forms.ComboBox
        Private label15 As System.Windows.Forms.Label
        Private WithEvents ComboBoxThermocoupleCalibration As System.Windows.Forms.ComboBox
        Private label14 As System.Windows.Forms.Label
        Private WithEvents NumericUpDownStatisticScale As System.Windows.Forms.NumericUpDown
        Private WithEvents ComboBoxTemperatureSource As System.Windows.Forms.ComboBox
        Private label19 As System.Windows.Forms.Label
        Private WithEvents CheckBoxInverse As System.Windows.Forms.CheckBox
        Private label23 As System.Windows.Forms.Label
        Private WithEvents TextBoxSignalName As System.Windows.Forms.TextBox
        Private label22 As System.Windows.Forms.Label
        Private label24 As System.Windows.Forms.Label
        Private WithEvents NumericUpDownSweepCount As System.Windows.Forms.NumericUpDown
        Private WithEvents CheckBoxNormalizeViaBypass As System.Windows.Forms.CheckBox
        Private WithEvents CheckBoxIncludeRegionInSootLoadCalculation As System.Windows.Forms.CheckBox
        Private WithEvents CheckBoxUseRegionForStatus As System.Windows.Forms.CheckBox
        Friend WithEvents NumericUpDownMagnitudeBias As System.Windows.Forms.NumericUpDown
        Friend WithEvents NumericUpDownPhaseBias As System.Windows.Forms.NumericUpDown
        Private WithEvents CheckBoxGenerateRawOutputFiles As System.Windows.Forms.CheckBox
        Private panelButons As System.Windows.Forms.Panel
        Private WithEvents ButtonGenerate As System.Windows.Forms.Button
        Private SaveGeneratedFileDialog As System.Windows.Forms.SaveFileDialog
        Private WithEvents CheckBoxAutoStart As System.Windows.Forms.CheckBox
        Private WithEvents CheckBoxGeneratePOFile As System.Windows.Forms.CheckBox
        Private WithEvents CheckBoxGenerateROFiles As System.Windows.Forms.CheckBox
        Private WithEvents CopyThenEditButton As System.Windows.Forms.Button
        Private MeasurementIDLabel As System.Windows.Forms.Label
        Private MeasurementIdTextBox As System.Windows.Forms.TextBox
    End Class
