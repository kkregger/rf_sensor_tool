﻿Imports System.Collections.Concurrent
Imports System.Security.Cryptography
Imports System.Management
Imports System.IO
Imports System.Threading
Imports System.Reflection
Imports System.Threading.Tasks
Imports CTS_CAN_Services
Imports CTS_CAN_Services.Peak.Can.Basic

Partial Public Class FormRfSensorMain
    Inherits Form

#Region "constants"
#End Region

#Region "fields"
    Private _scanRunning                As Boolean
    Private _runCount                   As Integer
    Private _startingInletTemperature   As Short
    Private _startingOutletTemperature  As Short
    Private _startingBoardTemperature   As Short
    Private _endingInletTemperature     As Short
    Private _endingOutletTemperature    As Short
    Private _endingBoardTemperature     As Short
    Private _inletTemperatureError      As Boolean
    Private _outletTemperatureError     As Boolean
    Private _averageTemperature         As Double
    Private _sootLoad                   As Double
    Private _sootPercentage             As Double
    Private _captureStartingTemperature As Boolean
    Private _saveToXml                  As Boolean
    Private _chartAutoSize              As Boolean = True
    Private _chartMinimumIndex          As Integer
    Private _chartMaximumIndex          As Integer
    Private _stopTime                   As Double
    Private _sweepTime                  As Double
    Private _limitFlags                 As Byte
    Private _nonDecimalEntered          As Boolean
    Private _primaryMeasurementLabel    As String
    Private _measurementConfigObj       As FormMeasurementConfig
    Private _formRoiConfigObj           As FormROIConfiguration
    Private _baseTimeUnit               As Byte
    Private _canNodeID                  As Byte
    Private _outOfReset                 As Boolean
    Private _scanOperatingMode          As OperatingMode
    Private _bypass                     As Boolean
    Private _closing                    As Boolean
    Private _firstScan                  As Boolean = True
    Private _unlockEnabled              As Boolean
    Private _unlocked                   As Boolean
    Private _timerUnlockKeyPress        As Timers.Timer
    Private _calibrationFileRead        As Boolean

    Private Shared _scanSequenceState       As SweepSequenceState = SweepSequenceState.Complete
    Private Shared _visibleInstance         As FormRfSensorMain
    Private Shared _canConnected            As Boolean
    Private Shared _sensorState             As AppState = AppState.Undef
    Private Shared _calibrationFileLoaded   As Boolean
    Private Shared _sweepStartTime          As Double
    Private Shared _sweepStepsComplete      As Integer
    Private Shared _sweepTimeTotal          As Double
    Private Shared _sweepRateTimer          As Timers.Timer
    Private Shared _sweepRateTimerDisabled  As Boolean
    Private Shared _sweepRateTimerElapsed   As Boolean
    Private Shared _timerSweepDataMsg       As Timers.Timer
    Private Shared _sweepRateLockingObj     As New Object()
    Private Shared _testDirectoryName       As String
    Private Shared _sweepStarted            As Boolean
    Private Shared _loadingCals             As Boolean
    Private Shared _initTasksLockingObj     As New Object()
    Private Shared _timerCanActivityTimeout As Timers.Timer
    Private Shared _canActivityTimeout      As Boolean = True
    Private Shared _timerReadSensorState    As Timers.Timer
    Private Shared _readStateTimerDisabled  As Boolean

    ' Events to raise when there is an error/exception to be logged.
    Private Shared Event LogErrorEvent     As ActivityLog.LogErrorStackDelegate
    Private Shared Event LogExceptionEvent As ActivityLog.LogExceptionDelegate

    ' Dictionary of configuration class types.
    ' Uses a generic dictionary which implements IEnumerable to initialize
    ' the ConcurrentDictionary which has no Add() method.
    Public Shared ReadOnly ConfigClasses As ConcurrentDictionary(Of String, Type) =
            New ConcurrentDictionary(Of String, Type)(New Dictionary(Of String, Type) From {
            {"CanCommunication"     , GetType(CanCommunication)},
            {"CustomerConfiguration", GetType(CustomerConfiguration)},
            {"Measurement"          , GetType(Measurement)},
            {"RegionOfInterest"     , GetType(RegionOfInterest)},
            {"RunConfiguration"     , GetType(RunConfiguration)}
        })
#End Region ' fields

    Public Sub New()

        ' Required.
        InitializeComponent()
        
        ' Setup error logging directory, file path and write header to file.
        ActivityLog.InitErrorLogging()

        ' Add error/exception logging event handlers.
        AddHandler LogErrorEvent, AddressOf ActivityLog.LogError
        AddHandler LogExceptionEvent, AddressOf ActivityLog.LogError

    End Sub

    Private Sub FormRFSensorMain_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

        Dim inputFilePath As String = String.Empty
        Dim names As List(Of String)

        Try

            Text &= " v" & Application.ProductVersion

            ' Used to control the look of Tool Strip Menu Items.
            MenuStripRFSensorMain.Renderer = new MyRenderer()

            ' Initialize Load graphic display to a value that visually coincides with 0%. 
            PanelLoadBarPercentage.Height = 2

            ' Load the Output Power and Aux Ouput Power combo box Items collections.
            names = CanMessageLib.GetOutputPowerNames()
            For Each outPwrName In names

                Dim prefix = ""
                If (Not outPwrName.Substring(0, 1).Equals("-") AndAlso
                    Not outPwrName.Substring(0, 1).Equals("0")) Then

                    prefix = "+"
                End If
                ToolStripComboBoxOutputPower.Items.Add(prefix & outPwrName & "dBm")
                ToolStripComboBoxOutputPowerAux.Items.Add(prefix & outPwrName & "dBm")
            Next

            ' Add a last chance exception handler.
            AddHandler AppDomain.CurrentDomain.UnhandledException, AddressOf LastChanceHandler

            ' Add an exception handler for handling UI thread exceptions.
            AddHandler Application.ThreadException, AddressOf UIThreadExceptionHandler

            ' Initialize the Operating Mode ToolStripComboBox Items collection.
            ToolStripComboBoxOperatingMode.Items.Add(CanMessageLib.OperatingModeStrDict(OperatingMode.S12))
            ToolStripComboBoxOperatingMode.Items.Add(CanMessageLib.OperatingModeStrDict(OperatingMode.S11))
            ToolStripComboBoxOperatingMode.Items.Add(CanMessageLib.OperatingModeStrDict(OperatingMode.Bypass))
            ToolStripComboBoxOperatingMode.Items.Add(CanMessageLib.OperatingModeStrDict(OperatingMode.S11Load))
            ToolStripComboBoxOperatingMode.Items.Add(CanMessageLib.OperatingModeStrDict(OperatingMode.S11Open))

            ' Remove unnecessary white space from the ToolStripMenuItem DropDown boxes.
            CType(ToolStripTextBoxAtoDReadingsForAvgValue.Owner, ToolStripDropDownMenu).ShowImageMargin = False
            CType(ToolStripTextBoxSweepIntervalValue.Owner     , ToolStripDropDownMenu).ShowImageMargin = False
            CType(ToolStripTextBoxLockDelayValue.Owner         , ToolStripDropDownMenu).ShowImageMargin = False
            CType(ToolStripComboBoxOutputPower.Owner           , ToolStripDropDownMenu).ShowImageMargin = False
            CType(ToolStripComboBoxOutputPowerAux.Owner        , ToolStripDropDownMenu).ShowImageMargin = False
            CType(ToolStripTextBoxSweepRateValue.Owner         , ToolStripDropDownMenu).ShowImageMargin = False
            CType(ToolStripComboBoxOperatingMode.Owner         , ToolStripDropDownMenu).ShowImageMargin = False
            CType(ToolStripComboBoxOperatingMode.Owner         , ToolStripDropDownMenu).ShowImageMargin = False
            CType(ToolStripTextBoxMinDetectableValue.Owner     , ToolStripDropDownMenu).ShowImageMargin = False
            CType(ToolStripTextBoxMaxDetectableValue.Owner     , ToolStripDropDownMenu).ShowImageMargin = False
            CType(ToolStripTextBoxStepDelayValue.Owner         , ToolStripDropDownMenu).ShowImageMargin = False

            Try
                ' Read the configuration file.
                If File.Exists(Application.StartupPath & "\" & My.Settings.XmlConfigurationFile) Then

                    inputFilePath = Application.StartupPath & "\" & My.Settings.XmlConfigurationFile
                    If FileSystem.FileLen(inputFilePath) > 0 Then

                        ' Load configurations from xml file.
                        Dim doc = XDocument.Load(inputFilePath)

                        XmlToConfig(doc)
                        _saveToXml = True
                    Else
                        MessageBox.Show("The xml tool configuration file is empty!")
                    End If
                ElseIf File.Exists(Application.StartupPath & "\" & My.Settings.BinConfigurationFile) Then

                    inputFilePath = Application.StartupPath & "\" & My.Settings.BinConfigurationFile
                    If FileSystem.FileLen(inputFilePath) > 0 Then

                        Dim doc As XDocument = Nothing
                        FormFileEncryption.GetDecryptedXmlFile(doc, inputFilePath)

                        ' Load configurations from the xml document.
                        XmlToConfig(doc)
                        _saveToXml = False
                    Else
                        MessageBox.Show("The binary tool configuration file is empty!")
                    End If
                Else
                    MessageBox.Show("No tool configuration file found!")
                End If

                If CustomerSettingsList.Count = 0 Then
                    CustomerSettingsList.Add(New CustomerConfiguration())
                End If
                _primaryMeasurementLabel         = CustomerSettingsList(0).PrimaryMeasurementLabel.Replace(" "c, "_"c)
                PrimaryMeasurementTextBox.Text   = CustomerSettingsList(0).PrimaryMeasurementLabel
                labelAboveMaxDetectable.Text     = CustomerSettingsList(0).FileNamePrefix.Replace("_", " ") & "High"
                labelBelowMinDetectable.Text     = CustomerSettingsList(0).FileNamePrefix.Replace("_", " ") & "Low"
                SecondaryMeasurementTextBox.Text = CustomerSettingsList(0).SecondaryMeasurementLabel

            Catch knfe As KeyNotFoundException
                RaiseEvent LogExceptionEvent(New StackTrace, knfe)
            Catch ex As Exception
                MsgBox("Failed to read the Configuration file: " & inputFilePath & vbCrLf &
                       "Previously saved user settings will not be available.",
                       MsgBoxStyle.SystemModal + MsgBoxStyle.Exclamation)
                RaiseEvent LogExceptionEvent(New StackTrace, ex)
            End Try

            ' Initialize controls.
            ' Currently there should only be one run configuration.
            ' If that changes we'll have to add an ID or Description.
            If (Not Directory.Exists(CustomerSettingsList(0).OutputFileDirectory))

                ' When using copied configuration files there's a excellent
                ' chance that the output directory path will be invalid.
                CustomerSettingsList(0).OutputFileDirectory = 
                    Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
            End If
            TextBoxDataOutputFolder.Text                 = CustomerSettingsList(0).OutputFileDirectory
            'TextBoxTestName.Text                         = CustomerSettingsList(0).OutputFileName
            ToolStripTextBoxSweepRateValue.Text          = RunConfigurations.SweepRate
            ToolStripComboBoxOperatingMode.SelectedIndex = ToolStripComboBoxOperatingMode.FindStringExact(
                                                               RunConfigurations.OperatingMode)
            ToolStripTextBoxAtoDReadingsForAvgValue.Text = RunConfigurations.ReadingsForAvg.ToString()
            ToolStripTextBoxSweepIntervalValue.Text      = RunConfigurations.SweepInterval.ToString()
            ToolStripTextBoxLockDelayValue.Text          = RunConfigurations.LockDelay.ToString()
            ToolStripTextBoxMinDetectableValue.Text      = RunConfigurations.MinDetectable.ToString()
            ToolStripTextBoxMaxDetectableValue.Text      = RunConfigurations.MaxDetectable.ToString()
            ToolStripTextBoxStepDelayValue.Text          = RunConfigurations.TimeBetweenSteps.ToString()
            ToolStripMenuItemDisableCalibration.Checked  = Not RunConfigurations.CalibrationOn
            _calibrationOn                               = RunConfigurations.CalibrationOn
            PictureBoxCalibrationOff.Image =
                If(_calibrationOn, My.Resources.GreenLEDOff, My.Resources.GreenLEDOn)
            toolTip1.SetToolTip(TextBoxDataOutputFolder, CustomerSettingsList(0).OutputFileDirectory)
            'toolTip1.SetToolTip(TextBoxTestName, CustomerSettingsList(0).OutputFileName)

            ' Retrieve the active CAN communications configuration, if there is one.
            LoadActiveCanCommunication()

            ' Initialize the Output Power selection controls.
            Dim cbstr As String = Nothing
            If CanMessageLib.OutputPowerStrDict.TryGetValue(RunConfigurations.OutputPower, cbstr) Then
                ToolStripComboBoxOutputPower.SelectedIndex = ToolStripComboBoxOutputPower.FindStringExact(cbstr)
            End If
            If CanMessageLib.OutputPowerStrDict.TryGetValue(RunConfigurations.AuxOutputPower, cbstr) Then
                ToolStripComboBoxOutputPowerAux.SelectedIndex = ToolStripComboBoxOutputPowerAux.FindStringExact(cbstr)
            End If

            ' Start processing received CAN messages.
            CanMessageSort.Start()

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Private Sub FormRFSensorMain_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown

        Try
            Dim dirName  = String.Empty
            Dim query    As SelectQuery
            Dim searcher As ManagementObjectSearcher
            Dim drivers  As ManagementObjectCollection

            ' Reference to this instance of the FormRFSensorMain class.
            _visibleInstance = Me

            If (RunConfigurations.SweepRate = 0) Then
                _sweepRateTimer                 = New Timers.Timer(Common.SweepRateMin)
            Else
                                                  ' Scale from seconds to milliseconds.
                _sweepRateTimer                 = New Timers.Timer(RunConfigurations.SweepRate * 1000.0)
            End If
            _sweepRateTimer.Enabled             = False
            _sweepRateTimer.AutoReset           = False
            _sweepRateTimer.SynchronizingObject = Me       ' The timer event handler will execute on the main thread.
            AddHandler _sweepRateTimer.Elapsed, AddressOf SweepRateTimer_Elapsed

            _timerSweepDataMsg                     = New Timers.Timer(Common.SweepDataMsgTimerInterval)
            _timerSweepDataMsg.Enabled             = False
            _timerSweepDataMsg.AutoReset           = False
            _timerSweepDataMsg.SynchronizingObject = Me     ' The timer event handler will execute on the main thread.
            AddHandler _timerSweepDataMsg.Elapsed, AddressOf TimerSweepDataMsg_Elapsed

            _timerUnlockKeyPress                     = New Timers.Timer(Common.TimerUnlockKeyPressTimeout)
            _timerUnlockKeyPress.Enabled             = False
            _timerUnlockKeyPress.AutoReset           = False
            _timerUnlockKeyPress.SynchronizingObject = Me   ' The timer event handler will execute on the main thread.
            AddHandler _timerUnlockKeyPress.Elapsed, AddressOf TimerUnlockKeyPress_Elapsed

            _timerCanActivityTimeout                     = New Timers.Timer(Common.TimerCanActivityTimeout)
            _timerCanActivityTimeout.Enabled             = False
            _timerCanActivityTimeout.AutoReset           = False
            _timerCanActivityTimeout.SynchronizingObject = Me ' The timer event handler will execute on the main thread.
            AddHandler _timerCanActivityTimeout.Elapsed, AddressOf TimerCanActivityTimeout_Elapsed

            _timerReadSensorState                     = New Timers.Timer(Common.TimerReadSensorStateTimeout)
            _timerReadSensorState.AutoReset           = True
            _timerReadSensorState.SynchronizingObject = Me ' The timer event handler will execute on the main thread.
            AddHandler _timerReadSensorState.Elapsed, AddressOf TimerReadSensorState_ElapsedAsync
            _timerReadSensorState.Enabled             = False ' True to turn on sensor state query

            ' Register event handlers.
            CanMessageLib.AddStateChangeEventHandler(AddressOf StateChangedHandlerAsync)
            CanMessageLib.AddUpdateSweepTimeEventHandler(AddressOf UpdateSweepTimeHandler)
            CanMessageLib.AddCanNodeIdEventHandler(AddressOf CanNodeIDChanged)
            CanMessageLib.AddInfoMsgReceivedEventHandler(AddressOf InfoMsgReceivedHandler)
            CanMessageLib.AddSweepIndexChangedEventHandler(AddressOf SweepIndexChangedHandler)

            ' Don't select any text in initially focused control.
            TextBoxTestName.SelectionLength = 0

            ' Add TextChanged event handlers for the ToolStripTextBoxes after their initial value is assigned.
            AddHandler ToolStripTextBoxAtoDReadingsForAvgValue.TextChanged,
                AddressOf ToolStripTextBoxAtoDReadingsForAvgValue_TextChanged
            AddHandler ToolStripTextBoxSweepIntervalValue.TextChanged,
                AddressOf ToolStripTextBoxSweepIntervalValue_TextChanged
            AddHandler ToolStripTextBoxLockDelayValue.TextChanged,
                AddressOf ToolStripTextBoxLockDelayValue_TextChanged
            AddHandler ToolStripTextBoxSweepRateValue.TextChanged,
                AddressOf ToolStripTextBoxSweepRateValue_TextChanged
            AddHandler ToolStripTextBoxMinDetectableValue.TextChanged,
                AddressOf ToolStripTextBoxMinDetectableValue_TextChanged
            AddHandler ToolStripTextBoxMaxDetectableValue.TextChanged,
                AddressOf ToolStripTextBoxMaxDetectableValue_TextChanged
            AddHandler ToolStripTextBoxStepDelayValue.TextChanged,
                AddressOf ToolStripTextBoxStepDelayValue_TextChanged
            AddHandler TextBoxDataOutputFolder.TextChanged,
                AddressOf TextBoxDataOutputFolder_TextChanged
            AddHandler TextBoxTestName.TextChanged,
                AddressOf TextBoxTestName_TextChanged

            If (Not String.IsNullOrWhiteSpace(CustomerSettingsList(0).OutputFileName)) Then
                ' Check to see if the logging directory exists.
                dirName = CustomerSettingsList(0).OutputFileDirectory & "\" &
                          CustomerSettingsList(0).OutputFileName &
                          If(_directoryCount = 0, String.Empty, "_" & _directoryCount.ToString())

                If (Directory.Exists(dirName)) Then
                    CustomerSettingsList(0).OutputFileName = String.Empty
                    TextBoxTestName.Text = String.Empty
                End If
            End If

            query = new SelectQuery("Win32_SystemDriver")
            query.Condition = "Name = 'pcan_usb'"
            searcher = New ManagementObjectSearcher(query)
            drivers = searcher.Get()

            If (drivers.Count = 0) Then
                ' Peak CAN driver is not installed.
            End If

            If ActiveCanDevice IsNot Nothing Then
                Try
                    ' Connect to the previously selected CAN communications device, if available.
                    CanConnect()
                    If (_canConnected) Then
                        ButtonCanConfig.Enabled = False
                        ButtonCanEnable.Enabled = False
                    End If

                Catch ex As Exception
                    MsgBox("Failed to connect to the selected CAN device due to an exception.",
                           MsgBoxStyle.SystemModal + MsgBoxStyle.Exclamation)
                    RaiseEvent LogExceptionEvent(New StackTrace, ex)
                Finally
                    ButtonCanConfig.Enabled = True
                    ButtonCanEnable.Enabled = True
                End Try
            Else
                MessageBox.Show("No active CAN configuration was found." & vbCrLf &
                                "You must configure a CAN device connection.",
                                "RF Sensor Communications")
            End If
            DisplayCurrentStatus()

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Private Sub FormRFSensorMain_FormClosing(ByVal sender As Object, ByVal e As FormClosingEventArgs) _
        Handles Me.FormClosing

        Dim mbResult = MsgBoxResult.No

        Try
            If (_canConnected) Then
                If (Not _closing AndAlso _scanRunning) Then

                    mbResult = MsgBox("Sweep in progress." & vbCrLf &
                                      "Do you want to terminate the Sweep?",
                                      MsgBoxStyle.SystemModal + MsgBoxStyle.YesNo)
                    e.Cancel = True
                    If (mbResult = MsgBoxResult.Yes) Then
                        _closing = True
                        Invoke(CType(Sub()
                                         DisplayCurrentStatus()
                                         StopScanAsync()
                                     End Sub, MethodInvoker))
                    End If
                    Exit Try
                End If
            End If

            ' Terminate the CAN Message Sort thread.
            CanMessageSort.Terminate()

            ' Terminate the Sweep Data Processing thread.
            SweepDataProcessing.Terminate()

            ' Terminate CAN communications threads.
            CanServicesObj.Terminate()

            ' Free timer resources.
            If (_sweepRateTimer IsNot Nothing) Then
                _sweepRateTimer.Close()
            End If

            If (_timerSweepDataMsg IsNot Nothing) Then
                _timerSweepDataMsg.Close()
            End If

            If (_timerUnlockKeyPress IsNot Nothing) Then
                _timerUnlockKeyPress.Close()
            End If

            If (_timerCanActivityTimeout IsNot Nothing) Then
                _timerCanActivityTimeout.Close()
            End If

            If (_timerReadSensorState IsNot Nothing) Then
                _timerReadSensorState.Close()
            End If

            ' Save the current configurations.
            SaveConfiguration()

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Public Shared ReadOnly Property GetInstance() As FormRfSensorMain
        Get
            Return _visibleInstance
        End Get
    End Property

    Public Shared ReadOnly Property ConfigProperties() As _
            ConcurrentDictionary(Of String, ConcurrentDictionary(Of String, PropertyInfo))
        Get
            ' Returns a dictionary of dictionaries keyed by configuration class name.
            Dim dictPropertyInfoDict = New ConcurrentDictionary(Of String, ConcurrentDictionary(Of String, PropertyInfo))()

            For Each kvpConfigType In ConfigClasses
                Dim propertiesDict = New ConcurrentDictionary(Of String, PropertyInfo)()
                Dim properties = kvpConfigType.Value.GetProperties()
                For Each propInfo In properties

                    ' Add property info to a dictionary keyed by the property name.
                    propertiesDict.TryAdd(propInfo.Name, propInfo)
                Next propInfo

                ' Add property info dictionary to a dictionary of dictionaries
                ' keyed by configuration class name.
                dictPropertyInfoDict.TryAdd(kvpConfigType.Key, propertiesDict)
            Next kvpConfigType
            Return dictPropertyInfoDict
        End Get
    End Property

    Public Shared Property ActiveMeasurement As Measurement

    Public Shared Property ActiveROI() As RegionOfInterest

    Public Shared Property ActiveCanDevice() As CanCommunication = New CanCommunication()

    Public Shared ReadOnly Property CanServicesObj As CAN_Services = CAN_Services.GetInstance()

    ' Properties used to access Configuration Lists.
    Public Shared ReadOnly Property CanCommunications() As New ConcurrentDictionary(Of String, CanCommunication)()

    Public Shared ReadOnly Property Measurements() As New ConcurrentDictionary(Of String, Measurement)()

    Public Shared ReadOnly Property RegionsOfInterest() As New ConcurrentDictionary(Of String, RegionOfInterest)()

    ' Currently only one run configuration allowed.
    Public Shared Property RunConfigurations() As New RunConfiguration()

    Public Shared ReadOnly Property CustomerSettingsList() As New List(Of CustomerConfiguration)()

    Public Shared ReadOnly Property CanConnected() As Boolean
        Get
            Return _canConnected
        End Get
    End Property

    Public Property InletTemp() As Double

    Public Property OutletTemp() As Double

    Public Shared ReadOnly Property ScanSequenceState As SweepSequenceState
        Get
            Return _scanSequenceState
        End Get
    End Property

    Public Shared ReadOnly Property SensorState As AppState
        Get
            Return _sensorState
        End Get
    End Property

    Public Shared Property SweepDataIndex As UInteger

    Public Shared Property CommandedStepCount As UShort

    Public Shared Readonly Property TestDirectoryName As String
        Get
            Return If(_testDirectoryName, String.Empty)
        End Get
    End Property

    Public Shared ReadOnly Property DirectoryCount As UInteger

    Public Property ChartAutoSize() As Boolean
        Get
            Return _chartAutoSize
        End Get
        Set(ByVal value As Boolean)
            If value Then
                ChartSootLoad.ChartAreas(0).AxisX.Minimum = Double.NaN
                ChartSootLoad.ChartAreas(0).AxisX.Maximum = Double.NaN
            Else
                ChartSootLoad.ChartAreas(0).AxisX.Minimum = ChartSootLoad.Series(0).Points(_chartMinimumIndex).XValue
                ChartSootLoad.ChartAreas(0).AxisX.Maximum = ChartSootLoad.Series(0).Points(_chartMaximumIndex - 1).XValue
            End If
            _chartAutoSize = value
        End Set
    End Property

    Public Property ChartMinimumIndex() As Integer
        Get
            Return _chartMinimumIndex
        End Get
        Set(ByVal value As Integer)
            If value < 0 Then
                value = 0
            End If

            If value > 0 Then
                ChartAutoSize                   = False
                _chartMinimumIndex              = value
                'ButtonCompressPlot.Enabled     = True
                ButtonCompressPlot.ForeColor    = Color.Chartreuse
                ' ButtonScrollPlotLeft.Enabled  = True
                ButtonScrollPlotLeft.ForeColor  = Color.Chartreuse
                'ButtonScrollPlotRight.Enabled  = True
                ButtonScrollPlotRight.ForeColor = Color.Chartreuse
            Else
                _chartMinimumIndex              = 0
                ChartAutoSize                   = _chartMaximumIndex = ChartSootLoad.Series(0).Points.Count
                'ButtonCompressPlot.Enabled     = False
                ButtonCompressPlot.ForeColor    = Color.Orange
                'ButtonScrollPlotRight.Enabled  = Not ChartAutoSize
                ButtonScrollPlotRight.ForeColor = If(ChartAutoSize, Color.Orange, Color.Chartreuse)
            End If
            'ButtonExpandPlot.Enabled = _chartMinimumIndex < _chartMaximumIndex - 2
            ButtonExpandPlot.ForeColor = If(_chartMinimumIndex < _chartMaximumIndex - 2, Color.Chartreuse, Color.Orange)
        End Set
    End Property

    Public Property ChartMaximumIndex() As Integer
        Get
            Return _chartMaximumIndex
        End Get
        Set(ByVal value As Integer)
            If value > ChartSootLoad.Series(0).Points.Count Then
                value = ChartSootLoad.Series(0).Points.Count
            End If

            If value < ChartSootLoad.Series(0).Points.Count Then
                _chartMaximumIndex = value
                ChartAutoSize                   = False
                'ButtonClearPlot.Enabled        = True
                ButtonClearPlot.ForeColor       = Color.Chartreuse
                'ButtonScrollPlotLeft.Enabled   = True
                ButtonScrollPlotLeft.ForeColor  = Color.Chartreuse
                'ButtonScrollPlotRight.Enabled  = True
                ButtonScrollPlotRight.ForeColor = Color.Chartreuse
            Else
                _chartMaximumIndex             = ChartSootLoad.Series(0).Points.Count
                ChartAutoSize                  = _chartMinimumIndex = 0
                'ButtonClearPlot.Enabled       = False
                ButtonClearPlot.ForeColor      = Color.Orange
                'ButtonScrollPlotLeft.Enabled  = Not ChartAutoSize
                ButtonScrollPlotLeft.ForeColor = If(ChartAutoSize, Color.Orange, Color.Chartreuse)
            End If
            'ButtonExpandPlot.Enabled = _chartMinimumIndex < _chartMaximumIndex - 2
            ButtonExpandPlot.ForeColor = If(_chartMinimumIndex < _chartMaximumIndex - 2, Color.Chartreuse, Color.Orange)
        End Set
    End Property

    Public Shared ReadOnly Property CalibrationOn As Boolean

    Public Shared ReadOnly Property WaitForLastSweepDataMsg As Boolean

    Public Shared Readonly Property AppSoftwareID As String

    Public Shared Readonly Property AppSoftwareVersion As String

    Public Shared ReadOnly Property SerialNumber As Byte()

    ' Contains the Calibration file name.
    Public Shared ReadOnly Property ConfigCalsFileName As String

    Public Shared Property LastLogFileName As String

    Public Property ScanRunning() As Boolean
        Get
            Return _scanRunning
        End Get
        Set(ByVal value As Boolean)
            _scanRunning = value
            If _scanRunning Then
                If (InvokeRequired) Then
                    Invoke(
                        CType(Sub()
                                  ButtonStartStopScan.BackgroundImage = My.Resources.GreenLEDOn
                                  ButtonStartStopScan.Text = "Stop"
                              End Sub, MethodInvoker))
                Else
                    ButtonStartStopScan.BackgroundImage = My.Resources.GreenLEDOn
                    ButtonStartStopScan.Text = "Stop"
                End If
            Else
                If (InvokeRequired) Then
                    Invoke(
                        CType(Sub()
                                  ButtonStartStopScan.BackgroundImage = My.Resources.GreenLEDOff
                                  ButtonStartStopScan.Text = "Start"
                              End Sub, MethodInvoker))
                Else
                    ButtonStartStopScan.BackgroundImage = My.Resources.GreenLEDOff
                    ButtonStartStopScan.Text = "Start"
                End If
            End If
        End Set
    End Property

    Public Sub InfoMsgReceivedHandler(ByVal inlet As Double, ByVal outlet As Double)

        InletTemp  = inlet
        OutletTemp = outlet
        Invoke(CType(
            Sub()
                TextBoxInletTemperature.Text = inlet
                TextBoxOutletTemperature.Text = outlet
            End Sub, MethodInvoker))
    End Sub

    Public Sub GenerateLogFileName(ByVal bypass As Boolean)

        Dim fileName = String.Empty
        Dim dateStr = Date.Now.Year.ToString() & Date.Now.Month.ToString() & Date.Now.Day.ToString("D2")

        ' Time since midnight in seconds.
        Dim timeStr = (Date.Now.TimeOfDay.Ticks \ TimeSpan.TicksPerSecond).ToString()

        ' Generate Log file name.
        fileName = "RO_" & CustomerSettingsList(0).FileNamePrefix
        fileName &= CustomerSettingsList(0).OutputFileName

        ' In cases where a sweep is re-run using the same test name append a number to the file name.
        fileName &= If(DirectoryCount = 0, String.Empty, "_" & DirectoryCount.ToString())
        fileName &= "_" & dateStr & "-" & timeStr & If(bypass, "-B", String.Empty)

        If (SweepDataProcessing.EncryptLogFiles) Then
            fileName &= ".bin"
        Else
            fileName &= ".csv"
        End If
        TextBoxCurrentOutputFilename.Text = fileName
        toolTip1.SetToolTip(_visibleInstance.TextBoxCurrentOutputFilename, fileName)
        SweepDataProcessing.LogFileNames.Enqueue(fileName)

    End Sub

    Private Sub StartSweepRateTimer()

            _sweepRateTimer.Enabled = False

            ' Scale from seconds to milliseconds.
            If (RunConfigurations.SweepRate = 0) Then

                _sweepRateTimer.Interval = Common.SweepRateMin
            Else
                _sweepRateTimer.Interval = RunConfigurations.SweepRate * 1000.0
            End If

            _sweepRateTimerElapsed  = False
            _sweepRateTimer.Enabled = True
            _sweepRateTimerDisabled = False
    End Sub

    Private Async Shared Sub TimerReadSensorState_ElapsedAsync(sender As Object, e As Timers.ElapsedEventArgs)

        If (Not _readStateTimerDisabled) Then

            Try
                Dim value As UInteger
                Dim readTask As Task(Of Boolean)

                readTask = Task.Run(Function() SendParamReadMsgWithRetry(RfSensorConfiguration.ActiveState, value))
                Await readTask

                If (readTask.Result AndAlso value <> _sensorState) Then

                    ' The sensor state has changed so call the handler.
                    _visibleInstance.StateChangedHandlerAsync(value)
                End If
            Catch ex As Exception
                RaiseEvent LogExceptionEvent(New StackTrace, ex)
            End Try
        End If
    End Sub

    Public Shared Sub RestartSweepDataMsgTimer()

        _timerSweepDataMsg.Enabled = False
        _timerSweepDataMsg.Enabled = True

    End Sub

    Public Shared Sub CanActivityDetected()

        Try
            If (_canActivityTimeout) Then

                _canActivityTimeout = False
                _visibleInstance.Invoke(New MethodInvoker(AddressOf _visibleInstance.DisplayCurrentStatus))
            End If

            If (_timerCanActivityTimeout IsNot Nothing) Then

                _timerCanActivityTimeout.Stop()
                _timerCanActivityTimeout.Interval = Common.TimerCanActivityTimeout
                _timerCanActivityTimeout.Start()
            End If

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Public Async Function RunInitializationTasksAsync() As Task(Of Boolean)

        Dim result As Boolean
        Dim readTask As Task(Of Boolean)
        Dim params As String() = {CanMessageLib.ApplicationLetter,
                                  CanMessageLib.ApplicationID,
                                  CanMessageLib.AppVoldDirLetter,
                                  CanMessageLib.ApplicationRevLev,
                                  CanMessageLib.SoftwareDevLevel,
                                  CanMessageLib.ApplicationDevLevel}
        Dim softIdBytes = New Byte() {}
        Dim param As New ParamTblEntry()

        Try
            ' Try not to allow multiple threads to execute this simultaneously.
            If (Not _loadingCals) Then

                _loadingCals = True

                If (_visibleInstance.InvokeRequired) Then

                    _visibleInstance.Invoke(New MethodInvoker(AddressOf DisplayCurrentStatus))
                Else
                    DisplayCurrentStatus()
                End If

                ' Functions LoadCalibrationsFile and ReadCalibrationValues log failures.
                Dim loadTask = Task.Run(Function() LoadCalibrationsFile())
                Await loadTask

                If (loadTask.Result) Then

                    readTask = Task.Run(Function() RfSensorConfiguration.ReadCalibrationValues())
                    Await readTask
                End If

                readTask = Task.Run(Function() CanMessageLib.AllowToolCmdFunctions())
                Await readTask

                If (readTask.Result) Then

                    readTask = Task.Run(Function() CanMessageLib.AllowToolCmdFunctions())
                    Await readTask

                End If

                If (readTask.Result) Then

                    Dim readSerNumTask As Task(Of Byte()) = Task.Run(Function() CanMessageLib.ReadSerialNumFromFlash())
                    Await readSerNumTask

                    If (readSerNumTask.Result Is Nothing) Then

                        readSerNumTask = Task.Run(Function() CanMessageLib.ReadSerialNumFromFlash())
                        Await readSerNumTask

                    End If

                    If (readSerNumTask.Result IsNot Nothing) Then

                        _serialNumber = readSerNumTask.Result
                        result = True

                    End If
                End If

                readTask = Task.Run(Function() CanMessageLib.SendParamReadMsg(params, softIdBytes))
                Await readTask

                If (Not readTask.Result) Then

                    readTask = Task.Run(Function() CanMessageLib.SendParamReadMsg(params, softIdBytes))
                    Await readTask

                End If

                If (readTask.Result) Then

                    If (softIdBytes IsNot Nothing AndAlso
                        softIdBytes.Length <= params.Length) Then

                        If (softIdBytes.Length = 6) Then

                            ' Append final character onto the software ID.
                            _AppSoftwareID &= Chr(softIdBytes(4)) & "." & softIdBytes(5).ToString()
                        End If
                    End If

                    _AppSoftwareVersion = String.Empty
                    For idx = 0 To softIdBytes.Length - 1

                        If ((RfSensorConfiguration.ParamTblDict IsNot Nothing) AndAlso
                                (RfSensorConfiguration.ParamTblDict.TryGetValue(params(idx), param))) Then

                            If (param.DisplayMode = 3) Then

                                _AppSoftwareVersion &= softIdBytes(idx).ToString()

                            ElseIf (param.DisplayMode = 10) Then

                                _AppSoftwareVersion &= Chr(softIdBytes(idx))

                            End If
                        End If
                    Next
                End If

                _loadingCals = False

                If (_visibleInstance.InvokeRequired) Then

                    _visibleInstance.Invoke(New MethodInvoker(AddressOf DisplayCurrentStatus))
                Else
                    DisplayCurrentStatus()
                End If
            End If

        Catch ex As Exception

            ' Make sure the status display is updated.
            _loadingCals = False

            If (_visibleInstance.InvokeRequired) Then

                _visibleInstance.Invoke(New MethodInvoker(AddressOf DisplayCurrentStatus))
            Else
                DisplayCurrentStatus()
            End If

            ' Re-throw the exception.
            Throw

        End Try
        Return result
    End Function

    Public Sub UpdateSweepTimeHandler()

        Dim sweepEndTime As Double

        ' Don't update the total sweep time whilst waiting for the sensor to send
        ' the last sweep data message after the user has chosen to interrupt a
        ' sweep in progress.
        If (Not _waitForLastSweepDataMsg) Then

            sweepEndTime = Common.HighResTime
            _sweepTimeTotal += sweepEndTime - _sweepStartTime

        End If
    End Sub

    Private Async Sub ButtonStartStopScan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonStartStopScan.Click

        Dim dirs     As String()
        Dim sendTask As Task(Of Boolean)
        Dim mbResult = MsgBoxResult.Ignore
        Dim result   = False
        Dim dirName  = String.Empty

        Try
            If (Not ScanRunning) Then

                If ((Not (_sensorState = AppState.NormalOperation     OrElse
                          _sensorState = AppState.WaitingForCommand)) OrElse
                    (String.IsNullOrWhiteSpace(TextBoxTestName.Text))   OrElse (Not _canConnected)) Then

                    If (Not _canConnected) Then

                        ProgressBarSweepProgress.Value = ProgressBarSweepProgress.Minimum
                        PictureBoxSweeping.Image = My.Resources.GreenLEDOff
                        PictureBoxIdle.Image = My.Resources.GreenLEDOff
                        ScanRunning = False
                        MsgBox("Please establish a CAN bus connection first.", MsgBoxStyle.SystemModal)

                    ElseIf (String.IsNullOrWhiteSpace(TextBoxTestName.Text)) Then

                        MsgBox("Please enter a Test Name first.", MsgBoxStyle.SystemModal)

                    ElseIf (Not _sensorState = AppState.NormalOperation) Then

                        MsgBox("Tried to start a measurement sweep and the Rf Sensor application" & vbCrLf &
                               "is not in the NormalOperation or WaitForCommand state.", MsgBoxStyle.SystemModal)
                    End If

                    Exit Try
                End If

                ' Check to see if logging the directory exists.
                ' If so create a new one by appending the directory count to the name.
                dirName = CustomerSettingsList(0).OutputFileDirectory & "\" &
                          CustomerSettingsList(0).OutputFileName

                If (_firstScan) Then

                    dirs = Directory.GetDirectories(CustomerSettingsList(0).OutputFileDirectory,
                                                    CustomerSettingsList(0).OutputFileName & "_*")
                    Dim numericSuffix As UInteger
                    For Each dirName In dirs

                        If (dirName.Length > 0) Then

                            Dim idx = dirName.LastIndexOf("_")

                            If ((dirName.Length - 1) > idx) Then

                                Dim suffix = dirName.Substring(idx + 1)
                                UInteger.TryParse(suffix, numericSuffix)

                                If (numericSuffix > 0) Then

                                    ' A matching directory name with a numeric suffix was found.
                                    Exit For
                                End If
                            End If
                        End If
                    Next
                    If ((numericSuffix > 0) OrElse (New DirectoryInfo(dirName).Exists)) Then

                        ' This is the first scan since the app started and either a directory
                        ' exists by the same name with a numeric suffix or an exact match was found.
                        ' Have the user enter a new test name.
                        MsgBox("A directory corresponding to this test name already exists!" & vbCrLf &
                               "Please enter a new test name.")
                        CustomerSettingsList(0).OutputFileName = String.Empty
                        TextBoxTestName.Text = String.Empty
                        TextBoxTestName.Focus()

                        Exit Try
                    End If
                Else
                    If (New DirectoryInfo(dirName).Exists) Then
                        _directoryCount += 1
                    End If

                    dirName &= If(_directoryCount = 0, String.Empty, "_" & _directoryCount.ToString())
                End If

                Try
                    Directory.CreateDirectory(dirName)
                    _testDirectoryName = dirName

                Catch ex As Exception

                    RaiseEvent LogExceptionEvent(New StackTrace, ex)
                    _testDirectoryName = String.Empty
                End Try

                ' The first sweep is being started. Subsequent sweeps can use the same test name.
                _firstScan = False

                ' Store starting temperatures.
                _startingBoardTemperature  = RfSensorConfiguration.BoardTemp
                _startingInletTemperature  = InletTemp
                _startingOutletTemperature = OutletTemp

                ' Initialize the Sweep count and sweep duration displays.
                _runCount = 0
                TextBoxRunCount.Text      = _runCount.ToString()
                TextBoxSweepDuration.Text = String.Empty

                ' Initialize the Sweep progress bar.
                ProgressBarSweepProgress.Minimum = 0
                ProgressBarSweepProgress.Value   = ProgressBarSweepProgress.Minimum

                ' Initialize the sweep data point index sweep steps complete count.
                ' If the last sweep was interrupted it may not be 0.
                SweepDataIndex      = 0
                _sweepStepsComplete = 0

                ' Used to capture measurement time
                _sweepStartTime = Common.HighResTime

                SweepDataProcessing.EncryptLogFiles = Not _unlocked
                GenerateLogFileName(True)       ' True for Bypass mode

                CommandedStepCount = Common.DefaultStepCount
                ' Configure the RF Sensor to run measurement sweep.
                sendTask = Task.Run(Function() SendCommandMsgWithRetry(CommandMsgType.ConfigureVCO,
                                                                       Common.DefaultStartFreqRange1))
                Await sendTask
                result = sendTask.Result
                sendTask = Task.Run(Function() SendCommandMsgWithRetry(CommandMsgType.ConfigStepParams,
                                                                       CommandedStepCount))
                Await sendTask
                result = result And sendTask.Result

                If result = True Then

                    ' Currently the first sweep sequence is done in Bypass mode.
                    _scanOperatingMode = OperatingMode.Bypass
                    sendTask = Task.Run(Function() SendCommandMsgWithRetry(CommandMsgType.RunSingleScan,
                                                                           _scanOperatingMode))
                    Await sendTask

                    If (sendTask.Result) Then

                        LabelOutputFileName.Text         = "Current Output File Name"
                        LastLogFileName                  = String.Empty
                        SweepDataProcessing.StopLogging  = False
                        _scanSequenceState               = SweepSequenceState.First
                        PictureBoxSweeping.Image         = My.Resources.GreenLEDOn
                        PictureBoxIdle.Image             = My.Resources.GreenLEDOff
                        TextBoxTestName.Enabled          = False
                        ButtonSelectOutputFolder.Enabled = False
                        TimerUpdateProgressBar.Enabled   = True
                        ScanRunning                      = True
                        StartSweepRateTimer()
                        ToolStripMenuItemSweepSettings.DropDown.Enabled = False
                        LabelSweepProgress.Text = "Bypass Sweep Progress"

                        ' Update status display to show logging in progress.
                        DisplayCurrentStatus()
                    Else
                        MsgBox("Failed to initiate the measurement sweep.",
                                MsgBoxStyle.SystemModal + MsgBoxStyle.Exclamation)
                    End If
                Else
                    ProgressBarSweepProgress.Value = ProgressBarSweepProgress.Minimum
                    PictureBoxSweeping.Image = My.Resources.GreenLEDOff
                    PictureBoxIdle.Image = My.Resources.GreenLEDOff
                    MsgBox("Failed to configure the measurement sweep.",
                            MsgBoxStyle.SystemModal + MsgBoxStyle.Exclamation)
                End If
            Else
                LabelOutputFileName.Text = "Last Output File Name"
                If (_sweepStarted) Then

                    mbResult = MsgBox("Sweep in progress." & vbCrLf &
                                  "Do you want to terminate the Sweep?",
                                  MsgBoxStyle.SystemModal + MsgBoxStyle.YesNo)

                    If (mbResult = MsgBoxResult.Yes) Then

                        SweepDataProcessing.StopLogging = True
                        StopScanAsync()
                        TextBoxCurrentOutputFilename.Text = LastLogFileName
                    End If
                Else
                    SweepDataProcessing.StopLogging = True
                    StopScanAsync()
                End If
            End If

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        Finally
            DisplayCurrentStatus()
        End Try
    End Sub

    Private Async Sub ContinueScan()

        Try
            Dim sendTask As Task(Of Boolean)
            Dim bypass   As Boolean = (_scanOperatingMode = OperatingMode.Bypass)
            Dim result = False

            If (_sensorState = AppState.WaitingForCommand) Then

                ' The first user initiated scan was performed in Bypass mode.
                ' Subsequent sweeps will be performed in the S12Normal mode.
                _scanOperatingMode = OperatingMode.S12

                ' Store starting temperatures.
                _startingBoardTemperature  = RfSensorConfiguration.BoardTemp
                _startingInletTemperature  = InletTemp
                _startingOutletTemperature = OutletTemp

                ' Initialize the Sweep progress bar.
                ProgressBarSweepProgress.Minimum = 0
                ProgressBarSweepProgress.Value   = ProgressBarSweepProgress.Minimum
                PictureBoxSweeping.Image         = My.Resources.GreenLEDOn
                PictureBoxIdle.Image             = My.Resources.GreenLEDOff

                ' Used to capture sweep elapsed time.
                _sweepStartTime = Common.HighResTime

                GenerateLogFileName(False)      ' False for S12 mode

                CommandedStepCount = Common.DefaultStepCount
                ' Configure the RF Sensor to run measurement sweep.
                sendTask = Task.Run(Function() SendCommandMsgWithRetry(CommandMsgType.ConfigureVCO,
                                                                       Common.DefaultStartFreqRange1))
                Await sendTask
                result = sendTask.Result
                sendTask = Task.Run(Function() SendCommandMsgWithRetry(CommandMsgType.ConfigStepParams,
                                                                       CommandedStepCount))
                Await sendTask
                result = result And sendTask.Result

                If result = True Then

                    sendTask = Task.Run(Function() SendCommandMsgWithRetry(CommandMsgType.RunSingleScan,
                                                                           _scanOperatingMode))
                    Await sendTask
                    If (sendTask.Result) Then
                        StartSweepRateTimer()
                        ScanRunning        = True
                        _scanSequenceState = SweepSequenceState.First
                        If (bypass) Then
                            bypass    = False
                            _runCount = 0
                            TextBoxRunCount.Text = _runCount.ToString()
                            LabelSweepProgress.Text = "S12 Sweep Progress"
                        End If
                    Else
                        SweepDataProcessing.StopLogging = True
                        StopScanAsync()
                        ProgressBarSweepProgress.Value = ProgressBarSweepProgress.Minimum
                        PictureBoxSweeping.Image       = My.Resources.GreenLEDOff
                        PictureBoxIdle.Image           = My.Resources.GreenLEDOff
                        MsgBox("Failed to initiate the next measurement sweep.",
                                MsgBoxStyle.SystemModal + MsgBoxStyle.Exclamation)
                    End If
                Else
                    SweepDataProcessing.StopLogging = True
                    StopScanAsync()
                    ProgressBarSweepProgress.Value = ProgressBarSweepProgress.Minimum
                    PictureBoxSweeping.Image       = My.Resources.GreenLEDOff
                    PictureBoxIdle.Image           = My.Resources.GreenLEDOff
                    MsgBox("Failed to configure the next measurement sweep.",
                            MsgBoxStyle.SystemModal + MsgBoxStyle.Exclamation)
                End If
            Else
                MsgBox("Tried to start the next measurement sweep and the" & vbCrLf &
                       "Rf Sensor application is not in the WaitForCommand state.", MsgBoxStyle.SystemModal)
                SweepDataProcessing.StopLogging = True
                StopScanAsync()
            End If

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        Finally

            ' Calling StopScanAsync() wll set ScanRunnng to False.
            If (Not ScanRunning) Then
                _sweepRateTimer.Enabled = False
                _sweepRateTimerDisabled = True
            End If
            DisplayCurrentStatus()
        End Try
    End Sub

    Private Async Sub StopScanAsync(Optional ByVal complete As Boolean = False)

        Dim sendTask As Task(Of Boolean)

        _sweepRateTimer.Enabled = False
        _sweepRateTimerDisabled = True

        If ((Not complete) AndAlso (_sensorState = AppState.RunSingleScan OrElse
            _sensorState = AppState.RunContinuousScan)) Then

            sendTask = Task.Run(Function() SendCommandMsgWithRetry(CommandMsgType.StopMeasurement))
            Await sendTask

            If (sendTask.Result) Then

                sendTask = Task.Run(Function() SendCommandMsgWithRetry(CommandMsgType.NormalOperation))
                Await sendTask

                If (Not sendTask.Result) Then
                    RaiseEvent LogErrorEvent(New StackTrace,
                                             "FormRfSensorMain.StopScanAsync() Failed to receive response to" &
                                             "NormalOperation command when terminating active measurement.")
                End If
            Else
                RaiseEvent LogErrorEvent(New StackTrace,
                                         "FormRfSensorMain.StopScanAsync() Failed to receive " &
                                         "response to StopMeasurement command.")
            End If
        ElseIf (_sensorState <> AppState.NormalOperation) Then

            sendTask = Task.Run(Function() SendCommandMsgWithRetry(CommandMsgType.NormalOperation))
            Await sendTask

            If (Not sendTask.Result) Then

                RaiseEvent LogErrorEvent(New StackTrace,
                                         "FormRfSensorMain.StopScanAsync() Failed to receive " &
                                         "response to NormalOperation command.")
            End If
        End If

        TimerUpdateProgressBar.Enabled   = False
        ProgressBarSweepProgress.Value   = ProgressBarSweepProgress.Minimum
        SweepDataIndex                   = 0
        _sweepStepsComplete              = 0
        _sweepTimeTotal                  = 0.0
        PictureBoxSweeping.Image         = My.Resources.GreenLEDOff
        PictureBoxIdle.Image             = My.Resources.GreenLEDOff
        ScanRunning                      = False
        TextBoxTestName.Enabled          = True
        ButtonSelectOutputFolder.Enabled = True
        _scanSequenceState               = SweepSequenceState.Complete
        ToolStripMenuItemSweepSettings.DropDown.Enabled = True

        ' When a scan is interrupted sweep data messages will continue to be transmitted
        ' by the sensor even though it reports that it's in the NormalOperation state.
        ' Disable the start/stop button until all remaining messages have been sent.
        ButtonWaitSweep.BringToFront()
        ButtonStartStopScan.Enabled   = False
        _waitForLastSweepDataMsg      = True
        _timerSweepDataMsg.Enabled    = True

        ' Clear the Log File Names queue of unused names.
        SweepDataProcessing.LogFileNames = New ConcurrentQueue(Of String)
    End Sub

    Public Async Shared Sub SendROIToSensorAsync(ByVal seqState As SweepSequenceState)

        Dim calIDs As UShort() = Nothing
        Dim sendTask As Task(Of Boolean)

        sendTask = Task.Run(Function() SendToolFunctionCmdWithRetry(ToolFunctionMsg.AllowToolCmdFunctions))
        Await sendTask

        If (sendTask.Result) Then

            Select (seqState)
                Case SweepSequenceState.First

                    calIDs = {RfSensorConfiguration.RoiStartFreq1}
                    sendTask = Task.Run(Function() SendAppCalWriteMsgWithRetry(calIDs, {Common.DefaultStartFreqRange1}))
                    Await sendTask

                    calIDs = {RfSensorConfiguration.RoiFreqSteps1}
                    sendTask = Task.Run(Function() SendAppCalWriteMsgWithRetry(calIDs, {Common.DefaultStepCount}))
                    Await sendTask

                    calIDs = {RfSensorConfiguration.RoiFreqStepSize1}
                    sendTask = Task.Run(Function() SendAppCalWriteMsgWithRetry(calIDs, {Common.DefaultFrequencyStepSize}))
                    Await sendTask

                Case SweepSequenceState.Second

                    calIDs = {RfSensorConfiguration.RoiStartFreq2}
                    sendTask = Task.Run(Function() SendAppCalWriteMsgWithRetry(calIDs, {Common.DefaultStartFreqRange2}))
                    Await sendTask

                    calIDs = {RfSensorConfiguration.RoiFreqSteps2}
                    sendTask = Task.Run(Function() SendAppCalWriteMsgWithRetry(calIDs, {Common.DefaultStepCount}))
                    Await sendTask

                    calIDs = {RfSensorConfiguration.RoiFreqStepSize2}
                    sendTask = Task.Run(Function() SendAppCalWriteMsgWithRetry(calIDs, {Common.DefaultFrequencyStepSize}))
                    Await sendTask

                Case SweepSequenceState.Final

                    calIDs = {RfSensorConfiguration.RoiStartFreq3}
                    sendTask = Task.Run(Function() SendAppCalWriteMsgWithRetry(calIDs, {Common.DefaultStartFreqRange3}))
                    Await sendTask

                    calIDs = {RfSensorConfiguration.RoiFreqSteps3}
                    sendTask = Task.Run(Function() SendAppCalWriteMsgWithRetry(calIDs, {Common.DefaultStepCount}))
                    Await sendTask

                    calIDs = {RfSensorConfiguration.RoiFreqStepSize3}
                    sendTask = Task.Run(Function() SendAppCalWriteMsgWithRetry(calIDs, {Common.DefaultFrequencyStepSize}))
                    Await sendTask

                Case Else
                    RaiseEvent LogErrorEvent(New StackTrace,
                                             "SendROIToSensor() Unexpected Sweep Sequence State. " &
                                             "State argument value = " & seqState.ToString())
            End Select
        Else
            RaiseEvent LogErrorEvent(New StackTrace,
                                     "SendROIToSensor() Failed to receive an AllowToolCommand reponse.")
        End If

        If ((sendTask IsNot Nothing) AndAlso (Not sendTask.Result)) Then

            If ((calIDs IsNot Nothing) AndAlso (calIDs.Length > 0)) Then

                RaiseEvent LogErrorEvent(New StackTrace,
                                         "Failed to receive an Application Cal write response." &
                                         " Cal ID = " + CStr(calIDs(0)))
            End If
        End IF
        ' Kay_RFS_ROI_EnableType[0] ????????????
        ' <CalVariable>Kaw_RFS_ROI_StartFreq[0]</CalVariable><Bytes>2</Bytes> Why only 2 bytes????
        ' Kaw_RFS_ROI_ContributionSelection[0]?????????????????
        ' Kaw_RFS_ROI_ExecutionTimes[0]
    End Sub

    Private Shared Function SendCommandMsgWithRetry(ByVal cmdMsgType As CommandMsgType) As Boolean

        Dim result As Boolean

        result = CanMessageLib.SendCommandMessage(cmdMsgType)

        If (Not result) Then

            ' Try again.
            result = CanMessageLib.SendCommandMessage(cmdMsgType)
        End If

        Return result
    End Function

    Private Shared Function SendCommandMsgWithRetry(ByVal cmdMsgType As CommandMsgType,
                                                    ByVal param As Object) As Boolean

        Dim result As Boolean

        result = CanMessageLib.SendCommandMessage(cmdMsgType, param)

        If (Not result) Then

            ' Try again.
            result = CanMessageLib.SendCommandMessage(cmdMsgType, param)
        End If

        Return result
    End Function

    Private Shared Function SendAppCalWriteMsgWithRetry(ByVal calIDs As UShort(), ByVal values As UInteger()) As Boolean

        Dim result As Boolean

        result = CanMessageLib.SendAppCalWriteMsg(calIDs, values)

        If (Not result) Then

            ' Try again.
            result = CanMessageLib.SendAppCalWriteMsg(calIDs, values)
        End If

        Return result
    End Function

    Private Shared Function SendToolFunctionCmdWithRetry(ByVal toolFuncMsgID As ToolFunctionMsg) As Boolean

        Dim result As Boolean

        result = CanMessageLib.SendToolFunctionCmd(toolFuncMsgID)

        If (Not result) Then

            ' Try again.
            result = CanMessageLib.SendToolFunctionCmd(toolFuncMsgID)
        End If

        Return result
    End Function

    Private Shared Function SendParamReadMsgWithRetry(ByVal paramToolName As String, ByRef value As UInteger) As Boolean

        Dim result As Boolean

        result = CanMessageLib.SendParamReadMsg(paramToolName, value)
        If (Not result) Then

            ' Try again.
            result = CanMessageLib.SendParamReadMsg(paramToolName, value)
        End If

        Return result
    End Function

    Private Shared Function SendParamReadMsgWithRetry(ByVal paramToolNames() As String, ByRef values() As Byte) As Boolean


        Dim result As Boolean

        result = CanMessageLib.SendParamReadMsg(paramToolNames, values)
        If (Not result) Then

            ' Try again.
            result = CanMessageLib.SendParamReadMsg(paramToolNames, values)
        End If

        Return result
    End Function

    Public Async Sub StateChangedHandlerAsync(ByVal state As AppState)

        Dim outOfReset  = _outOfReset

        Try
             If ((_sensorState = AppState.PowerUpScan OrElse _sensorState = AppState.Undef) AndAlso
                 (state = AppState.NormalOperation)) Then

                ' The RF Sensor has started and transitioned to the NormalOperation state.
                ' Read the serial number.
                Dim readSerNumTask = Task.Run(Function() CanMessageLib.ReadSerialNumFromFlash())
                Await readSerNumTask

                If (readSerNumTask.Result Is Nothing) Then

                    readSerNumTask = Task.Run(Function() CanMessageLib.ReadSerialNumFromFlash())
                    Await readSerNumTask

                End If

                If (readSerNumTask.Result IsNot Nothing) Then

                    _serialNumber = readSerNumTask.Result

                End If

                ' Must assign before calling RunInitializationTasksAsync().
                _sensorState = state

                ' Load calibrations.
                Dim runInitTask = _visibleInstance.RunInitializationTasksAsync()
                Await runInitTask
            Else
                _sensorState = state
            End If

            If (Not (state = AppState.WaitingForCommand AndAlso
                     _scanSequenceState <> SweepSequenceState.Complete)) Then

                ' Don't try to display brief state changes to WaitingForCommand that occur during sweeps.
                _visibleInstance.Invoke(New MethodInvoker(AddressOf _visibleInstance.DisplayCurrentStatus))

                If (state = AppState.DiagShutdown) Then

                    If (_sweepStarted) Then

                        SweepDataProcessing.StopLogging = True
                        _visibleInstance.Invoke(New MethodInvoker(AddressOf _visibleInstance.StopScanAsync))
                        _sensorState = AppState.Undef
                        CanMessageLib.ClearRcvdState = True
                        CanMessageLib.ClearAppNodeID = True
                        _sweepStarted = False
                    End If
                End If
            End If

            If (state <> AppState.Reset)Then

                _outOfReset = True
            Else
                _outOfReset = False
            End If

            If (state = AppState.RunSingleScan) Then

                _sweepStarted  = True

            ElseIf (state = AppState.WaitingForCommand AndAlso _sweepStarted) Then

                ' Sweep completed.
                _sweepStarted  = False
                _runCount += 1
                SweepComplete()

            ElseIf (state = AppState.Reset AndAlso outOfReset) Then

                ' The sensor previously exited reset mode and now 
                RaiseEvent LogErrorEvent(New StackTrace,"StateChanged() Unexpected reset state value received.")

            End If

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Private Sub SweepRateTimer_Elapsed(sender As Object, e As Timers.ElapsedEventArgs)

        Try
            _sweepRateTimer.Enabled = False
            SyncLock _sweepRateLockingObj
                _sweepRateTimerElapsed = True
                If (_scanSequenceState = SweepSequenceState.Complete AndAlso
                Not _sweepRateTimerDisabled) Then

                    ContinueScan()
                End If
            End SyncLock

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Public Sub DisplayCurrentStatus()

        Dim stateStr As String
        static attemptLoadCals As Boolean

        If (_closing) Then

            LabelStatus.Text = "Terminating Sweep - Please Wait"

        ElseIf (Not CanConnected) Then

            LabelStatus.Text = "CAN Bus Disconnected"

        ElseIf (_canActivityTimeout) Then

            If (_calibrationFileRead) Then

                LabelStatus.Text = "RF Sensor Communications Lost"
            Else
                LabelStatus.Text = "RF Sensor Communications Undetected"
            End If

        ElseIf (_sensorState = AppState.DiagShutdown) Then

            LabelStatus.Text = "The RF Sensor has Entered the Diagnostic Shutdown State."

        ElseIf (_loadingCals) Then

            LabelStatus.Text = "Loading Calibrations"
            attemptLoadCals = True

        ElseIf (_sensorState = AppState.PowerUpScan) Then

            LabelStatus.Text = "The RF Sensor is running power-up scans, please wait."
        Else
            LabelStatus.Text = "Sensor: Node ID = "
            LabelStatus.Text &= If(_canNodeID = 0, String.Empty, _canNodeID.ToString())

            If (_serialNumber IsNot Nothing AndAlso _serialNumber.Length > 0) Then

                LabelStatus.Text &= ", S/N = "

                For Each number In _serialNumber
                    LabelStatus.Text &= number.ToString("X2")
                Next
            End If

            stateStr = If(_sensorState = AppState.Undef, String.Empty, CanMessageLib.AppStateDict(_sensorState))

            If (_calibrationFileRead) Then

                attemptLoadCals = False
                If (Not String.IsNullOrWhiteSpace(AppSoftwareID)) Then

                    LabelStatus.Text &= ", SW = " & AppSoftwareID
                End If
                If (Not String.IsNullOrWhiteSpace(AppSoftwareID)) Then

                    LabelStatus.Text &= ", App = " & _AppSoftwareVersion
                End If
                LabelStatus.Text &= ", State = " & stateStr

            ElseIf (attemptLoadCals)

                LabelStatus.Text &= ", State = " & stateStr
                LabelStatus.Text &= ", Calibration File Unavailable"
            Else

                LabelStatus.Text &= ", State = " & stateStr
            End If
        End If
    End Sub

    Public Sub SweepComplete()

        ' Called whenever the sensor state changes from RunSingleScan to WaitingForCommand.
        Try
            Select Case (_scanSequenceState)

                Case SweepSequenceState.First

                    _scanSequenceState = SweepSequenceState.Second

                    ' Configure the RF Unit to run measurement sweep 2nd frequency range.
                    If ((CanMessageLib.SendCommandMessage(CommandMsgType.ConfigureVCO, Common.DefaultStartFreqRange2) OrElse
                         CanMessageLib.SendCommandMessage(CommandMsgType.ConfigureVCO, Common.DefaultStartFreqRange2)) AndAlso
                        (CanMessageLib.SendCommandMessage(CommandMsgType.RunSingleScan, _scanOperatingMode) OrElse
                         CanMessageLib.SendCommandMessage(CommandMsgType.RunSingleScan, _scanOperatingMode))) Then

                        _sweepStartTime = Common.HighResTime
                    Else
                        RaiseEvent LogErrorEvent(New StackTrace, "Failed to start the second sweep in the sweep sequence.")
                    End If

                Case SweepSequenceState.Second

                    _scanSequenceState = SweepSequenceState.Final

                    ' Configure the RF Unit to run measurement sweep 3rd frequency range.
                    CommandedStepCount = Common.DefaultStepCountFinal
                    If ((CanMessageLib.SendCommandMessage(CommandMsgType.ConfigureVCO, Common.DefaultStartFreqRange3) OrElse
                         CanMessageLib.SendCommandMessage(CommandMsgType.ConfigureVCO, Common.DefaultStartFreqRange3)) AndAlso
                         (CanMessageLib.SendCommandMessage(CommandMsgType.ConfigStepParams, CommandedStepCount) OrElse
                          CanMessageLib.SendCommandMessage(CommandMsgType.ConfigStepParams, CommandedStepCount)) AndAlso
                         (CanMessageLib.SendCommandMessage(CommandMsgType.RunSingleScan, _scanOperatingMode) OrElse
                          CanMessageLib.SendCommandMessage(CommandMsgType.RunSingleScan, _scanOperatingMode))) Then

                        _sweepStartTime = Common.HighResTime
                    Else
                        RaiseEvent LogErrorEvent(New StackTrace, "Failed to start the third sweep in the sweep sequence.")
                    End If

                Case SweepSequenceState.Final
                    
                    ' The last sweep is complete.
                    Invoke(CType(Sub()
                                     ' Sweep time is in milliseconds. Scale it to seconds.
                                     TextBoxSweepDuration.Text = (_sweepTimeTotal / 1000).ToString("F1")
                                     TextBoxRunCount.Text = (_runCount \ 3).ToString()
                                     ProgressBarSweepProgress.Value = ProgressBarSweepProgress.Minimum
                                 End Sub, MethodInvoker))
                     ProgressBarSweepProgress.Value = ProgressBarSweepProgress.Minimum
                     SweepDataIndex      = 0
                     _sweepStepsComplete = 0
                     _sweepTimeTotal     = 0.0

                    SyncLock _sweepRateLockingObj
                        _scanSequenceState  = SweepSequenceState.Complete
                        If (_sweepRateTimerElapsed)

                            ' The timer interval value is less than the time it takes run a complete sweep.
                            ' So start the next sweep immediately.
                            _visibleInstance.Invoke(New MethodInvoker(AddressOf ContinueScan))
                        Else
                            _visibleInstance.Invoke(CType(Sub()
                                                              PictureBoxSweeping.Image = My.Resources.GreenLEDOff
                                                              PictureBoxIdle.Image     = My.Resources.GreenLEDOn
                                                          End Sub, MethodInvoker))
                        End If
                    End SyncLock

                Case Else
                    ' Unexpected, so terminate the sequence.
                    RaiseEvent LogErrorEvent(New StackTrace,
                                             "FormRFSensorMain.StateChange() unexpected sweep " &
                                             "sequence value: " & _scanSequenceState.ToString())
                    _scanSequenceState = SweepSequenceState.Complete
            End Select

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Public Sub CanNodeIDChanged(ByVal nodeID As Byte)
        _canNodeID = nodeID
        Invoke(New MethodInvoker(AddressOf _visibleInstance.DisplayCurrentStatus))
    End Sub

    Public Shared Sub CanConnect()

        ' CAN communications has been selected.
        If (ActiveCanDevice?.DeviceType = "PCAN USB") Then

            ' Peak CAN has been selected.
            If CanServicesObj.GetPCAN_DeviceOpen = PCANBasic.PCAN_NONEBUS OrElse
                   CAN_Common.PCAN_HandleStr(CanServicesObj.GetPCAN_DeviceOpen) <> ActiveCanDevice.PcanDesignation Then

                CanServicesObj.DisconnectCAN_Device()
                Dim pcanHandle = CAN_Common.PCAN_StrToHandle(ActiveCanDevice.PcanDesignation)
                CanServicesObj.SelectPCAN_Device()
                CanServicesObj.SetCAN_BaudRate(CInt(ActiveCanDevice.BitRate))
                CanServicesObj.FindPCAN_Devices()
                If CanServicesObj.GetPCAN_Devices.Count = 0 Then

                    ' No PCAN Devices are present.
                    MessageBox.Show("A Peak CAN device is selected," & vbCrLf & "but none were found.", "")
                    _canConnected = False
                Else

                    ' Connect to the selected PCAN Device if available.
                    _canConnected = CanServicesObj.ConnectPCAN_Device(pcanHandle)
                    If Not _canConnected Then
                        MessageBox.Show("Failed to connect to the selected" & vbCrLf & "Peak CAN device: " &
                                            ActiveCanDevice.PcanDesignation & ".")
                    End If
                End If
            End If
        ElseIf ActiveCanDevice?.DeviceType = "NICAN USB" Then

            ' NI CAN has been selected.
            If CanServicesObj.GetNiCAN_DeviceOpen = "NONE" OrElse
                   CanServicesObj.GetNiCAN_DeviceOpen <> ActiveCanDevice.NicanDesignation Then

                CanServicesObj.DisconnectCAN_Device()
                CanServicesObj.SelectNiCAN_Device()
                CanServicesObj.SetCAN_BaudRate(CInt(ActiveCanDevice.BitRate / 1000))
                Dim niObject = Integer.Parse(ActiveCanDevice.NicanDesignation.Substring(3))
                _canConnected = CanServicesObj.ConnectNiCAN_Device(niObject)
                If Not _canConnected Then
                    MessageBox.Show("Failed to connect to the selected" & vbCrLf & "NI CAN device: " &
                                    ActiveCanDevice.NicanDesignation & ".")
                End If
            End If
        Else
            If (ActiveCanDevice Is Nothing) Then
                MessageBox.Show("You must configure a CAN Device first.")
            Else
                MessageBox.Show("The CAN Device type is invalid: " &
                                ActiveCanDevice.DeviceType & ".")
            End If
            _canConnected = False
        End If
        If _canConnected Then
            _visibleInstance.Invoke(CType(Sub()
                                         _visibleInstance.ButtonCanEnable.Text = "Disconnect"
                                         _visibleInstance.ButtonCanEnable.ForeColor = Color.Orange 'Tomato
                                     End Sub, MethodInvoker))
        Else
            _visibleInstance.Invoke(CType(Sub()
                                         _visibleInstance.ButtonCanEnable.Text = "Connect"
                                         _visibleInstance.ButtonCanEnable.ForeColor = Color.Chartreuse
                                     End Sub, MethodInvoker))
        End If
    End Sub

    Private Sub ToolStripComboBox_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) _
            Handles ToolStripComboBoxOutputPower.KeyPress, ToolStripComboBoxOutputPowerAux.KeyPress,
                ToolStripComboBoxOperatingMode.KeyPress

        If (e.KeyChar = Chr(13)) Then
            CType(sender, ToolStripComboBox).Owner.Hide()
        End If
        e.Handled = True
    End Sub

    Private Sub TextBoxTestName_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles TextBoxTestName.KeyDown

        If e.Alt OrElse e.Control Then
            ' Prevents that annoying beep.
            e.SuppressKeyPress = True
            If e.KeyCode = Keys.U Then
                If _unlocked Then
                    e.Handled = True
                    Return
                Else If (Not _unlockEnabled)
                    TextBoxTestName.Text = ""
                    toolTip1.SetToolTip(TextBoxTestName, "")
                    TextBoxTestName.PasswordChar = "*"c
                    _unlockEnabled = True
                    _timerUnlockKeyPress.Start()
                    e.Handled = True
                    Return
                End If
                _unlocked = TextBoxTestName.Text = "CtsRfSensor"
                If _unlocked Then
                    ' Enable features that should not be visible to the customer.
                    ToolStripMenuItemFile.Visible = True
                    ToolStripMenuItemSweepSettings.Visible = True
                    If (Debugger.IsAttached) Then
                        ToolStripMenuItemConfigure.Visible = True
                    End If
                    toolTip1.SetToolTip(PrimaryMeasurementTextBox, "Double-click to edit.")
                    toolTip1.SetToolTip(SecondaryMeasurementTextBox, "Double-click to edit.")
                End If
                TextBoxTestName.Text = ""
                toolTip1.SetToolTip(TextBoxTestName, "")
                TextBoxTestName.PasswordChar = ControlChars.NullChar
                _unlockEnabled = False
                _timerUnlockKeyPress.Stop()
                e.Handled = True
                Return
            End If
            If Not _unlocked Then
                Return
            End If
            If e.KeyCode = Keys.L Then
                _unlockEnabled = False
                _unlocked = False
                ' Disable features that should not be visible to the customer.
                ToolStripMenuItemFile.Visible = False
                ToolStripMenuItemConfigure.Visible = False
                ToolStripMenuItemSweepSettings.Visible = False
                PrimaryMeasurementTextBox.Text = CustomerSettingsList(0).PrimaryMeasurementLabel
                LabelEditCancelledOrComplete(PrimaryMeasurementTextBox)
                PrimaryMeasurementTextBox.ContextMenuStrip = Nothing
                toolTip1.SetToolTip(PrimaryMeasurementTextBox, String.Empty)
                SecondaryMeasurementTextBox.Text = CustomerSettingsList(0).SecondaryMeasurementLabel
                LabelEditCancelledOrComplete(SecondaryMeasurementTextBox)
                SecondaryMeasurementTextBox.ContextMenuStrip = Nothing
                toolTip1.SetToolTip(SecondaryMeasurementTextBox, String.Empty)
                TextBoxTestName.Text = ""
                e.Handled = True
            End If
        Else
            If (_unlockEnabled) Then
                _timerUnlockKeyPress.Stop()
                _timerUnlockKeyPress.Start()
            End If
        End If
    End Sub

    Private Sub TextBoxTestName_LostFocus(sender As Object, e As EventArgs) Handles TextBoxTestName.LostFocus

        If (_unlockEnabled) Then
            _timerUnlockKeyPress.Stop()
            TextBoxTestName.Text = ""
            toolTip1.SetToolTip(TextBoxTestName, "")
            TextBoxTestName.PasswordChar = ControlChars.NullChar
            _unlockEnabled = False
        End If
    End Sub

    Private Sub TimerUnlockKeyPress_Elapsed(sender As Object, e As Timers.ElapsedEventArgs)

        Try
            _timerUnlockKeyPress.Stop()
            TextBoxTestName.Text = ""
            toolTip1.SetToolTip(TextBoxTestName, "")
            TextBoxTestName.PasswordChar = ControlChars.NullChar
            _unlockEnabled = False

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Private Sub TimerCanActivityTimeout_Elapsed(sender As Object, e As Timers.ElapsedEventArgs)

        Try
            If (_sweepStarted) Then
                _timerCanActivityTimeout.Stop()
                SweepDataProcessing.StopLogging = True
                StopScanAsync()
                _sensorState = AppState.Undef
                _sweepStarted = False
            End If
            _canNodeID = 0
            CanMessageLib.ClearRcvdState = True
            CanMessageLib.ClearAppNodeID = True
            _AppSoftwareID = String.Empty
            _SerialNumber = New Byte() {}
            _canActivityTimeout = True
            DisplayCurrentStatus()

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Friend Sub LaunchFlashDeviceForm()
        Dim flash = New FormFlashDevice()
        flash.ShowDialog()
    End Sub

    Public Sub SaveConfiguration()

        ' Convert the configuration settings Lists to XML.
        Dim configDoc      = New XDocument()
        Dim configFilePath = String.Empty

        Try

            ConfigToXml(configDoc)
            If _saveToXml Then ' AndAlso Unlocked Then
                configFilePath = Application.StartupPath & "\" & My.Settings.XmlConfigurationFile
                If File.Exists(configFilePath) Then

                    ' Save the configuration data to an XML file.
                    configDoc.Save(configFilePath)
                End If
            Else
                ' Save the configuration encrypted XML data to binary file.
                configFilePath = Application.StartupPath & "\" & My.Settings.BinConfigurationFile
                FormFileEncryption.EncryptAndSaveXmlFile(configDoc, configFilePath)
            End If

        Catch ex As Exception
            MsgBox("Failed to save configuration data to file: " & vbCrLf & configFilePath,
                   MsgBoxStyle.SystemModal + MsgBoxStyle.Exclamation)
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Public Shared Sub ConfigToXml(ByRef configDoc As XDocument)

        ' Create the root node.
        configDoc.Add(New XElement("Configurations"))

        ' Convert each configuration class collection to XML and add it
        ' to the XML document.

        For Each kvpCanCommunication In CanCommunications
            configDoc.Root?.Add(kvpCanCommunication.Value.ToXml())
        Next kvpCanCommunication

        For Each custSettings In CustomerSettingsList
            configDoc.Root?.Add(custSettings.ToXml())
        Next custSettings

        ' Currently only one RunConfiguration.
        configDoc.Root?.Add(RunConfigurations.ToXml())

        For Each kvpMeasurement In Measurements
            configDoc.Root?.Add(kvpMeasurement.Value.ToXml())
        Next kvpMeasurement

        For Each kvpRegionOfInterest In RegionsOfInterest
            configDoc.Root?.Add(kvpRegionOfInterest.Value.ToXml())
        Next kvpRegionOfInterest
    End Sub

    Private Sub XmlToConfig(ByRef doc As XDocument)

        ' Will be cast as appropriate configuration class type.
        Dim configuration As Object = Nothing
        Dim configName    As String = Nothing
        Dim classType     As Type   = Nothing
        Dim index         As Integer
        Dim lastConfigElement As XElement = Nothing
        Dim dictConfigPropertyDict = ConfigProperties
        Dim rootNodes              = doc?.Root?.DescendantNodes().OfType(Of XElement)()

        ' Parse the Configuration XML document.
        If rootNodes Is Nothing Then
            Return
        End If
        Dim xElements = If(TryCast(rootNodes, IList(Of XElement)), rootNodes.ToList())
        For Each ele In xElements.Where(Function(element) element.HasElements)
            configName = ele.Name.ToString()
            If configName.Contains("}") Then

                ' Strip the XML namespace, if one is present.
                index = configName.LastIndexOf("}", StringComparison.Ordinal) + 1
                If configName.Length > index Then
                    configName = configName.Substring(index, configName.Length - index)
                End If
            End If
            If (ConfigClasses.ContainsKey(configName)) Then

                ' Valid configuration name.
                classType = ConfigClasses(configName)
                If (classType IsNot Nothing) Then

                    ' Instantiate new configuration class.
                    configuration = Activator.CreateInstance(classType)

                    ' Calls either the base class deserializer or the deserializer
                    ' implemented by the configuration class currently being deserialized.
                    Dim configClass = configuration.FromXML(ele)
                    StoreConfiguration(configName, configClass)
                    lastConfigElement = ele
                End If
            Else
                ' Check to see if this XElement is a descendant of the previous XElement
                ' which represwented a configuration class. If so, ignore it.
                If (lastConfigElement Is Nothing OrElse
                    Not lastConfigElement.Descendants.Contains(ele)) Then

                    ' Unrecognized configuration name.
                    RaiseEvent LogErrorEvent(New StackTrace, "Found an unrecognized configuration name: " &
                                             configName & vbCrLf & "in the configuration file.")
                End If
            End If
        Next
    End Sub

    Private Function XmlToCal(ByRef doc As XDocument) As Boolean
        Try
            Dim value As String
            Dim xElements = doc?.Root?.DescendantNodes().OfType(Of XElement)().ToList()

            ' Parse the Calibrations XML document.
            If xElements Is Nothing Then
                Return False
            End If

            ' Load the Calibrations dictionaries.
            RfSensorConfiguration.AppCalDict = New ConcurrentDictionary(Of UShort, AppCalibration)()
            RfSensorConfiguration.UsageDataDict = New ConcurrentDictionary(Of String, UsageData)()
            RfSensorConfiguration.E2MapDict = New ConcurrentDictionary(Of String, E2MapEntry)()
            RfSensorConfiguration.ParamTblDict = New ConcurrentDictionary(Of String, ParamTblEntry)()

            For Each ele As XElement In xElements
                Dim name = ele.Name.ToString().ToUpper()
                If name = "CALNUM" Then
                    For Each descElement As Object In ele.Descendants()
                        If descElement.Name.ToString().ToUpper() = "CALIDNUM" Then
                            value = descElement.Value.ToUpper()

                            If Not (value.Contains("TBD") OrElse
                                    value.Contains("RESERVED") OrElse
                                    value.Contains("SPARE")) Then

                                Dim calID As UShort
                                If ((UShort.TryParse(descElement.Value, calID)) AndAlso
                                    (Not RfSensorConfiguration.AppCalDict.TryAdd(descElement.Value,
                                                                                 New AppCalibration(ele)))) Then
                                    RaiseEvent LogErrorEvent(New StackTrace,
                                                             "Failed to parse App Cal xml element:" &
                                                             vbCrLf & ele.Value & " in file: " & _configCalsFileName)
                                    Exit For
                                End If
                            End If
                            Exit For
                        End If
                    Next
                ElseIf name = "ITEM" Then
                    For Each descElement As Object In ele.Descendants()
                        If descElement.Name.ToString().ToUpper() = "VARNAME" Then
                            value = descElement.Value.ToUpper()

                            If Not (value.Contains("TBD") OrElse
                                        value.Contains("RESERVED") OrElse
                                        value.Contains("SPARE")) Then

                                If Not RfSensorConfiguration.UsageDataDict.TryAdd(descElement.Value, New UsageData(ele)) Then
                                    RaiseEvent LogErrorEvent(New StackTrace, "Failed to parse Usage Data xml element:" &
                                                             vbCrLf & ele.Value & " in file: " & _configCalsFileName)
                                    Exit For
                                End If
                            End If
                            Exit For
                        End If
                    Next
                ElseIf name = "ENTRY" Then
                    For Each descElement As Object In ele.Descendants()
                        If descElement.Name.ToString().ToUpper() = "VNAME" Then
                            value = descElement.Value.ToUpper()

                            If Not (value.Contains("TBD") OrElse
                                        value.Contains("RESERVED") OrElse
                                        value.Contains("SPARE")) Then

                                If Not RfSensorConfiguration.E2MapDict.TryAdd(descElement.Value, New E2MapEntry(ele)) Then
                                    RaiseEvent LogErrorEvent(New StackTrace, "Failed to parse E2 Map xml element:" &
                                                             vbCrLf & ele.Value & " in file: " & _configCalsFileName)
                                    Exit For
                                End If
                            End If
                            Exit For
                        End If
                    Next
                ElseIf name = "PARAMETER" Then
                    For Each descElement As Object In ele.Descendants()
                        If descElement.Name.ToString().ToUpper() = "TOOLNAME" Then
                            value = descElement.Value.ToUpper()

                            If Not (value.Contains("TBD") OrElse
                                        value.Contains("RESERVED") OrElse
                                        value.Contains("SPARE")) Then

                                If Not RfSensorConfiguration.ParamTblDict.TryAdd(descElement.Value, New ParamTblEntry(ele)) Then
                                    RaiseEvent LogErrorEvent(New StackTrace, "Failed to parse Param Table xml element:" &
                                                             vbCrLf & descElement.Value & " in file: " & _configCalsFileName)
                                    Exit For
                                End If
                            End If
                            Exit For
                        End If
                    Next
                End If
            Next
            Return True

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Function

    Private Shared Sub StoreConfiguration(ByVal configName As String, ByVal configuration As Object)
        Select Case configName
            Case "CanCommunication"
                If Not String.IsNullOrEmpty(DirectCast(configuration, CanCommunication).Description) Then
                    CanCommunications.TryAdd(DirectCast(configuration, CanCommunication).Description,
                                                 DirectCast(configuration, CanCommunication))
                End If
            Case "CustomerConfiguration"
                If Not String.IsNullOrEmpty(DirectCast(configuration, CustomerConfiguration).PrimaryMeasurementLabel) Then
                    CustomerSettingsList.Add(DirectCast(configuration, CustomerConfiguration))
                End If
            Case "RunConfiguration"
                If Not String.IsNullOrEmpty(DirectCast(configuration, RunConfiguration).OperatingMode) Then
                    RunConfigurations = DirectCast(configuration, RunConfiguration)
                End If
            Case "Measurement"
                If (DirectCast(configuration, Measurement).Name IsNot Nothing) AndAlso
                        (Not String.IsNullOrEmpty(DirectCast(configuration, Measurement).Name)) Then

                    Measurements.TryAdd(DirectCast(configuration, Measurement).Name, DirectCast(configuration, Measurement))
                End If
            Case "RegionOfInterest"
                If (Not String.IsNullOrEmpty(DirectCast(configuration, RegionOfInterest).Name)) Then
                    RegionsOfInterest.TryAdd(DirectCast(configuration, RegionOfInterest).Name,
                                             DirectCast(configuration, RegionOfInterest))
                End If
        End Select
    End Sub

    Private Shared Sub GetActiveMeasurement(ByRef measureConfig As Measurement)

        ' Find the active Measurement configuration, if there is one.
        measureConfig = Measurements.Where(Function(measure) measure.Value.Active).
                Select(Function(kvpMeasurement) kvpMeasurement.Value).FirstOrDefault()
    End Sub

    Public Shared Sub LoadActiveMeasurement()

        ActiveMeasurement = Nothing

        ' Load the active Measurement configuration, if there is one.
        For Each kvpMeasurement In Measurements.Where(Function(measure) measure.Value.Active)
            ' There should only be one active.
            ActiveMeasurement = kvpMeasurement.Value
            Exit For
        Next kvpMeasurement
    End Sub

    Public Shared Sub LoadActiveCanCommunication()

        ActiveCanDevice = Nothing
        If CanCommunications.Count = 0 Then
            Return
        End If
        For Each can_Commun In
                From canCom In CanCommunications
                Where canCom.Value.Active
                Select canCom.Value
            ActiveCanDevice = can_Commun
            Exit For
        Next can_Commun
    End Sub

    Private Function LoadCalibrationsFile() As Boolean

        ' Send a Param Read message to get the information necessary to select
        ' and read the appropriate Calibration XML file.
        Dim result As Boolean

        Try
            Dim values As Byte()   = {}
            Dim params As String() = {CanMessageLib.SoftwareID,
                                      CanMessageLib.SoftwareRevLevel,
                                      CanMessageLib.SoftwareDevLevel}
            Dim decryptorStream As CryptoStream = Nothing

            If (CanMessageLib.SendParamReadMsg(params, values) OrElse
                CanMessageLib.SendParamReadMsg(params, values)) Then

                If (values.Length = params.Length AndAlso
                    values(0) > 0 AndAlso ((values(0) >> 4) <= (Common.MicroIdStr.Length - 1))) Then

                    _AppSoftwareID =(values(0) >> 4).ToString() & "." & values(1).ToString() & "."
                    _configCalsFileName = "RFS_" & Common.MicroIdStr(values(0) >> 4) &
                                          (values(0) And &HF).ToString("D2") & "_R" &
                                          (values(0) >> 4).ToString() & "_" &
                                           values(1).ToString() & "_" & Chr(values(2))

                    If File.Exists(Application.StartupPath & "\" & _configCalsFileName & ".xml") Then

                        _configCalsFileName &= ".xml"
                        If FileSystem.FileLen(_configCalsFileName) > 0 Then

                            ' Load calibrations from xml file into an XML document.
                            Dim doc = XDocument.Load(_configCalsFileName)

                            ' Load calibrations from the xml document.
                            If Not XmlToCal(doc) Then
                                RaiseEvent LogErrorEvent(New StackTrace(),"Failed to load the RF Sensor " &
                                                         "Calibrations file: " & _configCalsFileName)
                            Else
                                result = True
                            End If
                        Else
                            RaiseEvent LogErrorEvent(New StackTrace(), "The calibration XML file: " &
                                                     Application.StartupPath & "\" & _configCalsFileName & " is empty!")
                        End If
                    ElseIf File.Exists(Application.StartupPath & "\" & _configCalsFileName & ".bin") Then

                        _configCalsFileName &= ".bin"
                        If FileSystem.FileLen(Application.StartupPath & "\" & _configCalsFileName) > 0 Then

                            Dim doc As XDocument = Nothing

                            ' Decrypt the Calibrations file into an XML document.
                            FormFileEncryption.GetDecryptedXmlFile(doc, Application.StartupPath & "\" & _configCalsFileName)

                            ' Load calibrations from the xml document.
                            If Not XmlToCal(doc) Then
                                RaiseEvent LogErrorEvent(New StackTrace(), "Failed to load the RF Sensor " &
                                                         "Calibrations file: " & _configCalsFileName)
                            Else
                                result = True
                            End If
                        Else
                            RaiseEvent LogErrorEvent(New StackTrace(), "The calibration binary file: " &
                                                     Application.StartupPath & "\" & _configCalsFileName & " is empty!")
                        End If
                    Else
                        RaiseEvent LogErrorEvent(New StackTrace(), "The calibration file: " & Application.StartupPath &
                                                 "\" + _configCalsFileName & ".bin/xml was not found!")
                    End If
                End If
            Else
                RaiseEvent LogErrorEvent(New StackTrace(),"Failed to read the RF Sensor Calibrations file. " &
                                         "Could not determine the calibration file name.")
            End If

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try

        If (result) Then
            _calibrationFileRead = True
        Else
            MsgBox("Failed to load a calibration file.", MsgBoxStyle.SystemModal)
            _calibrationFileRead = False
        End If

        Return result
    End Function

    Private Sub ButtonSelectOutputFolder_Click(ByVal sender As Object, ByVal e As EventArgs) _
            Handles ButtonSelectOutputFolder.Click

        FolderBrowserDialogOutputDirectory.SelectedPath = TextBoxDataOutputFolder.Text
        If (FolderBrowserDialogOutputDirectory.ShowDialog() = System.Windows.Forms.DialogResult.OK) Then

            TextBoxDataOutputFolder.Text = FolderBrowserDialogOutputDirectory.SelectedPath
            toolTip1.SetToolTip(TextBoxDataOutputFolder, FolderBrowserDialogOutputDirectory.SelectedPath)
        End If
    End Sub

    ' Last chance exception handler
    ' Handle unhandled exceptions by showing a dialog box, and giving the user
    ' a chance to retry or abort execution.
    Private Shared Sub LastChanceHandler(ByVal sender As Object, ByVal args As UnhandledExceptionEventArgs)

        Dim ex = DirectCast(args.ExceptionObject, Exception)
        Dim result = MsgBoxResult.Ignore

        Try
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
            result = MsgBox("An unhandled exception has occurred." & vbCrLf &
                            "Please notify the application developer" & vbCrLf &
                            "and include the following information: " & vbCrLf & ex.ToString(),
                            MsgBoxStyle.SystemModal Or MsgBoxStyle.Critical Or MsgBoxStyle.RetryCancel)
        Catch
            Try
                MsgBox("Encountered fatal Windows forms error." & vbCrLf &
                       "This application will now terminate.",
                       MsgBoxStyle.SystemModal Or MsgBoxStyle.Critical Or MsgBoxStyle.OkOnly,
                       "Fatal Windows Forms Error!")
            Finally
                Application.Exit()
            End Try
        End Try
        ' Exits the program when the user clicks Cancel.
        If result = MsgBoxResult.Cancel Then
            Application.Exit()
        End If
    End Sub

    ' Handle the UI exceptions by showing a dialog box, and giving the user
    ' a chance to retry or abort execution.
    Private Shared Sub UIThreadExceptionHandler(ByVal sender As Object, ByVal t As ThreadExceptionEventArgs)

        Dim result = MsgBoxResult.Ignore

        Try
            ActivityLog.LogError(t.Exception.Message & vbCrLf & t.Exception.StackTrace)
            result = MsgBox("An unhandled exception has occurred." & vbCrLf &
                            "Please notify the application developer" & vbCrLf &
                            "and include the following information: " & vbCrLf &
                            t.Exception.ToString(), MsgBoxStyle.SystemModal Or MsgBoxStyle.Critical Or
                            MsgBoxStyle.RetryCancel)
        Catch
            Try
                MsgBox("Encountered fatal Windows forms error." & vbCrLf &
                       "This application will now terminate.",
                       MsgBoxStyle.SystemModal Or MsgBoxStyle.Critical Or MsgBoxStyle.OkOnly,
                       "Fatal Windows Forms Error!")
            Finally
                Application.Exit()
            End Try
        End Try
        ' Exits the program when the user clicks Cancel.
        If result = MsgBoxResult.Cancel Then
            Application.Exit()
        End If
    End Sub

    Private Sub PanelChartAdjustment_Resize(ByVal sender As Object, ByVal e As EventArgs) Handles PanelChartAdjustments.Resize

        ButtonScrollPlotRight.Left = (PanelChartAdjustments.Width - ButtonScrollPlotRight.Width) \ 2
        ButtonScrollPlotLeft.Left = ButtonScrollPlotRight.Right + 6
        ButtonExpandPlot.Left = ButtonScrollPlotRight.Left - 6 - ButtonExpandPlot.Width
    End Sub

    Private Sub SetSweepInterval(ByVal sweepInterval As UInteger)

        ' Set the SweepInterval in the Run Configuration.
        ' We currently allow only one Run Configuration.
        RunConfigurations.SweepInterval = sweepInterval
    End Sub

    Private Sub SetOutputDirectory(ByVal dirName As String)

        ' Set the OutputDirectoryName in the Customer Settings Configuration.
        ' We currently allow only one Customer Settings Configuration.
        CustomerSettingsList(0).OutputFileDirectory = dirName
    End Sub

    Private Sub SetOutputFileName(ByVal fileName As String)

        ' Set the OutputFileName in the Customer Settings Configuration.
        ' We currently allow only one Customer Settings Configuration.
        CustomerSettingsList(0).OutputFileName = fileName
    End Sub

    Private Sub ButtonClearPlot_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonClearPlot.Click

        If (ButtonClearPlot.ForeColor = Color.Chartreuse) Then
            ChartSootLoad.Series(0).Points.Clear()
            'ButtonExpandPlot.Enabled      = False
            ButtonExpandPlot.ForeColor     = Color.Orange
            'ButtonScrollPlotLeft.Enabled  = False
            ButtonScrollPlotLeft.ForeColor = Color.Orange
            'ButtonCompressPlot.Enabled     = False
            ButtonCompressPlot.ForeColor   = Color.Orange
            'ButtonClearPlot.Enabled       = False
            ButtonClearPlot.ForeColor      = Color.Orange
            _chartMaximumIndex             = 0
            _chartMaximumIndex             = 0
            _chartAutoSize                 = True
        End If
    End Sub

    Private Sub ButtonExpandPlot_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonExpandPlot.Click

        Dim midPoint = (ChartMinimumIndex + ChartMaximumIndex) \ 2

        If (ButtonExpandPlot.ForeColor = Color.Chartreuse) Then
            ChartMinimumIndex = (ChartMinimumIndex + midPoint) \ 2
            ChartMaximumIndex = (ChartMaximumIndex + midPoint) \ 2
        End If
    End Sub

    Private Sub ButtonCompressPlot_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonScrollPlotLeft.Click

        Dim chartWidth = ChartMaximumIndex - ChartMinimumIndex

        If (ButtonScrollPlotLeft.ForeColor = Color.Chartreuse) Then
            ChartMinimumIndex -= chartWidth \ 2
            ChartMaximumIndex += chartWidth \ 2
        End If
    End Sub

    Private Sub ButtonScrollPlotLeft_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonCompressPlot.Click

        Dim chartWidth = ChartMaximumIndex - ChartMinimumIndex

        If (ButtonCompressPlot.ForeColor = Color.Chartreuse) Then
            ChartMinimumIndex -= If(chartWidth \ 4 = 0, 1, chartWidth \ 4)
            ChartMaximumIndex = ChartMinimumIndex + chartWidth
        End If
    End Sub

    Private Sub ButtonScrollPlotRight_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonScrollPlotRight.Click

        Dim chartWidth = ChartMaximumIndex - ChartMinimumIndex

        If (ButtonScrollPlotRight.ForeColor = Color.Chartreuse) Then
            ChartMaximumIndex += If(chartWidth \ 4 = 0, 1, chartWidth \ 4)
            ChartMinimumIndex = ChartMaximumIndex - chartWidth
        End If
    End Sub

    Private Sub TextBoxDataOutputFolder_TextChanged(ByVal sender As Object, ByVal e As EventArgs) _

        SetOutputDirectory(TextBoxDataOutputFolder.Text)
    End Sub

    Private Sub TextBoxTestName_TextChanged(ByVal sender As Object, ByVal e As EventArgs)
        If (Not _unlockEnabled) Then
            SetOutputFileName(TextBoxTestName.Text)
            toolTip1.SetToolTip(TextBoxTestName, CustomerSettingsList(0).OutputFileName)
        End If
    End Sub

    Private Sub ToolStripComboBoxOutputPower_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) _
        Handles ToolStripComboBoxOutputPower.SelectedIndexChanged

        Dim power As OutputPowerLevel = Nothing

        If ToolStripComboBoxOutputPower.SelectedIndex = -1 Then
            Return
        End If

        Dim idx = DirectCast(ToolStripComboBoxOutputPower.SelectedItem, String).IndexOf("d"c)
        Dim pwrStr = DirectCast(ToolStripComboBoxOutputPower.SelectedItem, String).Substring(0, idx)

        If CanMessageLib.GetOutputPower(pwrStr, power) Then
            RunConfigurations.OutputPower = power
        End If
    End Sub

    Private Sub ToolStripComboBoxAuxOutputPower_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) _
        Handles ToolStripComboBoxOutputPowerAux.SelectedIndexChanged

        Dim auxPower As OutputPowerLevel = Nothing

        If ToolStripComboBoxOutputPowerAux.SelectedIndex = -1 Then
            Return
        End If

        Dim idx = DirectCast(ToolStripComboBoxOutputPowerAux.SelectedItem, String).IndexOf("d"c)
        Dim pwrStr = DirectCast(ToolStripComboBoxOutputPowerAux.SelectedItem, String).Substring(0, idx)

        If CanMessageLib.GetOutputPower(pwrStr, auxPower) Then
            RunConfigurations.AuxOutputPower = auxPower
        End If
    End Sub
    Private Sub MeasurementTextBox_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) _
            Handles SecondaryMeasurementTextBox.KeyDown, PrimaryMeasurementTextBox.KeyDown

        If (_unlocked AndAlso Not e.Control) Then

            If (Not e.Shift AndAlso e.KeyCode <> Keys.Enter) Then

                If e.KeyCode = Keys.Escape Then

                    ' The Primary or Secondary Measurement label edit is being cancelled.
                    If ReferenceEquals(sender, PrimaryMeasurementTextBox) Then

                        PrimaryMeasurementTextBox.Text = CustomerSettingsList(0).PrimaryMeasurementLabel
                        LabelEditCancelledOrComplete(PrimaryMeasurementTextBox)
                    Else
                        SecondaryMeasurementTextBox.Text = CustomerSettingsList(0).SecondaryMeasurementLabel
                        LabelEditCancelledOrComplete(SecondaryMeasurementTextBox)
                    End If
                End If
            ElseIf (e.KeyCode = Keys.Enter) Then

                ' The primary or secondary measurement label edit is being saved.
                SaveMeasurementLabel(sender)
            End If
        End If
    End Sub

    Private Sub SaveMeasurementLabel(ByVal sender As Object)

        If (ReferenceEquals(sender, PrimaryMeasurementTextBox)) Then

            ' Set the DPF file prefix based on the primary measurement label.
            Dim strEnd = PrimaryMeasurementTextBox.Text.IndexOfAny({"["c, "("c, "{"c})
            If strEnd < 0 Then

                ' No brackets or parens present.
                strEnd = PrimaryMeasurementTextBox.Text.Length
            End If
            If strEnd > 0 Then
                strEnd -= 1

                ' Store the new file name prefix minus measurement units
                ' in brackets, braces or paremtheses.
                ' Substitute underscores for spaces.
                Dim fileNamePrefix = PrimaryMeasurementTextBox.Text.Substring(0, strEnd).TrimEnd(" "c).Replace(" "c, "_"c)

                ' Check for illegal file name characters.
                Dim invChars = Path.GetInvalidFileNameChars()
                For Each ch In invChars
                    If fileNamePrefix.IndexOf(ch) > 0 Then
                        MessageBox.Show("The following chracter: '" & ch & "' is invalid!")
                        Return
                    End If
                Next ch

                ' If necessary, place an underscore at the end of the file prefix name.
                If Not fileNamePrefix.EndsWith("_") Then
                    fileNamePrefix &= "_"
                End If
                ' Save editted Primary measurement label.
                CustomerSettingsList(0).FileNamePrefix = fileNamePrefix
                _primaryMeasurementLabel = PrimaryMeasurementTextBox.Text.Trim(" "c)

                ' Adding leading space just to adjust appearance.
                CustomerSettingsList(0).PrimaryMeasurementLabel = _primaryMeasurementLabel
                PrimaryMeasurementTextBox.Text = CustomerSettingsList(0).PrimaryMeasurementLabel
                labelAboveMaxDetectable.Text = fileNamePrefix.Replace("_", " ") & "High"
                labelBelowMinDetectable.Text = fileNamePrefix.Replace("_", " ") & "Low"
                Dim area = ChartSootLoad.ChartAreas.FindByName("ChartArea1")
                If area IsNot Nothing Then
                    ChartSootLoad.ChartAreas.Item("ChartArea1").AxisY.Title = CustomerSettingsList(0).PrimaryMeasurementLabel
                End If
                LabelEditCancelledOrComplete(PrimaryMeasurementTextBox)
            Else
                MessageBox.Show("No valid characters found!" & vbCrLf &
                                "Only measurement units should be" & vbCrLf &
                                "surrounded by brackets, braces or parentheses.")
            End If
        Else
            ' Save editted Secondary measurement label.
            ' Adding leading space just to adjust appearance.
            CustomerSettingsList(0).SecondaryMeasurementLabel = SecondaryMeasurementTextBox.Text.Trim(" "c)
            SecondaryMeasurementTextBox.Text = CustomerSettingsList(0).SecondaryMeasurementLabel
            LabelEditCancelledOrComplete(SecondaryMeasurementTextBox)
        End If
    End Sub

    Private Sub PrimaryMeasurementTextBox_DoubleClick(sender As Object, e As EventArgs) Handles PrimaryMeasurementTextBox.DoubleClick

        If (_unlocked) Then
            PrimaryMeasurementTextBox.ReadOnly = False
            PrimaryMeasurementTextBox.ForeColor = Color.Teal

            ' Cancel edit of Secondary measurement label, if necessary.
            If (SecondaryMeasurementTextBox.ReadOnly = False) Then
                SecondaryMeasurementTextBox.Text = CustomerSettingsList(0).SecondaryMeasurementLabel
                LabelEditCancelledOrComplete(SecondaryMeasurementTextBox)
            End If
        End If
    End Sub

    Private Sub PrimaryMeasurementTextBox_LostFocus(sender As Object, e As EventArgs) Handles PrimaryMeasurementTextBox.LostFocus

        If (_unlocked) Then
            PrimaryMeasurementTextBox.Text = CustomerSettingsList(0).PrimaryMeasurementLabel
            LabelEditCancelledOrComplete(PrimaryMeasurementTextBox)
        End If
    End Sub

    Private Sub SecondaryMeasurementTextBox_DoubleClick(sender As Object, e As EventArgs) Handles SecondaryMeasurementTextBox.DoubleClick

        If (_unlocked) Then
            SecondaryMeasurementTextBox.ReadOnly = False
            SecondaryMeasurementTextBox.ForeColor = Color.Teal

            ' Cancel edit of Primary measurement label, if necessary.
            If (PrimaryMeasurementTextBox.ReadOnly = False) Then
                PrimaryMeasurementTextBox.Text = CustomerSettingsList(0).PrimaryMeasurementLabel
                LabelEditCancelledOrComplete(PrimaryMeasurementTextBox)
            End If
        End If
    End Sub

    Private Sub SecondaryMeasurementTextBox_LostFocus(sender As Object, e As EventArgs) Handles SecondaryMeasurementTextBox.LostFocus

        If (_unlocked) Then
            SecondaryMeasurementTextBox.Text = CustomerSettingsList(0).SecondaryMeasurementLabel
            LabelEditCancelledOrComplete(SecondaryMeasurementTextBox)
        End If
    End Sub

    Private Sub LabelEditCancelledOrComplete(ByVal measureTestBox As TextBox)

        ' Called when a Primary or Secondary Measurement label edit is cancelled or save.
        If ReferenceEquals(measureTestBox, PrimaryMeasurementTextBox) Then

            PrimaryMeasurementTextBox.ReadOnly = True
            PrimaryMeasurementTextBox.ForeColor = Color.Black

        ElseIf ReferenceEquals(measureTestBox, SecondaryMeasurementTextBox) Then

            SecondaryMeasurementTextBox.ReadOnly = True
            SecondaryMeasurementTextBox.ForeColor = Color.Black
        End If
    End Sub

    Private Sub ButtonCanConfig_Click(sender As Object, e As EventArgs) Handles ButtonCanConfig.Click
        LaunchCAN_DeviceConfigAsync()
    End Sub

    Private Sub ButtonCanConfig_MouseEnter(sender As Object, e As EventArgs) Handles ButtonCanConfig.MouseEnter
        ButtonCanConfig.ForeColor = Color.Orange
    End Sub

    Private Sub ButtonCanConfig_MouseLeave(sender As Object, e As EventArgs) Handles ButtonCanConfig.MouseLeave
        ButtonCanConfig.ForeColor = Color.Chartreuse
    End Sub

    Private Sub LabelCtsWebAddress_Click(sender As Object, e As EventArgs) Handles LabelCtsWebAddress.Click

        ' Uses the default browser to navigate to the URL in the tag property.
        Process.Start(CType(sender, Label).Tag.ToString())

        ' Change the color to indicate that the link has been Visited.
        LabelCtsWebAddress.ForeColor = Color.DarkMagenta
    End Sub

    Friend Async Sub LaunchCAN_DeviceConfigAsync()

        Try
            Dim dResult = DialogResult.No
            Dim readTask As Task(Of Boolean)
            Dim canDeviceConfiguration = New FormCanDeviceConfig()

            dResult = canDeviceConfiguration.ShowDialog()

            If (dResult = DialogResult.Yes) Then

                CanConnect()
                If (_canConnected) Then

                    ButtonCanConfig.Enabled = False
                    ButtonCanEnable.Enabled = False
                    readTask = RunInitializationTasksAsync()
                    Await readTask
                End If
            End If

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        Finally
            ButtonCanConfig.Enabled = True
            ButtonCanEnable.Enabled = True
        End Try
    End Sub

    Private Async Sub ButtonCanEnable_Click(sender As Object, e As EventArgs) Handles ButtonCanEnable.Click

        Try
            Dim mbResult = MsgBoxResult.Ignore
            Dim readTask As Task(Of Boolean)

            If (ButtonCanEnable.Text = "Connect") Then
                CanConnect()
                If (_canConnected) Then
                    ButtonCanConfig.Enabled = False
                    ButtonCanEnable.Enabled = False
                    readTask = RunInitializationTasksAsync()
                    Await readTask
                    _readStateTimerDisabled = False
                    _timerReadSensorState.Enabled = True
                End If
            Else
                If (_scanSequenceState <> SweepSequenceState.Complete) Then
                    mbResult = MsgBox("Sweep In Progress." & vbCrLf &
                                      "Do you want to terminate the Sweep?",
                                      MsgBoxStyle.SystemModal + MsgBoxStyle.YesNo)
                    If (mbResult = MsgBoxResult.Yes) Then
                        _timerCanActivityTimeout.Stop()
                        SweepDataProcessing.StopLogging = True
                        StopScanAsync()
                        _readStateTimerDisabled = True
                        _timerReadSensorState.Enabled = False
                        CanServicesObj.DisconnectCAN_Device()
                        _canConnected = False
                        _sweepStarted = False
                        ButtonCanEnable.Text = "Connect"
                        ButtonCanEnable.ForeColor = Color.Chartreuse
                    End If
                Else
                    _readStateTimerDisabled = True
                    _timerReadSensorState.Enabled = False
                    CanServicesObj.DisconnectCAN_Device()
                    _canConnected = False
                    ButtonCanEnable.Text = "Connect"
                    ButtonCanEnable.ForeColor = Color.Chartreuse
                End If
                _sensorState = AppState.Undef
                CanMessageLib.ClearRcvdState = True
                CanMessageLib.ClearAppNodeID = True
                _calibrationFileLoaded = False
            End If

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        Finally
            ButtonCanConfig.Enabled = True
            ButtonCanEnable.Enabled = True
            DisplayCurrentStatus()
        End Try
    End Sub

    Private Sub ToolStripMenuItemDisableCalibration_Click(sender As Object, e As EventArgs) _
        Handles ToolStripMenuItemDisableCalibration.Click

        If (ToolStripMenuItemDisableCalibration.Checked) = True Then
            ToolStripMenuItemDisableCalibration.Checked = False
            _calibrationOn = True
        Else
            ToolStripMenuItemDisableCalibration.Checked = True
            _calibrationOn = False
        End If
        RunConfigurations.CalibrationOn = _calibrationOn
        PictureBoxCalibrationOff.Image = If(_calibrationOn, My.Resources.GreenLEDOff, My.Resources.GreenLEDOn)
        ChartSootLoad.Series(0).Points.Clear()
    End Sub

    Private Sub ToolStripTextBox_KeyDown(sender As Object, e As KeyEventArgs) _
        Handles ToolStripTextBoxAtoDReadingsForAvgValue.KeyDown, ToolStripTextBoxSweepIntervalValue.KeyDown,
                ToolStripTextBoxLockDelayValue.KeyDown         , ToolStripTextBoxSweepRateValue.KeyDown,
                ToolStripTextBoxMinDetectableValue.KeyDown     , ToolStripTextBoxMaxDetectableValue.KeyDown,
                ToolStripTextBoxStepDelayValue.KeyDown

        If ((e.Alt OrElse e.Control OrElse e.Shift) OrElse
            ((e.KeyCode < Keys.D0 OrElse e.KeyCode > Keys.D9) AndAlso
             (e.KeyCode < Keys.NumPad0 OrElse e.KeyCode > Keys.NumPad9))) Then
           
            If (e.KeyCode <> Keys.Back) Then
                _nonDecimalEntered = True
            End If
        End If
    End Sub

    Private Sub ToolStripTextBox_KeyPress(sender As Object, e As KeyPressEventArgs) _
        Handles ToolStripTextBoxAtoDReadingsForAvgValue.KeyPress, ToolStripTextBoxSweepIntervalValue.KeyPress,
                ToolStripTextBoxLockDelayValue.KeyPress         , ToolStripTextBoxSweepRateValue.KeyPress,
                ToolStripTextBoxMinDetectableValue.KeyPress     , ToolStripTextBoxMaxDetectableValue.KeyPress,
                ToolStripTextBoxStepDelayValue.KeyPress

        If (_nonDecimalEntered) Then
            _nonDecimalEntered = False
            If (e.KeyChar = Chr(13))
                CType(sender, ToolStripTextBox).Owner.Hide()
            End If
            e.Handled  = True
        End If
    End Sub

    Private Sub ToolStripTextBoxAtoDReadingsForAvgValue_TextChanged(sender As Object, e As EventArgs) _
        Handles ToolStripTextBoxAtoDReadingsForAvgValue.TextChanged

        Try
            If (Not String.IsNullOrWhiteSpace(ToolStripTextBoxAtoDReadingsForAvgValue.Text)) Then

                Dim readForAvg As UInteger

                If (UInteger.TryParse(ToolStripTextBoxAtoDReadingsForAvgValue.Text, readForAvg)) Then
                    If (readForAvg > 0) Then
                        If (ToolStripTextBoxAtoDReadingsForAvgValue.Text.Substring(0, 1) = "0") Then

                            ToolStripTextBoxAtoDReadingsForAvgValue.Text =
                                    ToolStripTextBoxAtoDReadingsForAvgValue.Text.TrimStart("0"c)
                        End If
                    End If
                    If (readForAvg > Common.ReadingsForAvgMax) Then

                        ToolStripTextBoxAtoDReadingsForAvgValue.Text = Common.ReadingsForAvgMax.ToString()
                        RunConfigurations.ReadingsForAvg = Common.ReadingsForAvgMax
                    Else
                        RunConfigurations.ReadingsForAvg = readForAvg
                    End If
                End If
            Else
                ToolStripTextBoxAtoDReadingsForAvgValue.Text = "0"
            End If

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Private Sub ToolStripTextBoxSweepIntervalValue_TextChanged(sender As Object, e As EventArgs) _
        Handles ToolStripTextBoxSweepIntervalValue.TextChanged

        Try
            If (Not String.IsNullOrWhiteSpace(ToolStripTextBoxSweepIntervalValue.Text)) Then

                Dim sweepInterval As UInteger

                UInteger.TryParse(ToolStripTextBoxSweepIntervalValue.Text, sweepInterval)
                If (sweepInterval > 0) Then
                    If (ToolStripTextBoxSweepIntervalValue.Text.Substring(0, 1) = "0") Then

                        ToolStripTextBoxSweepIntervalValue.Text = ToolStripTextBoxSweepIntervalValue.Text.TrimStart("0"c)
                    End If
                    If (sweepInterval > Common.SweepIntervalMax) Then

                        RunConfigurations.SweepInterval = Common.SweepIntervalMax
                        ToolStripTextBoxSweepIntervalValue.Text = Common.SweepIntervalMax.ToString()
                    Else
                        RunConfigurations.SweepInterval = sweepInterval
                    End If
                End If
            Else
                ToolStripTextBoxSweepIntervalValue.Text = "0"
            End If

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Private Sub ToolStripTextBoxLockDelayValue_TextChanged(sender As Object, e As EventArgs) _
        Handles ToolStripTextBoxLockDelayValue.TextChanged

        Try
            If (Not String.IsNullOrWhiteSpace(ToolStripTextBoxLockDelayValue.Text)) Then

                Dim lockDelay As UInteger

                UInteger.TryParse(ToolStripTextBoxLockDelayValue.Text, lockDelay)
                If (lockDelay > 0) Then
                    If (ToolStripTextBoxLockDelayValue.Text.Substring(0, 1) = "0") Then

                        ToolStripTextBoxLockDelayValue.Text = ToolStripTextBoxLockDelayValue.Text.TrimStart("0"c)
                    End If
                End If
                If (lockDelay > Common.LockDelayMax) Then

                    RunConfigurations.LockDelay = Common.LockDelayMax
                    ToolStripTextBoxLockDelayValue.Text = Common.LockDelayMax.ToString()
                Else
                    RunConfigurations.LockDelay = lockDelay
                End If
            Else
                ToolStripTextBoxLockDelayValue.Text = "0"
            End If

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Private Sub ToolStripTextBoxSweepRateValue_TextChanged(sender As Object, e As EventArgs) _
        Handles ToolStripTextBoxSweepRateValue.TextChanged

        Try
            If (Not String.IsNullOrWhiteSpace(ToolStripTextBoxSweepRateValue.Text)) Then

                Dim sweepRate As UInteger

                UInteger.TryParse(ToolStripTextBoxSweepRateValue.Text, sweepRate)
                If (sweepRate > 0) Then
                    If (ToolStripTextBoxSweepRateValue.Text.Substring(0, 1) = "0") Then

                        ToolStripTextBoxSweepRateValue.Text = ToolStripTextBoxSweepRateValue.Text.TrimStart("0"c)
                    End If
                End If
                If (sweepRate > Common.SweepRateMax) Then

                    RunConfigurations.SweepRate = Common.SweepRateMax
                    ToolStripTextBoxSweepRateValue.Text = Common.SweepRateMax.ToString()
                Else
                    RunConfigurations.SweepRate = sweepRate
                End If
            Else
                ToolStripTextBoxSweepRateValue.Text = "0"
            End If

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Private Sub ToolStripTextBoxMinDetectableValue_TextChanged(sender As Object, e As EventArgs) _
        Handles ToolStripTextBoxMinDetectableValue.TextChanged

        Try
            If (Not String.IsNullOrWhiteSpace(ToolStripTextBoxMinDetectableValue.Text)) Then

                Dim minDetectable As UInteger

                UInteger.TryParse(ToolStripTextBoxMinDetectableValue.Text, minDetectable)
                If (minDetectable > 0) Then
                    If (ToolStripTextBoxMinDetectableValue.Text.Substring(0, 1) = "0") Then

                        ToolStripTextBoxMinDetectableValue.Text = ToolStripTextBoxMinDetectableValue.Text.TrimStart("0"c)
                    End If
                End If
                If (minDetectable > Common.DetectableMax) Then

                    RunConfigurations.MinDetectable = Common.DetectableMax
                    ToolStripTextBoxMinDetectableValue.Text = Common.DetectableMax.ToString()
                Else
                    RunConfigurations.MinDetectable = minDetectable
                End If
            Else
                ToolStripTextBoxMinDetectableValue.Text = "0"
            End If

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Private Sub ToolStripTextBoxMaxDetectableValue_TextChanged(sender As Object, e As EventArgs) _
        Handles ToolStripTextBoxMaxDetectableValue.TextChanged

        Try
            If (Not String.IsNullOrWhiteSpace(ToolStripTextBoxMaxDetectableValue.Text)) Then

                Dim maxDetectable As UInteger

                UInteger.TryParse(ToolStripTextBoxMaxDetectableValue.Text, maxDetectable)
                If (maxDetectable > 0) Then
                    If (ToolStripTextBoxMaxDetectableValue.Text.Substring(0, 1) = "0") Then

                        ToolStripTextBoxMaxDetectableValue.Text = ToolStripTextBoxMaxDetectableValue.Text.TrimStart("0"c)
                    End If
                End If
                If (maxDetectable > Common.DetectableMax) Then

                    RunConfigurations.MaxDetectable = Common.DetectableMax
                    ToolStripTextBoxMaxDetectableValue.Text = Common.DetectableMax.ToString()
                Else
                    RunConfigurations.MaxDetectable = maxDetectable
                End If
            Else
                ToolStripTextBoxMaxDetectableValue.Text = "0"
            End If

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Private Sub ToolStripTextBoxStepDelayValue_TextChanged(sender As Object, e As EventArgs) _
        Handles ToolStripTextBoxStepDelayValue.TextChanged

        Try
            If (Not String.IsNullOrWhiteSpace(ToolStripTextBoxStepDelayValue.Text)) Then

                Dim stepDelay As UInteger

                UInteger.TryParse(ToolStripTextBoxStepDelayValue.Text, stepDelay)
                If (stepDelay > 0) Then
                    If (ToolStripTextBoxStepDelayValue.Text.Substring(0, 1) = "0") Then

                        ToolStripTextBoxStepDelayValue.Text = ToolStripTextBoxStepDelayValue.Text.TrimStart("0"c)
                    End If
                End If
                If (stepDelay > Common.StepDelayMax) Then

                    RunConfigurations.TimeBetweenSteps = Common.StepDelayMax
                    ToolStripTextBoxStepDelayValue.Text = Common.StepDelayMax.ToString()
                Else
                    RunConfigurations.TimeBetweenSteps = stepDelay
                End If
            Else
                ToolStripTextBoxStepDelayValue.Text = "0"
            End If

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Private Sub RegionsOfInterestROIToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItemRegionsOfInterest.Click
        _formRoiConfigObj = New FormROIConfiguration()
        _formRoiConfigObj.Show()
    End Sub

    Private Sub MeasurementsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItemMeasurements.Click
        _measurementConfigObj = New FormMeasurementConfig()
        _measurementConfigObj.Show()
    End Sub

    Public Sub ToolStripMenuItem_MouseEnter(sender As Object, e As EventArgs) _
        Handles  ToolStripMenuItemSweepSettings.MouseEnter     , ToolStripMenuItemConfigure.MouseEnter,
                 ToolStripMenuItemAccess.MouseEnter            , ToolStripMenuItemAtoDReadingsForAvg.MouseEnter,
                 ToolStripMenuItemSweepInterval.MouseEnter     , ToolStripMenuItemLockDelay.MouseEnter,
                 ToolStripMenuItemDisableCalibration.MouseEnter, ToolStripMenuItemMeasurements.MouseEnter,
                 ToolStripMenuItemRegionsOfInterest.MouseEnter , ToolStripMenuItemAppEngineer.MouseEnter,
                 ToolStripMenuItemCustomer.MouseEnter          , ToolStripMenuItemOutputPower.MouseEnter,
                 ToolStripMenuItemOutputPowerAux.MouseEnter    , ToolStripMenuItemFile.MouseEnter, 
                 ToolStripMenuItemEncryption.MouseEnter        , ToolStripMenuItemSweepRate.MouseEnter,
                 ToolStripMenuItemOperatingMode.MouseEnter     , ToolStripMenuItemMinDetectable.MouseEnter,
                 ToolStripMenuItemMaxDetectable.MouseEnter     , ToolStripMenuItemStepDelay.MouseEnter
       
        Dim menuStripItem = CType(sender, ToolStripMenuItem)

        If (menuStripItem.HasDropDown) Then
            If (Not menuStripItem.DropDown.Visible)
                menuStripItem.ForeColor = Color.Orange
            End If
        Else
            menuStripItem.ForeColor = Color.Orange
        End If
    End Sub

    Public Sub ToolStripMenuItem_MouseLeave(sender As Object   , e As EventArgs) _
        Handles  ToolStripMenuItemSweepSettings.MouseLeave     , ToolStripMenuItemConfigure.MouseLeave,
                 ToolStripMenuItemAccess.MouseLeave            , ToolStripMenuItemAtoDReadingsForAvg.MouseLeave,
                 ToolStripMenuItemSweepInterval.MouseLeave     , ToolStripMenuItemLockDelay.MouseLeave,
                 ToolStripMenuItemDisableCalibration.MouseLeave, ToolStripMenuItemMeasurements.MouseLeave,
                 ToolStripMenuItemRegionsOfInterest.MouseLeave , ToolStripMenuItemAppEngineer.MouseLeave,
                 ToolStripMenuItemCustomer.MouseLeave          , ToolStripMenuItemOutputPower.MouseLeave,
                 ToolStripMenuItemOutputPowerAux.MouseLeave    , ToolStripMenuItemFile.MouseLeave,
                 ToolStripMenuItemEncryption.MouseLeave        , ToolStripMenuItemSweepRate.MouseLeave,
                 ToolStripMenuItemOperatingMode.MouseLeave     , ToolStripMenuItemMinDetectable.MouseLeave,
                 ToolStripMenuItemMaxDetectable.MouseLeave     , ToolStripMenuItemStepDelay.MouseLeave
       
        Dim menuStripItem = CType(sender, ToolStripMenuItem)

        If (menuStripItem.HasDropDown) Then
            If (Not menuStripItem.DropDown.Visible) Then
                menuStripItem.ForeColor = Color.Chartreuse
            End If
        Else
            menuStripItem.ForeColor = Color.Chartreuse
        End If
    End Sub

    Private Sub ToolStripMenuItemDropDownOpened (sender As Object, e As EventArgs) _
        Handles ToolStripMenuItemSweepSettings.DropDownOpened, ToolStripMenuItemConfigure.DropDownOpened,
                ToolStripMenuItemAccess.DropDownOpened       , ToolStripMenuItemAtoDReadingsForAvg.DropDownOpened,
                ToolStripMenuItemSweepInterval.DropDownOpened, ToolStripMenuItemLockDelay.DropDownOpened,
                ToolStripMenuItemOutputPower.DropDownOpened  , ToolStripMenuItemOutputPowerAux.DropDownOpened,
                ToolStripMenuItemFile.DropDownOpened         , ToolStripMenuItemSweepRate.DropDownOpened,
                ToolStripMenuItemOperatingMode.DropDownOpened, ToolStripMenuItemMinDetectable.DropDownOpened,
                ToolStripMenuItemMaxDetectable.DropDownClosed, ToolStripMenuItemStepDelay.DropDownOpened

        CType(sender, ToolStripMenuItem).ForeColor = Color.Orange
    End Sub

    Private Sub ToolStripMenuItemDropDownClosed (sender As Object, e As EventArgs) _
        Handles ToolStripMenuItemSweepSettings.DropDownClosed, ToolStripMenuItemConfigure.DropDownClosed,
                ToolStripMenuItemAccess.DropDownClosed       , ToolStripMenuItemAtoDReadingsForAvg.DropDownClosed,
                ToolStripMenuItemSweepInterval.DropDownClosed, ToolStripMenuItemLockDelay.DropDownClosed,
                ToolStripMenuItemOutputPower.DropDownClosed  , ToolStripMenuItemOutputPowerAux.DropDownClosed,
                ToolStripMenuItemFile.DropDownClosed         , ToolStripMenuItemSweepRate.DropDownClosed, 
                ToolStripMenuItemOperatingMode.DropDownClosed, ToolStripMenuItemMinDetectable.DropDownClosed,
                ToolStripMenuItemMaxDetectable.DropDownClosed, ToolStripMenuItemStepDelay.DropDownClosed

        CType(sender, ToolStripMenuItem).ForeColor = Color.Chartreuse
    End Sub

    Private Sub ToolStripMenuItemAppEngineerMode_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItemAppEngineer.Click

    End Sub

    Private Sub ToolStripMenuItemCustomerMode_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItemCustomer.Click

    End Sub

    Private Sub ToolStripMenuItemEncryption_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItemEncryption.Click

        Dim encryptor = New FormFileEncryption()

        ToolStripMenuItemFile.DropDown.Close()
        ToolStripMenuItemFile.ForeColor = Color.Chartreuse
        encryptor.ShowDialog()
    End Sub

    Private Sub ToolStripComboBoxOperatingMode_SelectedIndexChanged(sender As Object, e As EventArgs) _
        Handles ToolStripComboBoxOperatingMode.SelectedIndexChanged
        
        If ToolStripComboBoxOperatingMode.SelectedIndex = -1 Then
            Return
        End If

        Dim operModeStr = CType(ToolStripComboBoxOperatingMode.SelectedItem, String)

        RunConfigurations.OperatingMode = operModeStr

    End Sub

    Private Sub SweepIndexChangedHandler(ByVal index As Integer)
        _sweepStepsComplete += index
    End Sub

    Private Sub TimerUpdateProgressBar_Tick(sender As Object, e As EventArgs) Handles TimerUpdateProgressBar.Tick
        ' Update the sweep progress bar.
        If (_sweepStepsComplete + SweepDataIndex) > ProgressBarSweepProgress.Maximum Then
            ProgressBarSweepProgress.Value = ProgressBarSweepProgress.Maximum
        Else
            ProgressBarSweepProgress.Value = (_sweepStepsComplete + SweepDataIndex).ToString()
        End If
    End Sub

    Private Sub TimerSweepDataMsg_Elapsed(sender As Object, e As EventArgs)

        Try
            _WaitForLastSweepDataMsg = False
            _timerSweepDataMsg.Enabled = False
            ButtonStartStopScan.Enabled = True
            ButtonStartStopScan.Text = "Start"
            ButtonWaitSweep.SendToBack()
            If (_closing) Then
                Close()
            End If

        Catch ex As Exception
            ActivityLog.LogError(New StackTrace, ex)
        End Try
    End Sub
End Class
