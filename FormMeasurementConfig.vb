﻿Imports System.Text.RegularExpressions

Public Class FormMeasurementConfig
    Inherits Form

    Private _selectedMeasurement As Measurement
    Private _measurementEdit     As FormMeasurementEdit
    Private _selectedROIName     As String = String.Empty

    ' Events to raise when there is an error/exception to be logged.
    Private Shared Event LogErrorEvent As ActivityLog.LogErrorStackDelegate
    Private Shared Event LogExceptionEvent As ActivityLog.LogExceptionDelegate

    Public Sub New()
        ' Required.
        InitializeComponent()

        ' Used to control the look of Tool Strip Menu Items.
        ContextMenuStripMeasurementConfiguration.Renderer = new MyRenderer()
        ContextMenuStripAssociatedROIs.Renderer           = new MyRenderer()

        ' Register error logging event handlers.
        AddHandler LogErrorEvent, AddressOf ActivityLog.LogError
        AddHandler LogExceptionEvent, AddressOf ActivityLog.LogError

        CType(ToolStripMenuItemEdit.Owner    , ToolStripDropDownMenu).ShowImageMargin = False
        CType(ToolStripMenuItemNew.Owner     , ToolStripDropDownMenu).ShowImageMargin = False
        CType(ToolStripMenuItemDelete.Owner  , ToolStripDropDownMenu).ShowImageMargin = False
        CType(ToolStripMenuItemAddExistingROI.Owner  , ToolStripDropDownMenu).ShowImageMargin = False
        CType(ToolStripMenuItemCreateAndAddROI.Owner , ToolStripDropDownMenu).ShowImageMargin = False
        CType(ToolStripMenuItemRemoveROI.Owner       , ToolStripDropDownMenu).ShowImageMargin = False

        UpdateMeasurementTreeView()
    End Sub

    Private Sub UpdateMeasurementTreeView()

        Dim level0NodeIndex As Integer
        Dim level1NodeIndex As Integer
        Dim level2NodeIndex As Integer

        ' Load Descriptions of all currently available measurement configurations.
        TreeViewAssociatedROIs.BeginUpdate()
        TreeViewAssociatedROIs.Nodes.Clear()
        TreeViewAssociatedROIs.EndUpdate()
        TreeViewMeasurements.BeginUpdate()
        TreeViewMeasurements.Nodes.Clear()
        For Each measure In FormRfSensorMain.Measurements
            level0NodeIndex = TreeViewMeasurements.Nodes.Count
            TreeViewMeasurements.Nodes.Add(measure.Value.Name)
            TreeViewMeasurements.Nodes(level0NodeIndex).Nodes.Add("Min Temperature = " &
                                                                  measure.Value.MinTemperature.ToString() & "°C")
            TreeViewMeasurements.Nodes(level0NodeIndex).Nodes.Add("Max Temperature = " &
                                                                  measure.Value.MaxTemperature.ToString() & "°C")
            TreeViewMeasurements.Nodes(level0NodeIndex).Nodes.Add("Min Detectable = " &
                                                                  measure.Value.MinDetectable.ToString())
            TreeViewMeasurements.Nodes(level0NodeIndex).Nodes.Add("Max Detectable = " & measure.Value.MaxDetectable.ToString())
            TreeViewMeasurements.Nodes(level0NodeIndex).Nodes.Add("Active = " & If(measure.Value.Active, "True", "False"))
            TreeViewMeasurements.Nodes(level0NodeIndex).Nodes.Add("Region(s) Of Interest:")
            For Each roi In measure.Value.RegionsOfInterest
                level1NodeIndex = TreeViewMeasurements.Nodes(level0NodeIndex).Nodes.Count
                TreeViewMeasurements.Nodes(level0NodeIndex).Nodes.Add(roi.Name)
                For Each statWeight In roi.StatisticWeights
                    level2NodeIndex = TreeViewMeasurements.Nodes(level0NodeIndex).Nodes(level1NodeIndex).Nodes.Count
                    TreeViewMeasurements.Nodes(level0NodeIndex).Nodes(level1NodeIndex).
                        Nodes.Add(Common.StatisticNameDict(statWeight.Statistic))
                    TreeViewMeasurements.Nodes(level0NodeIndex).Nodes(level1NodeIndex).
                        Nodes(level2NodeIndex).Nodes.Add("Weight = " & statWeight.Weight.ToString())
                Next
            Next
        Next measure
        TreeViewMeasurements.EndUpdate()
    End Sub

    Private Sub TreeViewMeasurements_AfterSelect(ByVal sender As Object, ByVal e As TreeViewEventArgs) _
            Handles TreeViewMeasurements.AfterSelect

        Static previousMeasurementName As String

        ' Use the measurement node label as key to Measurement dictionary.
        Dim node = e.Node
        Dim name As String

        '  Get the name of the top level node of this branch.
        If (node.Level > 0) Then
            While (node.Level > 0)
                node = node.Parent
            End While
        End If
        name = node.Text

        ' If necessary, re-load the Associated ROI TreeView.
        If (name <> previousMeasurementName) Then

            If (FormRfSensorMain.Measurements.TryGetValue(name, _selectedMeasurement)) Then
                TreeViewAssociatedROIs.BeginUpdate()
                TreeViewAssociatedROIs.Nodes.Clear()
                For Each roi In _selectedMeasurement.RegionsOfInterest
                    Dim operMode  = roi.OperatingMode
                    Dim nodeCount = TreeViewAssociatedROIs.Nodes.Count
                    TreeViewAssociatedROIs.Nodes.Add(roi.Name)
                    TreeViewAssociatedROIs.Nodes(nodeCount).ToolTipText = roi.Name
                    TreeViewAssociatedROIs.Nodes(nodeCount).Nodes.
                        Add("Operating Mode = " & RegionOfInterest.OperatingModeStrToEnum(operMode).ToString())
                    TreeViewAssociatedROIs.Nodes(nodeCount).Nodes.
                        Add("Interval = "       & roi.SweepInterval.ToString() & "ms")
                    TreeViewAssociatedROIs.Nodes(nodeCount).Nodes.
                        Add("Start Freq = "     & roi.StartFrequency.ToString() & "kHz")
                    TreeViewAssociatedROIs.Nodes(nodeCount).Nodes.
                        Add("Step Size = "      & roi.FreqStepSize.ToString() & "kHz")
                    TreeViewAssociatedROIs.Nodes(nodeCount).Nodes.
                        Add("Freq Steps = "     & roi.FreqStepSize.ToString())
                    TreeViewAssociatedROIs.Nodes(nodeCount).Nodes.Add("RF Statistics:")
                    For Each statWeight In roi.StatisticWeights
                        Dim statNodeCount  = TreeViewAssociatedROIs.Nodes(nodeCount).Nodes.Count - 1
                        TreeViewAssociatedROIs.Nodes(nodeCount).Nodes(statNodeCount).Nodes.
                            Add(Common.StatisticNameDict(statWeight.Statistic))
                        Dim weightNodeCount  = TreeViewAssociatedROIs.Nodes(nodeCount).Nodes(statNodeCount).Nodes.Count
                        TreeViewAssociatedROIs.Nodes(nodeCount).Nodes(statNodeCount).
                            Nodes(weightNodeCount).Nodes.Add(statWeight.Weight.ToString)
                    Next
                Next
                ToolStripMenuItemEdit.Enabled   = True
                ToolStripMenuItemDelete.Enabled = True
            Else
                TreeViewAssociatedROIs.SelectedNode = Nothing
                ToolStripMenuItemEdit.Enabled       = False
                ToolStripMenuItemDelete.Enabled     = False
            End If
            previousMeasurementName = name
        End If
    End Sub

    Private Sub TreeViewAssociatedROIs_AfterSelect(sender As Object, e As TreeViewEventArgs) Handles TreeViewAssociatedROIs.AfterSelect

        Try
            ' Use the ROI node label as key to Region of Interest dictionary.
            Dim node = e.Node

            '  Get the name of the top level node of this branch.
            If (node.Level > 0) Then
                While (node.Level > 0)
                    node = node.Parent
                End While
            End If
            _selectedROIName = node.Text
            If (_selectedMeasurement.ContainsROI(_selectedROIName)) Then
                ToolStripMenuItemRemoveROI.Enabled = True
            Else
                TreeViewAssociatedROIs.SelectedNode = Nothing
                ToolStripMenuItemRemoveROI.Enabled  = False
                MsgBox("The selected ROI configuration name was not found.",
                       MsgBoxStyle.SystemModal + MsgBoxStyle.Exclamation)
                RaiseEvent LogErrorEvent(New StackTrace, "TreeViewAssociatedROIs_AfterSelect() failed to find " &
                                         "selected ROI configuration name: " & _selectedROIName)
            End If

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
   End Sub

    Private Sub TreeView_DrawNode(sender As object, e As DrawTreeNodeEventArgs) _
        Handles TreeViewMeasurements.DrawNode, TreeViewAssociatedROIs.DrawNode

        Try
            If (e.Node Is Nothing) Then
                Return
            End If

            ' If treeview's HideSelection property is "True", 
            ' this will always returns "False" on unfocused treeview
            Dim selected  = (e.State And TreeNodeStates.Selected) = TreeNodeStates.Selected
            Dim unfocused = Not e.Node.TreeView.Focused

            ' We need to do owner drawing only on a selected node
            ' and when the treeview is unfocused, else let the OS do it for us
            If (selected AndAlso unfocused) Then
                Dim font = If(e.Node.NodeFont, e.Node.TreeView.Font)
                e.Graphics.FillRectangle(SystemBrushes.Highlight, e.Bounds)
                TextRenderer.DrawText(e.Graphics, e.Node.Text, font, e.Bounds,
                    SystemColors.HighlightText, TextFormatFlags.GlyphOverhangPadding)
            Else
                e.DrawDefault = True
            End If

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Private Sub ToolStripMenuItemDelete_Click(sender As Object, e As EventArgs)

        Try
            Dim mbResult As MsgBoxResult
            Dim measure = New Measurement()

            If (TreeViewMeasurements.SelectedNode IsNot Nothing) Then
                If (_selectedMeasurement IsNot Nothing) Then
                    mbResult = MsgBox("Are you sure?" & vbCrLf &
                                      "The selected Measurement will be permanently deleted.",
                                      MsgBoxStyle.SystemModal + MsgBoxStyle.YesNo)
                    If (mbResult = MsgBoxResult.Yes) Then

                        ' Remove the Measurement Configuration.
                        If (FormRfSensorMain.Measurements.TryRemove(_selectedMeasurement.Name, measure)) Then
                            _selectedMeasurement            = Nothing
                            ToolStripMenuItemEdit.Enabled   = False
                            ToolStripMenuItemDelete.Enabled = False
                            UpdateMeasurementTreeView()
                        Else
                            MsgBox("Failed to remove the selected Measurement.",
                                   MsgBoxStyle.SystemModal + MsgBoxStyle.Exclamation)
                            RaiseEvent LogErrorEvent(New StackTrace, "DeleteToolStripMenuItem_Click() failed " &
                                                     "to remove the selected Measurement: " &
                                                     _selectedMeasurement.Name)
                        End If
                    End If
                End If
            Else
                MsgBox("First select a Measurement, then click Delete.", MsgBoxStyle.SystemModal)
            End If

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Private Sub ToolStripMenuItemEdit_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItemEdit.Click

        Try
            If (TreeViewMeasurements.SelectedNode IsNot Nothing) Then
                If (_selectedMeasurement IsNot Nothing) Then

                    ' Constructor does a deep copy.
                    _measurementEdit = New FormMeasurementEdit(_selectedMeasurement)

                    ' Display the Measurement Edit dialog.
                    _measurementEdit.ShowDialog()
                    UpdateMeasurementTreeView()
                End If
            Else
                MsgBox("First select a Measurement, then click Edit.", MsgBoxStyle.SystemModal)
                _selectedMeasurement            = Nothing
                ToolStripMenuItemEdit.Enabled   = False
                ToolStripMenuItemDelete.Enabled = False
            End If

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Private Sub ToolStripMenuItemNew_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItemNew.Click

        Try
            ' Create New Measurement that contains default values.
            Dim newMeasurement = New Measurement("Enter Measurement Name", 0, 0, 0, 0, New List (Of RegionOfInterest))

            ' Instantiate Measurement edit form.
            _measurementEdit   = New FormMeasurementEdit(newMeasurement)

            ' Display the Measurement Edit dialog.
            _measurementEdit.ShowDialog()
            UpdateMeasurementTreeView()

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Private Sub AddExistingROIToolStripMenuItem1_Click(sender As Object, e As EventArgs) _
        Handles ToolStripMenuItemAddExistingROI.Click

        ' XXX KTK


    End Sub

    Private Sub ToolStripMenuItemCreateAndAddROI_Click(sender As Object, e As EventArgs) _
        Handles ToolStripMenuItemCreateAndAddROI.Click

        ' XXX KTK

    End Sub

    Private Sub RemoveROIToolStripMenuItem1_Click(sender As Object, e As EventArgs) _
        Handles ToolStripMenuItemRemoveROI.Click

        Try
            If (Not String.IsNullOrWhiteSpace(_selectedROIName))
                If (Not _selectedMeasurement.RemoveROI(_selectedROIName))
                    MsgBox("Failed to Remove the selected ROI.", MsgBoxStyle.SystemModal)
                    RaiseEvent LogErrorEvent(New StackTrace,
                                             "FormMeasurementConfig.RemoveROIToolStripMenuItem1_Click() failed to " &
                                             "remove the selected ROI.")
                End If
            Else
                MsgBox("First select an ROI, then click Remove ROI.", MsgBoxStyle.SystemModal)
            End If

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Private Sub ToolStripMenuItem_MouseEnter(sender As Object, e As EventArgs) _
        Handles ToolStripMenuItemAddExistingROI.MouseEnter,
                ToolStripMenuItemCreateAndAddROI.MouseEnter,
                ToolStripMenuItemRemoveROI.MouseEnter,
                ToolStripMenuItemEdit.MouseEnter,
                ToolStripMenuItemNew.MouseEnter,
                ToolStripMenuItemDelete.MouseEnter

                CType(sender, ToolStripMenuItem).ForeColor = Color.Chartreuse
    End Sub

    Private Sub ToolStripMenuItem_MouseLeave(sender As Object, e As EventArgs) _
        Handles ToolStripMenuItemAddExistingROI.MouseLeave,
                ToolStripMenuItemCreateAndAddROI.MouseLeave,
                ToolStripMenuItemRemoveROI.MouseLeave,
                ToolStripMenuItemEdit.MouseLeave,
                ToolStripMenuItemNew.MouseLeave,
                ToolStripMenuItemDelete.MouseLeave

                CType(sender, ToolStripMenuItem).ForeColor = Color.Orange
    End Sub
End Class
