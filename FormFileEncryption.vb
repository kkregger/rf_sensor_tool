﻿Imports System.IO
Imports System.Text
Imports System.Security.Cryptography

    Partial Public Class FormFileEncryption
        Inherits Form

        ' Event to raise when there is an error to be logged.
        Private Shared Event LogErrorEvent As ActivityLog.LogErrorStackDelegate

        ' DES encryption Key
        Public Shared ReadOnly Property CryptKey As Byte() = {&H3F, &H3F, &H6E, &H30, &H56, &H03, &H2D, &H1F}

        Sub New()
            InitializeComponent()
            ' Add error logging delegate.
            AddHandler LogErrorEvent, AddressOf ActivityLog.LogError
        End Sub

        Public Shared Sub GetDecryptedXmlFile(ByRef doc As XDocument, ByVal filePathName As String)

            Dim decryptorStream As CryptoStream = Nothing
            Dim des = New DESCryptoServiceProvider With
                    {
                        .Key = CryptKey,
                        .IV  = CryptKey     ' Initial Vector
                    }
            Dim input = New FileStream(filePathName, FileMode.Open, FileAccess.Read)
            Dim decryptor = des.CreateDecryptor()

            decryptorStream = New CryptoStream(input, decryptor, CryptoStreamMode.Read)
            Using decryptorStream

                ' Load an xml document from the specified binary file via a decryptor.
                doc = XDocument.Load(decryptorStream)
            End Using
        End Sub

        Public Shared Sub EncryptAndSaveXmlFile(byVal doc As XDocument, ByVal filePathName As String)

                Dim output = New FileStream(filePathName, FileMode.Create, FileAccess.Write)
                Dim des = New DESCryptoServiceProvider With
                    {
                        .Key = CryptKey,
                        .IV  = CryptKey     ' Initial Vector
                    }
                Dim encryptor = des.CreateEncryptor()
                Dim encryptorStream = New CryptoStream(output, encryptor, CryptoStreamMode.Write)

                ' Save encrypted XML document to a binary file.
                doc.Save(encryptorStream)

                ' Necessary to add PKCS-padding (1-8 bytes) to the end.
                encryptorStream.FlushFinalBlock()
                encryptorStream.Close()

        End Sub

        Private Sub ButtonEncrypt_Click(sender As Object, e As EventArgs) Handles ButtonEncrypt.Click

            If String.IsNullOrWhiteSpace(My.Settings.EncryptionSourceDirectory) Then
                OpenFileDialogEncrypt.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
            Else
                OpenFileDialogEncrypt.InitialDirectory = My.Settings.EncryptionSourceDirectory
            End If
            OpenFileDialogEncrypt.Title = "Selects the file(s) to be encrypted."
            OpenFileDialogEncrypt.Filter = "Text Files (*.txt)|*.txt|XML Files (*.xml)| *.xml|All Files (*.*)|*.*"
            Dim dResult = OpenFileDialogEncrypt.ShowDialog()
            If dResult = DialogResult.OK Then
                Dim fileNames = OpenFileDialogEncrypt.FileNames
                If fileNames.Length > 0 Then
                    My.Settings.EncryptionSourceDirectory = Path.GetDirectoryName(fileNames(0))
                    For Each fileName In fileNames
                        If File.Exists(fileName) Then

                            Dim input = New FileStream(fileName, FileMode.Open, FileAccess.Read)
                            Dim directPath = Path.GetDirectoryName(fileName)
                            FolderBrowserDialogEncrypt.Description = "Select the Encrypted File Destination Directory"
                            If String.IsNullOrWhiteSpace(My.Settings.EncryptionSaveDirectory) Then
                                FolderBrowserDialogEncrypt.SelectedPath = directPath
                            Else
                                FolderBrowserDialogEncrypt.SelectedPath = My.Settings.EncryptionSaveDirectory
                            End If
                            dResult = FolderBrowserDialogEncrypt.ShowDialog()
                            If dResult = DialogResult.OK Then
                                My.Settings.EncryptionSaveDirectory = FolderBrowserDialogEncrypt.SelectedPath
                                fileName = My.Settings.EncryptionSaveDirectory & "\" &
                                          Path.GetFileNameWithoutExtension(fileName) & ".bin"
                                If File.Exists(fileName) Then
                                    dResult = MsgBox("Do you want to overwrite existing file(s)?",
                                                 MsgBoxStyle.YesNoCancel + MsgBoxStyle.SystemModal,
                                                 "File(s) Exist!")
                                Else
                                    dResult = MsgBoxResult.Yes
                                End If
                                If (dResult = MsgBoxResult.Yes) Then
                                    Dim output = New FileStream(fileName, FileMode.Create, FileAccess.Write)
                                    Dim des = New DESCryptoServiceProvider With
                                        {
                                            .Key = CryptKey,
                                            .IV  = CryptKey     ' Initial Vector
                                        }
                                    Dim encryptor = des.CreateEncryptor()
                                    Dim encryptorStream = New CryptoStream(output, encryptor, CryptoStreamMode.Write)
                                    Dim bytesToRead As Integer = input.Length
                                    Dim readBytes = New Byte(bytesToRead - 1) {}
                                    Dim count As Integer

                                    ' Save encrypted file to a binary (.bin) file.
                                    While bytesToRead > 0
                                        count = input.Read(readBytes, 0, bytesToRead)
                                        If count <> 0 Then
                                            encryptorStream.Write(readBytes, 0, count)
                                        End If
                                        bytesToRead -= count
                                    End While

                                    ' Adds PKCS-padding to the end of the last block when necessary.
                                    encryptorStream.FlushFinalBlock()
                                    encryptorStream.Close()
                                End If
                            End If
                            input.Close()
                        End If
                    Next
                End If
            End If
        End Sub

        Private Sub ButtonDecrypt_Click(sender As Object, e As EventArgs) Handles ButtonDecrypt.Click

            If String.IsNullOrWhiteSpace(My.Settings.DecryptionSourceDirectory) Then
                OpenFileDialogEncrypt.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
            Else
                OpenFileDialogEncrypt.InitialDirectory = My.Settings.DecryptionSourceDirectory
            End If
            OpenFileDialogEncrypt.Title = "Selects the file(s) to be decrypted."
            OpenFileDialogEncrypt.Filter = "Binary Files (*.bin)| *.bin|All Files (*.*)|*.*"
            Dim dResult = OpenFileDialogEncrypt.ShowDialog()
            If dResult = DialogResult.OK Then
                Dim fileNames = OpenFileDialogEncrypt.FileNames
                If fileNames.Length > 0 Then
                    My.Settings.DecryptionSourceDirectory = Path.GetDirectoryName(fileNames(0))
                    For Each fileName In fileNames
                        If File.Exists(fileName) Then

                            Dim input = New FileStream(fileName, FileMode.Open, FileAccess.Read)
                            Dim directPath = Path.GetDirectoryName(fileName)
                            FolderBrowserDialogEncrypt.Description = "Select the Decrypted File Destination Directory"
                            If String.IsNullOrWhiteSpace(My.Settings.DecryptionSaveDirectory) Then
                                FolderBrowserDialogEncrypt.SelectedPath = directPath
                            Else
                                FolderBrowserDialogEncrypt.SelectedPath = My.Settings.DecryptionSaveDirectory
                            End If
                            dResult = FolderBrowserDialogEncrypt.ShowDialog()
                            If dResult = DialogResult.OK Then
                                My.Settings.DecryptionSaveDirectory = FolderBrowserDialogEncrypt.SelectedPath
                                fileName = My.Settings.DecryptionSaveDirectory & "\" &
                                           Path.GetFileNameWithoutExtension(fileName) & ".txt"
                                If File.Exists(fileName) Then
                                    dResult = MsgBox("Do you want to overwrite existing file(s)?",
                                                     MsgBoxStyle.YesNoCancel + MsgBoxStyle.SystemModal,
                                                     "File(s) Exist!")
                                Else
                                    dResult = MsgBoxResult.Yes
                                End If
                                If (dResult = MsgBoxResult.Yes) Then
                                    Dim decryptorStream As CryptoStream = Nothing
                                    Dim des = New DESCryptoServiceProvider With
                                        {
                                            .Key = CryptKey,
                                            .IV  = CryptKey     ' Initial Vector
                                        }
                                    Dim decryptor = des.CreateDecryptor()
                                    decryptorStream = New CryptoStream(input, decryptor, CryptoStreamMode.Read)
                                    Dim readBytes = New Byte(7) {}
                                    Dim count As Integer
                                    Dim output = New FileStream(fileName, FileMode.Create, FileAccess.Write)
                                    ' Save decrypted file to a text (.txt) file.
                                    Do
                                        count = decryptorStream.Read(readBytes, 0, 8)
                                        If count <> 0 Then
                                            output.Write(readBytes, 0, count)
                                        End If
                                    Loop While count <> 0
                                    output.Close()
                                End If
                            End If
                            input.Close()
                        End If
                    Next
                End If
            End If
        End Sub
    End Class

    ' AES
    ' XXX KTK - added crypto RNG for initialization vector
    'RNGCryptoServiceProvider CRNG = new System.Security.Cryptography.RNGCryptoServiceProvider();
    'Dim initVector = new byte(16){};
    'CRNG.GetBytes(initVector);
    'Dim output = New FileStream(TextBoxDataOutputFolder.Text & "\" & outputFileName & ".bin",
    '                            FileMode.Create, FileAccess.Write)
    'Output.Write(initVector, 0, 16);       ' XXX KTK - Change this to end.
    'Output.Flush();
    'AesCryptoServiceProvider AES = new AesCryptoServiceProvider();     // XXX KTK - Changed encryption algorithm, added random initialization vector
    'AES.KeySize = 128;
    'AES.Key = m_CalibrationAES;
    'AES.IV = initVector;

