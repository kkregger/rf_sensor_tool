﻿Partial Class FormMeasurementConfig
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormMeasurementConfig))
        Me.SaveGeneratedFileDialog = New System.Windows.Forms.SaveFileDialog()
        Me.ToolTipMeasure = New System.Windows.Forms.ToolTip(Me.components)
        Me.TreeViewMeasurements = New System.Windows.Forms.TreeView()
        Me.ContextMenuStripMeasurementConfiguration = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ToolStripMenuItemEdit = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItemNew = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItemDelete = New System.Windows.Forms.ToolStripMenuItem()
        Me.TreeViewAssociatedROIs = New System.Windows.Forms.TreeView()
        Me.ContextMenuStripAssociatedROIs = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ToolStripMenuItemAddExistingROI = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItemCreateAndAddROI = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItemRemoveROI = New System.Windows.Forms.ToolStripMenuItem()
        Me.LabelMeasurements = New System.Windows.Forms.Label()
        Me.LabelROIs = New System.Windows.Forms.Label()
        Me.SplitContainerMeasurementConfiguration = New System.Windows.Forms.SplitContainer()
        Me.ContextMenuStripMeasurementConfiguration.SuspendLayout
        Me.ContextMenuStripAssociatedROIs.SuspendLayout
        CType(Me.SplitContainerMeasurementConfiguration,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SplitContainerMeasurementConfiguration.Panel1.SuspendLayout
        Me.SplitContainerMeasurementConfiguration.Panel2.SuspendLayout
        Me.SplitContainerMeasurementConfiguration.SuspendLayout
        Me.SuspendLayout
        '
        'SaveGeneratedFileDialog
        '
        Me.SaveGeneratedFileDialog.DefaultExt = "hex"
        Me.SaveGeneratedFileDialog.Filter = "hex|*.hex|All Files|*.*"
        Me.SaveGeneratedFileDialog.Title = "Generate Configuration File"
        '
        'TreeViewMeasurements
        '
        Me.TreeViewMeasurements.BackColor = System.Drawing.Color.MidnightBlue
        Me.TreeViewMeasurements.ContextMenuStrip = Me.ContextMenuStripMeasurementConfiguration
        Me.TreeViewMeasurements.DrawMode = System.Windows.Forms.TreeViewDrawMode.OwnerDrawText
        Me.TreeViewMeasurements.Font = New System.Drawing.Font("Consolas", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.TreeViewMeasurements.ForeColor = System.Drawing.Color.Chartreuse
        Me.TreeViewMeasurements.FullRowSelect = true
        Me.TreeViewMeasurements.HideSelection = false
        Me.TreeViewMeasurements.Location = New System.Drawing.Point(8, 21)
        Me.TreeViewMeasurements.Name = "TreeViewMeasurements"
        Me.TreeViewMeasurements.ShowNodeToolTips = true
        Me.TreeViewMeasurements.Size = New System.Drawing.Size(350, 411)
        Me.TreeViewMeasurements.TabIndex = 107
        Me.TreeViewMeasurements.TabStop = false
        Me.ToolTipMeasure.SetToolTip(Me.TreeViewMeasurements, "Left-click to select a Measurement Configuration."&Global.Microsoft.VisualBasic.ChrW(13)&Global.Microsoft.VisualBasic.ChrW(10)&"Right-click to view menu.")
        '
        'ContextMenuStripMeasurementConfiguration
        '
        Me.ContextMenuStripMeasurementConfiguration.AutoSize = false
        Me.ContextMenuStripMeasurementConfiguration.BackColor = System.Drawing.Color.MidnightBlue
        Me.ContextMenuStripMeasurementConfiguration.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold)
        Me.ContextMenuStripMeasurementConfiguration.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemEdit, Me.ToolStripMenuItemNew, Me.ToolStripMenuItemDelete})
        Me.ContextMenuStripMeasurementConfiguration.Name = "ContextMenuStripMeasurementConfiguration"
        Me.ContextMenuStripMeasurementConfiguration.Size = New System.Drawing.Size(61, 72)
        '
        'ToolStripMenuItemEdit
        '
        Me.ToolStripMenuItemEdit.AutoSize = false
        Me.ToolStripMenuItemEdit.Enabled = false
        Me.ToolStripMenuItemEdit.ForeColor = System.Drawing.Color.Orange
        Me.ToolStripMenuItemEdit.Name = "ToolStripMenuItemEdit"
        Me.ToolStripMenuItemEdit.Size = New System.Drawing.Size(60, 22)
        Me.ToolStripMenuItemEdit.Text = "Edit"
        '
        'ToolStripMenuItemNew
        '
        Me.ToolStripMenuItemNew.AutoSize = false
        Me.ToolStripMenuItemNew.ForeColor = System.Drawing.Color.Orange
        Me.ToolStripMenuItemNew.Name = "ToolStripMenuItemNew"
        Me.ToolStripMenuItemNew.Size = New System.Drawing.Size(60, 22)
        Me.ToolStripMenuItemNew.Text = "New"
        '
        'ToolStripMenuItemDelete
        '
        Me.ToolStripMenuItemDelete.AutoSize = false
        Me.ToolStripMenuItemDelete.Enabled = false
        Me.ToolStripMenuItemDelete.ForeColor = System.Drawing.Color.Orange
        Me.ToolStripMenuItemDelete.Name = "ToolStripMenuItemDelete"
        Me.ToolStripMenuItemDelete.Size = New System.Drawing.Size(60, 22)
        Me.ToolStripMenuItemDelete.Text = "Delete"
        '
        'TreeViewAssociatedROIs
        '
        Me.TreeViewAssociatedROIs.BackColor = System.Drawing.Color.MidnightBlue
        Me.TreeViewAssociatedROIs.ContextMenuStrip = Me.ContextMenuStripAssociatedROIs
        Me.TreeViewAssociatedROIs.DrawMode = System.Windows.Forms.TreeViewDrawMode.OwnerDrawText
        Me.TreeViewAssociatedROIs.Font = New System.Drawing.Font("Consolas", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.TreeViewAssociatedROIs.ForeColor = System.Drawing.Color.Chartreuse
        Me.TreeViewAssociatedROIs.FullRowSelect = true
        Me.TreeViewAssociatedROIs.HideSelection = false
        Me.TreeViewAssociatedROIs.Location = New System.Drawing.Point(7, 21)
        Me.TreeViewAssociatedROIs.Name = "TreeViewAssociatedROIs"
        Me.TreeViewAssociatedROIs.ShowNodeToolTips = true
        Me.TreeViewAssociatedROIs.Size = New System.Drawing.Size(350, 411)
        Me.TreeViewAssociatedROIs.TabIndex = 108
        Me.TreeViewAssociatedROIs.TabStop = false
        Me.ToolTipMeasure.SetToolTip(Me.TreeViewAssociatedROIs, "Right-click to view menu.")
        '
        'ContextMenuStripAssociatedROIs
        '
        Me.ContextMenuStripAssociatedROIs.AutoSize = false
        Me.ContextMenuStripAssociatedROIs.BackColor = System.Drawing.Color.MidnightBlue
        Me.ContextMenuStripAssociatedROIs.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ContextMenuStripAssociatedROIs.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemAddExistingROI, Me.ToolStripMenuItemCreateAndAddROI, Me.ToolStripMenuItemRemoveROI})
        Me.ContextMenuStripAssociatedROIs.Name = "ContextMenuStripAssociatedROIs"
        Me.ContextMenuStripAssociatedROIs.Size = New System.Drawing.Size(113, 72)
        '
        'ToolStripMenuItemAddExistingROI
        '
        Me.ToolStripMenuItemAddExistingROI.AutoSize = false
        Me.ToolStripMenuItemAddExistingROI.ForeColor = System.Drawing.Color.Orange
        Me.ToolStripMenuItemAddExistingROI.Name = "ToolStripMenuItemAddExistingROI"
        Me.ToolStripMenuItemAddExistingROI.Size = New System.Drawing.Size(105, 22)
        Me.ToolStripMenuItemAddExistingROI.Text = "Add Existing ROI"
        '
        'ToolStripMenuItemCreateAndAddROI
        '
        Me.ToolStripMenuItemCreateAndAddROI.AutoSize = false
        Me.ToolStripMenuItemCreateAndAddROI.ForeColor = System.Drawing.Color.Orange
        Me.ToolStripMenuItemCreateAndAddROI.Name = "ToolStripMenuItemCreateAndAddROI"
        Me.ToolStripMenuItemCreateAndAddROI.Size = New System.Drawing.Size(105, 22)
        Me.ToolStripMenuItemCreateAndAddROI.Text = "Create ROI"
        '
        'ToolStripMenuItemRemoveROI
        '
        Me.ToolStripMenuItemRemoveROI.AutoSize = false
        Me.ToolStripMenuItemRemoveROI.Enabled = false
        Me.ToolStripMenuItemRemoveROI.ForeColor = System.Drawing.Color.Orange
        Me.ToolStripMenuItemRemoveROI.Name = "ToolStripMenuItemRemoveROI"
        Me.ToolStripMenuItemRemoveROI.Size = New System.Drawing.Size(105, 22)
        Me.ToolStripMenuItemRemoveROI.Text = "Remove ROI"
        '
        'LabelMeasurements
        '
        Me.LabelMeasurements.AutoSize = true
        Me.LabelMeasurements.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.LabelMeasurements.ForeColor = System.Drawing.Color.White
        Me.LabelMeasurements.Location = New System.Drawing.Point(132, 3)
        Me.LabelMeasurements.Name = "LabelMeasurements"
        Me.LabelMeasurements.Size = New System.Drawing.Size(102, 15)
        Me.LabelMeasurements.TabIndex = 109
        Me.LabelMeasurements.Text = "Measurements"
        '
        'LabelROIs
        '
        Me.LabelROIs.AutoSize = true
        Me.LabelROIs.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.LabelROIs.ForeColor = System.Drawing.Color.White
        Me.LabelROIs.Location = New System.Drawing.Point(80, 3)
        Me.LabelROIs.Name = "LabelROIs"
        Me.LabelROIs.Size = New System.Drawing.Size(203, 15)
        Me.LabelROIs.TabIndex = 110
        Me.LabelROIs.Text = "Associated Regions Of Interest"
        '
        'SplitContainerMeasurementConfiguration
        '
        Me.SplitContainerMeasurementConfiguration.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SplitContainerMeasurementConfiguration.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerMeasurementConfiguration.Name = "SplitContainerMeasurementConfiguration"
        '
        'SplitContainerMeasurementConfiguration.Panel1
        '
        Me.SplitContainerMeasurementConfiguration.Panel1.Controls.Add(Me.LabelMeasurements)
        Me.SplitContainerMeasurementConfiguration.Panel1.Controls.Add(Me.TreeViewMeasurements)
        '
        'SplitContainerMeasurementConfiguration.Panel2
        '
        Me.SplitContainerMeasurementConfiguration.Panel2.Controls.Add(Me.TreeViewAssociatedROIs)
        Me.SplitContainerMeasurementConfiguration.Panel2.Controls.Add(Me.LabelROIs)
        Me.SplitContainerMeasurementConfiguration.Size = New System.Drawing.Size(742, 444)
        Me.SplitContainerMeasurementConfiguration.SplitterDistance = 371
        Me.SplitContainerMeasurementConfiguration.SplitterWidth = 1
        Me.SplitContainerMeasurementConfiguration.TabIndex = 111
        '
        'FormMeasurementConfig
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.DimGray
        Me.ClientSize = New System.Drawing.Size(742, 444)
        Me.Controls.Add(Me.SplitContainerMeasurementConfiguration)
        Me.ForeColor = System.Drawing.Color.White
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"),System.Drawing.Icon)
        Me.Name = "FormMeasurementConfig"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Measurement Configurations"
        Me.ContextMenuStripMeasurementConfiguration.ResumeLayout(false)
        Me.ContextMenuStripAssociatedROIs.ResumeLayout(false)
        Me.SplitContainerMeasurementConfiguration.Panel1.ResumeLayout(false)
        Me.SplitContainerMeasurementConfiguration.Panel1.PerformLayout
        Me.SplitContainerMeasurementConfiguration.Panel2.ResumeLayout(false)
        Me.SplitContainerMeasurementConfiguration.Panel2.PerformLayout
        CType(Me.SplitContainerMeasurementConfiguration,System.ComponentModel.ISupportInitialize).EndInit
        Me.SplitContainerMeasurementConfiguration.ResumeLayout(false)
        Me.ResumeLayout(false)

End Sub
    Private SaveGeneratedFileDialog As System.Windows.Forms.SaveFileDialog
    Friend WithEvents ToolTipMeasure As ToolTip
    Friend WithEvents TreeViewMeasurements As TreeView
    Friend WithEvents TreeViewAssociatedROIs As TreeView
    Friend WithEvents LabelMeasurements As Label
    Friend WithEvents LabelROIs As Label
    Friend WithEvents SplitContainerMeasurementConfiguration As SplitContainer
    Friend WithEvents ContextMenuStripMeasurementConfiguration As ContextMenuStrip
    Friend WithEvents ToolStripMenuItemEdit As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItemNew As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItemDelete As ToolStripMenuItem
    Friend WithEvents ContextMenuStripAssociatedROIs As ContextMenuStrip
    Friend WithEvents ToolStripMenuItemAddExistingROI As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItemCreateAndAddROI As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItemRemoveROI As ToolStripMenuItem
End Class
