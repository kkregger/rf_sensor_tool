﻿Imports System.Reflection


' Configuration data classes.

Public MustInherit Class ConfigurationBase  '==========================================================================

    Public Overridable Function ToXml() As XElement

        ' Generic serializer.
        Dim properties = Me.GetType().GetProperties()
        Dim config     = New XElement(Me.GetType().Name)

        Try
            For Each propInfo In properties
                If propInfo.PropertyType Is GetType(String) Then

                    ' Handling for null strings.
                    config.Add(New XElement(propInfo.Name, If(propInfo.GetValue(Me), "")))
                Else
                    ' Convert all other types.
                    config.Add(New XElement(propInfo.Name, propInfo.GetValue(Me).ToString()))
                End If
            Next propInfo

        Catch ex As Exception
            ActivityLog.LogError(New StackTrace, ex)
        End Try
        Return config
    End Function

    Public Overridable Function FromXML(ByVal element As XElement) As Object

        ' Generic deserializer.
        Dim configuration As Object = Nothing
        Dim configName    As String = Nothing
        Dim classType     As Type   = Nothing
        Dim index         As Integer
        Dim dictConfigPropertyDict  = FormRfSensorMain.ConfigProperties

        Try
            configName = element.Name.ToString()
            If configName.Contains("}") Then

                ' Strip the XML namespace, if one is present.
                index = configName.LastIndexOf("}", StringComparison.Ordinal) + 1
                If configName.Length > index Then
                    configName = configName.Substring(index, configName.Length - index)
                End If
            End If
            classType = FormRfSensorMain.ConfigClasses(configName)
            If (classType IsNot Nothing) Then

                ' Instantiate new configuration class.
                configuration = Activator.CreateInstance(classType)
                For Each ele In element.Elements

                    ' Add parsed child Elements to the current configuration.
                    Dim elementName = ele.Name.ToString()
                    index = elementName.LastIndexOf("}", StringComparison.Ordinal) + 1
                    If elementName.Contains("}") Then

                        ' Strip the XML namespace, if one is present.
                        If elementName.Length > index Then
                            elementName = elementName.Substring(index, elementName.Length - index)
                        End If
                    End If

                    ' Add the latest element name and value to the configuration
                    ' class currently being deserialized. Start by getting the
                    ' properties for this configuration class.
                    Dim configPropertyDict = dictConfigPropertyDict(configName)
                    Dim configProp = configPropertyDict(elementName)
                    If configProp IsNot Nothing Then

                        Dim value As Object     = Nothing
                        Dim parse As MethodInfo = Nothing
                        Dim propType As Type    = configProp.PropertyType

                        If propType Is GetType(String) Then

                            ' No need to parse a string.
                            ' string booleans must be lower case to be
                            ' recognized by the parser.
                            value = If(propType Is GetType(Boolean), ele.Value.ToLower(), ele.Value)
                        ElseIf propType Is GetType(Integer?) Then

                            ' Get the parser method for the int type.
                            parse = GetType(Integer).GetMethod("Parse", {GetType(String)})
                        Else
                            ' Get the parser method for type of the current property.
                            parse = propType.GetMethod("Parse", {GetType(String)})
                        End If

                        ' Invoke the parser to convert the current element
                        ' value string to the type of the current property.
                        ' Not necessary for strings, parse will equal null.
                        If parse IsNot Nothing Then
                            value = parse.Invoke(Nothing, New Object() {ele.Value})
                        ElseIf propType.IsEnum Then

                            ' Parse for Enum values
                            If [Enum].IsDefined(propType, ele.Value) Then
                                value = [Enum].Parse(propType, ele.Value)
                            End If
                        End If
                        If (value IsNot Nothing) Then

                            ' Call the set accessor for the current property.
                            configProp.SetValue(configuration, value, Nothing)
                        End If
                    End If
                Next
            Else
                configuration = Nothing
            End If

        Catch ex As Exception
            ActivityLog.LogError(New StackTrace, ex)
        End Try
        Return configuration
    End Function

    Public Function StringCopy(ByVal text As String) As String
        Return If(Not String.IsNullOrWhiteSpace(text), String.Copy(text), Nothing)
    End Function
End Class

Public Class CanCommunication '========================================================================================
    Inherits ConfigurationBase

    Private _deviceType       As String = "PCAN USB"
    Private _description      As String = "Active by default."
    Private _pcanDesignation  As String = "PCAN_USB1"
    Private _nicanDesignation As String = "CAN0"

    Public Sub New()
    End Sub

    Public Sub New(ByVal devType As String, ByVal bRate As UInteger, ByVal desc As String,
                   ByVal pcanDesignation As String, ByVal nicanDesignation As String,
                   ByVal act As Boolean)

        _deviceType       = StringCopy(devType)?.ToUpper()
        _description      = StringCopy(desc)
        _pcanDesignation  = StringCopy(pcanDesignation)?.ToUpper()
        _nicanDesignation = StringCopy(nicanDesignation)?.ToUpper()
        BitRate           = bRate
        Active            = act
    End Sub

    Public Sub New(ByVal previous As CanCommunication)

        _deviceType       = StringCopy(previous._deviceType)?.ToUpper()
        _description      = StringCopy(previous._description)
        _pcanDesignation  = StringCopy(previous._pcanDesignation)?.ToUpper()
        _nicanDesignation = StringCopy(previous._nicanDesignation)?.ToUpper()
        BitRate           = previous.BitRate
        Active            = previous.Active
    End Sub

    Public Property DeviceType() As String
        Get
            Return StringCopy(_deviceType)
        End Get
        Set(ByVal value As String)
            _deviceType = StringCopy(value)?.ToUpper()
        End Set
    End Property

    Public Property Description() As String
        Get
            Return StringCopy(If(_description, ""))
        End Get
        Set(ByVal value As String)
            _description = StringCopy(value)
        End Set
    End Property

    Public Property PcanDesignation() As String
        Get
            Return StringCopy(_pcanDesignation)
        End Get
        Set(ByVal value As String)
            _pcanDesignation = StringCopy(value)?.ToUpper()
        End Set
    End Property

    Public Property NicanDesignation() As String
        Get
            Return StringCopy(_nicanDesignation)
        End Get
        Set(ByVal value As String)
            _nicanDesignation = StringCopy(value)?.ToUpper()
        End Set
    End Property


    ' Auto Properties
    Public Property BitRate() As UInteger = CanMessageLib.DefaultCanBitRate

    Public Property Active()  As Boolean

End Class

Public Class CustomerConfiguration '===================================================================================
    Inherits ConfigurationBase


    Private _fileNamePrefix            As String = "Mass_Load_"
    Private _primaryMeasurementLabel   As String = "Mass Load [g/L]"
    Private _secondaryMeasurementLabel As String = "Average Temp (°C)"
    Private _outputFileDirectory       As String = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
    Private _outputFileName            As String

    Public Sub New()
    End Sub

    Public Sub New(ByVal filePrefix As String, ByVal primaryLabel As String,
                   ByVal secondaryLabel As String, ByVal directory As String,
                   ByVal fileName As String)

        _fileNamePrefix            = StringCopy(filePrefix)
        _primaryMeasurementLabel   = StringCopy(primaryLabel)
        _secondaryMeasurementLabel = StringCopy(secondaryLabel)
        _primaryMeasurementLabel   = directory
        _secondaryMeasurementLabel = fileName
    End Sub

    Public Sub New(ByVal previous As CustomerConfiguration)

        _fileNamePrefix            = StringCopy(previous._fileNamePrefix)
        _primaryMeasurementLabel   = StringCopy(previous._primaryMeasurementLabel)
        _secondaryMeasurementLabel = StringCopy(previous._secondaryMeasurementLabel)
        _primaryMeasurementLabel   = previous._primaryMeasurementLabel
        _secondaryMeasurementLabel = previous._secondaryMeasurementLabel
    End Sub

    Public Property FileNamePrefix() As String
        Get
            Return StringCopy(_fileNamePrefix)
        End Get
        Set(ByVal value As String)
            _fileNamePrefix = StringCopy(value)
        End Set
    End Property

    Public Property PrimaryMeasurementLabel() As String
        Get
            Return StringCopy(_primaryMeasurementLabel)
        End Get
        Set(ByVal value As String)
            _primaryMeasurementLabel = StringCopy(value)
        End Set
    End Property

    Public Property SecondaryMeasurementLabel() As String
        Get
            Return StringCopy(_secondaryMeasurementLabel)
        End Get
        Set(ByVal value As String)
            _secondaryMeasurementLabel = StringCopy(value)
        End Set
    End Property

    Public Property OutputFileDirectory() As String
        Get
            Return StringCopy(_outputFileDirectory)
        End Get
        Set(ByVal value As String)
            _outputFileDirectory = StringCopy(value)
        End Set
    End Property

    Public Property OutputFileName() As String
        Get
            Return StringCopy(_outputFileName)
        End Get
        Set(ByVal value As String)
            _outputFileName = StringCopy(value)
        End Set
    End Property
End Class

Public Class Measurement '=============================================================================================
    Inherits ConfigurationBase

    Private _name As String = "Enter Measurement Name"

    Public Sub New()
    End Sub

    Public Sub New(ByVal name As String, ByVal minTemp As UShort,
                   ByVal maxTemp As UShort, ByVal minDetect As UShort,
                   ByVal maxDetect As UShort)

        ' Used to create Measurement with initial values and no Associated ROIs.
        _name          = StringCopy(name)
        MinTemperature = minTemp
        MaxTemperature = maxTemp
        MinDetectable  = minDetect
        MaxDetectable  = maxDetect
    End Sub

    Public Sub New(ByVal name As String, ByVal minTemp As UShort,
                   ByVal maxTemp As UShort, ByVal minDetect As UShort,
                   ByVal maxDetect As UShort, ByVal regOfInt As List(Of RegionOfInterest))

        _name             = StringCopy(name)
        MinTemperature    = minTemp
        MaxTemperature    = maxTemp
        MinDetectable     = minDetect
        MaxDetectable     = maxDetect
        RegionsOfInterest = New List(Of RegionOfInterest)(regOfInt)
    End Sub

    Public Sub New(ByVal previous As Measurement)
        If previous IsNot Nothing Then
            _name             = StringCopy(previous._name)
            MinTemperature    = previous.MinTemperature
            MaxTemperature    = previous.MaxTemperature
            MinDetectable     = previous.MinDetectable
            MaxDetectable     = previous.MaxDetectable
            RegionsOfInterest = New List(Of RegionOfInterest)(previous.RegionsOfInterest)
        End If
    End Sub

    Public Shared Operator =(ByVal a As Measurement, ByVal b As Measurement) As Boolean

        Dim result As Boolean

        result = a._name          = b._name          AndAlso
                 a.MinTemperature = b.MinTemperature AndAlso
                 a.MaxTemperature = b.MaxTemperature AndAlso
                 a.MinDetectable  = b.MinDetectable  AndAlso
                 a.MaxDetectable  = b.MaxDetectable  AndAlso
                 a.Active         = b.Active

        If ((result) AndAlso (a.RegionsOfInterest.Count = b.RegionsOfInterest.Count)) Then

            For idx = 0 To a.RegionsOfInterest.Count -1
                result = result And a.RegionsOfInterest(idx) = b.RegionsOfInterest(idx)
            Next
        End If
        Return result
    End Operator

    Public Shared Operator <>(ByVal a As Measurement, ByVal b As Measurement) As Boolean
        Return Not a = b
    End Operator

    Public Property Name() As String
        Get
            Return StringCopy(If(_name, ""))
        End Get
        Set(ByVal value As String)
            _name = StringCopy(value)
        End Set
    End Property

    Public Function ContainsROI(ByVal roiName As String) As Boolean
    
        Dim result = False

        For Each regOfInterest In RegionsOfInterest
            If (roiName = regOfInterest.Name) Then
                result = True
                Exit For
            End If
        Next
        Return result
    End Function
  
    Public Function  RemoveROI(ByVal roiName As String) As Boolean

        Dim result = False

        For Each regOfInterest In RegionsOfInterest
            If (roiName = regOfInterest.Name) Then
                RegionsOfInterest.Remove(regOfInterest)
                result = True
                Exit For
            End If
        Next
        Return result
    End Function

    Public Function GetROI(ByVal roiName As String) As RegionOfInterest
    
        Dim result = New RegionOfInterest()

        For Each regOfInterest In RegionsOfInterest
            If (roiName = regOfInterest.Name) Then
                result = New RegionOfInterest(regOfInterest)
                Exit For
            End If
        Next
        Return result
    End Function

    Public Overrides Function ToXml() As XElement

        Dim xEleMeasure As XElement = New XElement("Measurement")

        xEleMeasure.Add("MinTemperature", MinTemperature)
        xEleMeasure.Add("MaxTemperature", MaxTemperature)
        xEleMeasure.Add("MinDetectable" , MinDetectable)
        xEleMeasure.Add("MaxDetectable" , MaxDetectable)
        xEleMeasure.Add("Active"        , Active)
        For Each roi In RegionsOfInterest
            xEleMeasure.Add(roi.ToXml())
        Next

        Return xEleMeasure
    End Function

    Public Overrides Function FromXML(ByVal ele As XElement) As Object

        Dim measure  As Measurement      = Nothing
        Dim roi      As RegionOfInterest = Nothing
        Dim propName As String

        If (ele.HasElements) Then
            roi = New RegionOfInterest()
            For Each child In ele.Elements
                propName = child.Name.ToString()
                Select (propName)
                    Case "Name"
                        measure.Name = child.Value
                    Case "MinTemperature"
                        UShort.TryParse(child.Value, measure.MinTemperature)
                    Case "MaxTemperature"
                        UShort.TryParse(child.Value, measure.MaxTemperature)
                    Case "MinDetectable"
                        UShort.TryParse(child.Value, measure.MinDetectable)
                    Case "MaxDetectable"
                        UShort.TryParse(child.Value, measure.MaxDetectable)
                    Case "Active"
                        Boolean.TryParse(child.Value, measure.Active)
                    Case "RegionsOfInterest"
                        If (child.HasElements) Then
                            For Each grandChild In child.Elements
                                If (grandChild.Name = "RegionOfInterest") Then
                                    If (grandChild.HasElements) Then
                                        roi = (New RegionOfInterest).FromXML(grandchild)
                                        If (roi IsNot Nothing) Then
                                            measure.RegionsOfInterest.Add(roi)
                                        End If
                                    End If
                                End If
                            Next
                        End If
                    Case Else
                        ActivityLog.LogError(New StackTrace, "Unrecognized property name: " & propName)
                End Select
            Next
        End If
        Return measure
    End Function

    ' Auto Properties --------------------------------------------------------

    Public Property MinTemperature()  As UShort

    Public Property MaxTemperature()  As UShort

    Public Property MinDetectable()   As UShort

    Public Property MaxDetectable()   As UShort

    Public Property RegionsOfInterest As List(Of RegionOfInterest) = New List(Of RegionOfInterest)()

    Public Property Active()          As Boolean

End Class

Public Class RegionOfInterest '========================================================================================
    Inherits ConfigurationBase

    Private _name          As String = "Automatically Named"
    Private _operatingMode As String = String.Empty

    Public Sub New()
        CanMessageLib.OperatingModeStrDict.TryGetValue(CTS_RF_Sensor_Tool.OperatingMode.S12, _operatingMode)
    End Sub

    Public Sub New(ByVal name As String, ByVal indie As Boolean, ByVal mode As String, ByVal start As Double,
                   ByVal stepSize As Double, ByVal count As UShort, ByVal interval As UShort, ByVal number As Byte,
                   ByVal stepDly As Byte, ByVal lockDly As UShort, ByVal statWeights As List(Of ROIStatisticWeight))

        _name            = StringCopy(name)
        Independent      = indie
        _operatingMode   = StringCopy(mode)
        StartFrequency   = start
        FreqStepSize     = stepSize
        StepCount        = count
        SweepInterval    = interval
        ReadingsForAvg   = number
        TimeBetweenSteps = stepDly
        LockDelay        = lockDly
        StatisticWeights = New List(Of ROIStatisticWeight)(statWeights)
    End Sub

    Public Sub New(ByVal previous As RegionOfInterest)

        _name            = StringCopy(previous._name)
        Independent      = previous.Independent
        _operatingMode   = StringCopy(previous._operatingMode)
        StartFrequency   = previous.StartFrequency
        FreqStepSize     = previous.FreqStepSize
        StepCount        = previous.StepCount
        SweepInterval    = previous.SweepInterval
        ReadingsForAvg   = previous.ReadingsForAvg
        TimeBetweenSteps = previous.TimeBetweenSteps
        LockDelay        = previous.LockDelay
        StatisticWeights = New List(Of ROIStatisticWeight)(previous.StatisticWeights)
    End Sub

    Public Shared Operator =(ByVal a As RegionOfInterest, ByVal b As RegionOfInterest) As Boolean

        Dim result As Boolean

        result = a._name            = b._name AndAlso
                 a._operatingMode   = b._operatingMode AndAlso
                 a.StartFrequency   = b.StartFrequency AndAlso
                 a.FreqStepSize     = b.FreqStepSize   AndAlso
                 a.StepCount        = b.StepCount      AndAlso
                 a.SweepInterval    = b.SweepInterval  AndAlso
                 a.ReadingsForAvg   = b.ReadingsForAvg
                 a.TimeBetweenSteps = b.TimeBetweenSteps
                 a.LockDelay        = b.LockDelay

        ' Compare StatisticWeight lists.
        If (result And a.StatisticWeights.Count = a.StatisticWeights.Count) Then
            Dim count = 0
            For idx = 0 To a.StatisticWeights.Count - 1
                For idx2 = 0 To b.StatisticWeights.Count - 1
                    If (a.StatisticWeights(idx) = b.StatisticWeights(idx2)) Then
                        count += 1
                    End If
                Next
            Next
            result = result And count = a.StatisticWeights.Count
        Else
            result = false
        End If

        Return result
    End Operator

    Public Shared Operator <>(ByVal a As RegionOfInterest, ByVal b As RegionOfInterest) As Boolean
        Return Not a = b
    End Operator

    Public Shared Function OperatingModeStrToEnum(ByVal operatingModeStr As String) As OperatingMode

        Dim operMode = CTS_RF_Sensor_Tool.OperatingMode.Undef

        Select Case operatingModeStr
            Case "S11"
                operMode = CTS_RF_Sensor_Tool.OperatingMode.S11
            Case "S12"
                operMode = CTS_RF_Sensor_Tool.OperatingMode.S12
            Case "Bypass"
                operMode = CTS_RF_Sensor_Tool.OperatingMode.Bypass
            Case "S11Load"
                operMode = CTS_RF_Sensor_Tool.OperatingMode.S11Load
            Case "S11Open"
                operMode = CTS_RF_Sensor_Tool.OperatingMode.S11Open
        End Select
        Return operMode
    End Function

    Public Property Name() As String
        Get
            Return StringCopy(If(_name, ""))
        End Get
        Set(ByVal value As String)
            _name = StringCopy(value)
        End Set
    End Property

    Public Property OperatingMode() As String
        Get
            Return StringCopy(If(_operatingMode, ""))
        End Get
        Set(ByVal value As String)
            _operatingMode = StringCopy(value)
        End Set
    End Property

    Public Function ContainsStatistic(ByVal stat As RfStatistic) As Boolean
    
        Dim result = False

        For Each statweight In StatisticWeights
            If (stat = statweight.Statistic) Then
                result = True
                Exit For
            End If
        Next
        Return result
    End Function

    Public Function RemoveStatistic(ByVal stat As RfStatistic) As Boolean
    
        Dim result = False

        For Each statWeight In StatisticWeights
            If (stat = statWeight.Statistic) Then
                StatisticWeights.Remove(statWeight)
                result = True
                Exit For
            End If
        Next
        Return result
    End Function

    Public Function GetWeight(ByVal stat As RfStatistic) As UShort

        Dim result As UShort

        For Each statWeight In StatisticWeights
            If (stat = statWeight.Statistic) Then
                result = statWeight.Weight
                Exit For
            End If
        Next
        Return result
    End Function

    Public Overrides Function ToXml() As XElement

        Dim xEleStatWeights As XElement
        Dim xEleROI = New XElement("RegionOfInterest")

        xEleROI.Add(New XElement("Name"            , _name))
        xEleROI.Add(New XElement("OperatingMode"   , _operatingMode))
        xEleROI.Add(New XElement("Independent"     , Independent))
        xEleROI.Add(New XElement("StartFrequency"  , StartFrequency))
        xEleROI.Add(New XElement("FreqStepSize"    , FreqStepSize))
        xEleROI.Add(New XElement("StepCount"       , StepCount))
        xEleROI.Add(New XElement("SweepInterval"   , SweepInterval))
        xEleROI.Add(New XElement("ReadingsForAvg"  , ReadingsForAvg))
        xEleROI.Add(New XElement("TimeBetweenSteps", TimeBetweenSteps))
        xEleROI.Add(New XElement("LockDelay"       , LockDelay))
        xEleStatWeights = New XElement("StatisticWeights")
        For Each statWeight In StatisticWeights
            Dim xEleStatWeight = New XElement("StatisticWeight")
            xEleStatWeight.Add(New XElement("Statistic", statWeight.Statistic))
            xEleStatWeight.Add(New XElement("Weight"   , statWeight.Weight))
            xEleStatWeights.Add(xEleStatWeight)
        Next
        xEleROI.Add(xEleStatWeights)

        Return xEleROI
    End Function

    Public Overrides Function FromXML(ByVal ele As XElement) As Object

        Dim roi        As RegionOfInterest = nothing
        Dim statWeight As ROIStatisticWeight
        Dim propName   As String

        If (ele.HasElements) Then
            roi = New RegionOfInterest()
            For Each child In ele.Elements
                propName = child.Name.ToString()
                Select (propName)
                    Case "Name"
                        roi.Name = child.Value
                    Case "OperatingMode"
                        roi.OperatingMode = child.Value
                    Case "Independent"
                        Boolean.TryParse(child.Value, roi.Independent)
                    Case "StartFrequency"
                         Double.TryParse(child.Value, roi.StartFrequency)
                    Case "FreqStepSize"
                        Double.TryParse(child.Value, roi.FreqStepSize)
                    Case "StepCount"
                        UShort.TryParse(child.Value, roi.StepCount)
                    Case "SweepInterval"
                        UShort.TryParse(child.Value, roi.SweepInterval)
                    Case "ReadingsForAvg"
                        Byte.TryParse(child.Value, roi.ReadingsForAvg)
                    Case "TimeBetweenSteps"
                        Byte.TryParse(child.Value, roi.TimeBetweenSteps)
                    Case "LockDelay"
                        UShort.TryParse(child.Value, roi.LockDelay)
                    Case "StatisticWeights"
                        If (child.HasElements) Then
                            For Each grandChild In child.Elements
                                If (grandChild.Name = "StatisticWeight") Then
                                    If (grandChild.HasElements) Then
                                        statWeight = New ROIStatisticWeight()
                                        For Each greatGrandChild In grandChild.Elements

                                            ' Assumes one statistic and one weight per StatisticWeight.
                                            If (greatGrandChild.Name = "Statistic") Then
                                                Common.StatisticDict.TryGetValue(greatGrandChild.Value,
                                                                                 statWeight.Statistic)
                                            ElseIf (greatGrandChild.Name = "Weight") Then
                                                UShort.TryParse(greatGrandChild.Value, statWeight.Weight)
                                            End If
                                        Next
                                        roi.StatisticWeights.Add(statWeight)
                                    End If
                                End If
                            Next
                        End If
                    Case Else
                        ActivityLog.LogError(New StackTrace, "Unrecognized property name: " & propName)
                End Select
            Next
        End If
        Return roi
    End Function

    ' Auto Properties -----------------------------------------------------------------------------------

    ' True for Independent ROIs.
    Public Property Independent()      As Boolean

    Public Property ReadingsForAvg()   As Byte     = Common.ReadingsForAvgDefault

    Public Property StartFrequency()   As Double   = Common.StartFreqDefault      ' kHz

    Public Property FreqStepSize()     As Double   = Common.FreqStepSizeDefault   ' KHz

    Public Property StepCount()        As UShort   = Common.FreqStepCountDefault

    Public Property SweepInterval()    As UShort   = Common.SweepIntervalDefault  ' milliseconds

    Public Property TimeBetweenSteps() As Byte     = Common.StepDelayDefault      ' milliseconds

    Public Property LockDelay()        As UShort   = Common.LockDelayDefault      ' microseconds

    Public Property StatisticWeights() As List(Of ROIStatisticWeight) = New List(Of ROIStatisticWeight)()

End Class

Public Class ROIStatisticWeight '======================================================================================

    Public Sub New()
    End Sub

    Public Sub New(ByVal stat As RfStatistic, ByVal statWeight As UShort)
        Statistic = stat
        Weight    = statWeight
    End Sub

    Public Sub New(ByVal previous As ROIStatisticWeight)
        Statistic = previous.Statistic
        Weight    = previous.Weight
    End Sub

    Public Shared Operator =(ByVal a As ROIStatisticWeight, ByVal b As ROIStatisticWeight) As Boolean
        Return a.Statistic = b.Statistic AndAlso
               a.Weight    = b.Weight
    End Operator

    Public Shared Operator <>(ByVal a As ROIStatisticWeight, ByVal b As ROIStatisticWeight) As Boolean
        Return Not a = b
    End Operator

    Public Property Statistic As RfStatistic
    Public Property Weight    As UShort

End Class

Public Class RunConfiguration '========================================================================================
    Inherits ConfigurationBase

    Private _outputFileDirectory As String = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
    Private _outputFileName      As String

    Public Sub New()
        CanMessageLib.OperatingModeStrDict.TryGetValue(CTS_RF_Sensor_Tool.OperatingMode.S12, OperatingMode)
    End Sub

    Public Sub New(ByVal interval As UShort, ByVal lockDly As Ushort, ByVal time As Byte,
                   ByVal [on] As Boolean, ByVal calSweepForAvg As UInteger,
                   ByVal outPwr As OutputPowerLevel, ByVal auxOutPwr As OutputPowerLevel,
                   ByVal msgRate As Byte, ByVaL rate As UShort, ByVal operMode As String,
                   ByVal minDetect As UShort, ByVal maxDetect As UShort)

        SweepInterval        = interval
        LockDelay            = lockDly
        TimeBetweenSteps     = time
        CalibrationOn        = [on]
        ReadingsForAvg       = calSweepForAvg
        OutputPower          = outPwr
        AuxOutputPower       = auxOutPwr 
        ScanDataMsgRate      = msgRate
        SweepRate         = rate
        OperatingMode        = StringCopy(operMode)
        MinDetectable        = minDetect
        MaxDetectable        = maxDetect
    End Sub

    Public Sub New(ByVal previous As RunConfiguration)

        SweepInterval        = previous.SweepInterval
        LockDelay            = previous.LockDelay
        TimeBetweenSteps     = previous.TimeBetweenSteps
        CalibrationOn        = previous.CalibrationOn
        ReadingsForAvg       = previous.ReadingsForAvg
        OutputPower          = previous.OutputPower
        AuxOutputPower       = previous.AuxOutputPower
        ScanDataMsgRate      = previous.ScanDataMsgRate
        SweepRate         = previous.SweepRate
        OperatingMode        = previous.OperatingMode
        MinDetectable        = previous.MinDetectable
        MaxDetectable        = previous.MaxDetectable
    End Sub

    ' Auto Properties ------------------------------------------------------------------------------------

    ' Values used for manually initated scan.

    Public Property SweepInterval()    As UShort           = Common.SweepIntervalDefault ' milliseconds

    Public Property LockDelay()        As UShort           = Common.LockDelayDefault     ' microseconds

    Public Property TimeBetweenSteps() As Byte             = Common.StepDelayDefault     ' milliseconds

    Public Property CalibrationOn()    As Boolean

    Public Property ReadingsForAvg()   As Byte             = Common.ReadingsForAvgDefault

    Public Property OutputPower()      As OutputPowerLevel = OutputPowerLevel.Minus4dBm

    Public Property AuxOutputPower()   As OutputPowerLevel = OutputPowerLevel.Minus4dBm

    Public Property ScanDataMsgRate()  As Byte             = Common.ScanDataMsgRateDefault ' milliseconds.

    Public Property SweepRate()        As UInteger         = Common.SweepRateDefault    ' Seconds.

    Public Property OperatingMode()    As String

    Public Property MinDetectable()    As UShort

    Public Property MaxDetectable()    As UShort

End Class
