﻿'CAN Message Enums.

Public Enum RfStatistic As UInteger
    None                     = &H0000
    SummedPhase              = &H0001
    AveragePhase             = &H0002
    SumMagnitudes            = &H0004
    AverageMagnitude         = &H0008
    PeakMagnitude            = &H0010
    FreqAtPeakMagnitude      = &H0020
    PhaseAtPeakMagnitude     = &H0040
    MinMagnitude             = &H0080
    FreqAtMinMagnitude       = &H0100
    PhaseAtMinMagnitude      = &H0200
    Degree90CrossoverFreq    = &H0400
    MagnitudeAtCrossoverFreq = &H0800
    HalfWidthIfMagnitude     = &H1000
    QualityOfMagnitude       = &H2000
End Enum

' RF Sensor application states
Public Enum AppState As Byte
    NormalOperation       = 1
    RunSingleScan         = 2
    RunContinuousScan     = 3
    SetCalibrationOption  = 5
    StopMeasurement       = 6
    CommandComplete       = 7
    DiagShutdown          = 8
    WaitingForCommand     = 9
    ConfigVco1            = 11
    ConfigVco2            = 12
    ConfigStepParameters  = 13
    PwrOffShutdownPending = 19
    PwrOffActionsComplete = 20
    ConfigSweep           = 23
    PowerUpScan           = 30
    TBD                   = 63
    Undef                 = 250
    Reset                 = 255
End Enum

' DBL Send tool message opcodes.
Public Enum DblSendOpCodes As UInteger
    DblAttention          = &H0
    DblInitateWrite       = &H1
    DblMemWriteData       = &H2
    DblInitiateRead       = &H3
    DblMemReadDataAck     = &H4
    DblCalcChksum         = &H7
    DblReadSerialNumber   = &H8
    DblWriteSerNumToFlash = &HA
    DblClrActiveSession   = &HB
    DblCloseSession       = &H11
    DblStartApp           = &H12
    DblRestart            = &H13
End Enum

' Received DBL tool message response opcodes.
Public Enum DblRcvOpCodes As UInteger
    DblAttnResp          = &H0
    DblInitWriteAck      = &H1
    DblMemWriteDataAck   = &H2
    DblInitReadAck       = &H3
    DblMemReadData       = &H4
    DblChksumResult      = &H7
    DblSerialNumberResp  = &H8
    DblWriteSerNumAck    = &HA
    DblActiveSessionClrd = &HB
    DblCloseSessionAck   = &H11
    DblStartAppAck       = &H12
    DblRestartAck        = &H13
    DblFaultResp         = &H32
End Enum

Public Enum DblErrorCodes As Byte
    UnsupportedCommand     = 1
    UnsupportedLocation    = 2
    WriteSequenceViolation = 17
    ReadSequenceViolation  = 18
    CommandSequenceError   = 19
    StartAddressOutOfRange = 33
    EndAddressOutOfRange   = 34
    AddressOrderEndLtStart = 35
    AddressOutOfRange      = 36
    WriteToProtectedArea   = 49
    ReadFromProtectedArea  = 50
    TooMuchData            = 66
    ReadAcknowledgeError   = 67
    ReadAcknowledgeTimeOut = 68
    ProcessError           = 70
    OtherSessionActive     = 71
    CopySizeError          = 81
    FlashWriteStatusError  = 96
    HardwareMismatch       = 100
End Enum

Public Enum PgnRequest As UInteger
    None              = 0
    AddressClaim      = &HEE00
    SoftWareID        = &HFEDA
    ApplicationConfig = &HFF0B
    ApplicationID     = &HFFDE
    DiagnosticMsg19   = &HD300
End Enum

Public Enum CalDataTypes As Byte
    AtoD = 0
    Raw = 1
    Calc = 2
End Enum

Public Enum CalDataPntMsgID As Byte
    Msg1 = &HF0
    Msg2 = &HF1
    Msg3 = &HF2
End Enum

Public Enum CalDataIndex As UInteger
    Min    = 0
    Max    = 1023
    Start  = &HFFF0
    [Step] = &HFFF1
    [End]  = &HFFF2
    Cmd    = &HFFF9
End Enum

Public Enum CalibrationType As Byte
    OneByte  = 1
    TwoByte  = 2
    FourByte = 4
End Enum

Public Enum WriteRamCalibration As Byte
    OneByte  = &H39
    TwoByte  = &H3A
    FourByte = &H3C
End Enum

Public Enum MessageTypeID As Byte
    None             = 0
    Eeprom           = &HF0
    ParamID          = &HE0
    Ram              = &HD0
    Flash            = &HC0
    TsnAccess        = &H40
    CalibrationRam   = &H30
    CalibrationFlash = &H20
    ToolFunction     = &H10
End Enum

Public Enum CommandMsgType As Byte
    NormalOperation       = 1
    RunSingleScan         = 2 
    RunContinuousScan     = 3
    StopMeasurement       = 6
    ConfigureVCO          = 11
    ConfigStepParams      = 13
    ClearCodes            = 18
    PowerOffPending       = 19
    SendToSynthesizer     = 40
End Enum

' UDS Application Supported Service IDs.
Public Enum AppSupportedServiceIDs As Byte
    None               = 0
    DiagSessionControl = &H10
    EcuReset           = &H11
    ReadDataByIdent    = &H22
    ReadMemoryByAddr   = &H23
    RoutineControl     = &H31
    WriteMemoryByAddr  = &H3D
    TesterPresent      = &H3E
    ControlDtcSetting  = &H85
End Enum

' UDS Read Memory By Address Service ID: Memory Address -> Byte #1 = Memory Type.
Public Enum ReadMemoryByAddrMemType As Byte
    None   = 0
    Flash  = &HC0
    Ram    = &HD0
    CtsPid = &HE0
    Eeprom = &HF0
End Enum

' UDS Data Identifiers (DIDs).
Public Enum UdsDataIdentifiers As UInteger
    None                   = 0
    ProgrammingAttemptCntr = &H100
    BoolLoaderVersion      = &H145
    ByteOrder              = &H2260
    EcuAuditTrail          = &H565F
    EcuBaseVariantID       = &H84FE
    EcuVariantID           = &H86CA
    CumminsEcuPartNumber   = &H92BC
    EcuSerialNumber        = &H92BD
    CumminsSoftwarePartNum = &H92BF
    DeviceName             = &H92C0
    SupplierIndentCode     = &H92C3
    AccessID               = &HB834
    ActiveDiagStatus       = &HF100
    ProtocolVersion7       = &HF10D
    HardwarePartNumber     = &HF112
    SoftwarePartNumber     = &HF122
    EcuPartNumber          = &HF132
    HardwareVersionInfo    = &HF150
    SoftwareVersionInfo    = &HF151
    BootloaderVersionInfo  = &HF153
    HardwareSupplierID     = &HF154
    SoftwareSupplierID     = &HF155
    Fingerprint            = &HF15B
End Enum

Public Enum ToolFunctionMsg As UShort
    ' Function                  OpCode   Required Enables
    None                        = 0
    JumpToDbl                   = &H2   ' None
    AccessProdHwLineWord        = &H9   ' 0x11
    AllowToolCmdFunctions       = &H11  ' None
    ReadAppSoftwareConfigs      = &H12  ' None
    ReadSoftwareVersion         = &H121 ' None
    ReadDeviceID                = &H122 ' None
    ReadSerialNumFromFlash      = &H123 ' None
    EraseEepromRequest          = &H4B  ' 0x11, 0x91, SN
    InitializeUsageData         = &H4B1 ' 0x11, 0x91, SN
    InitUsageDataIncludingTime  = &H4B2 ' 0x11, 0x91, SN
    ApplicationReset            = &H5A  ' None
    ReadDblCalsFromFlashToRam   = &H80  ' 0x11
    WriteDblCalRamArrayToFlash  = &H89  ' 0x11
    DisableEepromDataWrite      = &H90  ' None
    EnableEepromDataWrite       = &H91  ' 0x11, SN
    UpdateSerialNumberInFlash   = &HA4  ' 0x11, 0x91
    WritePartNumberLsbToFlash   = &HA8  ' 0x11
    WritePartNumberMsbToFlash   = &HA9  ' 0x11
    UpdateRamWithDefaultCalVals = &HB2  ' 0x11
    WriteAppCalsFromRamToFlash  = &HBD  ' 0x11
    UpdateAppNodeIDRequest      = &HC1  ' 0x11, 0x91, SN
    SendCalScanData             = &HD0  ' None
End Enum


Public Enum OperatingMode As Byte
    S12     = 0
    S11     = 1
    Bypass  = 2
    S11Load = 3
    S11Open = 4
    Undef   = &HFF
End Enum

Public Enum DblCalAccess As Byte
    RamRead  = &H35 ' Read command and r/w response.
    RamWrite = &H3D ' Write command only.
End Enum

Public Enum TsnAccess As Byte
    Read  = &H41 ' Read command and r/w response.
    Write = &H49 ' Write command only.
End Enum

Public Enum ReadTargetMem As Byte
    Program = 1
    Eeprom  = 3
    Dbl     = 4
    Ccbl    = 5
    Ram     = 6
End Enum

Public Enum WriteTargetMem As Byte
    Program = 1
    Eeprom  = 3
    Ccbl    = 5
End Enum

Public Enum CalTargetRam As Byte
    App = 1
    DBL = 2
End Enum

' dB relative to 1 milliwatt into 50 ohms (50 mv).
Public Enum OutputPowerLevel As Byte
    Minus4dBm = 0
    Minus1dBm = 1
    Plus2dBm  = 2
    Plus5dBm  = 3
End Enum
