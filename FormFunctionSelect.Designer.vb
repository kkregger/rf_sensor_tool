﻿Namespace CTS_RF_Sensor_Tool
	Partial Friend Class FormFunctionSelect
		''' <summary>
		''' Required designer variable.
		''' </summary>
		Private components As System.ComponentModel.IContainer = Nothing

		''' <summary>
		''' Clean up any resources being used.
		''' </summary>
		''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		Protected Overrides Sub Dispose(ByVal disposing As Boolean)
			If disposing AndAlso (components IsNot Nothing) Then
				components.Dispose()
			End If
			MyBase.Dispose(disposing)
		End Sub

		#Region "Windows Form Designer generated code"

		''' <summary>
		''' Required method for Designer support - do not modify
		''' the contents of this method with the code editor.
		''' </summary>
		Private Sub InitializeComponent()
			Me.components = New System.ComponentModel.Container()
			Dim resources As New System.ComponentModel.ComponentResourceManager(GetType(FormFunctionSelect))
			Me.toolTip1 = New System.Windows.Forms.ToolTip(Me.components)
			Me.RunOSL_CalibrationAndNormalizationButton = New System.Windows.Forms.Button()
			Me.ConfigureRegionButton = New System.Windows.Forms.Button()
			Me.LockDeviceButton = New System.Windows.Forms.Button()
			Me.FunctionSelectGroupBox = New System.Windows.Forms.GroupBox()
			Me.FunctionSelectLabel = New System.Windows.Forms.Label()
			Me.ComPortConfigButton = New System.Windows.Forms.Button()
			Me.ConfigureSweepButton = New System.Windows.Forms.Button()
			Me.ConfigureCalibrationButton = New System.Windows.Forms.Button()
			Me.ConfigureMeasurementButton = New System.Windows.Forms.Button()
			Me.ShowDebugDataButton = New System.Windows.Forms.Button()
			Me.ShowLogDisplayButton = New System.Windows.Forms.Button()
			Me.FlashDeviceButton = New System.Windows.Forms.Button()
			Me.DecryptFileButton = New System.Windows.Forms.Button()
			Me.ConfigureCAN_DeviceButton = New System.Windows.Forms.Button()
			Me.SuspendLayout()
			' 
			' RunOSL_CalibrationAndNormalizationButton
			' 
			Me.RunOSL_CalibrationAndNormalizationButton.BackColor = System.Drawing.Color.Chartreuse
			Me.RunOSL_CalibrationAndNormalizationButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (CByte(0)))
			Me.RunOSL_CalibrationAndNormalizationButton.Location = New System.Drawing.Point(9, 211)
			Me.RunOSL_CalibrationAndNormalizationButton.Name = "RunOSL_CalibrationAndNormalizationButton"
			Me.RunOSL_CalibrationAndNormalizationButton.Size = New System.Drawing.Size(150, 23)
			Me.RunOSL_CalibrationAndNormalizationButton.TabIndex = 9
			Me.RunOSL_CalibrationAndNormalizationButton.Text = "Run OSL Cal and Norm"
			Me.toolTip1.SetToolTip(Me.RunOSL_CalibrationAndNormalizationButton, "Run OSL Calibration and Normalization.")
			Me.RunOSL_CalibrationAndNormalizationButton.UseVisualStyleBackColor = False
'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
'ORIGINAL LINE: this.RunOSL_CalibrationAndNormalizationButton.Click += new System.EventHandler(this.RunOSL_CalibrationAndNormalizationButton_Click);
			' 
			' ConfigureRegionButton
			' 
			Me.ConfigureRegionButton.BackColor = System.Drawing.Color.Chartreuse
			Me.ConfigureRegionButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (CByte(0)))
			Me.ConfigureRegionButton.Location = New System.Drawing.Point(9, 121)
			Me.ConfigureRegionButton.Name = "ConfigureRegionButton"
			Me.ConfigureRegionButton.Size = New System.Drawing.Size(150, 23)
			Me.ConfigureRegionButton.TabIndex = 4
			Me.ConfigureRegionButton.Text = "Configure  Region"
			Me.toolTip1.SetToolTip(Me.ConfigureRegionButton, "Region of Interest configuration.")
			Me.ConfigureRegionButton.UseVisualStyleBackColor = False
'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
'ORIGINAL LINE: this.ConfigureRegionButton.Click += new System.EventHandler(this.ConfigureRegionButton_Click);
			' 
			' LockDeviceButton
			' 
			Me.LockDeviceButton.BackColor = System.Drawing.Color.Gold
			Me.LockDeviceButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (CByte(0)))
			Me.LockDeviceButton.Location = New System.Drawing.Point(9, 361)
			Me.LockDeviceButton.Name = "LockDeviceButton"
			Me.LockDeviceButton.Size = New System.Drawing.Size(150, 23)
			Me.LockDeviceButton.TabIndex = 16
			Me.LockDeviceButton.Text = "Lock"
			Me.toolTip1.SetToolTip(Me.LockDeviceButton, "Lock-out all functions except COM port configuration.")
			Me.LockDeviceButton.UseVisualStyleBackColor = False
'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
'ORIGINAL LINE: this.LockDeviceButton.Click += new System.EventHandler(this.LockDeviceButton_Click);
			' 
			' FunctionSelectGroupBox
			' 
			Me.FunctionSelectGroupBox.Location = New System.Drawing.Point(13, 20)
			Me.FunctionSelectGroupBox.Name = "FunctionSelectGroupBox"
			Me.FunctionSelectGroupBox.Size = New System.Drawing.Size(141, 5)
			Me.FunctionSelectGroupBox.TabIndex = 13
			Me.FunctionSelectGroupBox.TabStop = False
			' 
			' FunctionSelectLabel
			' 
			Me.FunctionSelectLabel.AutoSize = True
			Me.FunctionSelectLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (CByte(0)))
			Me.FunctionSelectLabel.ForeColor = System.Drawing.Color.White
			Me.FunctionSelectLabel.Location = New System.Drawing.Point(36, 7)
			Me.FunctionSelectLabel.Name = "FunctionSelectLabel"
			Me.FunctionSelectLabel.Size = New System.Drawing.Size(96, 13)
			Me.FunctionSelectLabel.TabIndex = 1
			Me.FunctionSelectLabel.Text = "Function Select"
			Me.FunctionSelectLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
			' 
			' ComPortConfigButton
			' 
			Me.ComPortConfigButton.BackColor = System.Drawing.Color.Chartreuse
			Me.ComPortConfigButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (CByte(0)))
			Me.ComPortConfigButton.Location = New System.Drawing.Point(9, 31)
			Me.ComPortConfigButton.Name = "ComPortConfigButton"
			Me.ComPortConfigButton.Size = New System.Drawing.Size(150, 23)
			Me.ComPortConfigButton.TabIndex = 2
			Me.ComPortConfigButton.Text = "Configure  COM Port"
			Me.ComPortConfigButton.UseVisualStyleBackColor = False
'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
'ORIGINAL LINE: this.ComPortConfigButton.Click += new System.EventHandler(this.ComPortConfigButton_Click);
			' 
			' ConfigureSweepButton
			' 
			Me.ConfigureSweepButton.BackColor = System.Drawing.Color.Chartreuse
			Me.ConfigureSweepButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (CByte(0)))
			Me.ConfigureSweepButton.Location = New System.Drawing.Point(9, 91)
			Me.ConfigureSweepButton.Name = "ConfigureSweepButton"
			Me.ConfigureSweepButton.Size = New System.Drawing.Size(150, 23)
			Me.ConfigureSweepButton.TabIndex = 3
			Me.ConfigureSweepButton.Text = "Configure  Sweep"
			Me.ConfigureSweepButton.UseVisualStyleBackColor = False
'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
'ORIGINAL LINE: this.ConfigureSweepButton.Click += new System.EventHandler(this.ConfigureSweepButton_Click);
			' 
			' ConfigureCalibrationButton
			' 
			Me.ConfigureCalibrationButton.BackColor = System.Drawing.Color.Chartreuse
			Me.ConfigureCalibrationButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (CByte(0)))
			Me.ConfigureCalibrationButton.Location = New System.Drawing.Point(9, 151)
			Me.ConfigureCalibrationButton.Name = "ConfigureCalibrationButton"
			Me.ConfigureCalibrationButton.Size = New System.Drawing.Size(150, 23)
			Me.ConfigureCalibrationButton.TabIndex = 5
			Me.ConfigureCalibrationButton.Text = "Configure  Calibration"
			Me.ConfigureCalibrationButton.UseVisualStyleBackColor = False
'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
'ORIGINAL LINE: this.ConfigureCalibrationButton.Click += new System.EventHandler(this.ConfigureCalibrationButton_Click);
			' 
			' ConfigureMeasurementButton
			' 
			Me.ConfigureMeasurementButton.BackColor = System.Drawing.Color.Chartreuse
			Me.ConfigureMeasurementButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (CByte(0)))
			Me.ConfigureMeasurementButton.Location = New System.Drawing.Point(9, 181)
			Me.ConfigureMeasurementButton.Name = "ConfigureMeasurementButton"
			Me.ConfigureMeasurementButton.Size = New System.Drawing.Size(150, 23)
			Me.ConfigureMeasurementButton.TabIndex = 15
			Me.ConfigureMeasurementButton.Text = "Configure Measurement"
			Me.ConfigureMeasurementButton.UseVisualStyleBackColor = False
'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
'ORIGINAL LINE: this.ConfigureMeasurementButton.Click += new System.EventHandler(this.ConfigureMeasurementButton_Click);
			' 
			' ShowDebugDataButton
			' 
			Me.ShowDebugDataButton.BackColor = System.Drawing.Color.Cyan
			Me.ShowDebugDataButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (CByte(0)))
			Me.ShowDebugDataButton.Location = New System.Drawing.Point(9, 331)
			Me.ShowDebugDataButton.Name = "ShowDebugDataButton"
			Me.ShowDebugDataButton.Size = New System.Drawing.Size(150, 23)
			Me.ShowDebugDataButton.TabIndex = 7
			Me.ShowDebugDataButton.Text = "Show Debug Data"
			Me.ShowDebugDataButton.UseVisualStyleBackColor = False
'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
'ORIGINAL LINE: this.ShowDebugDataButton.Click += new System.EventHandler(this.ShowDebugDataButton_Click);
			' 
			' ShowLogDisplayButton
			' 
			Me.ShowLogDisplayButton.BackColor = System.Drawing.Color.Cyan
			Me.ShowLogDisplayButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (CByte(0)))
			Me.ShowLogDisplayButton.Location = New System.Drawing.Point(9, 301)
			Me.ShowLogDisplayButton.Name = "ShowLogDisplayButton"
			Me.ShowLogDisplayButton.Size = New System.Drawing.Size(150, 23)
			Me.ShowLogDisplayButton.TabIndex = 8
			Me.ShowLogDisplayButton.Text = "Show Log Display"
			Me.ShowLogDisplayButton.UseVisualStyleBackColor = False
'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
'ORIGINAL LINE: this.ShowLogDisplayButton.Click += new System.EventHandler(this.ShowLogDisplayButton_Click);
			' 
			' FlashDeviceButton
			' 
			Me.FlashDeviceButton.BackColor = System.Drawing.Color.Chartreuse
			Me.FlashDeviceButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (CByte(0)))
			Me.FlashDeviceButton.Location = New System.Drawing.Point(9, 241)
			Me.FlashDeviceButton.Name = "FlashDeviceButton"
			Me.FlashDeviceButton.Size = New System.Drawing.Size(150, 23)
			Me.FlashDeviceButton.TabIndex = 10
			Me.FlashDeviceButton.Text = "Flash Device"
			Me.FlashDeviceButton.UseVisualStyleBackColor = False
'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
'ORIGINAL LINE: this.FlashDeviceButton.Click += new System.EventHandler(this.FlashDeviceButton_Click);
			' 
			' DecryptFileButton
			' 
			Me.DecryptFileButton.BackColor = System.Drawing.Color.Chartreuse
			Me.DecryptFileButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (CByte(0)))
			Me.DecryptFileButton.Location = New System.Drawing.Point(9, 271)
			Me.DecryptFileButton.Name = "DecryptFileButton"
			Me.DecryptFileButton.Size = New System.Drawing.Size(150, 23)
			Me.DecryptFileButton.TabIndex = 11
			Me.DecryptFileButton.Text = "Decrypt File"
			Me.DecryptFileButton.UseVisualStyleBackColor = False
'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
'ORIGINAL LINE: this.DecryptFileButton.Click += new System.EventHandler(this.DecryptFileButton_Click);
			' 
			' ConfigureCAN_DeviceButton
			' 
			Me.ConfigureCAN_DeviceButton.BackColor = System.Drawing.Color.Chartreuse
			Me.ConfigureCAN_DeviceButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (CByte(0)))
			Me.ConfigureCAN_DeviceButton.Location = New System.Drawing.Point(9, 61)
			Me.ConfigureCAN_DeviceButton.Name = "ConfigureCAN_DeviceButton"
			Me.ConfigureCAN_DeviceButton.Size = New System.Drawing.Size(150, 23)
			Me.ConfigureCAN_DeviceButton.TabIndex = 17
			Me.ConfigureCAN_DeviceButton.Text = "Configure  CAN Device"
			Me.ConfigureCAN_DeviceButton.UseVisualStyleBackColor = False
'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
'ORIGINAL LINE: this.ConfigureCAN_DeviceButton.Click += new System.EventHandler(this.ConfigureCAN_DeviceButton_Click);
			' 
			' FormFunctionSelect
			' 
			Me.AutoScaleDimensions = New System.Drawing.SizeF(6F, 13F)
			Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
			Me.BackColor = System.Drawing.Color.DimGray
			Me.ClientSize = New System.Drawing.Size(167, 392)
			Me.Controls.Add(Me.ConfigureCAN_DeviceButton)
			Me.Controls.Add(Me.LockDeviceButton)
			Me.Controls.Add(Me.DecryptFileButton)
			Me.Controls.Add(Me.FlashDeviceButton)
			Me.Controls.Add(Me.RunOSL_CalibrationAndNormalizationButton)
			Me.Controls.Add(Me.ShowLogDisplayButton)
			Me.Controls.Add(Me.ShowDebugDataButton)
			Me.Controls.Add(Me.ConfigureMeasurementButton)
			Me.Controls.Add(Me.ConfigureCalibrationButton)
			Me.Controls.Add(Me.ConfigureRegionButton)
			Me.Controls.Add(Me.ConfigureSweepButton)
			Me.Controls.Add(Me.ComPortConfigButton)
			Me.Controls.Add(Me.FunctionSelectLabel)
			Me.Controls.Add(Me.FunctionSelectGroupBox)
			Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
			Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
			Me.Icon = (CType(resources.GetObject("$this.Icon"), System.Drawing.Icon))
			Me.MaximizeBox = False
			Me.Name = "FormFunctionSelect"
			Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
			Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
'ORIGINAL LINE: this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FunctionSelectForm_FormClosing);
			Me.ResumeLayout(False)
			Me.PerformLayout()

		End Sub

		#End Region
		Private toolTip1 As System.Windows.Forms.ToolTip
		Private FunctionSelectGroupBox As System.Windows.Forms.GroupBox
		Private FunctionSelectLabel As System.Windows.Forms.Label
		Private WithEvents ComPortConfigButton As System.Windows.Forms.Button
		Private WithEvents ConfigureSweepButton As System.Windows.Forms.Button
		Private WithEvents ConfigureRegionButton As System.Windows.Forms.Button
		Private WithEvents ConfigureCalibrationButton As System.Windows.Forms.Button
		Private WithEvents ConfigureMeasurementButton As System.Windows.Forms.Button
		Private WithEvents ShowDebugDataButton As System.Windows.Forms.Button
		Private WithEvents ShowLogDisplayButton As System.Windows.Forms.Button
		Private WithEvents RunOSL_CalibrationAndNormalizationButton As System.Windows.Forms.Button
		Private WithEvents FlashDeviceButton As System.Windows.Forms.Button
		Private WithEvents DecryptFileButton As System.Windows.Forms.Button
		Private WithEvents LockDeviceButton As System.Windows.Forms.Button
		Private WithEvents ConfigureCAN_DeviceButton As System.Windows.Forms.Button
	End Class
End Namespace