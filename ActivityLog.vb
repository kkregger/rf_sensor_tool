﻿Imports System
Imports System.IO
Imports System.Windows.Forms

    Public NotInheritable Class ActivityLog

        ' Error/exception logging delegates.
        Friend Delegate Sub LogErrorDelegate(ByVal errorStr As String)
        Friend Delegate Sub LogErrorStackDelegate(ByVal trace As StackTrace, ByVal errorStr As String)
        Friend Delegate Sub LogExceptionDelegate(ByVal trace As StackTrace, ByVal ex As Exception)
        Private Shared _appDataPath      As String = ""
        Private Shared _errorLogFilePath As String = ""
        Private Shared _errorLogFileName As String = ""

        Public Shared Sub InitErrorLogging()

            ' Call this procedure on startup.
            Dim todaysDate As Date = Date.Today
            Dim monthName As String = Date.Now.ToString("MMMM")
            Dim year As String = Date.Now.ToString("yyyy")
            Dim sw As StreamWriter = Nothing

            Try
                ' Add leading zero to single digit month
                If monthName.Length < 2 Then
                    monthName = "0" & monthName
                End If

                ' Applicaton startup folder
                _appDataPath = Application.StartupPath

                ' Root Path - Follows ISO8601   YYYY-MM
                Dim errorLogfilePath = Path.Combine(_appDataPath, year & "-" & monthName)

                If Not Directory.Exists(errorLogfilePath) Then
                    Directory.CreateDirectory(errorLogfilePath)
                End If

                ' Create Error log file pathname. Euphemistically referred to as the Activity Log.
                _errorLogFileName = todaysDate.ToShortDateString().Replace("/", "-") & "_Activity.Log"
                _errorLogFilePath = Path.Combine(errorLogfilePath, _errorLogFileName)

                ' Open or create text file for appending.
                sw = New StreamWriter(_errorLogFilePath, True)

                    ' Write header info to the file
                    sw.WriteLine("CTS RF Sensor Tool " & Application.ProductVersion & " starting @ " & Date.Now.ToLongTimeString)
                    sw.WriteLine("Application startup path: " & _appDataPath)
                    sw.WriteLine("File path: " & errorLogfilePath & vbcrlf)
                    sw.WriteLine("")

            Catch ex As Exception
            Finally
                If (sw?.BaseStream IsNot Nothing) Then
                    ' Close the StreamWriter.
                    sw.Close()
                End If
            End Try
        End Sub
        
        Public Shared Sub LogError(ByVal trace As StackTrace, ByVal ex As Exception)

            ' Call directly from the catch blocks of thread processes, otherwise use an event.
            Dim errorLogStr As String = "Exception:" & vbCrLf

            If (trace.FrameCount >= 2) Then
                errorLogStr &= " Caller = "   & trace.GetFrame(1).GetMethod.Name & vbCrLf
                errorLogStr &= " Method = "   & trace.GetFrame(0).GetMethod.Name & vbCrLf
                errorLogStr &= " " & ex.Message   & vbCrLf & ex.StackTrace
            Else
                errorLogStr = ex.Message & vbCrLf & ex.StackTrace
            End If
            LogError(errorLogStr)
        End Sub

        Public Shared Sub LogError(ByVal trace As StackTrace, ByVal errorStr As String)

        ' Call or use an event to log errors with stack info.
            Dim errorLogStr As String = "Error:" & vbCrLf

            If (trace.FrameCount >= 2) Then
                errorLogStr &= " Caller = "   & trace.GetFrame(1).GetMethod.Name & vbCrLf
                errorLogStr &= " Method = "   & trace.GetFrame(0).GetMethod.Name & vbCrLf
                errorLogStr &= " " & errorStr
            Else
                errorLogStr = errorStr
            End If
            LogError(errorLogStr)
        End Sub

        Public Shared Sub LogError(ByVal errorStr As String)

            ' Call or use an event to log errors without stack info.
            ' Log an error/exception message to file.
            Dim sw As StreamWriter = Nothing

            Try
                If (String.IsNullOrWhiteSpace(_errorLogFilePath) OrElse
                    _errorLogFileName <> Date.Today.ToShortDateString().Replace("/", "-") & "_Activity.Log") Then

                    InitErrorLogging()
                End If

                ' Open or create text file for appending.
                sw = New StreamWriter(_errorLogFilePath, True)

                ' Write the date, time and error description to the file
                sw.WriteLine(Date.Now.ToString("yyyy-MM-dd") & "," & Date.Now.ToString("hh:mm:ss"))
                sw.WriteLine(errorStr)
                sw.WriteLine("")

            Catch ex As Exception
            Finally
                If (sw?.BaseStream IsNot Nothing) Then
                    ' Close the StreamWriter.
                    sw.Close()
                End If
            End Try
        End Sub
    End Class
