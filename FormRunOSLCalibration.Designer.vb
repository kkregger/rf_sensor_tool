﻿Namespace CTS_RF_Sensor_Tool
	Partial Public Class FormRunOSLCalibration
		''' <summary>
		''' Required designer variable.
		''' </summary>
		Private components As System.ComponentModel.IContainer = Nothing

		''' <summary>
		''' Clean up any resources being used.
		''' </summary>
		''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		Protected Overrides Sub Dispose(ByVal disposing As Boolean)
			If disposing AndAlso (components IsNot Nothing) Then
				components.Dispose()
			End If
			MyBase.Dispose(disposing)
		End Sub

		#Region "Windows Form Designer generated code"

		''' <summary>
		''' Required method for Designer support - do not modify
		''' the contents of this method with the code editor.
		''' </summary>
		Private Sub InitializeComponent()
			Me.components = New System.ComponentModel.Container()
			Dim resources As New System.ComponentModel.ComponentResourceManager(GetType(FormRunOSLCalibration))
			Me.ButtonCancel = New System.Windows.Forms.Button()
			Me.LabelMessage = New System.Windows.Forms.Label()
			Me.LabelRunStatus = New System.Windows.Forms.Label()
			Me.CalSweepStartTimer = New System.Windows.Forms.Timer(Me.components)
			Me.SuspendLayout()
			' 
			' ButtonCancel
			' 
			Me.ButtonCancel.BackColor = System.Drawing.Color.Gold
			Me.ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
			Me.ButtonCancel.Location = New System.Drawing.Point(117, 66)
			Me.ButtonCancel.Name = "ButtonCancel"
			Me.ButtonCancel.Size = New System.Drawing.Size(75, 23)
			Me.ButtonCancel.TabIndex = 0
			Me.ButtonCancel.Text = "Cancel"
			Me.ButtonCancel.UseVisualStyleBackColor = False
'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
'ORIGINAL LINE: this.ButtonCancel.Click += new System.EventHandler(this.ButtonCancel_Click);
			' 
			' LabelMessage
			' 
			Me.LabelMessage.Font = New System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CByte(0)))
			Me.LabelMessage.ForeColor = System.Drawing.Color.White
			Me.LabelMessage.Location = New System.Drawing.Point(1, 9)
			Me.LabelMessage.Name = "LabelMessage"
			Me.LabelMessage.Size = New System.Drawing.Size(316, 20)
			Me.LabelMessage.TabIndex = 1
			Me.LabelMessage.Text = "Running OSL Calibration and Normalization"
			Me.LabelMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
			' 
			' LabelRunStatus
			' 
			Me.LabelRunStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (CByte(0)))
			Me.LabelRunStatus.ForeColor = System.Drawing.Color.White
			Me.LabelRunStatus.Location = New System.Drawing.Point(4, 38)
			Me.LabelRunStatus.Name = "LabelRunStatus"
			Me.LabelRunStatus.Size = New System.Drawing.Size(312, 22)
			Me.LabelRunStatus.TabIndex = 2
			Me.LabelRunStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
			' 
			' CalSweepStartTimer
			' 
			Me.CalSweepStartTimer.Interval = 250
'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
'ORIGINAL LINE: this.CalSweepStartTimer.Tick += new System.EventHandler(this.CalSweepStartTimer_Tick);
			' 
			' FormRunOSLCalibration
			' 
			Me.AutoScaleDimensions = New System.Drawing.SizeF(6F, 13F)
			Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
			Me.BackColor = System.Drawing.Color.DimGray
			Me.ClientSize = New System.Drawing.Size(319, 101)
			Me.Controls.Add(Me.LabelRunStatus)
			Me.Controls.Add(Me.LabelMessage)
			Me.Controls.Add(Me.ButtonCancel)
			Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
			Me.Icon = (CType(resources.GetObject("$this.Icon"), System.Drawing.Icon))
			Me.MaximizeBox = False
			Me.Name = "FormRunOSLCalibration"
			Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
			Me.Text = "Run Calibration"
'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
'ORIGINAL LINE: this.Shown += new System.EventHandler(this.FormRunOSLCalibration_Shown);
			Me.ResumeLayout(False)

		End Sub

		#End Region

		Private WithEvents ButtonCancel As System.Windows.Forms.Button
		Private LabelMessage As System.Windows.Forms.Label
		Private LabelRunStatus As System.Windows.Forms.Label
		Private WithEvents CalSweepStartTimer As System.Windows.Forms.Timer
	End Class
End Namespace