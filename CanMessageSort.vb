﻿Imports System.Collections.Concurrent
Imports System.Threading
Imports CTS_CAN_Services

Friend NotInheritable Class CanMessageSort

    ' Key used to access subscription delivery structure.
    Private Shared _subscriptionDeliveryKey As Integer

    ' Message deliver Subscriber object.
    Private Shared ReadOnly MySubscriber As New Subscriber()

    ' CAN Message Publisher object.
    Private Shared _canMsgPublisherObj As CAN_MsgPublisher

    ' Message received callback delegate.
    Private Shared _allMsgRcvdCallbkDelegate As Subscriber.AllMessageRcvdCallbackDelegate

    ' Received CAN message queue.
    Private Shared ReadOnly RcvdMsgQueue As New ConcurrentQueue(Of CAN_Services.RcvdCANMsg)()

    ' Received message processing thread.
    Private Shared _rcvdMessageProcessingThread As Thread

    ' Receive CAN message processing event wait handle
    Private Shared _rcvdMessageProcessingThreadWaitHandle As EventWaitHandle

    ' Set when closing the application to insure that the received message
    ' processing thread proc. returns.
    Private Shared _terminateRcvdMessageProcessingThread As Boolean

    ' CAN Activity Thread
    Private Shared _canActivityThread As Thread

    ' Indicates that the default bootloader responded to
    ' request for attention.
    Private Shared _dfltBootldrResponded As Boolean

    ' Events to raise when there is an error/exception to be logged.
    Private Shared Event LogErrorEvent As ActivityLog.LogErrorStackDelegate
    Private Shared Event LogExceptionEvent As ActivityLog.LogExceptionDelegate

    Shared Sub New()

        ' Register error logging event handlers.
        AddHandler LogErrorEvent, AddressOf ActivityLog.LogError
        AddHandler LogExceptionEvent, AddressOf ActivityLog.LogError

        ' Get the CAN Services and CAN Message Publisher objects.
        _canMsgPublisherObj = FormRfSensorMain.CanServicesObj.CAN_MsgPublisherObj

        ' Create message processing thread.
        _terminateRcvdMessageProcessingThread = False
        _rcvdMessageProcessingThreadWaitHandle = New EventWaitHandle(False, EventResetMode.AutoReset)
        _rcvdMessageProcessingThread = New Thread(AddressOf ProcessRcvdCanMsgs) With {.Priority = ThreadPriority.Highest}
        _rcvdMessageProcessingThread.SetApartmentState(ApartmentState.MTA)

        ' Create and register the message callback delegate.
        _allMsgRcvdCallbkDelegate = AddressOf MsgRcvdCallback
        MySubscriber.RegisterAllMessageRcvdCallback(_allMsgRcvdCallbkDelegate)

        ' Register for CAN message delivery and then start delivery.
        _subscriptionDeliveryKey = _canMsgPublisherObj.RegisterForDelivery(AddressOf MySubscriber.DeliverSubscription)
        _canMsgPublisherObj.StartDelivery(_subscriptionDeliveryKey)
    End Sub

    Public Shared Sub Start()
        _rcvdMessageProcessingThread.Start()
    End Sub

    Public Shared Sub Terminate()
        Try
            Dim delayWaitHandle = New EventWaitHandle(False, EventResetMode.AutoReset)

            ' Unregister the message callback delegate.
            MySubscriber.UnRegisterAllMessageRcvdCallback()

            ' Stop CAN message delivery.
            _canMsgPublisherObj.StopDelivery(_subscriptionDeliveryKey)

            ' Terminate the received message processing thread.
            If (_rcvdMessageProcessingThread IsNot Nothing) AndAlso (_rcvdMessageProcessingThread.IsAlive) Then

                _terminateRcvdMessageProcessingThread = True
                _rcvdMessageProcessingThreadWaitHandle.Set()
                _rcvdMessageProcessingThread.Join(New TimeSpan(1000000))      ' .1 sec / .0000001 "tick" = 1000000

                If (_rcvdMessageProcessingThread.IsAlive) Then

                    ' Precaution, don't want to hit this.
                    ActivityLog.LogError(New StackTrace,"The CAN msg processing thread is being aborted.")
                     _rcvdMessageProcessingThread.Abort()
                End If
                _rcvdMessageProcessingThread = Nothing
            End If

            If ((_canActivityThread IsNot Nothing) AndAlso (_canActivityThread.IsAlive)) Then

                _canActivityThread.Join(New TimeSpan(1000000))                ' .1 sec / .0000001 "tick" = 1000000

                If (_canActivityThread.IsAlive) Then

                    ' Precaution, don't want to hit this.
                    ActivityLog.LogError(New StackTrace,"A CAN activity thread is being aborted.")
                    _canActivityThread.Abort()
                End If
            End If

            ' Terminate the subscriber.
            If (MySubscriber IsNot Nothing) AndAlso (MySubscriber.IsAlive) Then

                ' Terminate the subscriber thread.
                MySubscriber.Terminate()
                MySubscriber.Dispose()
            End If
            _rcvdMessageProcessingThreadWaitHandle.Close()
            delayWaitHandle.Close()

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace,ex)
        End Try
    End Sub

    Private Shared Sub MsgRcvdCallback(ByVal rcvdMsgStruct As CAN_Services.RcvdCANMsg)
        Try
            ' Queue received messages for processing by the ProcessRcvdCanMsgs thread process.
            RcvdMsgQueue.Enqueue(rcvdMsgStruct)

            ' Unblock the thread.
            _rcvdMessageProcessingThreadWaitHandle.Set()
        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace,ex)
        End Try
    End Sub

    Private Shared Sub ProcessRcvdCanMsgs() ' Thread process.
        Try
            Dim count = 0
            Do
                ' Block until a receive message is queued.
                _rcvdMessageProcessingThreadWaitHandle.WaitOne()
                Dim rcvdMsgStruct As CAN_Services.RcvdCANMsg = Nothing
                Do While (Not _terminateRcvdMessageProcessingThread) AndAlso (RcvdMsgQueue.TryDequeue(rcvdMsgStruct))

                    ' If the CAN msg data array is unassigned then skip
                    ' this message. Otherwise it will just lead to an exception.
                    If rcvdMsgStruct.CANMsg.DATA Is Nothing Then
                        Continue Do
                    End If

                    Dim canMsgID = rcvdMsgStruct.CANMsg.ID
                    Dim data = rcvdMsgStruct.CANMsg.DATA

                    ' A CAN message is available.
                    If (canMsgID = CanMessageLib.DblToolRespMsgID) AndAlso
                        (rcvdMsgStruct.CANMsg.LEN = CanMessageLib.DblAttnRespMsgDataLength) AndAlso
                        (data(0) = CByte(DblRcvOpCodes.DblAttnResp)) Then

                        ' This is a DBL attention request response.
                        _dfltBootldrResponded = True
                    ElseIf canMsgID = CanMessageLib.PeriodicMsg1ID Then

                        ' This is an RF sensor status message.
                        _dfltBootldrResponded = False
                    End If
                    If Not _dfltBootldrResponded Then

                        ' This is an application autonomous/response message.
                        If (data.Length <> CanMessageLib.RespMsgDataLength) Then
                            Continue Do
                        End If

                        ' Priority is not considered when looking for tool
                        ' response messages.
                        If (canMsgID And CanMessageLib.CanMsgPriorityMask) = CanMessageLib.ToolMsgRespID Then

                            Dim messageType = CByte(data(0) And CanMessageLib.MsgTypeMask)

                            ' This a Tool response message.
                            If [Enum].IsDefined(GetType(MessageTypeID), messageType) Then
                                Select Case messageType
                                    Case CByte(MessageTypeID.ToolFunction)

                                        ' This is a Tool Function response message.
                                        CanMessageLib.ProcessToolFuncMsg(rcvdMsgStruct)

                                    Case CByte(MessageTypeID.ParamID)

                                        ' This a tool param read/write response message.
                                        CanMessageLib.ProcessParamRespMsg(rcvdMsgStruct)

                                    Case CByte(MessageTypeID.CalibrationRam)

                                        ' This is a Tool App Cal response message.
                                        CanMessageLib.ProcessAppCalRespMsg(rcvdMsgStruct)

                                    Case CByte(MessageTypeID.CalibrationFlash)

                                    Case CByte(MessageTypeID.Flash)

                                    Case CByte(MessageTypeID.Ram)

                                    Case CByte(MessageTypeID.Eeprom)

                                    Case CByte(MessageTypeID.TsnAccess)

                                End Select
                            Else
                                ' process other application Tool Response Messages.

                            End If
                        ElseIf canMsgID = CanMessageLib.InfoMessageID Then

                            ' This a temperature information message from the ECM.
                            CanMessageLib.ProcessInfoMsg(rcvdMsgStruct)

                        ElseIf ((canMsgID And CanMessageLib.CanMsgMaskForPGN) =
                                (CUint(CanMessageLib.PeriodicMsg1PGN) << 8)) Then

                            ' This is a PM1 RF sensor status message.
                            ' Receive from any node id.
                            CanMessageLib.ProcessPM1Msg(rcvdMsgStruct)

                        ElseIf canMsgID = CanMessageLib.PeriodicMsg2ID Then

                            ' This a PM2 message.
                            CanMessageLib.ProcessPM2Msg(rcvdMsgStruct)

                        ElseIf canMsgID = CanMessageLib.PeriodicMsg3ID Then

                            ' This is a PM3 message.
                            CanMessageLib.ProcessPM3Msg(rcvdMsgStruct)

                        ElseIf canMsgID = CanMessageLib.CalDataReadMsgID Then

                            ' This is a calibration reading message.
                            CanMessageLib.ProcessSweepDataReadingMsg(rcvdMsgStruct)

                        ElseIf canMsgID = CanMessageLib.AlternateInfoMsgID Then

                            ' This a temperature information message from a thermocouple scanner.
                            CanMessageLib.ProcessInfoMsg(rcvdMsgStruct)
                        Else

                            ' Process other non-tool messages.

                        End If

                        If (canMsgID = CanMessageLib.MonitorForActivityMsgID) Then

                            ' The message ID that we're monitoring for CAN activity has been received.
                            If ((_canActivityThread Is Nothing) OrElse
                                (Not _canActivityThread.IsAlive)) Then

                                _canActivityThread = New Thread(AddressOf FormRfSensorMain.CanActivityDetected)
                                _canActivityThread.Start()
                            End If
                        End If
                    Else
                        ' This a Bootloader response message.
                        If canMsgID = CanMessageLib.DblToolRespMsgID Then

                            ' This is a Tool response message.
                            CanMessageLib.ProcessDblResponse(rcvdMsgStruct)
                        End If
                    End If
                Loop
            Loop While Not _terminateRcvdMessageProcessingThread

        Catch ex As Exception
            ActivityLog.LogError(New StackTrace,ex)
        End Try
    End Sub
End Class
