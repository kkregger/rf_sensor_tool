﻿Imports System.Linq

Public Class FormMeasurementEdit
    Inherits Form

    Private _selectedCell         As DataGridViewCell
    Private _previousValue        As Decimal
    Private _selectedRow          As Integer?
    Private _selectedColumn       As Integer?
    Private _selectedRoiName      As String = String.Empty
    Private _selectedRoiStatistic As RfStatistic
    Private _measurementOriginal  As Measurement
    Private _editResult           As DialogResult = DialogResult.Cancel

    Private WithEvents _gridViewUpDown As NumericUpDown = New NumericUpDown()
    Private WithEvents _gridViewStatisticComboBox As ComboBox = New ComboBox()
    Private WithEvents _gridViewRoiNameComboBox As ComboBox = New ComboBox()

    ' Event to raise when there is an error to be logged.
    Private Shared Event LogErrorEvent As ActivityLog.LogErrorStackDelegate
    Private Shared Event LogExceptionEvent As ActivityLog.LogExceptionDelegate

    Public sub New(ByRef measure As Measurement)

        ' Required.
        InitializeComponent()

        ' Register error logging event handlers.
        AddHandler LogErrorEvent, AddressOf ActivityLog.LogError
        AddHandler LogExceptionEvent, AddressOf ActivityLog.LogError

        Try

            ' Keep a copy of the original Measurement.
            _measurementOriginal = New Measurement(measure)

            ' Make working copy. Constructor does a deep copy.
            MeasurementWorkingCopy = New Measurement(measure)

            ' Initialize the Form controls with the selected Measurement values.
            TextBoxMeasurementName.Text      = MeasurementWorkingCopy.Name
            NumericUpDownMinTemp.Value       = MeasurementWorkingCopy.MinTemperature
            NumericUpDownMaxTemp.Value       = MeasurementWorkingCopy.MaxTemperature
            NumericUpDownMinDetectable.Value = MeasurementWorkingCopy.MinDetectable
            NumericUpDownMaxDetectable.Value = MeasurementWorkingCopy.MaxDetectable
            CheckBoxActive.Checked           = MeasurementWorkingCopy.Active

            ' Load Measurement and associated ROIs into data grid views.
            UpdateMeasurementDataGridView()

            ' Set up NumericUpDown and ComboBox cell edit controls.
            _gridViewUpDown.Font    = New Font("Microsoft Sans Serif", 9.0!, FontStyle.Bold, GraphicsUnit.Point, CType(0, Byte))
            _gridViewUpDown.Name    = "RoiGridViewNumericUpDown"
            _gridViewUpDown.Size    = New Size(92, 21)
            _gridViewUpDown.Minimum = 0
            _gridViewUpDown.Maximum = Common.SweepIntervalMax
            _gridViewUpDown.Enabled = True
            _gridViewUpDown.Visible = False
            Controls.Add(_gridViewUpDown)

            ' Rf Statistic combo box for ROI grid view.
            _gridViewStatisticComboBox.Font = New Font("Microsoft Sans Serif", 9.0!, FontStyle.Bold, GraphicsUnit.Point, CType(0, Byte))
            _gridViewStatisticComboBox.Name = "RoiGridViewStatisticComboBox"
            _gridViewStatisticComboBox.Size = New Size(225, 21)

            ' Populate the RF Statistic combo box.
            UpdateRoiStatisticComboBox()
            _gridViewStatisticComboBox.Enabled = True
            _gridViewStatisticComboBox.Visible = False
            Controls.Add(_gridViewStatisticComboBox)

            ' ROI Name combo box for ROI grid view.
            _gridViewRoiNameComboBox.Font = New Font("Microsoft Sans Serif", 9.0!, FontStyle.Bold, GraphicsUnit.Point, CType(0, Byte))
            _gridViewRoiNameComboBox.Name = "RoiGridViewRoiNameComboBox"
            _gridViewRoiNameComboBox.Size = New Size(65535, 21)

            ' Populate the ROI Name combo box.
            Dim rioNames = FormRfSensorMain.RegionsOfInterest.Keys
            For Each rioName In rioNames
                _gridViewRoiNameComboBox.Items.Add(rioName)
            Next
            _gridViewRoiNameComboBox.Enabled = True
            _gridViewRoiNameComboBox.Visible = False
            Controls.Add(_gridViewRoiNameComboBox)

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Private Sub FormMeasurementEdit_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing

        Try
            If (_measurementOriginal <> MeasurementWorkingCopy) Then
                Dim mbResult = MsgBox("Are You Sure?" & vbCrLf &
                                      "All changes will be discarded.",
                                  MsgBoxStyle.SystemModal + MsgBoxStyle.YesNo)
                If (mbResult = MsgBoxResult.No) Then
                    e.Cancel = True
                Else
                    DialogResult = _editResult
                End If
            End If

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Public Shared Property MeasurementWorkingCopy As Measurement

    Private Sub UpdateRoiStatisticComboBox()

        Try
            ' Populate the RF Statistic combo box.
            Dim statList  = [Enum].GetValues(GetType(RfStatistic)).Cast(Of RfStatistic).ToList
            Dim statistic = RfStatistic.None

            statList.Remove(RfStatistic.None)
            If (Not String.IsNullOrWhiteSpace(_selectedRoiName) AndAlso
                MeasurementWorkingCopy.ContainsROI(_selectedRoiName)) Then

                For Each statWeight In MeasurementWorkingCopy.GetROi(_selectedRoiName).StatisticWeights
                    statList.Remove(statWeight.Statistic)
                Next
            End If
            For Each stat In statList
                If (Common.StatisticNameDict.TryGetValue(stat, statistic)) Then
                    _gridViewStatisticComboBox.Items.Add(statistic)
                End If
            Next

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Private Sub UpdateMeasurementDataGridView()

        Try
            ' Allow for new Measurement in which dictionaries have not yet been assigned to.
            For Each roi In MeasurementWorkingCopy.RegionsOfInterest
                ' Display ROI first row.
                DataGridViewAssociatedROIs.Rows.Add(roi.Name, roi.SweepInterval.ToString(),
                                                    Common.StatisticNameDict(roi.StatisticWeights(0).Statistic),
                                                    roi.StatisticWeights(0).Weight.ToString())
                For idx = 1 To roi.StatisticWeights.Count - 1
                    DataGridViewAssociatedROIs.Rows.Add("^", "^", Common.StatisticNameDict(roi.StatisticWeights(idx).Statistic),
                                                        roi.StatisticWeights(idx).Weight.ToString())
                Next
            Next

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Private Sub DataGridViewAssociatedROIs_CellBeginEdit(sender As Object, e As DataGridViewCellCancelEventArgs) _
        Handles DataGridViewAssociatedROIs.CellBeginEdit

        Try
            Dim content As String = CType(sender, DataGridView).SelectedCells(0).Value
            Dim locX As Integer = DataGridViewAssociatedROIs.Location.X
            Dim locY As Integer = DataGridViewAssociatedROIs.Location.Y

            ' Only one cell may be selected at a time.
            If (String.IsNullOrWhiteSpace(content) OrElse content = "^") Then

                ' Don't edit blank cells or cells containing '-' that belong to rows containing only statistics and weights. 
                e.Cancel = True
            ElseIf (e.ColumnIndex = Common.RoiNameColumn) Then

                ' This is an ROI Name cell.
                _selectedCell = CType(sender, DataGridView).SelectedCells(0)
                ' Position the combo box control over the selected cell, size it to match, and make it visible.
                locX += 2
                locY += (CType(sender, DataGridView).Item(0, e.RowIndex).Size.Height * (e.RowIndex + 1)) + 2
                _gridViewRoiNameComboBox.Location = New Point(locX, locY)
                _gridViewRoiNameComboBox.Size = New Size(_selectedCell.Size)
                Dim index = _gridViewRoiNameComboBox.FindStringExact(_selectedCell.Value)
                If (index >= 0) Then
                    _gridViewRoiNameComboBox.SelectedIndex = index
                End If
                _gridViewRoiNameComboBox.Visible = True
                _gridViewRoiNameComboBox.BringToFront()
                e.Cancel = True
            Else
                If (e.ColumnIndex = Common.RoiSweepIntervalColumn OrElse
                    e.ColumnIndex = Common.RoiStatisticWeightColumn) Then

                    ' This is a period or weight cell. Store a reference to the cell and its initial value.
                    _selectedCell = CType(sender, DataGridView).SelectedCells(0)
                    _previousValue = CDec(_selectedCell.Value)

                    ' Position the numeric up down control over the selected cell, size it to match, and make it visible.
                    If (e.ColumnIndex = Common.RoiSweepIntervalColumn) Then

                        ' Period cell.
                        locX += CType(sender, DataGridView).Item(0, e.RowIndex).Size.Width + 2
                        locY += (CType(sender, DataGridView).Item(0, e.RowIndex).Size.Height * (e.RowIndex + 1)) + 2
                    Else

                        ' Weight cell.
                        locX += CType(sender, DataGridView).Item(0, e.RowIndex).Size.Width + 2
                        locX += CType(sender, DataGridView).Item(1, e.RowIndex).Size.Width
                        locX += CType(sender, DataGridView).Item(2, e.RowIndex).Size.Width
                        locY += (CType(sender, DataGridView).Item(0, e.RowIndex).Size.Height * (e.RowIndex + 1)) + 2
                    End If
                    _gridViewUpDown.Location = New Point(locX, locY)
                    _gridViewUpDown.Size = New Size(_selectedCell.Size)
                    _gridViewUpDown.Value = CDec(_selectedCell.Value)
                    _gridViewUpDown.Visible = True
                    _gridViewUpDown.BringToFront()
                    e.Cancel = True
                ElseIf (e.ColumnIndex = Common.RoiRfStatisticColumn) Then

                    ' This is an RF Statistic cell.
                    _selectedCell = CType(sender, DataGridView).SelectedCells(0)
                    ' Position the combo box control over the selected cell, size it to match, and make it visible.
                    locX += CType(sender, DataGridView).Item(0, e.RowIndex).Size.Width + 2
                    locX += CType(sender, DataGridView).Item(1, e.RowIndex).Size.Width
                    locY += (CType(sender, DataGridView).Item(0, e.RowIndex).Size.Height * (e.RowIndex + 1)) + 2
                    _gridViewStatisticComboBox.Location = New Point(locX, locY)
                    _gridViewStatisticComboBox.Size = New Size(_selectedCell.Size)
                    Dim index = _gridViewStatisticComboBox.FindStringExact(_selectedCell.Value)
                    If (index >= 0) Then
                        _gridViewStatisticComboBox.SelectedIndex = index
                        _selectedRoiStatistic = _gridViewStatisticComboBox.SelectedItem
                        _gridViewStatisticComboBox.Visible = True
                        _gridViewStatisticComboBox.BringToFront()
                    Else
                        MsgBox("Failed to retrive the RF Statistic that corresponds to the selected cell.",
                               MsgBoxStyle.SystemModal + MsgBoxStyle.Exclamation)
                        RaiseEvent LogErrorEvent(New StackTrace,
                                                 "DataGridViewAssociatedROIs_CellBeginEdit() Failed to retrive" & vbCrLf &
                                                 "the RF Statistic that corresponds to the selected cell.")
                        _selectedCell.Selected = False
                        _selectedCell    = Nothing
                        _selectedRow     = Nothing
                        _selectedColumn  = Nothing
                        _selectedRoiName = String.Empty
                    End If
                    e.Cancel = True
                End If
            End If

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Private Sub DataGridViewAssociatedROIs_CellClick(sender As Object, e As DataGridViewCellEventArgs) _
        Handles DataGridViewAssociatedROIs.CellClick

        Try
            Dim roiName = String.Empty

            _selectedRow    = e.RowIndex
            _selectedColumn = e.ColumnIndex
            If (_selectedRow >= 0) Then
                Dim rowIndex = CInt(_selectedRow)
                Do
                    ' Find the name of the selected ROI by the selected row index.
                    ' ROI names are in column 0.
                    roiName = DataGridViewAssociatedROIs.Item(0, rowIndex).Value.ToString()
                    rowIndex = If(rowIndex > 0, rowIndex - 1, rowIndex)
                Loop While ((Not MeasurementWorkingCopy.ContainsROI(roiName)) AndAlso (rowIndex > 0))
                _selectedRoiName = roiName
            Else
                _selectedRoiName = String.Empty
            End If

            ' In case the numeric up down control is visible. Hide it because the user has clicked a different cell.
            _gridViewUpDown.Visible = False
            _gridViewStatisticComboBox.Visible = False
            _gridViewRoiNameComboBox.Visible   = False
            If (_selectedCell IsNot Nothing) Then
                _selectedCell.Selected = False
            End If
            _selectedCell = Nothing

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Private Sub DataGridViewAssociatedROIs_Click(sender As Object, e As EventArgs) _
        Handles DataGridViewAssociatedROIs.Click

        Try
            If (DataGridViewAssociatedROIs.SelectedCells.Count = 0) Then
                Return
            End If
            ' Position of the selected cell - beginning, end, top and bottom.
            Dim locX As Integer = DataGridViewAssociatedROIs.Location.X
            Dim locY As Integer = DataGridViewAssociatedROIs.Location.Y
            Dim locX1 As Integer
            Dim locY1 As Integer

            ' Mouse position.
            Dim x = MousePosition.X
            Dim y = MousePosition.Y
            Dim pnt = PointToClient(New Point(x, y))

            ' DataGridView cell coordinates.
            Dim column = DataGridViewAssociatedROIs.SelectedCells(0).ColumnIndex
            Dim col = column
            Dim row = DataGridViewAssociatedROIs.SelectedCells(0).RowIndex

            ' Locate the selected cell.
            locY += (CType(sender, DataGridView).Item(0, row).Size.Height * (row + 1)) + 2
            locY1 = locY + CType(sender, DataGridView).Item(0, row).Size.Height
            If (col = 0) Then
                locX += 2
            Else
                locX += CType(sender, DataGridView).Item(0, row).Size.Width + 2
                col -= 1
                While (col > 0)
                    locX += CType(sender, DataGridView).Item(col, row).Size.Width
                    col -= 1
                End While
            End If
            locX1 = locX + CType(sender, DataGridView).Item(column, row).Size.Width

            ' Determine if the mouse position falls withing the boundaries of the selected cell.
            If ((pnt.X < locX OrElse pnt.X > locX1) OrElse
                (pnt.Y < locY OrElse pnt.Y > locY1)) Then

                ' In case the numeric up down and combo box controls are visible.
                ' Hide them because the user has either clicked a different cell or
                ' another area of the control.
                _gridViewUpDown.Hide()
                _gridViewStatisticComboBox.Hide()
                _gridViewRoiNameComboBox.Hide()

                ' The mouse is not on the selected cell. so desselect the cell.
                DataGridViewAssociatedROIs.SelectedCells(0).Selected = False
                _selectedCell    = Nothing
                _selectedRow     = Nothing
                _selectedColumn  = Nothing
                _selectedRoiName = String.Empty
            End If

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Private Sub Control_GotFocus(sender As Object, e As EventArgs) _
        Handles TextBoxMeasurementName.GotFocus, NumericUpDownMinTemp.GotFocus,
                NumericUpDownMaxTemp.GotFocus, NumericUpDownMinDetectable.GotFocus,
                NumericUpDownMaxDetectable.GotFocus, ButtonSave.GotFocus,
                ButtonClose.GotFocus, PanelMeasurement.Click
        Try
            ' The DatagRidView has lost focus.
            ' If the numeric up down and combo box controls are visible, then
            ' hide them because the user has clicked outside of the DataGridView.
            _gridViewUpDown.Hide()
            _gridViewStatisticComboBox.Hide()
            _gridViewRoiNameComboBox.Hide()

            ' If necessary, deselect a previously selected cell.
            If (DataGridViewAssociatedROIs.SelectedCells.Count > 0) Then

                DataGridViewAssociatedROIs.SelectedCells(0).Selected = False
            End If
            _selectedCell    = Nothing
            _selectedRow     = Nothing
            _selectedColumn  = Nothing
            _selectedRoiName = String.Empty

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Private Sub GridViewUpDown_Leave(Sender As Object, e As EventArgs) Handles _gridViewUpDown.Leave

        ' Hide the numeric up down control when it loses focus.
        _gridViewUpDown.Visible = False
        _selectedCell           = Nothing
    End Sub

    Private Sub GridViewComboBox_Leave(Sender As Object, e As EventArgs) _
        Handles _gridViewStatisticComboBox.Leave, _gridViewRoiNameComboBox.Leave

        ' Hide the combo box control when it loses focus.
        _gridViewStatisticComboBox.Visible = False
        _gridViewRoiNameComboBox.Visible   = False
        _selectedCell                      = Nothing
    End Sub

    Private Sub GridViewUpDown_ValueChanged(Sender As Object, e As EventArgs) Handles _gridViewUpDown.ValueChanged

        Try
            If (_selectedColumn Is Nothing OrElse _selectedRow Is Nothing Or _selectedRoiName = String.Empty) Then
                Return
            End If

            ' Only one cell may be selected at a time.
            If (_selectedCell.ColumnIndex = Common.RoiStatisticWeightColumn) Then

                ' This is a Weight so don't allow its value to exceed the Weight max limit.
                If (_gridViewUpDown.Value > Common.MaxStatisticWeight) Then

                    ' The value is too high so restore the previous value.
                    _gridViewUpDown.Value = _previousValue

                    ' At this level thse precautions are probably unnecessary, but. . .
                    If (_selectedRoiStatistic <> RfStatistic.None) Then
                        If (MeasurementWorkingCopy.GetROI(_selectedRoiName).ContainsStatistic(_selectedRoiStatistic)) Then

                            If (MeasurementWorkingCopy.GetROI(_selectedRoiName).RemoveStatistic(_selectedRoiStatistic)) Then
                                MeasurementWorkingCopy.GetROI(_selectedRoiName).StatisticWeights.
                                    Add(New ROIStatisticWeight(_selectedRoiStatistic, _gridViewUpDown.Value))
                            Else
                                MsgBox("Failed to store the new Weight value of the RF " & vbCrLf &
                                       "Statistic that corresponds to the selected cell." & vbCrLf &
                                       _selectedRoiName & " - " & Common.StatisticNameDict(_selectedRoiStatistic),
                                       MsgBoxStyle.SystemModal + MsgBoxStyle.Exclamation)
                                RaiseEvent LogErrorEvent(New StackTrace, 
                                                         "FormMeasurementEdit.GridViewUpDown_ValueChanged():" & vbCrLf &
                                                         "Failed to store the new Weight value of the RF " & vbCrLf &
                                                         "Statistic that corresponds to the selected cell."& vbCrLf &
                                                         _selectedRoiName & " - " & Common.StatisticNameDict(_selectedRoiStatistic))
                            End If
                            UpdateMeasurementDataGridView()
                        Else
                            MsgBox("Failed to update the Weight value of the RF " & vbCrLf &
                                   "Statistic that corresponds to the selected cell: " & vbCrLf &
                                   _selectedRoiName & " - " & Common.StatisticNameDict(_selectedRoiStatistic),
                                   MsgBoxStyle.SystemModal + MsgBoxStyle.Exclamation)
                            RaiseEvent LogErrorEvent(New StackTrace, 
                                                     "FormMeasurementEdit.GridViewUpDown_ValueChanged():" & vbCrLf &
                                                     "Failed to update the Weight value of the RF " & vbCrLf &
                                                     "Statistic that corresponds to the selected cell: " & vbCrLf &
                                                     _selectedRoiName & " - " & Common.StatisticNameDict(_selectedRoiStatistic))
                        End If
                    Else
                        MsgBox("Failed to update the Weight value due to an uninitialized RF Statistic value: " & vbCrLf &
                               _selectedRoiName & " - " & Common.StatisticNameDict(_selectedRoiStatistic),
                               MsgBoxStyle.SystemModal + MsgBoxStyle.Exclamation)
                        RaiseEvent LogErrorEvent(New StackTrace, 
                                                 "FormMeasurementEdit.GridViewUpDown_ValueChanged():" & vbCrLf &
                                                 "Failed to update the Weight value due to an uninitialized RF Statistic value: " &
                                                 vbCrLf & _selectedRoiName & " - " & Common.StatisticNameDict(_selectedRoiStatistic))
                    End If
                Else
                    ' Store the new value and update the value of the selected cell.
                    _previousValue = _gridViewUpDown.Value
                    _selectedCell.Value = _previousValue

                    ' XXX KTK update the Measurement working copy!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                End If
            Else

                ' This is a Sweep Period. The numeric up down control max limit is set to the Period max limit,
                ' which is higher than the Weight max limit, so the control limits the Period value for us.
                ' Update the value of the selected cell.
                _selectedCell.Value = _gridViewUpDown.Value

               ' XXX KTK update the Measurement working copy!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            End If

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Private Sub GridViewComboBox_ValueChanged(Sender As Object, e As EventArgs) _
        Handles _gridViewStatisticComboBox.SelectedIndexChanged, _gridViewRoiNameComboBox.SelectedIndexChanged

        Try
            _selectedCell.Value = CType(Sender, ComboBox).Items(CType(Sender, ComboBox).SelectedIndex)

            ' XXX KTK update the Measurement working copy!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            ' If statistic is being added then add to Measurement along with weight.

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try

    End Sub

    Private Sub ToolStripMenuItemCreateROI_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItemCreateROI.Click

        Try
            ' FormRoiEdit

            ' XXX KTK Launch ROI Edit with empty ROI!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            ' When ROI edit closes reload datagridview create fumction for this!!!!!!!!!! Have ROI Edit indicate when new ROI saved?
            ' Add created ROI to Measurement working copy.

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Private Sub ToolStripMenuItemRemoveROI_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItemRemoveROI.Click

        Try
            If (_selectedCell Is Nothing AndAlso _selectedRow Is Nothing Or _selectedRoiName = String.Empty) Then
                MsgBox("First select an ROI, then click Remove.", MsgBoxStyle.SystemModal)
            Else

                ' Remove the selected ROI.
                Dim mbResult = MsgBox("Are you sure?" & vbCrLf &
                                      "The selected ROI configuration will be permanently removed.",
                                      MsgBoxStyle.SystemModal + MsgBoxStyle.Question + MsgBoxStyle.YesNo)
                If (mbResult = MsgBoxResult.Yes) Then



                    ' XXX KTK remove selected ROI from working Measurement!!!!!!!!!!
                    UpdateMeasurementDataGridView()
                    _selectedRow     = Nothing
                    _selectedCell    = Nothing
                    _selectedRoiName = String.Empty
                End If
            End If

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Private Sub ButtonSave_Click(sender As Object, e As EventArgs) Handles ButtonSave.Click

        Try
            If (_measurementOriginal = MeasurementWorkingCopy) Then

                If (Not FormRfSensorMain.Measurements.ContainsKey(_measurementOriginal.Name)) Then

                    ' This looks like an unmodified (New) Measurement containing default values.
                    ' I'll ask the user if they want to save it anyway.
                    Dim mbResult = MsgBox("The Measurement is new and unmodified." & vbCrLf &
                                          "Would you like to save it anyway?",
                                          MsgBoxStyle.SystemModal + MsgBoxStyle.YesNo)
                    If (mbResult = MsgBoxResult.Yes) Then
                        If (Not FormRfSensorMain.Measurements.TryAdd(MeasurementWorkingCopy.Name,
                                                                     MeasurementWorkingCopy)) Then
                            MsgBox("Failed to save the new Measurement!",
                                   MsgBoxStyle.SystemModal + MsgBoxStyle.Exclamation)
                            RaiseEvent LogErrorEvent(New StackTrace, 
                                                     "ButtonSave_Click() failed to save the new Measurement: " &
                                                     MeasurementWorkingCopy.Name)
                        Else
                            _measurementOriginal = New Measurement(MeasurementWorkingCopy)
                            ButtonSave.Enabled   = False
                            _editResult          = DialogResult.OK
                        End If
                    End If
                End If
            Else
                'This name of this ROI has been changed.
                If (Not FormRfSensorMain.Measurements.ContainsKey(MeasurementWorkingCopy.Name)) Then

                    ' This ROI's name does not exist in the ROI dictionary.
                    If (Not FormRfSensorMain.Measurements.TryAdd(MeasurementWorkingCopy.Name,
                                                                 MeasurementWorkingCopy)) Then
                        MsgBox("Failed to save the modified Measurement!",
                               MsgBoxStyle.SystemModal + MsgBoxStyle.Exclamation)
                        RaiseEvent LogErrorEvent(New StackTrace, 
                                                 "FormMeasurementEdit.ButtonSave_Click() failed to save the " &
                                                 "modified Measurement: " &MeasurementWorkingCopy.Name)
                    Else
                        _measurementOriginal = New Measurement(MeasurementWorkingCopy)
                        ButtonSave.Enabled   = False
                        _editResult          = DialogResult.OK
                    End If
                Else
                    Dim mbResult = MsgBox("An Measurement by this name already exists." & vbCrLf &
                                          "Would you like to overwrite the existing Measurement?",
                                          MsgBoxStyle.SystemModal + MsgBoxStyle.YesNo)
                    If (mbResult = MsgBoxResult.Yes) Then
                        FormRfSensorMain.Measurements(MeasurementWorkingCopy.Name) = MeasurementWorkingCopy
                        If (Not FormRfSensorMain.Measurements.ContainsKey(MeasurementWorkingCopy.Name)) Then

                            MsgBox("Failed to save the modified ROI!",
                                   MsgBoxStyle.SystemModal + MsgBoxStyle.Exclamation)
                            RaiseEvent LogErrorEvent(New StackTrace, 
                                                     "FormMeasurementEdit.ButtonSave_Click() failed to save the " &
                                                     "modified ROI: " & MeasurementWorkingCopy.Name)
                        Else
                            _measurementOriginal = New Measurement(MeasurementWorkingCopy)
                            ButtonSave.Enabled   = False
                            _editResult          = DialogResult.Ok
                        End If
                    End If
                End If
            End If

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Private Sub ButtonClose_Click(sender As Object, e As EventArgs) Handles ButtonClose.Click
        Close()
    End Sub

    Private Sub CheckBoxActive_Click(sender As Object, e As EventArgs) Handles CheckBoxActive.Click

        If (CheckBoxActive.Checked) Then

            CheckBoxActive.Checked = False
        Else
            CheckBoxActive.Checked = True
        End If

        ' XXX KTK UPDATE MEASUREMENT WORKING COPY. WHEN GOING ACTIVE CHECK FOR OTHER ACTIVE MEASUREMENTS AND INACTIVATE THEM.

    End Sub
End Class
