﻿Imports System
Imports System.Drawing
Imports System.Windows.Forms

Namespace CTS_RF_Sensor_Tool
	Partial Friend Class FormFunctionSelect
		Inherits Form

		Private ReadOnly _formRfdpfObj As FormRFSensorMain

		Public Sub New(ByVal parentForm As FormRFSensorMain)
			InitializeComponent()
			_formRfdpfObj = parentForm
		End Sub

		Private Sub ComPortConfigButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ComPortConfigButton.Click
			ComPortConfigButton.BackColor = Color.Coral
			_formRfdpfObj.LaunchCOM_Config()
			ComPortConfigButton.BackColor = Color.Chartreuse
		End Sub

		Private Sub ConfigureCAN_DeviceButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ConfigureCAN_DeviceButton.Click
			ConfigureCAN_DeviceButton.BackColor = Color.Coral
			_formRfdpfObj.LaunchCAN_DeviceConfig()
			ConfigureCAN_DeviceButton.BackColor = Color.Chartreuse
		End Sub

		Private Sub ConfigureSweepButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ConfigureSweepButton.Click
			ConfigureSweepButton.BackColor = Color.Coral
			_formRfdpfObj.LaunchSweepConfig()
			ConfigureSweepButton.BackColor = Color.Chartreuse
		End Sub

		Private Sub ConfigureRegionButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ConfigureRegionButton.Click
			ConfigureRegionButton.BackColor = Color.Coral
			_formRfdpfObj.LaunchRegionConfig()
			ConfigureRegionButton.BackColor = Color.Chartreuse
		End Sub

		Private Sub ConfigureCalibrationButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ConfigureCalibrationButton.Click
			ConfigureCalibrationButton.BackColor = Color.Coral
			_formRfdpfObj.LaunchCalConfig()
			ConfigureCalibrationButton.BackColor = Color.Chartreuse
		End Sub

		Private Sub ConfigureMeasurementButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ConfigureMeasurementButton.Click
			ConfigureMeasurementButton.BackColor = Color.Coral
			_formRfdpfObj.LaunchMeasurementConfig()
			ConfigureMeasurementButton.BackColor = Color.Chartreuse
		End Sub

		Private Sub ShowDebugDataButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ShowDebugDataButton.Click
			_formRfdpfObj.ShowDebugData()
		End Sub

		Private Sub ShowLogDisplayButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ShowLogDisplayButton.Click
			_formRfdpfObj.ShowLogDisplay()
		End Sub

		Private Sub RunOSL_CalibrationAndNormalizationButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles RunOSL_CalibrationAndNormalizationButton.Click
			RunOSL_CalibrationAndNormalizationButton.BackColor = Color.Coral
			_formRfdpfObj.RunOSL_CalAndNormalization()
			RunOSL_CalibrationAndNormalizationButton.BackColor = Color.Chartreuse
		End Sub

		Private Sub FlashDeviceButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles FlashDeviceButton.Click
			FlashDeviceButton.BackColor = Color.Coral
			_formRfdpfObj.LaunchFlashDeviceForm()
			FlashDeviceButton.BackColor = Color.Chartreuse
		End Sub

		Private Sub DecryptFileButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles DecryptFileButton.Click
			DecryptFileButton.BackColor = Color.Coral
			_formRfdpfObj.LaunchFileDecrypt()
			DecryptFileButton.BackColor = Color.Chartreuse
		End Sub

		Private Sub LockDeviceButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles LockDeviceButton.Click
			Hide()
			_formRfdpfObj.Unlocked = False
		End Sub

		Private Sub FunctionSelectForm_FormClosing(ByVal sender As Object, ByVal e As FormClosingEventArgs) Handles Me.FormClosing
			Hide()
			_formRfdpfObj.BringToFront()
			e.Cancel = True
		End Sub
	End Class
End Namespace
