﻿Imports System
Imports System.Drawing
Imports System.IO
Imports System.Text
Imports System.Windows.Forms

Partial Public Class FormFlashDevice
    Inherits Form

    Private Const MinFontSize As Single = 8.25F
    'private const int buttonWidth = 75;
    'private const int buttonHeight = 23;

    Private _input As StreamReader
    Private _output As StreamWriter
    Private _downloadFile As Stream = Nothing
    Private _downloadFilePath As String
    Private formArea As Single
    Private formWidth As Integer
    Private formHeight As Integer
    Private pbHeight As Integer
    Private labelInputWidth As Integer
    Private labelOutputWidth As Integer
    Private isVisible As Boolean
    ' Event raised when an error is being logged.
    Private Shared Event LogErrorEvent As ActivityLog.LogErrorStackDelegate
    Private Shared Event LogExceptionEvent As ActivityLog.LogExceptionDelegate

    Public Sub New()
        InitializeComponent()
        AutoScaleMode = AutoScaleMode.Dpi
        formArea = Size.Width * Size.Height
        formWidth = Size.Width
        formHeight = Size.Height
        pbHeight = ProgressBarUpload.Height
        labelInputWidth = LabelInputFile.Width
        labelOutputWidth = LabelOutputFile.Width
    End Sub

    Private Sub FormFlashDevice_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        ButtonUpload.Location = New Point(TextBoxInputFile.Location.X + (ButtonUpload.Size.Width \ 2), ButtonUpload.Location.Y)
        ButtonDownload.Location = New Point(TextBoxInputFile.Location.X + (TextBoxInputFile.Width - CInt(Math.Truncate(ButtonDownload.Size.Width * 1.5F))), ButtonDownload.Location.Y)
    End Sub

    Private Sub FormFlashDevice_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
        ' Register error logging event handler.
        AddHandler LogErrorEvent, AddressOf ActivityLog.LogError
        AddHandler LogExceptionEvent, AddressOf ActivityLog.LogError
        isVisible = True
    End Sub

    Private Sub FormFlashDevice_Resize(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Resize

        ' Scale all of the controls on this form.
        If Size.Width > 0 AndAlso Size.Height > 0 Then
            Dim newArea As Single = Size.Width * Size.Height
            Dim ratio = newArea / formArea
            Dim newWidth = Size.Width
            Dim widthDelta = newWidth - formWidth
            Dim newHeight = Size.Height
            Dim hRatio As Single = CSng(newHeight) / formHeight
            Dim buttonHeight As Integer = 0
            Dim heightDelta As Integer = 0
            Dim loc As New Point()

            formArea = newArea
            formWidth = newWidth

            Try
                ' Scale the font based on the increase in form area.
                Dim scaledFont = New Font(ButtonProgramApplication.Font.FontFamily, ButtonProgramApplication.Font.Size * ratio)
                For Each ctrl As Control In Controls
                    If ctrl.GetType() Is GetType(Button) Then
                        buttonHeight = ButtonProgramApplication.Size.Height
                        ctrl.Font = New Font(scaledFont, scaledFont.Style)

                        ' All buttons scale as the form scales.
                        If Not (ReferenceEquals(ctrl, ButtonProgramApplication) OrElse ReferenceEquals(ctrl, ButtonProgramEeprom) OrElse ReferenceEquals(ctrl, ButtonProgramCcbl)) Then

                            ' These buttons move vertically as the form scales.
                            heightDelta = ctrl.Size.Height - buttonHeight
                            loc = New Point(ctrl.Location.X, ctrl.Location.Y + (heightDelta * 4))
                            ctrl.Location = loc
                        ElseIf ReferenceEquals(ctrl, ButtonProgramEeprom) Then

                            ' Center the program EEPROM button in the form.
                            Dim X = (Size.Width \ 2) - (ctrl.Size.Width \ 2)
                            loc = New Point(X, ctrl.Location.Y)
                            ctrl.Location = loc
                        End If
                    ElseIf (ctrl.GetType() Is GetType(TextBox)) OrElse (ctrl.GetType() Is GetType(Label)) Then
                        Dim oldHeight = ctrl.Size.Height
                        ctrl.Font = New Font(scaledFont, scaledFont.Style)
                        heightDelta = ctrl.Size.Height - oldHeight

                        ' The text boxes and Labels scale and move vertically as the form scales.
                        ' Vertical position is based on height change due to font size change.
                        If ReferenceEquals(ctrl, LabelInputFile) OrElse ReferenceEquals(ctrl, LabelOutputFile) Then

                            ' Center the Input and Output File textbox labels in the form.
                            Dim X = (Size.Width \ 2) - (ctrl.Size.Width \ 2)
                            loc = New Point(X, ctrl.Location.Y)
                        ElseIf ReferenceEquals(ctrl, TextBoxInputFile) Then

                            ' Set the vertical position of the Input file label based on
                            ' the scaling of the Input text box due to font size change.
                            ' Allows the label to better track the test box during scaling.
                            LabelInputFile.Location = New Point(LabelInputFile.Location.X, LabelInputFile.Location.Y + heightDelta)

                            ' Set the vertical position of the Input text box based
                            ' on its size change due to font size change.
                            loc = New Point(ctrl.Location.X, ctrl.Location.Y + (heightDelta * 2))
                        ElseIf ReferenceEquals(ctrl, TextBoxOutputFile) Then

                            ' Set the vertical position of the Output file label based on
                            ' the scaling of the Output text box due to font size change.
                            ' Allows the label to better track the test box during scaling.
                            LabelOutputFile.Location = New Point(LabelOutputFile.Location.X, LabelOutputFile.Location.Y + (heightDelta * 5))

                            ' Set the vertical position of the Output text box based
                            ' on its size change due to font size change.
                            loc = New Point(ctrl.Location.X, ctrl.Location.Y + (heightDelta * 6))
                        End If
                        If Not loc.IsEmpty Then
                            ctrl.Location = loc
                        End If
                    End If
                Next ctrl

                ' The progress bar moves vertically as the form scales based on the 
                ' top-row button height changes due to font size changes.
                ' Adjust the width based on the form width change.
                heightDelta = ButtonProgramApplication.Size.Height - buttonHeight
                loc = New Point(ProgressBarUpload.Location.X, ProgressBarUpload.Location.Y + (heightDelta * 3))
                Dim adjustedSize = New Size(ProgressBarUpload.Width + widthDelta, CInt(Math.Truncate(pbHeight * hRatio)))
                ProgressBarUpload.Size = adjustedSize
                ProgressBarUpload.Location = loc

                ' The Input File text box has already been scaled and moved based
                ' on font size change. Adjust the width based on the form width change.
                adjustedSize = New Size(TextBoxInputFile.Size.Width + widthDelta, TextBoxInputFile.Size.Height)
                TextBoxInputFile.Size = adjustedSize
                loc = New Point(TextBoxInputFile.Location.X + (ButtonUpload.Size.Width \ 2), ButtonUpload.Location.Y)
                ButtonUpload.Location = loc

                ' The Output File text box has already been scaled and moved based
                ' on font size change. Adjust the width based on the form width change.
                adjustedSize = New Size(TextBoxOutputFile.Size.Width + widthDelta, TextBoxOutputFile.Size.Height)
                TextBoxOutputFile.Size = adjustedSize
                loc = New Point(TextBoxInputFile.Location.X + (TextBoxInputFile.Width - CInt(Math.Truncate(ButtonDownload.Size.Width * 1.5F))), ButtonDownload.Location.Y)
                ButtonDownload.Location = loc
            Catch ex As Exception
                RaiseEvent LogExceptionEvent(New StackTrace, ex)
            End Try
        End If
    End Sub

    Private Sub FormFlashDevice_FormClosing(ByVal sender As Object, ByVal e As FormClosingEventArgs) Handles Me.FormClosing
        Try


            ' Add cleanup code


        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        Finally
            ' Unregister error logging event handlers.
            RemoveHandler LogErrorEvent, AddressOf ActivityLog.LogError
            RemoveHandler LogExceptionEvent, AddressOf ActivityLog.LogError
        End Try
    End Sub

    'private void PostMessage ( string Message )
    '{
    '    this.Invoke ( ( MethodInvoker ) delegate
    '    {
    '        LabelMessage.Text = Message;
    '    } );
    '}


    Private Function GetString(ByVal Packet() As Byte, ByVal Offset As Integer) As String
        Dim Result As New StringBuilder()
        Do While Packet(Offset) <> 0
            Result.Append(ChrW(Packet(Offset)))
            Offset += 1
        Loop

        Return Result.ToString()
    End Function

    Private Function DataToString(ByVal Data() As Byte, ByVal Count As Byte) As String
        Dim Result As New StringBuilder()

        For Index As Integer = 0 To Count - 1
            Result.Append(Data(Index).ToString("X2") & " ")
        Next Index

        Return Result.ToString()
    End Function

    Private Sub button1_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim BaseAddress As UInteger = 0
        Dim Count As Byte
        Dim AddressHigh As Byte
        Dim AddressLow As Byte
        Dim Address As UShort
        Dim RecordType As Byte
        Dim Checksum As Byte
        Dim Data(99) As Byte

        If String.IsNullOrEmpty(TextBoxInputFile.Text) Then
            Return
        End If

        Try
            _input = New StreamReader(TextBoxInputFile.Text)
            Do While Not _input.EndOfStream
                Dim Line As String = _input.ReadLine()
                If Line.Chars(0) <> ":"c Then
                Else
                    ' XXX KTK do app and ccbl files have embedded checksums (crc)? If not, generate it and check against value reported by micro.
                    Count = Byte.Parse(Line.Substring(1, 2), System.Globalization.NumberStyles.AllowHexSpecifier)
                    Checksum = Count
                    AddressHigh = Byte.Parse(Line.Substring(3, 2), System.Globalization.NumberStyles.AllowHexSpecifier)
                    Checksum += AddressHigh
                    AddressLow = Byte.Parse(Line.Substring(5, 2), System.Globalization.NumberStyles.AllowHexSpecifier)
                    Checksum += AddressLow
                    RecordType = Byte.Parse(Line.Substring(7, 2), System.Globalization.NumberStyles.AllowHexSpecifier)
                    Checksum += RecordType
                    For Index As Integer = 0 To Count - 1
                        Data(Index) = Byte.Parse(Line.Substring(Index * 2 + 9, 2), System.Globalization.NumberStyles.AllowHexSpecifier)
                        Checksum += Data(Index)
                    Next Index
                    Checksum += Byte.Parse(Line.Substring(Count * 2 + 9, 2), System.Globalization.NumberStyles.AllowHexSpecifier)
                    If Checksum <> 0 Then

                        Address = CUShort((CInt(AddressHigh) << 8) Or AddressLow)
                    End If
                    Select Case RecordType
                        Case &H0
                                ' Data

                        Case &H1
                                ' End Of File
                        Case &H4
                            ' Extended Linear Address
                            If Count <> 2 Then

                            Else
                                BaseAddress = CUInt(((CInt(Data(0)) << 8) Or Data(1)) << 16)
                            End If
                        Case Else
                            ' Invalid record type - display error with record type.
                    End Select
                End If
            Loop
            _input.Close()
        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Private Function GetHexFilePath(ByVal sender As Object) As String

        Dim startDirectory = My.Settings.ApplicationDirectory

        If ReferenceEquals(sender, ButtonProgramApplication) Then
            ButtonProgramApplication.BackColor = Color.Cyan
            ButtonProgramEeprom.BackColor = SystemColors.Control
            ButtonProgramCcbl.BackColor = SystemColors.Control
        ElseIf ReferenceEquals(sender, ButtonProgramEeprom) Then
            ButtonProgramEeprom.BackColor = Color.Cyan
            ButtonProgramApplication.BackColor = SystemColors.Control
            ButtonProgramCcbl.BackColor = SystemColors.Control
        ElseIf ReferenceEquals(sender, ButtonProgramCcbl) Then
            ButtonProgramCcbl.BackColor = Color.Cyan
            ButtonProgramApplication.BackColor = SystemColors.Control
            ButtonProgramEeprom.BackColor = SystemColors.Control
        End If
        If Not Directory.Exists(startDirectory) Then
            startDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
        End If
        OpenHexFileDialog.InitialDirectory = startDirectory
        If OpenHexFileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            TextBoxInputFile.Text = OpenHexFileDialog.FileName
            My.Settings.ApplicationDirectory = Path.GetDirectoryName(OpenHexFileDialog.FileName)
            ButtonUpload.Enabled = True
            ButtonDownload.Enabled = True
        Else
            If Not File.Exists(_downloadFilePath) Then
                TextBoxInputFile.Text = ""
                ButtonUpload.Enabled = False
                ButtonDownload.Enabled = False
            End If
        End If
        Return TextBoxInputFile.Text
    End Function

    Private Sub EnableButtons(ByVal enable As Boolean)
        ButtonProgramApplication.Enabled = enable
        ButtonProgramEeprom.Enabled = enable
        ButtonProgramCcbl.Enabled = enable
        ButtonUpload.Enabled = enable

        ButtonDownload.Enabled = enable
    End Sub

    Private Sub ButtonProgramApplication_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonProgramApplication.Click
        _downloadFilePath = GetHexFilePath(sender)
    End Sub

    Private Sub ButtonProgramEeprom_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonProgramEeprom.Click
        _downloadFilePath = GetHexFilePath(sender)
    End Sub

    Private Sub ButtonProgramCcbl_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonProgramCcbl.Click
        _downloadFilePath = GetHexFilePath(sender)
    End Sub


    Private Sub ButtonDownload_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonDownload.Click
        EnableButtons(False)
        If String.IsNullOrEmpty(_downloadFilePath) Then
            Try




                '    _output = new StreamWriter(TextBoxOutputFile.Text, false);
                'PostMessage("Downloading File");
            Catch ex As Exception
                RaiseEvent LogExceptionEvent(New StackTrace, ex)
                Return
            End Try
        Else

        End If
    End Sub

    Private Sub ButtonUpload_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonUpload.Click
        EnableButtons(False)
        Try



            '_output = new StreamWriter(TextBoxOutputFile.Text, false);
            'PostMessage("Downloading File");
        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
            Return
        End Try
    End Sub
End Class
