﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FormROIConfiguration
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormROIConfiguration))
        Me.TreeViewROIs = New System.Windows.Forms.TreeView()
        Me.ContextMenuStripRegionsOfInterest = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ToolStripMenuItemEditROI = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItemNewROI = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItemDeleteROI = New System.Windows.Forms.ToolStripMenuItem()
        Me.LabelROIConfigurations = New System.Windows.Forms.Label()
        Me.ToolTipDisplayROI = New System.Windows.Forms.ToolTip(Me.components)
        Me.ContextMenuStripRegionsOfInterest.SuspendLayout
        Me.SuspendLayout
        '
        'TreeViewROIs
        '
        Me.TreeViewROIs.BackColor = System.Drawing.Color.MidnightBlue
        Me.TreeViewROIs.ContextMenuStrip = Me.ContextMenuStripRegionsOfInterest
        Me.TreeViewROIs.DrawMode = System.Windows.Forms.TreeViewDrawMode.OwnerDrawText
        Me.TreeViewROIs.Font = New System.Drawing.Font("Consolas", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.TreeViewROIs.ForeColor = System.Drawing.Color.Chartreuse
        Me.TreeViewROIs.FullRowSelect = true
        Me.TreeViewROIs.HideSelection = false
        Me.TreeViewROIs.Location = New System.Drawing.Point(9, 21)
        Me.TreeViewROIs.Name = "TreeViewROIs"
        Me.TreeViewROIs.ShowNodeToolTips = true
        Me.TreeViewROIs.Size = New System.Drawing.Size(387, 476)
        Me.TreeViewROIs.TabIndex = 109
        Me.TreeViewROIs.TabStop = false
        Me.ToolTipDisplayROI.SetToolTip(Me.TreeViewROIs, "Right-click to see menu.")
        '
        'ContextMenuStripRegionsOfInterest
        '
        Me.ContextMenuStripRegionsOfInterest.AutoSize = false
        Me.ContextMenuStripRegionsOfInterest.BackColor = System.Drawing.Color.MidnightBlue
        Me.ContextMenuStripRegionsOfInterest.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ContextMenuStripRegionsOfInterest.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemEditROI, Me.ToolStripMenuItemNewROI, Me.ToolStripMenuItemDeleteROI})
        Me.ContextMenuStripRegionsOfInterest.Name = "ContextMenuStripRegionsOfInterest"
        Me.ContextMenuStripRegionsOfInterest.Size = New System.Drawing.Size(135, 72)
        '
        'ToolStripMenuItemEditROI
        '
        Me.ToolStripMenuItemEditROI.AutoSize = false
        Me.ToolStripMenuItemEditROI.Enabled = false
        Me.ToolStripMenuItemEditROI.ForeColor = System.Drawing.Color.Orange
        Me.ToolStripMenuItemEditROI.Name = "ToolStripMenuItemEditROI"
        Me.ToolStripMenuItemEditROI.Size = New System.Drawing.Size(130, 22)
        Me.ToolStripMenuItemEditROI.Text = "Edit Selected ROI"
        '
        'ToolStripMenuItemNewROI
        '
        Me.ToolStripMenuItemNewROI.AutoSize = false
        Me.ToolStripMenuItemNewROI.ForeColor = System.Drawing.Color.Orange
        Me.ToolStripMenuItemNewROI.Name = "ToolStripMenuItemNewROI"
        Me.ToolStripMenuItemNewROI.Size = New System.Drawing.Size(130, 22)
        Me.ToolStripMenuItemNewROI.Text = "Create an ROI"
        '
        'ToolStripMenuItemDeleteROI
        '
        Me.ToolStripMenuItemDeleteROI.AutoSize = false
        Me.ToolStripMenuItemDeleteROI.Enabled = false
        Me.ToolStripMenuItemDeleteROI.ForeColor = System.Drawing.Color.Orange
        Me.ToolStripMenuItemDeleteROI.Name = "ToolStripMenuItemDeleteROI"
        Me.ToolStripMenuItemDeleteROI.Size = New System.Drawing.Size(130, 22)
        Me.ToolStripMenuItemDeleteROI.Text = "Delete Selected ROI"
        '
        'LabelROIConfigurations
        '
        Me.LabelROIConfigurations.AutoSize = true
        Me.LabelROIConfigurations.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.LabelROIConfigurations.ForeColor = System.Drawing.Color.White
        Me.LabelROIConfigurations.Location = New System.Drawing.Point(118, 3)
        Me.LabelROIConfigurations.Name = "LabelROIConfigurations"
        Me.LabelROIConfigurations.Size = New System.Drawing.Size(168, 15)
        Me.LabelROIConfigurations.TabIndex = 111
        Me.LabelROIConfigurations.Text = "Regions Of Interest (ROI)"
        '
        'ToolTipDisplayROI
        '
        Me.ToolTipDisplayROI.AutomaticDelay = 100
        Me.ToolTipDisplayROI.AutoPopDelay = 10000
        Me.ToolTipDisplayROI.InitialDelay = 100
        Me.ToolTipDisplayROI.ReshowDelay = 20
        '
        'FormROIConfiguration
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8!, 15!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.DimGray
        Me.ClientSize = New System.Drawing.Size(405, 505)
        Me.Controls.Add(Me.LabelROIConfigurations)
        Me.Controls.Add(Me.TreeViewROIs)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ForeColor = System.Drawing.Color.White
        Me.Icon = CType(resources.GetObject("$this.Icon"),System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Name = "FormROIConfiguration"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Region Of Interest Display"
        Me.ContextMenuStripRegionsOfInterest.ResumeLayout(false)
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub

    Friend WithEvents TreeViewROIs As TreeView
    Friend WithEvents LabelROIConfigurations As Label
    Friend WithEvents ContextMenuStripRegionsOfInterest As ContextMenuStrip
    Friend WithEvents ToolStripMenuItemEditROI As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItemNewROI As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItemDeleteROI As ToolStripMenuItem
    Friend WithEvents ToolTipDisplayROI As ToolTip
End Class
