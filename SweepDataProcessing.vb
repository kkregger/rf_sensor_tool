﻿Imports System.IO
Imports System.Threading
Imports System.Runtime.InteropServices
Imports System.Collections.Concurrent
Imports System.Security.Cryptography

Public NotInheritable Class SweepDataProcessing

    Private Shared _loggingThreadList As List(Of Thread)
    Private Shared _stopLogging       As Boolean
    Private Shared _lockingObj        As New Object()

    ' Calibration data points queue of queues.
    Private Shared _sweepDataPointQueue    As New ConcurrentQueue(Of ConcurrentQueue(Of SweepDataPoint))
    Private Shared _logSweepDataPointQueue As New ConcurrentQueue(Of ConcurrentQueue(Of ConcurrentQueue(Of SweepDataPoint)))

    ' Calibration sweep data reading Queues.
    Private Shared _sweepStartDataQueue    As New ConcurrentQueue(Of SweepStartData)()
    Private Shared _sweepStepDataQueue     As New ConcurrentQueue(Of SweepStepData)()
    Private Shared _sweepEndDataQueue      As New ConcurrentQueue(Of SweepEndData)()
    Private Shared _sweepCmdDataQueue      As New ConcurrentQueue(Of SweepCmdData)()

    ' Event raised when an error is being logged.
    Private Shared Event LogErrorEvent     As ActivityLog.LogErrorStackDelegate
    Private Shared Event LogExceptionEvent As ActivityLog.LogExceptionDelegate

    Shared Sub New()

        AddHandler LogErrorEvent, AddressOf ActivityLog.LogError
        AddHandler LogExceptionEvent, AddressOf ActivityLog.LogError

        ' Add delegates.
        CanMessageLib.AddSweepActiveDataAvailableEventHandler(AddressOf SweepActiveDataAvailable)
        CanMessageLib.AddSweepEndingDataReceivedEventHandler(AddressOf SweepEndingDataReceived)
        CanMessageLib.AddSweepStartingValuesRcvdHandler(AddressOf SweepStartingDataReceived)

    End Sub

    Public Shared Property LogFileNames As New ConcurrentQueue(Of String)

    Public Shared Readonly Property LoggingInProgress As Boolean

    Public Shared Property TerminateActiveSweep() As Boolean

    Public Shared Property LoggingDisabled()    As Boolean

    Public Shared Property EncryptLogFiles() As Boolean = True

    Public Shared Property StopLogging() As Boolean
        Get
            Return _stopLogging
        End Get
        Set(value As Boolean)
            _stopLogging = value
            If (_stopLogging) Then

                ' Re-initialize the active sweep data received queues.
                SyncLock _lockingObj
                    _sweepDataPointQueue = New ConcurrentQueue(Of ConcurrentQueue(Of SweepDataPoint))
                    _sweepStartDataQueue = New ConcurrentQueue(Of SweepStartData)
                    _sweepStepDataQueue  = New ConcurrentQueue(Of SweepStepData)
                    _sweepEndDataQueue   = New ConcurrentQueue(Of SweepEndData)
                    _sweepCmdDataQueue   = New ConcurrentQueue(Of SweepCmdData)
                End Synclock
            End If
        End Set
    End Property

    Public Shared Sub Terminate()

        ' Called by the main form before it closes.
        If (_loggingThreadList IsNot Nothing) Then

            for each threadRef In _loggingThreadList

                ' Don't ever want to hit this.
                threadRef.Abort()
                ActivityLog.LogError(New StackTrace,"A logging thread is being aborted.")
            Next
        End If

    End Sub

    Public Shared Sub SweepActiveDataAvailable(ByVal dataPnts As ConcurrentQueue(Of SweepDataPoint))

        If (Not StopLogging) Then
            SyncLock _lockingObj
                _sweepDataPointQueue.Enqueue(dataPnts) ' Queue latest received sweep data points.
            End SyncLock
        End If
    End Sub

    Public Shared Sub SweepStartingDataReceived(ByVal startData As SweepStartData,
                                                ByVal stepData As SweepStepData)
        If (Not StopLogging) Then
            SyncLock _lockingObj
                _sweepStartDataQueue.Enqueue(startData)
                _sweepStepDataQueue.Enqueue(stepData)  ' Queue latest received sweep Start and sweep step data.
            End Synclock
        End If
    End Sub

    Public Shared Sub SweepEndingDataReceived(ByVal endData As SweepEndData,
                                              ByVal cmdData As SweepCmdData)

        Dim dateStr = Date.Now.Year.ToString() & Date.Now.Month.ToString() & Date.Now.Day.ToString("D2")
        Dim timeStr = (Date.Now.TimeOfDay.Ticks \ TimeSpan.TicksPerSecond).ToString()  ' Time since midnight in seconds.
        Dim loggingThread As Thread
        Dim dataLogger    As SweepDataLogger

        If (Not StopLogging) Then

            SyncLock _lockingObj
                _sweepEndDataQueue.Enqueue(endData)
                _sweepCmdDataQueue.Enqueue(cmdData)    ' Queue latest received sweep end and sweep cmd data.
            End Synclock

            If (_sweepCmdDataQueue.Count >= Common.DefaultSweepCount) Then

                    If (_loggingThreadList Is Nothing) Then
                        _loggingThreadList = New List(Of Thread)
                    End If

                    SyncLock _lockingObj

                        dataLogger = New SweepDataLogger(dateStr & "-" & timeStr, _sweepStartDataQueue,
                                                         _sweepStepDataQueue, _sweepEndDataQueue,
                                                         _sweepCmdDataQueue, _sweepDataPointQueue)
                    End Synclock

                    ' Start a sweep logging thread.
                    loggingThread = New Thread(AddressOf dataLogger.LogSweepData)

                    ' I didn't use the threadpool here because I couldn't guarentee the order of execution.
                    ' Allows checking to make sure all logging threads are terminated.
                    _loggingThreadList.Add(loggingThread)
                    loggingThread.Start()

                ' Re-initialize the active sweep data received queues.
                SyncLock _lockingObj
                    _sweepDataPointQueue = New ConcurrentQueue(Of ConcurrentQueue(Of SweepDataPoint))
                    _sweepStartDataQueue = New ConcurrentQueue(Of SweepStartData)
                    _sweepStepDataQueue  = New ConcurrentQueue(Of SweepStepData)
                    _sweepEndDataQueue   = New ConcurrentQueue(Of SweepEndData)
                    _sweepCmdDataQueue   = New ConcurrentQueue(Of SweepCmdData)
                End Synclock
            End If
        End If
    End Sub

    Private Shared Function IsFileLocked(exception As Exception) As Boolean

        ' Get the error code from the exception HRESULT.
        Dim errorCode = Marshal.GetHRForException(exception) And ((1 << 16) - 1)

        Return errorCode = Common.ErrorSharingViolation OrElse errorCode = Common.ErrorLockViolation
    End Function

    Private Shared Sub RemoveThreadFromList(ByVal threadRef As Thread)
        If (_loggingThreadList IsNot Nothing) Then
            _loggingThreadList?.Remove(threadRef)
            If (_loggingThreadList.Count = 0) Then
                _LoggingInProgress = False
                FormRfSensorMain.GetInstance?.BeginInvoke(
                        New MethodInvoker(AddressOf FormRfSensorMain.GetInstance.DisplayCurrentStatus))
            End If
        End If
    End Sub

    Private Class SweepDataLogger

        Private _dateTimeStr         As String   = String.Empty
        Private _sweepStartQueue     As New ConcurrentQueue(Of SweepStartData)
        Private _sweepStepQueue      As New ConcurrentQueue(Of SweepStepData)
        Private _sweepEndQueue       As New ConcurrentQueue(Of SweepEndData)
        Private _sweepCmdQueue       As New ConcurrentQueue(Of SweepCmdData)
        Private _sweepDataPointQueue As New ConcurrentQueue(Of ConcurrentQueue(Of SweepDataPoint))

        Private Shared _directoryCount As UInteger = 0

        Public Sub New()
        End Sub

        Public Sub New (ByVal dateTimeStr         As String,
                        ByVal sweepStartQueue     As ConcurrentQueue(Of SweepStartData),
                        ByVal sweepStepQueue      As ConcurrentQueue(Of SweepStepData),
                        ByVal sweepEndQueue       As ConcurrentQueue(Of SweepEndData),
                        ByVal sweepCmdQueue       As ConcurrentQueue(Of SweepCmdData),
                        ByVal sweepDataPointQueue As ConcurrentQueue(Of ConcurrentQueue(Of SweepDataPoint)))

            _dateTimeStr         = dateTimeStr
            _sweepStartQueue     = sweepStartQueue
            _sweepStepQueue      = sweepStepQueue
            _sweepEndQueue       = sweepEndQueue
            _sweepCmdQueue       = sweepCmdQueue
            _sweepDataPointQueue = sweepDataPointQueue
        End Sub

        Public Sub LogSweepData()

            Dim logStr    = String.Empty
            Dim filePath  = String.Empty
            Dim fileName  = String.Empty
            Dim headerStr = String.Empty
            Dim dateStr   = Date.Now.Year.ToString() & Date.Now.Month.ToString() & Date.Now.Day.ToString("D2")

            ' Time since midnight in seconds.
            Dim timeStr = (Date.Now.TimeOfDay.Ticks \ TimeSpan.TicksPerSecond).ToString()
            Dim bypass        As Boolean
            Dim calVersion    As UInteger
            Dim frequency     As UInteger
            Dim cpuTemp       As UInteger
            Dim sweepStart    As New SweepStartData
            Dim sweepStep     As New SweepStepData
            Dim sweepEnd      As New SweepEndData
            Dim sweepCmd      As New SweepCmdData
            Dim dataPoint     As New SweepDataPoint
            Dim sweepDataPnts As New ConcurrentQueue(Of SweepDataPoint)
            Dim loggingStreamWriter As StreamWriter = Nothing

            Try
                If (StopLogging) Then
                    Exit Try
                End If

                ' Format received data for logging the open a log file and write data to the file.
                Do Until (_sweepCmdQueue.IsEmpty)

                    ' Log the last entry in the cmd data queue.
                    If (_sweepCmdQueue.TryDequeue(sweepCmd)) Then
                        If (_sweepCmdQueue.IsEmpty) Then
                            logStr = "Operating Mode:,"
                            If ([Enum].IsDefined(GetType(OperatingMode), sweepCmd.OperatingMode)) Then

                                Dim opStr = String.Empty
                                CanMessageLib.OperatingModeStrDict.TryGetValue(sweepCmd.OperatingMode, opStr)
                                logStr &= opStr
                                If (sweepCmd.OperatingMode = OperatingMode.Bypass) Then
                                    bypass = True
                                End If
                            Else
                                logStr &= String.Empty
                            End If
                            logStr &= ",Board Temp:," & sweepCmd.BoardTemp.ToString()
                        End If
                    End If
                Loop

                If (CanMessageLib.SendParamReadMsg(CanMessageLib.CpuTemperature, cpuTemp) OrElse
                    CanMessageLib.SendParamReadMsg(CanMessageLib.CpuTemperature, cpuTemp)) Then

                    logStr &= ",CPU Temp:," & (CLng(cpuTemp) - 55).ToString() & vbCrLf
                Else
                    logStr &= ",CPU Temp:," & String.Empty & vbCrLf
                End If

                If (_sweepStartQueue.TryDequeue(sweepStart)) Then

                    ' Log the first entry in the start data queue.
                    frequency = sweepStart.StartFrequency
                    logStr &= "Start Inlet Temp:," & (CDbl(sweepStart.StartInletTemp) / 10.0).ToString("F1") & "," &
                              "Start Outlet Temp:," & (CDbl(sweepStart.StartOutletTemp) / 10.0).ToString("F1") & ","
                End If

                Do Until (_sweepEndQueue.IsEmpty)

                    ' Log the last entry in the end data queue.
                    If (_sweepEndQueue.TryDequeue(sweepEnd)) Then
                        If (_sweepEndQueue.IsEmpty) Then
                            logStr &= "End Inlet Temp:," & (CDbl(sweepEnd.EndInletTemp) / 10.0).ToString("F1") & "," &
                                      "End Outlet Temp:," & (CDbl(sweepEnd.EndOutletTemp) / 10.0).ToString("F1") & vbCrLf &
                                      vbCrLf & "Frequency,Phase A/D,Magnitude A/D" & vbCrLf

                        End If
                    End If
                Loop

                ' Log all sweep data points.
                Do Until (_sweepDataPointQueue.IsEmpty)
                    If (_sweepDataPointQueue.TryDequeue(sweepDataPnts)) Then
                        Do While (sweepDataPnts.TryDequeue(dataPoint))
                            logStr &= frequency.ToString() & "," &
                                      dataPoint.PhaseAtoD.ToString() & "," &
                                      dataPoint.MagnitudeAtoD.ToString() & vbCrLf

                            ' Increment step frequency and step count.
                            frequency += Common.DefaultFrequencyStepSize
                        Loop
                    End If
                Loop

                If (StopLogging) Then
                    Exit Try
                End If

                headerStr = Date.Now.ToShortDateString & " " & Date.Now.ToShortTimeString & ",SN:,"

                If ((FormRfSensorMain.SerialNumber IsNot Nothing) AndAlso
                    (FormRfSensorMain.SerialNumber.Length > 0)) Then

                    For Each number In FormRfSensorMain.SerialNumber
                        headerStr &= number.ToString("X2")
                    Next
                Else
                    headerStr &= "0"
                End If

                RfSensorConfiguration.CalibrationValueDict.TryGetValue(
                    RfSensorConfiguration.CalibrationVersion, calVersion)
                headerStr &= ",Version:," & FormRfSensorMain.AppSoftwareVersion & ",App Sw Level:," &
                             FormRfSensorMain.AppSoftwareID & ",Cal Version:," & calVersion.ToString() &
                             ",Cfg File:," & FormRfSensorMain.ConfigCalsFileName & ",RST Version:," &
                             Application.ProductVersion & ",Build Date:," & My.Resources.BuildDate

                logStr = headerStr & logStr

                ' Open log file for writing.
                If (Not String.IsNullOrWhiteSpace(FormRfSensorMain.TestDirectoryName)) Then

                    If (Not LogFileNames.IsEmpty) Then

                        Try
                            LogFileNames.TryDequeue(fileName)
                            filePath = FormRfSensorMain.TestDirectoryName & "\" & fileName

                            If (EncryptLogFiles) Then

                                Dim loggingFileStream = New FileStream(filePath, FileMode.Create, FileAccess.Write)

                                If (loggingFileStream IsNot Nothing) Then

                                    Dim des = New DESCryptoServiceProvider With {.Key = FormFileEncryption.CryptKey,
                                                                                 .IV = FormFileEncryption.CryptKey}    ' Initial Vector.
                                    Dim encryptor = des.CreateEncryptor()
                                    Dim encryptorStream = New CryptoStream(loggingFileStream, encryptor, CryptoStreamMode.Write)

                                    If (encryptorStream IsNot Nothing) Then

                                        Dim logBytes = Text.Encoding.Unicode.GetBytes(logStr)
                                        _LoggingInProgress = True
                                        encryptorStream.Write(logBytes, 0, logBytes.Count)

                                        ' Close the crypto stream writer. Closes the FileStream as well.
                                        ' Adds PKCS-padding to the end of the last block when necessary.
                                        encryptorStream.FlushFinalBlock()
                                        encryptorStream.Close()
                                    End If
                                End If
                            Else
                                loggingStreamWriter = New StreamWriter(filePath, False)

                                If (loggingStreamWriter IsNot Nothing AndAlso
                                    loggingStreamWriter.BaseStream IsNot Nothing) Then

                                    _LoggingInProgress  = True
                                    loggingStreamWriter.Write(logStr)
                                End If

                                ' Close the stream writer.
                                If (loggingStreamWriter?.BaseStream IsNot Nothing) Then

                                    loggingStreamWriter.Flush()
                                    loggingStreamWriter.Close()
                                    loggingStreamWriter = Nothing
                                End If
                            End If

                            FormRfSensorMain.LastLogFileName = fileName

                        Catch ex As Exception
                            ActivityLog.LogError(New StackTrace, ex)
                        End Try
                    Else
                        ActivityLog.LogError(New StackTrace, "Log file name not available.")
                    End If
                End If

                FormRfSensorMain.GetInstance?.BeginInvoke(
                        New MethodInvoker(AddressOf FormRfSensorMain.GetInstance.DisplayCurrentStatus))

            Catch taex As ThreadAbortException

                ' Possible when application is closing.
                ActivityLog.LogError(New StackTrace, taex)
            Catch ex As Exception
                If (loggingStreamWriter?.BaseStream IsNot Nothing) Then
                    Try
                        loggingStreamWriter.Flush()
                        loggingStreamWriter.Close()
                        loggingStreamWriter = Nothing
                    Catch odex As ObjectDisposedException
                    End Try
                End If
                If TypeOf ex Is IOException AndAlso IsFileLocked(ex) Then

                    MsgBox("The log file " & filePath & vbCrLf & " is open in another process. " &
                               "Sweep data will not be logged.", MsgBoxStyle.SystemModal)
                Else
                    'MsgBox("A sweep data logging thread is closing due to an exception.", MsgBoxStyle.SystemModal)
                    ActivityLog.LogError(New StackTrace, ex)
                End If
            Finally
                Try
                    RemoveThreadFromList(Thread.CurrentThread)
                Catch ex As Exception
                    ' Exception is completely unexpected, but just to be on the safe side.
                End Try
            End Try
        End Sub
    End Class
End Class
