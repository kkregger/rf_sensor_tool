﻿Public Class FormROIConfiguration

    Private _formRoiEdit As FormRoiEdit
    Private _selectedROI As RegionOfInterest

    ' Events to raise when there is an error/exception to be logged.
    Private Shared Event LogErrorEvent As ActivityLog.LogErrorStackDelegate
    Private Shared Event LogExceptionEvent As ActivityLog.LogExceptionDelegate

    Public Sub New()

        ' Required.
        InitializeComponent()

        ' Used to control the look of Tool Strip Menu Items.
        ContextMenuStripRegionsOfInterest.Renderer = new MyRenderer()

        ' Register error logging event handlers.
        AddHandler LogErrorEvent, AddressOf ActivityLog.LogError
        AddHandler LogExceptionEvent, AddressOf ActivityLog.LogError

        CType(ToolStripMenuItemEditROI.Owner  , ToolStripDropDownMenu).ShowImageMargin = False
        CType(ToolStripMenuItemNewROI.Owner   , ToolStripDropDownMenu).ShowImageMargin = False
        CType(ToolStripMenuItemDeleteROI.Owner, ToolStripDropDownMenu).ShowImageMargin = False

        UpdateROITreeview()
    End Sub

    Private Sub UpdateROITreeview()

        Try
            Dim level0NodeIndex As Integer
            Dim level1NodeIndex As Integer
            Dim level2NodeIndex As Integer

            TreeViewROIs.BeginUpdate()
            TreeViewROIs.Nodes.Clear()
            For Each roi In FormRfSensorMain.RegionsOfInterest.Values
                level0NodeIndex = TreeViewROIs.Nodes.Count
                TreeViewROIs.Nodes.Add(roi.Name)
                TreeViewROIs.Nodes(level0NodeIndex).ToolTipText = roi.Name
                TreeViewROIs.Nodes(level0NodeIndex).Nodes.Add("Operating Mode = " & roi.OperatingMode)
                TreeViewROIs.Nodes(level0NodeIndex).Nodes.Add("Interval   = " & roi.SweepInterval.ToString() & " ms")
                TreeViewROIs.Nodes(level0NodeIndex).Nodes.Add("Start Freq = " & roi.StartFrequency.ToString() & " kHz")
                TreeViewROIs.Nodes(level0NodeIndex).Nodes.Add("Freq Steps = " & roi.StepCount)
                TreeViewROIs.Nodes(level0NodeIndex).Nodes.Add("Step Size  = " & roi.FreqStepSize & " KHz")
                If (roi.StatisticWeights.Count > 0) Then
                    TreeViewROIs.Nodes(level0NodeIndex).Nodes.Add("RF Statistics:")
                    level1NodeIndex = TreeViewROIs.Nodes(level0NodeIndex).Nodes.Count - 1

                    For Each statWeight In roi.StatisticWeights
                        TreeViewROIs.Nodes(level0NodeIndex).Nodes(level1NodeIndex).Nodes.
                            Add(Common.StatisticNameDict(statWeight.statistic))
                        level2NodeIndex = TreeViewROIs.Nodes(level0NodeIndex).Nodes(level1NodeIndex).Nodes.Count - 1
                        TreeViewROIs.Nodes(level0NodeIndex).Nodes(level1NodeIndex).Nodes(level2NodeIndex).Nodes.
                            Add("Weight = " & statWeight.Weight.ToString())
                    Next
                End If
            Next

            For Each measure In FormRfSensorMain.Measurements
                For Each roi In measure.Value.RegionsOfInterest
                    level0NodeIndex = TreeViewROIs.Nodes.Count
                    TreeViewROIs.Nodes.Add(roi.Name)
                    TreeViewROIs.Nodes(level0NodeIndex).ToolTipText = roi.Name
                    TreeViewROIs.Nodes(level0NodeIndex).Nodes.Add("Operating Mode = " & roi.OperatingMode)
                    TreeViewROIs.Nodes(level0NodeIndex).Nodes.Add("Interval   = "     & roi.SweepInterval.ToString() & " ms")
                    TreeViewROIs.Nodes(level0NodeIndex).Nodes.Add("Start Freq = "     & roi.StartFrequency.ToString() & " kHz")
                    TreeViewROIs.Nodes(level0NodeIndex).Nodes.Add("Freq Steps = "     & roi.StepCount)
                    TreeViewROIs.Nodes(level0NodeIndex).Nodes.Add("Step Size  = "     & roi.FreqStepSize & " kHz")
                    If (roi.StatisticWeights.Count > 0) Then
                        TreeViewROIs.Nodes(level0NodeIndex).Nodes.Add("RF Statistics:")
                        level1NodeIndex = TreeViewROIs.Nodes(level0NodeIndex).Nodes.Count - 1

                        For Each statWeight In roi.StatisticWeights
                            TreeViewROIs.Nodes(level0NodeIndex).Nodes(level1NodeIndex).Nodes.
                                Add(Common.StatisticNameDict(statWeight.statistic))
                            level2NodeIndex = TreeViewROIs.Nodes(level0NodeIndex).Nodes(level1NodeIndex).Nodes.Count - 1
                            TreeViewROIs.Nodes(level0NodeIndex).Nodes(level1NodeIndex).Nodes(level2NodeIndex).Nodes.
                                Add("Weight = " & statWeight.Weight.ToString())
                        Next
                    End If
                Next
            Next
            TreeViewROIs.EndUpdate()

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Private Sub TreeViewROIs_AfterSelect(sender As Object, e As TreeViewEventArgs) Handles TreeViewROIs.AfterSelect

        Try
            ' Use the ROI node label as key to Region of Interest dictionary.
            Dim node = e.Node
            Dim roiName As String

            '  Get the name of the top level node of this branch.
            If (node.Level > 0) Then
                While (node.Level > 0)
                    node = node.Parent
                End While
            End If
            roiName = node.Text

            ' Retrieve the selected ROI by name.
            If (Not FormRfSensorMain.RegionsOfInterest.TryGetValue(roiName, _selectedROI)) Then
                MsgBox("The selected ROI configuration was not found.",
                       MsgBoxStyle.SystemModal + MsgBoxStyle.Exclamation)
                RaiseEvent LogErrorEvent(New StackTrace, "TreeViewROIs_AfterSelect() failed to find selected ROI: " & roiName)
                TreeViewROIs.SelectedNode = Nothing
            Else
                ToolStripMenuItemEditROI.Enabled = True
                ToolStripMenuItemDeleteROI.Enabled = True
            End If

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Private Sub FormROIConfiguration_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        TreeViewROIs.SelectedNode = Nothing
    End Sub

    Private Sub TreeView_DrawNode(sender As object, e As DrawTreeNodeEventArgs) Handles TreeViewROIs.DrawNode

        Try
            If (e.Node Is Nothing) Then
                Return
            End If

            ' If treeview's HideSelection property is "True", 
            ' this will always return "False" on unfocused treeview
            Dim selected = (e.State And TreeNodeStates.Selected) = TreeNodeStates.Selected
            Dim unfocused = Not e.Node.TreeView.Focused

            ' We need to do owner drawing only on a selected node
            ' and when the treeview is unfocused, else let the OS do it for us
            If (selected AndAlso unfocused) Then
                Dim font = If(e.Node.NodeFont, e.Node.TreeView.Font)
                e.Graphics.FillRectangle(SystemBrushes.Highlight, e.Bounds)
                TextRenderer.DrawText(e.Graphics, e.Node.Text, font,
                                  e.Bounds, SystemColors.HighlightText,
                                  TextFormatFlags.GlyphOverhangPadding)
            Else
                e.DrawDefault = True
            End If
        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Private Sub ToolStripMenuItemEditROI_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItemEditROI.Click

        Try
            If (_selectedROI IsNot Nothing) Then

                ' Constructor does a deep copy.
                _formRoiEdit = New FormRoiEdit(_selectedROI)

                ' Display the ROI Edit dialog.
                _formRoiEdit.ShowDialog()
                UpdateROITreeview()
            Else
                MsgBox("Please select an ROI, then try again.", MsgBoxStyle.SystemModal)
            End If
        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Private Sub ToolStripMenuItemNewROI_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItemNewROI.Click

        ' Create New ROI that contains default values.
        Dim newROI = New RegionOfInterest()
        newROI.Independent = True
        _formRoiEdit = New FormRoiEdit(newROI)

        ' Display the ROI Edit dialog.
        _formRoiEdit.ShowDialog()
        UpdateROITreeview()
    End Sub

    Private Sub ToolStripMenuItemDeleteROI_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItemDeleteROI.Click

        Try
            Dim roi As RegionOfInterest = New RegionOfInterest()

            If (_selectedROI IsNot Nothing) Then

                Dim mbResult = MsgBox("Are you sure?" & vbcrlf &
                                      "The selected ROI will be permanently deleted.",
                                      MsgBoxStyle.SystemModal + MsgBoxStyle.YesNo)

                If (mbResult = MsgBoxResult.Yes) Then
                    If (FormRfSensorMain.RegionsOfInterest.TryRemove(_selectedROI.Name, roi)) Then

                        ' Success.
                        TreeViewROIs.SelectedNode = Nothing
                        _selectedROI              = Nothing
                        UpdateROITreeview()
                    Else
                        MsgBox("Failed to delete the selected ROI: " & _selectedROI.Name,
                               MsgBoxStyle.SystemModal + MsgBoxStyle.Exclamation)
                        RaiseEvent LogErrorEvent(New StackTrace, "ToolStripMenuItemDeleteROI_Click() failed " & 
                                                 "to delete the selected ROI related settings for: " & _selectedROI.Name)
                    End If
                End If
            Else
                MsgBox("Please select an ROI, then try again.", MsgBoxStyle.SystemModal)
            End If

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Private Sub ToolStripMenuItem_MouseEnter(sender As Object, e As EventArgs) _
        Handles ToolStripMenuItemEditROI.MouseEnter,
                ToolStripMenuItemNewROI.MouseEnter,
                ToolStripMenuItemDeleteROI.MouseEnter

                CType(sender, ToolStripMenuItem).ForeColor = Color.Chartreuse
    End Sub

    Private Sub ToolStripMenuItem_MouseLeave(sender As Object, e As EventArgs) _
        Handles ToolStripMenuItemEditROI.MouseLeave,
                ToolStripMenuItemNewROI.MouseLeave,
                ToolStripMenuItemDeleteROI.MouseLeave

                CType(sender, ToolStripMenuItem).ForeColor = Color.Orange
    End Sub
End Class