﻿Imports System.Threading
Imports System.Collections.Concurrent
Imports System.Runtime.InteropServices
Imports CTS_CAN_Services
Imports CTS_CAN_Services.Peak.Can.Basic

Partial Public NotInheritable Class CanMessageLib

#Region "constants"

    Private Const ToolMsgInvalid           As Byte = &H7F
    Private Const ReadParamIdsMax          As Byte = &H6
    Private Const ReadParamBytesMax        As Byte = &H6
    Private Const ReadAppCalIdxsMax        As Byte = &H6
    Private Const WriteAppCalIdxMax        As Byte = &H3
    Private Const ReadAppCalBytesMax       As Byte = &H6
    Private Const DefaultCanNodeID         As Byte = &H2
    Private Const DefaultDblCanNodeID      As Byte = &H2
    Private Const ToolCmdDataLength        As Byte = &H8
    Private Const SweepDataReadMsgRate     As Byte = &H1 ' msec., 1/.001 = 1KHz
    Private Const ReadMsgDataOffset        As Byte = &H2
    Private Const WriteMsgDataOffset       As Byte = &H2
    Private Const RespMsgDataOffset        As Byte = &H2
    Private Const WriteParamBytesMax       As Byte = &H3
    Private Const WriteAppCalBytesMax      As Byte = &H4
    Private Const AppCalWriteBit           As Byte = &H8
    Private Const ParamWriteBit            As Byte = &H8
    Public  Const HwLineRead               As Byte = &H1
    Public  Const HwLineWrite              As Byte = &H80
    Public  Const HwLineErase              As Byte = &HEB
    Public  Const FunctionMsgOp            As Byte = &H10
    Public  Const MsgTypeMask              As Byte = &HF0
    Public  Const RespMsgDataLength        As Byte = &H8
    Public  Const DblAttnRespMsgDataLength As Byte = &H7
    Public  Const SerialNumberLength       As Byte = &H4
    Public  Const CanNodeIdMask            As UInteger = &HFFFFFF00UI
    Public  Const CanMsgPriorityMask       As UInteger = &HFFFFFF
    Public  Const CanMsgMaskForPGN         As UInteger = &HFFFF00
    Public  Const DefaultCanBitRate        As Integer  = 250000
    Public  Const ApplicationDevLevel      As String   = "App_DevLev"
    Public  Const ApplicationID            As String   = "App_ID"
    Public  Const ApplicationLetter        As String   = "AppLetter"
    Public  Const ApplicationRevLev        As String   = "App_RevLev"
    Public  Const SoftwareDevLevel         As String   = "SW_DevRev"
    Public  Const SoftwareID               As String   = "SW_ID"
    Public  Const SoftwareRevLevel         As String   = "SW_MajorRev"
    Public  Const AppVoldDirLetter         As String   = "VoltDirLetter"
    Public  Const CpuTemperature           As String   = "InternalTemperature"

    ' Invalid Tool message responses IDs.
    Private Const ToolParamMsgInvalid As UInteger  = (CUInt(MessageTypeID.ParamID) << 8) + ToolMsgInvalid
    Private Const ToolAppCalMsgInvalid As UInteger = (CUInt(MessageTypeID.CalibrationRam) << 8) + ToolMsgInvalid

    ' Receive message timeout in milliseconds.
    Private Const RcvMsgTimeout As Double = 50.0

    ' Default message priority values.
    Private Const DefaultPm1MsgPriority         As UInteger = &H18000000 ' priority = 6
    Private Const DefaultPm2MsgPriority         As UInteger = &H18000000
    Private Const DefaultPm3MsgPriority         As UInteger = &H18000000
    Private Const DefaultToolRespMsgPriority    As UInteger = &H10000000 ' priority = 4
    Private Const DefaultCalDataReadMsgPriority As UInteger = &H0C000000 ' priority = 3
    Private Const DefaultDblToolRespMsgPriority As UInteger = &H18000000

    ' Default message PGN values.
    Private Const DefaultPm1MsgPGN         As UInteger = &HFFC500
    Private Const DefaultPm2MsgPGN         As UInteger = &HFF0A00
    Private Const DefaultPm3MsgPGN         As UInteger = &HFFB500
    Private Const DefaultCalDataReadMsgPGN As UInteger = &HFFD000
    Private Const DefaultToolRespMsgPGN    As UInteger = &HFF0D00
    Private Const DefaultDblToolRespMsgPGN As UInteger = &HFF0D00

    ' Default Message ID values.
    Private Const DefaultCommandMsgID    As UInteger = &HCFFC600
    Private Const DefaultToolCmdMsgID    As UInteger = &H18FF0E63
    Private Const DfltPgnRequestMsgID    As UInteger = &H18EA0000
    Private Const DfltDiagnosticMsg13ID  As UInteger = &H18DF0000
    Private Const DefaultInfoMessageID   As UInteger = &HCFF2000
    Public  Const AlternateInfoMsgID     As UInteger = &H3F0
    Private Const DefaultDblToolCmdMsgID As UInteger = &HCFF0E00

    Public Const DeviceIdLength     As Byte    = 4
    Public Const DblSwVersionLength As Byte    = 6

    ' Initial decryption seded value.
    Public Const InitialSeedValue As Byte = &HAA

    ' Scan data message data byte numbers.

    ' All table indices.
    Public Const byteTableIndexMsb    As Byte = 1
    Public Const byteTableIndexLsb    As Byte = 5

    ' Table Index = 0 to 1023
    Public Const bytePhaseAtoDMsb     As Byte = 0
    Public Const bytePhaseAtoDLsb     As Byte = 7
    Public Const byteMagnitudeAtoDLsb As Byte = 2
    Public Const byteMagnitudeAtoDMsb As Byte = 4
    Public Const byteScanNumberMsb    As Byte = 3
    Public Const byteScanNumberLsb    As Byte = 6

    ' Table Index = 0xFFF0
    Public Const byteStartingOutletTempMsb As Byte = 0
    Public Const byteStartingOutletTempLsb As Byte = 4
    Public Const byteStartingFrequencyLsb  As Byte = 2
    Public Const byteStartingFrequency2nd  As Byte = 7
    Public Const byteStartingInletTempMsb  As Byte = 3
    Public Const byteStartingInletTempLsb  As Byte = 6

    ' Table Index = 0xFFF1
    Public Const byteStepSizeLsb          As Byte = 0
    Public Const byteStartingFrequencyMsb As Byte = 2
    Public Const byteReadDelayLsb         As Byte = 3
    Public Const byteStepSize2nd          As Byte = 4
    Public Const byteReadDelayMsb         As Byte = 6
    Public Const byteStepSizeMsb          As Byte = 7

    ' Table Index = 0xFFF2
    Public Const byteEndingOutletTempMsb As Byte = 0
    Public Const byteEndingOutletTempLsb As Byte = 4
    Public Const byteEndingFrequencyLsb  As Byte = 2
    Public Const byteEndingFrequency2nd  As Byte = 7
    Public Const byteEndingInletTempMsb  As Byte = 3
    Public Const byteEndingInletTempLsb  As Byte = 6

    ' Table Index = 0xFFF9
    Public Const byteOperatingMode      As Byte = 0
    Public Const byteEndingFrequencyMsb As Byte = 2
    Public Const byteStepDelay          As Byte = 3
    Public Const byteEntriesMsb         As Byte = 4
    Public Const byteEntriesLsb         As Byte = 7
    Public Const byteBoardTemp          As Byte = 6


#End Region ' constants

#Region "fields"

    ' OutputPower values accessed using combo box Output Power strings.
    Public Shared ReadOnly OutputPowerDict As ConcurrentDictionary(Of String, OutputPowerLevel) =
        New ConcurrentDictionary(Of String, OutputPowerLevel)(New Dictionary(Of String, OutputPowerLevel) From {
        {"-4", OutputPowerLevel.Minus4dBm},
        {"-1", OutputPowerLevel.Minus1dBm},
        {"+2", OutputPowerLevel.Plus2dBm},
        {"+5", OutputPowerLevel.Plus5dBm}
        })

    Public Shared ReadOnly OutputPowerStrDict As ConcurrentDictionary(Of OutputPowerLevel, String) =
        New ConcurrentDictionary(Of OutputPowerLevel, String)(New Dictionary(Of OutputPowerLevel, String) From {
        {OutputPowerLevel.Minus4dBm, "-4dBm"},
        {OutputPowerLevel.Minus1dBm, "-1dBm"},
        {OutputPowerLevel.Plus2dBm , "+2dBm"},
        {OutputPowerLevel.Plus5dBm , "+5dBm"}
        })

    Public Shared ReadOnly OperatingModeStrDict As ConcurrentDictionary(Of OperatingMode, String) =
        New ConcurrentDictionary(Of OperatingMode, String)(New Dictionary(Of OperatingMode, String) From {
        {OperatingMode.S12    , "S12"},
        {OperatingMode.S11    , "S11"},
        {OperatingMode.Bypass, "Bypass"},
        {OperatingMode.S11Load, "S11 Load"},
        {OperatingMode.S11Open, "S11 Open"}
        })

    Public Shared ReadOnly OperatingModeDict As ConcurrentDictionary(Of String, OperatingMode) =
        New ConcurrentDictionary(Of String, OperatingMode)(New Dictionary(Of String, OperatingMode) From {
        {"S12"      , OperatingMode.S12},
        {"S11"      , OperatingMode.S11},
        {"Bypass"   , OperatingMode.Bypass},
        {"S11 Load" , OperatingMode.S11Load},
        {"S11 Open" , OperatingMode.S11Open}
        })

    ' Number of bytes in DBL tool messages. Accessed using the DblSendOpCodes enum.
    Private Shared ReadOnly DblMsgDataLenDict As ConcurrentDictionary(Of DblSendOpCodes, Byte) =
        New ConcurrentDictionary(Of DblSendOpCodes, Byte)(New Dictionary(Of DblSendOpCodes, Byte) From {
        {DblSendOpCodes.DblAttention         , 1},
        {DblSendOpCodes.DblInitateWrite      , 8},
        {DblSendOpCodes.DblMemWriteData      , 2},    ' Minimum, 1 byte op code, 1 byte data.
        {DblSendOpCodes.DblInitiateRead      , 8},
        {DblSendOpCodes.DblMemReadDataAck    , 3},
        {DblSendOpCodes.DblCalcChksum        , 3},
        {DblSendOpCodes.DblReadSerialNumber  , 1},
        {DblSendOpCodes.DblWriteSerNumToFlash, 5},
        {DblSendOpCodes.DblClrActiveSession  , 1},
        {DblSendOpCodes.DblCloseSession      , 1},
        {DblSendOpCodes.DblStartApp          , 1},
        {DblSendOpCodes.DblRestart           , 1}
        })

    ' Number of bytes in DBL tool response messages. Accessed using the DblRcvOpCodes enum.
    Private Shared ReadOnly DblRespDataLenDict As ConcurrentDictionary(Of DblRcvOpCodes, Byte) =
        New ConcurrentDictionary(Of DblRcvOpCodes, Byte)(New Dictionary(Of DblRcvOpCodes, Byte) From {
        {DblRcvOpCodes.DblAttnResp         , 7},
        {DblRcvOpCodes.DblInitWriteAck     , 4},
        {DblRcvOpCodes.DblMemWriteDataAck  , 4},
        {DblRcvOpCodes.DblInitReadAck      , 4},
        {DblRcvOpCodes.DblMemReadData      , 2},    ' Minimum, 1 byte op code, 1 byte data.
        {DblRcvOpCodes.DblChksumResult     , 6},
        {DblRcvOpCodes.DblSerialNumberResp , 7},
        {DblRcvOpCodes.DblWriteSerNumAck   , 2},
        {DblRcvOpCodes.DblActiveSessionClrd, 1},
        {DblRcvOpCodes.DblCloseSessionAck  , 1},
        {DblRcvOpCodes.DblStartAppAck      , 1},
        {DblRcvOpCodes.DblRestartAck       , 1},
        {DblRcvOpCodes.DblFaultResp        , 2}
        })

    ' Write memory address ranges by memory type.
    Private Shared ReadOnly WriteAddrRangeDict As ConcurrentDictionary(Of WriteTargetMem, UInteger()) =
        New ConcurrentDictionary(Of WriteTargetMem, UInteger())(New Dictionary(Of WriteTargetMem, UInteger()) From {
        {WriteTargetMem.Program, New UInteger() {&H00200, &H10FCD}},
        {WriteTargetMem.Eeprom , New UInteger() {&H00000, &H00FFF}},
        {WriteTargetMem.Ccbl   , New UInteger() {&H12800, &H12FFF}}          ' Update Dict XXX KTK
        })

    ' Read memory address ranges by memory type.
    Private Shared ReadOnly ReadAddrRangeDict As ConcurrentDictionary(Of ReadTargetMem, UInteger()) =
        New ConcurrentDictionary(Of ReadTargetMem, UInteger())(New Dictionary(Of ReadTargetMem, UInteger()) From {
        {ReadTargetMem.Program, New UInteger() {&H00200, &H10FCD}},
        {ReadTargetMem.Eeprom , New UInteger() {&H00000, &H00FFF}},
        {ReadTargetMem.Dbl    , New UInteger() {&H11300, &H11367}},
        {ReadTargetMem.Ccbl   , New UInteger() {&H12800, &H12FFF}},
        {ReadTargetMem.Ram    , New UInteger() {&H01000, &H4FFF}}            ' Update Dict XXX KTK
        })

    ' Tool Function message data byte arrays accessed using Tool Function message enum.
    Private Shared ReadOnly ToolFuncReqDict As ConcurrentDictionary(Of ToolFunctionMsg, Byte()) =
        New ConcurrentDictionary(Of ToolFunctionMsg, Byte())(New Dictionary(Of ToolFunctionMsg, Byte()) From {
        {ToolFunctionMsg.JumpToDbl,
          New Byte() {&H10, &H1,  CByte(ToolFunctionMsg.JumpToDbl), &HFF, &HAA, &H55, &HFF, &HFF}},
        {ToolFunctionMsg.AccessProdHwLineWord,
          New Byte() {&H10, &HFF, CByte(ToolFunctionMsg.AccessProdHwLineWord), &HFF, &HFF, &HFF, &HFF, &HFF}},
        {ToolFunctionMsg.AllowToolCmdFunctions,
          New Byte() {&H10, &H2,  CByte(ToolFunctionMsg.AllowToolCmdFunctions), &H1A, &HFF, &HFF, &HFF, &HFF}},
        {ToolFunctionMsg.ReadAppSoftwareConfigs,
          New Byte() {&H10, &HFF, CByte(ToolFunctionMsg.ReadAppSoftwareConfigs), &H0, &HFF, &HFF, &HFF, &HFF}},
        {ToolFunctionMsg.ReadSoftwareVersion,
          New Byte() {&H10, &HFF, CByte(CUInt(ToolFunctionMsg.ReadSoftwareVersion) >> 4), &H1, &HFF, &HFF, &HFF, &HFF}},
        {ToolFunctionMsg.ReadDeviceID,
          New Byte() {&H10, &H4,  CByte(CUInt(ToolFunctionMsg.ReadDeviceID) >> 4), &H2, &HFF, &HFF, &HFF, &HFF}},
        {ToolFunctionMsg.ReadSerialNumFromFlash,
          New Byte() {&H10, &H4,  CByte(CUInt(ToolFunctionMsg.ReadSerialNumFromFlash) >> 4), &H3, &HFF, &HFF, &HFF, &HFF}},
        {ToolFunctionMsg.EraseEepromRequest,
          New Byte() {&H10, &H5,  CByte(ToolFunctionMsg.EraseEepromRequest), &H9F, &HFF, &HFF, &HFF, &HFF}},
        {ToolFunctionMsg.InitializeUsageData,
          New Byte() {&H10, &H5,  CByte(CUInt(ToolFunctionMsg.InitializeUsageData) >> 4), &H40, &HFF, &HFF, &HFF, &HFF}},
        {ToolFunctionMsg.InitUsageDataIncludingTime,
          New Byte() {&H10, &H5,  CByte(CUInt(ToolFunctionMsg.InitUsageDataIncludingTime) >> 4), &H44, &HFF, &HFF, &HFF, &HFF}},
        {ToolFunctionMsg.ApplicationReset,
          New Byte() {&H10, &H71, CByte(ToolFunctionMsg.ApplicationReset), &HFF, &HFF, &HFF, &HFF, &HFF}},
        {ToolFunctionMsg.ReadDblCalsFromFlashToRam,
          New Byte() {&H10, &H1,  CByte(ToolFunctionMsg.ReadDblCalsFromFlashToRam), &HFF, &HFF, &HFF, &HFF, &HFF}},
        {ToolFunctionMsg.WriteDblCalRamArrayToFlash,
          New Byte() {&H10, &H1,  CByte(ToolFunctionMsg.WriteDblCalRamArrayToFlash), &HFF, &HFF, &HFF, &HFF, &HFF}},
        {ToolFunctionMsg.DisableEepromDataWrite,
          New Byte() {&H10, &HFF, CByte(ToolFunctionMsg.DisableEepromDataWrite), &H1, &HFF, &HFF, &HFF, &HFF}},
        {ToolFunctionMsg.EnableEepromDataWrite,
          New Byte() {&H10, &HFF, CByte(ToolFunctionMsg.EnableEepromDataWrite), &HFF, &HFF, &HFF, &HFF, &HFF}},
        {ToolFunctionMsg.UpdateSerialNumberInFlash,
          New Byte() {&H10, &H1,  CByte(ToolFunctionMsg.UpdateSerialNumberInFlash), &H1A, &HFF, &HFF, &HFF, &HFF}},
        {ToolFunctionMsg.WritePartNumberLsbToFlash,
          New Byte() {&H10, &H1,  CByte(ToolFunctionMsg.WritePartNumberLsbToFlash), &H1A, &HFF, &HFF, &HFF, &HFF}},
        {ToolFunctionMsg.WritePartNumberMsbToFlash,
          New Byte() {&H10, &H1,  CByte(ToolFunctionMsg.WritePartNumberMsbToFlash), &H1A, &HFF, &HFF, &HFF, &HFF}},
        {ToolFunctionMsg.UpdateRamWithDefaultCalVals,
          New Byte() {&H10, &H1,  CByte(ToolFunctionMsg.UpdateRamWithDefaultCalVals), &HFF, &HFF, &HFF, &HFF, &HFF}},
        {ToolFunctionMsg.WriteAppCalsFromRamToFlash,
          New Byte() {&H10, &H1,  CByte(ToolFunctionMsg.WriteAppCalsFromRamToFlash), &HFF, &HFF, &HFF, &HFF, &HFF}},
        {ToolFunctionMsg.UpdateAppNodeIDRequest,
          New Byte() {&H10, &H1,  CByte(ToolFunctionMsg.UpdateAppNodeIDRequest), &HFF, &HFF, &HFF, &HFF, &HFF}},
        {ToolFunctionMsg.SendCalScanData,
          New Byte() {&H10, &H2,  CByte(ToolFunctionMsg.SendCalScanData), &HFF, &HFF, &HFF, &HFF, &HFF}}
        })


    ' Collection of received status messages.
    Private Shared ReadOnly StatusMsgs As New ConcurrentDictionary(Of Byte, TPCANMsg)()

    ' Collection of tool function response messages.
    Private Shared ReadOnly ToolFuncResponseMsgs As New ConcurrentDictionary(Of UShort, CanMsgRcvd)()

    ' Locking object for tool response message collection.
    Private Shared ReadOnly ToolFuncRespLock As New Object()

    ' Collection of tool param ID read response messages.
    Private Shared ReadOnly ToolParamReadRespMsgs As New ConcurrentDictionary(Of UInteger, CanMsgRcvd)()

    ' Locking object for tool read param message collection.
    Private Shared ReadOnly ToolParamReadLock As New Object()

    ' Locking object for tool read param message collection.
    Private Shared ReadOnly ToolParamWriteLock As New Object()

    ' Collection of tool param ID read response messages.
    Private Shared ReadOnly ToolAppCalReadRespMsgs As New ConcurrentDictionary(Of UInteger, CanMsgRcvd)()

    ' Locking object for tool read param message collection.
    Private Shared ReadOnly ToolAppCalReadLock As New Object()

    ' Collection of DBL tool response messages.
    Private Shared ReadOnly DblToolRespMsgs As New ConcurrentDictionary(Of UInteger, CanMsgRcvd)()

    ' Locking object for tool response message collection.
    Private Shared ReadOnly DblToolRespLock As New Object()

    ' Locking object for status message collection.
    Private Shared ReadOnly StatusMsgLock As New Object()

    ' State value reported in status message.
    Private Shared _state As Byte = &HFF

    ' Average Diesel Particulate Filter Inlet temperature.
    Private Shared _inletTempAvg As Integer

    ' Average Diesel Particulate Filter Outlet temperature.
    Private Shared _outletTempAvg As Integer

    ' RF Sensor microcontroller device ID number.
    Private Shared _serialNumber() As Byte

    ' Default boot loader software version.
    Private Shared _dblSoftwareVersion() As Byte

    ' RF Sensor 8 highest priority status codes in priority order.
    Private Shared _statusCodes() As Byte

    ' Calibration data points queue.
    Private Shared _sweepData As New ConcurrentQueue(Of SweepDataPoint)

    ' Calibration sweep data readings.
    Private Shared _sweepStartData As New SweepStartData
    Private Shared _sweepEndData   As New SweepEndData
    Private Shared _sweepStepData  As New SweepStepData
    Private Shared _sweepCmdData   As New SweepCmdData

    ' Current calibration data point.
    Private Shared _sweepDataPnt As SweepDataPoint

    ' True after sweep data points have been queued by the rcvd data processor. 
    Private Shared _dataPointsQueued As Boolean

    ' Multi-cast delegate invoked when an RF Sensor state change occurs.
    Public Delegate Sub StateChangeDelegate(ByVal state As AppState)
    Private Shared Event StateChangeEvent As StateChangeDelegate

    ' Delegate for event raised when a all of the Cal Data Readings for a given measurement have been received.
    Public Delegate Sub SweepEndingValuesRcvdDelegate(ByVal endData As SweepEndData,
                                                      ByVal cmdData As SweepCmdData)
    Private Shared Event SweepEndingValuesRcvdEvent As SweepEndingValuesRcvdDelegate

    ' Delegate for event raised when the first resaponse message  for a given measurement have been received.
    Public Delegate Sub SweepStartingValuesRcvdDelegate(ByVal startData As SweepStartData,
                                                        ByVal stepData As SweepStepData)
    Private Shared Event SweepStartingValuesRcvdEvent As SweepStartingValuesRcvdDelegate

    ' Multi-cast delegate invoked when the Sweep Step Count value is changing.
    Public Delegate Sub StepCntChangedDelegate(ByVal steps As UInteger)
    Public Shared Event StepCntChangedEvent As StepCntChangedDelegate

    ' Multi-cast delegate invoked when the RF Unit state changes from a
    ' state in which is is sweeping to the normal or stop state.
    Public Delegate Sub SweepEndDelegate()
    Private Shared Event SweepEndEvent As SweepEndDelegate

    ' Delegate used to call the function that loads the CAN message output queue.
    Private Delegate Sub LoadCanMsgQueueDelegate(ByVal canMsg As TPCANMsg)
    Private Shared LoadCanMsgQueue As LoadCanMsgQueueDelegate

    ' Multi-cast delegate invoked when the PM1 message is received.
    Public Delegate Sub Pm1RcvdDelegate(ByVal data() As Byte)
    Private Shared Event Pm1RcvdEvent As Pm1RcvdDelegate

    ' Multi-cast delegate invoked when the PM2 message is received.
    Public Delegate Sub Pm2RcvdDelegate(ByVal data() As Byte)
    Private Shared Event Pm2RcvdEvent As Pm2RcvdDelegate

    ' Multi-cast delegate invoked when the PM3 message is received.
    Public Delegate Sub Pm3RcvdDelegate(ByVal data() As Byte)
    Private Shared Event Pm3RcvdEvent As Pm3RcvdDelegate

    ' Multi-cast delegate invoked when an Info message is received.
    Public Delegate Sub InfoRcvdMsgDelegate(ByVal inlet As Double, ByVal outlet As Double)
    Private Shared Event InfoRcvdMsgEvent As InfoRcvdMsgDelegate

    'Tool Function Message Received Multi-cast Delegate.
    Public Delegate Sub ToolFuncMsgRcvdDelegate(ByVal msgOpCode As UShort)
    Private Shared Event ToolFuncMsgRcvdRvent As ToolFuncMsgRcvdDelegate

    'Tool Param Read Message Response Received Multi-cast Delegate.
    Public Delegate Sub ParamReadMsgRcvdDelegate(ByVal msgID As UInteger)
    Public Shared Event ParamReadMsgRcvdEvent As ParamReadMsgRcvdDelegate
    Private Shared _paramReadMsgReceived As [Delegate]

    'Tool Param Read Message Responsed  Received Multi-cast Delegate.
    Public Delegate Sub AppCalRespMsgRcvdDelegate(ByVal msgID As UInteger)
    Public Shared Event AppCalRespMsgRcvdEvent As AppCalRespMsgRcvdDelegate

    'DBL Response Message Received Multi-cast Delegate.
    Public Delegate Sub DblToolResponseMsgRcvdDelegate(ByVal opCode As Byte)
    Public Shared Event DblToolResponseMsgRcvdEvent As DblToolResponseMsgRcvdDelegate

    ' Muti-cast delegate invoked when new Sweep data is available.
    Public Delegate Sub SweepActiveValuesDelegate(ByVal dataPnts As ConcurrentQueue(Of SweepDataPoint))
    Private Shared Event SweepActiveValueEvent As SweepActiveValuesDelegate

    ' Muti-cast delegate invoked when new Sweep index is available.
    Public Delegate Sub SweepIndexChangedDelegate(ByVal index As Integer)
    Private Shared Event SweepIndexChangedEvent As SweepIndexChangedDelegate

    ' Muti-cast delegate invoked to update the sweep time.
    Public Delegate Sub UpdateSweepTimeDelegate()
    Private Shared Event UpdateSweepTimeEvent As UpdateSweepTimeDelegate

    ' CAN Node ID Changed Event delegate.
    Public Delegate Sub CanNodeIdEventDelegate(ByVal nodeID As Byte)
    Private Shared Event CanNodeIdEvent As CanNodeIdEventDelegate

    ' Initialize configurable message priorities and PGNs.
    Private Shared _periodicMsg1Priority As UInteger   = DefaultPm1MsgPriority
    Private Shared _periodicMsg1PGN As UInteger        = DefaultPm1MsgPGN
    Private Shared _periodicMsg2Priority As UInteger   = DefaultPm2MsgPriority
    Private Shared _periodicMsg2PGN As UInteger        = DefaultPm2MsgPGN
    Private Shared _periodicMsg3Priority As UInteger   = DefaultPm3MsgPriority
    Private Shared _periodicMsg3PGN As UInteger        = DefaultPm3MsgPGN
    Private Shared _toolMsgRespPriority As UInteger    = DefaultToolRespMsgPriority
    Private Shared _toolMsgRespPGN As UInteger         = DefaultToolRespMsgPGN
    Private Shared _calDataReadMsgPriority As UInteger = DefaultCalDataReadMsgPriority
    Private Shared _calDataReadMsgPGN As UInteger      = DefaultCalDataReadMsgPGN
    Private Shared _dblToolRespMsgPriority As UInteger = DefaultDblToolRespMsgPriority
    Private Shared _dblToolRespMsgPGN As UInteger      = DefaultDblToolRespMsgPGN

    ' Event to raise when there is an error to be logged.
    Private Shared Event LogErrorEvent As ActivityLog.LogErrorStackDelegate


#End Region ' fields

    Shared Sub New()

        ' Initialize configurable message ID values.
        AppCanNodeID       = DefaultCanNodeID
        DblCanNodeID       = DefaultDblCanNodeID
        CommandMsgID       = DefaultCommandMsgID
        ToolCmdMsgID       = DefaultToolCmdMsgID
        PgnRequestMsgID    = DfltPgnRequestMsgID
        DiagnosticMsg13ID  = DfltDiagnosticMsg13ID
        InfoMessageID      = DefaultInfoMessageID
        DblToolCmdID       = DefaultDblToolCmdMsgID
        MonitorForActivityMsgID = PeriodicMsg1ID

        ' Intialize private byte arrays.
        _serialNumber      = New Byte() {&HFF, &HFF, &HFF, &HFF}

        ' Initialize the load CAN message output queue delegate.
        LoadCanMsgQueue = AddressOf CAN_Services.GetInstance().LoadCANMsgOutputQueue

        AppStateDict    = [Enum].GetValues(GetType(AppState)).Cast(Of AppState)() _
                              .ToDictionary(Function(t) t, Function(t) [Enum].GetName(GetType(AppState), t))

        ' Add error logging delegate.
        AddHandler LogErrorEvent, AddressOf ActivityLog.LogError
    End Sub

#Region "delegates"

    Public Shared Sub AddStepCntChangedEventHandler(ByVal del As StepCntChangedDelegate)
        AddHandler StepCntChangedEvent, del
    End Sub

    Public Shared Sub RemoveStepCntChangedEventHandler(del As StepCntChangedDelegate)
        AddHandler StepCntChangedEvent, del
    End Sub

    Public Shared Sub AddSweepEndEventHandler(ByVal del As SweepEndDelegate)
        AddHandler SweepEndEvent, del
    End Sub

    Public Shared Sub RemoveSweepEndEventHandler(ByVal del As SweepEndDelegate)
        RemoveHandler SweepEndEvent, del
    End Sub

    Public Shared Sub AddStateChangeEventHandler(ByVal del As StateChangeDelegate)
        AddHandler StateChangeEvent, del 
    End Sub

    Public Shared Sub RemoveStateChangeEventHandler(ByVal del As StateChangeDelegate)
        RemoveHandler StateChangeEvent, del 
    End Sub

    Public Shared Sub AddSweepEndingDataReceivedEventHandler(ByVal del As SweepEndingValuesRcvdDelegate)
        AddHandler SweepEndingValuesRcvdEvent, del
    End Sub

    Public Shared Sub RemoveSweepEndingDataReceivedEventHandler(ByVal del As SweepEndingValuesRcvdDelegate)
        RemoveHandler SweepEndingValuesRcvdEvent, del
    End Sub

    Public Shared Sub AddSweepStartingValuesRcvdHandler(ByVal del As SweepStartingValuesRcvdDelegate)
        AddHandler SweepStartingValuesRcvdEvent, del
    End Sub

    Public Shared Sub RemoveSweepStartingValuesRcvdHandler(ByVal del As SweepStartingValuesRcvdDelegate)
        RemoveHandler SweepStartingValuesRcvdEvent, del
    End Sub

    'SweepStartingValuesRcvdEvent

    Public Shared Sub AddSweepActiveDataAvailableEventHandler (ByVal del As SweepActiveValuesDelegate)
        AddHandler SweepActiveValueEvent, del
    End Sub

    Public Shared Sub RemoveSweepActiveDataAvailableEventHandler(ByVal del As SweepActiveValuesDelegate)
        RemoveHandler SweepActiveValueEvent, del
    End Sub

    Public Shared Sub AddSweepIndexChangedEventHandler(ByVal del As SweepIndexChangedDelegate)
        AddHandler SweepIndexChangedEvent, del
    End Sub

    Public Shared Sub RemoveSweepIndexChangedEventHandler(ByVal del As SweepIndexChangedDelegate)
        RemoveHandler SweepIndexChangedEvent, del
    End Sub

    Public Shared Sub AddUpdateSweepTimeEventHandler(ByVal del As UpdateSweepTimeDelegate)
        AddHandler UpdateSweepTimeEvent, del
    End Sub

    Public Shared Sub RemoveUpdateSweepTimeEventHandler(ByVal del As UpdateSweepTimeDelegate)
        RemoveHandler UpdateSweepTimeEvent, del
    End Sub

    Public Shared Sub AddPm1ReceivedEventHandler(ByVal del As Pm1RcvdDelegate)
        AddHandler Pm1RcvdEvent, del
    End Sub

    Public Shared Sub RemovePm1ReceivedEventHandler(ByVal del As Pm1RcvdDelegate)
        RemoveHandler Pm1RcvdEvent, del
    End Sub

    Public Shared Sub AddPm2ReceivedEventHandler(ByVal del As Pm2RcvdDelegate)
        AddHandler Pm2RcvdEvent, del
    End Sub

    Public Shared Sub RemovePm2ReceivedEventHandler(ByVal del As Pm2RcvdDelegate)
        RemoveHandler Pm2RcvdEvent, del
    End Sub

    Public Shared Sub AddPm3ReceivedEventHandler(ByVal del As Pm3RcvdDelegate)
        AddHandler Pm3RcvdEvent, del
    End Sub

    Public Shared Sub RemovePm3ReceivedDelegate(ByVal del As Pm3RcvdDelegate)
        RemoveHandler Pm3RcvdEvent, del
    End Sub

    Public Shared Sub AddInfoMsgReceivedEventHandler(ByVal del As InfoRcvdMsgDelegate)
        AddHandler InfoRcvdMsgEvent, del
    End Sub

    Public Shared Sub RemoveInfoMsgReceivedEventHandler(ByVal del As InfoRcvdMsgDelegate)
        RemoveHandler InfoRcvdMsgEvent, del
    End Sub

    Public Shared Sub AddToolFuncMsgRcvdEventHandler(ByVal del As ToolFuncMsgRcvdDelegate)
        AddHandler ToolFuncMsgRcvdRvent, del
    End Sub

    Public Shared Sub RemoveToolFuncMsgRcvdEventHandler(ByVal del As ToolFuncMsgRcvdDelegate)
        RemoveHandler ToolFuncMsgRcvdRvent, del
    End Sub

    Public Shared Sub AddParamReadMsgReceivedEventHandler(ByVal del As ParamReadMsgRcvdDelegate)
        AddHandler ParamReadMsgRcvdEvent, del
    End Sub

    Public Shared Sub RemoveParamReadMsgReceivedEventHandler(ByVal del As ParamReadMsgRcvdDelegate)
        RemoveHandler ParamReadMsgRcvdEvent, del
    End Sub

    Public Shared Sub AddAppCalRespMsgRcvdDelegate(ByVal del As AppCalRespMsgRcvdDelegate)
        AddHandler AppCalRespMsgRcvdEvent, del
    End Sub

    Public Shared Sub RemoveAppCalRespMsgRcvdDelegate(ByVal del As AppCalRespMsgRcvdDelegate)
        RemoveHandler AppCalRespMsgRcvdEvent, del
    End Sub

    Public Shared Sub AddDblToolRespMsgRcvdEventHandler(ByVal del As DblToolResponseMsgRcvdDelegate)
        AddHandler DblToolResponseMsgRcvdEvent, del
    End Sub

    Public Shared Sub RemoveDblToolRespMsgRcvdEventHandler(ByVal del As DblToolResponseMsgRcvdDelegate)
        RemoveHandler DblToolResponseMsgRcvdEvent, del
    End Sub

    Public Shared Sub AddCanNodeIdEventHandler(ByVal del As CanNodeIdEventDelegate)
        AddHandler CanNodeIdEvent, del
    End Sub

    Public Shared Sub RemoveCanNodeIdEventHandler(ByVal del As CanNodeIdEventDelegate)
        RemoveHandler CanNodeIdEvent, del
    End Sub

#End Region ' delegates

#Region "properties"

    ' Op Code of the last Tool Function message sent.
    Public Shared Property LastToolFuncOpCode() As ToolFunctionMsg

    ' Updated when a Tool Function response message indicates that the
    ' received message was Invalid.
    Public Shared Property ToolFunctionReturnCode() As Byte

    ' Auto property used to access the configured CAN node ID.
    Public Shared Property AppCanNodeID() As Byte

    ' Auto property used to access the configured DBL CAN node ID.
    Public Shared Property DblCanNodeID() As Byte

    ' Properties used to access configured message IDs. ===========================================================

    ' Expecting raw value. ex: 0x6 not 0x18.
    Public Shared Property PeriodicMsg1Priority() As Byte
        Get
            Return CByte(_periodicMsg1Priority >> 26)
        End Get
        Set(ByVal value As Byte)
            _periodicMsg1Priority = CUInt(value) << 26
        End Set
    End Property

    Public Shared Property PeriodicMsg1PGN() As UShort
        Get
            Return CUShort(_periodicMsg1PGN >> 8)
        End Get
        Set(ByVal value As UShort)
            _periodicMsg1PGN = CUInt(value) << 8
        End Set
    End Property

    Public Shared ReadOnly Property PeriodicMsg1ID() As UInteger
        Get
            Return _periodicMsg1Priority + _periodicMsg1PGN + AppCanNodeID
        End Get
    End Property

    ' Expecting raw value. ex: 0x6 not 0x18.
    Public Shared Property PeriodicMsg2Priority() As Byte
        Get
            Return CByte(_periodicMsg2Priority >> 26)
        End Get
        Set(ByVal value As Byte)
            _periodicMsg2Priority = CUInt(value) << 26
        End Set
    End Property

    Public Shared Property PeriodicMsg2PGN() As UShort
        Get
            Return CUShort(_periodicMsg2PGN >> 8)
        End Get
        Set(ByVal value As UShort)
            _periodicMsg2PGN = CUInt(value) << 8
        End Set
    End Property

    Public Shared ReadOnly Property PeriodicMsg2ID() As UInteger
        Get
            Return _periodicMsg2Priority + _periodicMsg2PGN + AppCanNodeID
        End Get
    End Property

    ' Expecting raw value. ex: 0x6 not 0x18.
    Public Shared Property PeriodicMsg3Priority() As UInteger
        Get
            Return CByte(_periodicMsg3Priority >> 26)
        End Get
        Set(ByVal value As UInteger)
            _periodicMsg3Priority = CUInt(value) << 26
        End Set
    End Property

    Public Shared Property PeriodicMsg3PGN() As UInteger
        Get
            Return CUShort(_periodicMsg3PGN >> 8)
        End Get
        Set(ByVal value As UInteger)
            _periodicMsg3PGN = CUInt(value) << 8
        End Set
    End Property

    Public Shared ReadOnly Property PeriodicMsg3ID() As UInteger
        Get
            Return _periodicMsg3Priority + _periodicMsg3PGN + AppCanNodeID
        End Get
    End Property

    Public Shared Property CommandMsgID() As UInteger

    Public Shared Property ToolCmdMsgID() As UInteger

    ' Expecting raw value. ex: 0x6 not 0x18.
    Public Shared Property ToolMsgRespPriority() As Byte
        Get
            Return CByte(_toolMsgRespPriority >> 26)
        End Get
        Set(ByVal value As Byte)
            _toolMsgRespPriority = CUInt(value) << 26
        End Set
    End Property

    Public Shared Property ToolMsgRespPGN() As UShort
        Get
            Return CUShort(_toolMsgRespPGN >> 8)
        End Get
        Set(ByVal value As UShort)
            _toolMsgRespPGN = CUInt(value) << 8
        End Set
    End Property

    Public Shared ReadOnly Property ToolMsgRespID() As UInteger
        Get
            ' Excludes message priority.
            Return _toolMsgRespPGN + AppCanNodeID
        End Get
    End Property

    Public Shared Property DblToolCmdID() As UInteger

    ' Expecting raw value. ex: 0x6 not 0x18.
    Public Shared Property DblToolMsgRespPriority() As Byte
        Get
            Return CByte(_dblToolRespMsgPriority >> 26)
        End Get
        Set(ByVal value As Byte)
            _dblToolRespMsgPriority = CUInt(value) << 26
        End Set
    End Property

    Public Shared Property DblToolMsgRespPGN() As UShort
        Get
            Return CUShort(_dblToolRespMsgPGN >> 8)
        End Get
        Set(ByVal value As UShort)
            _dblToolRespMsgPGN = CUInt(value) << 8
        End Set
    End Property

    Public Shared ReadOnly Property DblToolRespMsgID() As UInteger
        Get
            Return _dblToolRespMsgPriority + _dblToolRespMsgPGN + DblCanNodeID
        End Get
    End Property

    ' Expecting raw value. ex: 0x6 not 0x18.
    Public Shared Property CalDataReadMsgPriority() As Byte
        Get
            Return CByte(_calDataReadMsgPriority >> 26)
        End Get
        Set(ByVal value As Byte)
            _calDataReadMsgPriority = CUInt(value) << 26
        End Set
    End Property

    Public Shared Property CalDataReadMsgPGN() As UShort
        Get
            Return CByte(_calDataReadMsgPGN >> 8)
        End Get
        Set(ByVal value As UShort)
            _calDataReadMsgPGN = CUInt(value) << 8
        End Set
    End Property

    Public Shared ReadOnly Property CalDataReadMsgID() As UInteger
        Get
            Return _calDataReadMsgPriority + _calDataReadMsgPGN + AppCanNodeID
        End Get
    End Property

    Public Shared ReadOnly Property InitialMsgData() As Byte()
        Get
            Return New Byte() {&HFF, &HFF, &HFF, &HFF, &HFF, &HFF, &HFF, &HFF}
        End Get
    End Property

    Public Shared Property PgnRequestMsgID() As UInteger

    Public Shared Property DiagnosticMsg13ID() As UInteger

    Public Shared Property InfoMessageID() As UInteger

    Public Shared Property AppStateDict() As Dictionary(Of AppState, String)

    ' ID of CAN message to monitor for activity.
    Public Shared Property MonitorForActivityMsgID() As UInteger

    ' Updated when a DBL error response message is received.
    Public Shared Property DblErrorCode() As Byte

    Public Shared Property ClearRcvdState() As Boolean

    Public Shared Property ClearAppNodeID() As Boolean

#End Region ' properties

#Region "methods"

    Public Shared Function ToolFunctionIdValid(ByVal id As ToolFunctionMsg) As Boolean
        Return ToolFuncReqDict.ContainsKey(id)
    End Function

    ' Used to check if the last Tool Function Message sent has an anonymous
    ' response.
    Public Shared Function AnonymousResponse(ByVal id As ToolFunctionMsg) As Boolean
        Return id = ToolFunctionMsg.ReadSoftwareVersion OrElse
               id = ToolFunctionMsg.ReadAppSoftwareConfigs OrElse
               id = ToolFunctionMsg.ReadDeviceID OrElse
               id = ToolFunctionMsg.ReadSerialNumFromFlash
    End Function

    Public Shared Function GetDblMsgDataLength(ByVal opCode As DblRcvOpCodes, ByRef length As Byte) As Boolean
        Return DblRespDataLenDict.TryGetValue(opCode, length)
    End Function

#End Region ' methods
End Class
