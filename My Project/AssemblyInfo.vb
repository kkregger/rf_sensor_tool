﻿Imports System.Reflection
Imports System.Runtime.CompilerServices
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.
<Assembly: AssemblyTitle("CTS_RF_Sensor_Tool")>
<Assembly: AssemblyDescription("CTS RF Sensor Tool Application")>
<Assembly: AssemblyConfiguration("")>
<Assembly: AssemblyCompany("CTS Corporation")>
<Assembly: AssemblyProduct("CTS_RF_Sensor_Tool")>
<Assembly: AssemblyCopyright("Copyright ©  2016")>
<Assembly: AssemblyTrademark("")>
<Assembly: AssemblyCulture("")>

' Setting ComVisible to false makes the types in this assembly not visible 
' to COM components.  If you need to access a type in this assembly from 
' COM, set the ComVisible attribute to true on that type.
<Assembly: ComVisible(False)>

' The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("29468ce5-1270-4284-ac6e-c2d52f2757dc")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' [assembly: AssemblyVersion("1.0.*")]
<Assembly: AssemblyVersion("0.0.2.1")>
<Assembly: AssemblyFileVersion("0.0.2.1")>
