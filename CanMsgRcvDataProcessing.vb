﻿Imports System.Collections.Concurrent
Imports System.Threading.Tasks
Imports CTS_CAN_Services
Imports CTS_CAN_Services.Peak.Can.Basic

Partial Public NotInheritable Class CanMessageLib

#Region "methods"

    Public Shared Function GetOutputPowerNames() As List(Of String)

        ' Sort the output power names (numeric strings) by value.
        Dim enumVals = (
            From key In OutputPowerDict.Keys
            Select Integer.Parse(key)).ToList()
        enumVals.Sort()
        Dim nameList = (
            From eVal In enumVals
            Select Convert.ToString(eVal)).ToList()
        Return nameList
    End Function

    Public Shared Function GetOutputPower(ByVal powerStr As String, ByRef outputPower As OutputPowerLevel) As Boolean
        Return OutputPowerDict.TryGetValue(powerStr, outputPower)
    End Function

    Public Shared Function GetSweepData() As ConcurrentQueue(Of SweepDataPoint)
        Return _sweepData
    End Function

    Public Shared Function GetSweepStartData() As SweepStartData
        Return _sweepStartData
    End Function

    Public Shared Function GetSweepEndData() As SweepEndData
        Return _sweepEndData
    End Function

    Public Shared Function GetSweepStepData() As SweepStepData
        Return _sweepStepData
    End Function

    Public Shared Function GetSweepCmdData() As SweepCmdData
        Return _sweepCmdData
    End Function

    Public Shared Function GetSweepDataPoints() As ConcurrentQueue(Of SweepDataPoint)
        Return _sweepData
    End Function

    Public Shared Sub ProcessPM1Msg(ByVal rcvdMsgStruct As CAN_Services.RcvdCANMsg)

        Dim rcvdState = rcvdMsgStruct.CANMsg.DATA(0)

        ' Store the source address for this RF sensor.
        If (ClearAppNodeID) Then

            ' Application code wants to update the CAN node ID.
            AppCanNodeID = 0
            ClearAppNodeID = False
        Else
            If ((_state = AppState.Reset AndAlso rcvdState <> _state) OrElse
                (CByte(rcvdMsgStruct.CANMsg.ID And &HFF) <> AppCanNodeID)) Then

                ' Store the received CAN node ID if it changes
                AppCanNodeID = CByte(rcvdMsgStruct.CANMsg.ID And &HFF)

                ' Store a new message ID (same priority/PGN), which includes the
                ' changed node ID, to monitor for CAN activity.
                MonitorForActivityMsgID = PeriodicMsg1ID
                RaiseEvent CanNodeIdEvent(AppCanNodeID)
            End If
        End If

        ' Store one message for each responding RF sensor.
        SyncLock StatusMsgLock

            If StatusMsgs.ContainsKey(AppCanNodeID) Then

                Dim canMsg As TPCANMsg = Nothing

                ' Remove the message previously received from this RF sensor.
                StatusMsgs.TryRemove(AppCanNodeID, canMsg)
            End If
            ' Store the latest message from this CAN node.
            StatusMsgs.TryAdd(AppCanNodeID, rcvdMsgStruct.CANMsg)

        End SyncLock

        If Not [Enum].IsDefined(GetType(AppState), rcvdState) Then
            rcvdState = CByte(AppState.Undef) ' Invalid state value received;
        End If

        If (ClearRcvdState) Then

            ' Application code wants to update the stored sensor state.
            _state = AppState.Undef
            ClearRcvdState = False
        Else
            If (rcvdState <> _state) Then
                RaiseEvent StateChangeEvent(CType(rcvdState, AppState))
            End If
            _state = rcvdState
        End If
    End Sub

    Public Shared Sub ProcessPM2Msg(ByVal rcvdMsgStruct As CAN_Services.RcvdCANMsg)

        RaiseEvent Pm2RcvdEvent(rcvdMsgStruct.CANMsg.DATA)
    End Sub

    Public Shared Sub ProcessPM3Msg(ByVal rcvdMsgStruct As CAN_Services.RcvdCANMsg)

        RaiseEvent Pm3RcvdEvent(rcvdMsgStruct.CANMsg.DATA)
    End Sub

    Public Shared Sub ProcessInfoMsg(ByVal rcvdMsgStruct As CAN_Services.RcvdCANMsg)

        Static lastInletTemp  As Double
        Static lastOutletTemp As Double

        ' Extract the inlet and outlet temperatures (signed).
        Dim inletTemp  = CDbl(BitConverter.ToInt16(rcvdMsgStruct.CANMsg.DATA, 0)) / 10
        Dim outletTemp = CDbl(BitConverter.ToInt16(rcvdMsgStruct.CANMsg.DATA, 2)) / 10

        If (inletTemp <> lastInletTemp OrElse lastOutletTemp <> outletTemp) Then

            ' Raise the event only if there has been a temperature change.
            RaiseEvent InfoRcvdMsgEvent(inletTemp, outletTemp)
        End If
        lastInletTemp = inletTemp
        lastOutletTemp = outletTemp
    End Sub

    Public Shared Sub ProcessToolFuncMsg(ByVal rcvdMsgStruct As CAN_Services.RcvdCANMsg)

        Dim msgStruct As CanMsgRcvd = New CanMsgRcvd(New TPCANMsg, New CAN_Services.CAN_MsgTimestamp)
        Dim msgOpcode As UShort = rcvdMsgStruct.CANMsg.DATA(2)

        ' This maybe one of the Tool Function response
        ' messages that doesn't contain an op code.
        If AnonymousResponse(LastToolFuncOpCode) Then

            ' The last Tool Function message sent does cause an anonymous
            ' response. Assume this is the message we're looking for.
            msgOpcode = CUShort(LastToolFuncOpCode)
            LastToolFuncOpCode = ToolFunctionMsg.None
        End If
        If Not ToolFunctionIdValid(CType(msgOpcode, ToolFunctionMsg)) Then

            ' The Tool Function response message contains an invalid op code.
            Return
        End If
        SyncLock ToolFuncRespLock
            If ToolFuncResponseMsgs.ContainsKey(msgOpcode) Then

                ' Remove previous message.
                ToolFuncResponseMsgs.TryRemove(msgOpcode, msgStruct)
            End If

            ' Store latest message.
            msgStruct = New CanMsgRcvd(rcvdMsgStruct.CANMsg, rcvdMsgStruct.CANMsgTimeStamp)
            ToolFuncResponseMsgs.TryAdd(msgOpcode, msgStruct)
        End SyncLock

        ' Notify interested parties that a Tool Function Response message has been received.
        RaiseEvent ToolFuncMsgRcvdRvent(rcvdMsgStruct.CANMsg.DATA(2))
    End Sub

    Public Shared Sub ProcessParamRespMsg(ByVal rcvdMsgStruct As CAN_Services.RcvdCANMsg)

        Dim msgStruct As CanMsgRcvd = Nothing
        Dim key As UInteger = (CUInt(rcvdMsgStruct.CANMsg.DATA(0)) << 8) + rcvdMsgStruct.CANMsg.DATA(1)
        Dim wtf = False

        ' This a read/write Param message response.
        SyncLock ToolParamReadLock
            If ToolParamReadRespMsgs.ContainsKey(key) Then

                ' Remove previous message.
                ToolParamReadRespMsgs.TryRemove(key, msgStruct)
            End If

            ' Store latest message.
            msgStruct = New CanMsgRcvd(rcvdMsgStruct.CANMsg, rcvdMsgStruct.CANMsgTimeStamp)
            ToolParamReadRespMsgs.TryAdd(key, msgStruct)
        End SyncLock

        ' Notify interested parties that a Param Read Response message has been received.
        RaiseEvent ParamReadMsgRcvdEvent(key)
    End Sub

    Public Shared Sub ProcessAppCalRespMsg(ByVal rcvdMsgStruct As CAN_Services.RcvdCANMsg)

        Dim msgStruct As CanMsgRcvd = Nothing
        Dim key As UInteger = (CUInt(rcvdMsgStruct.CANMsg.DATA(0)) << 8) + rcvdMsgStruct.CANMsg.DATA(1)

        ' This an Application Cal read/write response.
        SyncLock ToolAppCalReadLock
            If ToolAppCalReadRespMsgs.ContainsKey(key) Then

                ' Remove previous message.
                ToolAppCalReadRespMsgs.TryRemove(key, msgStruct)
            End If

            ' Store latest message.
            msgStruct = New CanMsgRcvd(rcvdMsgStruct.CANMsg, rcvdMsgStruct.CANMsgTimeStamp)
            ToolAppCalReadRespMsgs.TryAdd(key, msgStruct)
        End SyncLock

        ' Notify interested parties that a Application Cal Read Response message has been received.
        RaiseEvent AppCalRespMsgRcvdEvent((CUInt(rcvdMsgStruct.CANMsg.DATA(0)) << 8) + rcvdMsgStruct.CANMsg.DATA(1))
    End Sub

    Public Shared Sub ProcessSweepDataReadingMsg(ByVal rcvdMsgStruct As CAN_Services.RcvdCANMsg)

        Dim    tblIndex       As UInteger
        Dim    rcvdBytes      As Byte() = New Byte(7){}
        Static seed           As Byte = InitialSeedValue
        Static dataPointIndex As UInteger

        If (FormRfSensorMain.WaitForLastSweepDataMsg) Then

            ' Retrigger the Sweep Data Message timer.
            FormRfSensorMain.RestartSweepDataMsgTimer()
        End If
        
        For idx = 0 To rcvdMsgStruct.CANMsg.DATA.Length - 1

            ' Recover encrypted data.
            rcvdBytes(idx) = rcvdMsgStruct.CANMsg.DATA(idx) Xor seed
            seed = rcvdMsgStruct.CANMsg.DATA(idx)
        Next

        ' Extract the Table Index.
        tblIndex  = rcvdBytes(byteTableIndexLsb)
        tblIndex += CUInt(rcvdBytes(byteTableIndexMsb)) << 8

        Select Case CType(tblIndex, CalDataIndex)

            Case CalDataIndex.Start

                ' Reinitialize cal measurement sweep metadata storage.
                _sweepStartData = New SweepStartData()
                _sweepEndData   = New SweepEndData()
                _sweepStepData  = New SweepStepData()
                _sweepCmdData   = New SweepCmdData()

                ' Record Calibration measurement sweep starting conditions.
                _sweepStartData.StartOutletTemp = rcvdBytes(byteStartingOutletTempLsb)                         ' signed
                _sweepStartData.StartOutletTemp += CInt(rcvdBytes(byteStartingOutletTempMsb)) << 8

                If ((_sweepStartData.StartOutletTemp And &H8000) > 0) Then

                    _sweepStartData.StartOutletTemp = (Not _sweepStartData.StartOutletTemp) + 1
                End If

                _sweepStartData.StartFrequency = rcvdBytes(byteStartingFrequencyLsb)                           ' kHz
                _sweepStartData.StartFrequency += CUInt(rcvdBytes(byteStartingFrequency2nd)) << 8

                _sweepStartData.StartInletTemp = rcvdBytes(byteStartingInletTempLsb)                           ' signed
                _sweepStartData.StartInletTemp += CInt(rcvdBytes(byteStartingInletTempMsb)) << 8

                If ((_sweepStartData.StartInletTemp And &H8000) > 0) Then

                    _sweepStartData.StartInletTemp = (Not _sweepStartData.StartInletTemp) + 1
                End If

            Case CalDataIndex.Step

                _sweepStartData.StartFrequency += CUInt(rcvdBytes(byteStartingFrequencyMsb)) << 16

                ' Record Calibration measurement sweep step conditions.
                _sweepStepData.StepSize  = rcvdBytes(byteStepSizeLsb)                                          ' KHz
                _sweepStepData.StepSize  += CUInt(rcvdBytes(byteStepSize2nd)) << 8
                _sweepStepData.StepSize  += CUInt(rcvdBytes(byteStepSizeMsb)) << 16
                _sweepStepData.ReadDelay = rcvdBytes(byteReadDelayLsb)
                _sweepStepData.ReadDelay += CUInt(rcvdBytes(byteReadDelayMsb)) << 8
                RaiseEvent SweepStartingValuesRcvdEvent(_sweepStartData, _sweepStepData)

            Case CalDataIndex.End

                ' Record calibration measurement sweep End values.
                _sweepEndData.EndOutletTemp = rcvdBytes(byteEndingOutletTempLsb)                               ' signed
                _sweepEndData.EndOutletTemp += CInt(rcvdBytes(byteEndingOutletTempMsb)) << 8

                If ((_sweepEndData.EndOutletTemp And &H8000) > 0) Then

                    _sweepEndData.EndOutletTemp = (Not _sweepEndData.EndOutletTemp) + 1
                End If

                _sweepEndData.EndFrequency = rcvdBytes(byteEndingFrequencyLsb)                                 ' KHz
                _sweepEndData.EndFrequency += CUInt(rcvdBytes(byteEndingFrequency2nd)) << 8

                _sweepEndData.EndInletTemp = rcvdBytes(byteEndingInletTempLsb)                                 ' signed
                _sweepEndData.EndInletTemp += CInt(rcvdBytes(byteEndingInletTempMsb)) << 8 

                If ((_sweepEndData.EndInletTemp And &H8000) > 0) Then

                    _sweepEndData.EndInletTemp = (Not _sweepEndData.EndInletTemp) + 1
                End If

            Case CalDataIndex.Cmd

                ' Record calibration measurement sweep end frequency MSB, data point count,
                ' operating mode, step delay and  board temp.
                _sweepEndData.EndFrequency += CUInt(rcvdBytes(byteEndingFrequencyMsb)) << 16

                _sweepCmdData.OperatingMode = rcvdBytes(byteOperatingMode)
                _sweepCmdData.StepDelay     = rcvdBytes(byteStepDelay)
                _sweepCmdData.Entries       = rcvdBytes(byteEntriesLsb)
                _sweepCmdData.Entries      += CUInt(rcvdBytes(byteEntriesMsb)) << 8
                _sweepCmdData.BoardTemp     = rcvdBytes(byteBoardTemp)                                         ' signed
                _sweepCmdData.BoardTemp    -= CShort(55)

                If (Not _dataPointsQueued) Then

                    ' Precaution, in case we don't receive all data points.
                    RaiseEvent SweepActiveValueEvent(_sweepData)
                    _dataPointsQueued = True
                End If

                ' All of the sweep measurement data has been received so raise
                ' the sweep ending values received event to notify interested parties.
                RaiseEvent SweepEndingValuesRcvdEvent(_sweepEndData, _sweepCmdData)

                ' Update the sweep steps complete count.
                RaiseEvent SweepIndexChangedEvent(dataPointIndex)

                ' Update the sweep time.
                RaiseEvent UpdateSweepTimeEvent()

                ' Re-initialize the decryption seed.
                seed = InitialSeedValue

            Case Else

                If (tblIndex >= CUInt(CalDataIndex.Min) AndAlso tblIndex <= CUInt(CalDataIndex.Max)) Then

                    ' This is a measurement sweep data point. Load and store one
                    ' instance of the sweep data point class for each one.
                    If (tblIndex = 0) Then

                        ' Re-initialize sweep measurement data point storage.
                        _sweepData = New ConcurrentQueue(Of SweepDataPoint)()
                        _dataPointsQueued = False
                    End If

                    _sweepDataPnt = New SweepDataPoint()
                    _sweepDataPnt.Index         = tblIndex
                    _sweepDataPnt.PhaseAtoD     = rcvdBytes(bytePhaseAtoDLsb)
                    _sweepDataPnt.PhaseAtoD     += CUInt(rcvdBytes(bytePhaseAtoDMsb)) << 8
                    _sweepDataPnt.MagnitudeAtoD = rcvdBytes(byteMagnitudeAtoDLsb)
                    _sweepDataPnt.MagnitudeAtoD += CUInt(rcvdBytes(byteMagnitudeAtoDMsb)) << 8
                    _sweepDataPnt.ScanNumber    = rcvdBytes(byteScanNumberLsb)
                    _sweepDataPnt.ScanNumber    += CUInt(rcvdBytes(byteScanNumberMsb)) << 8
                    _sweepData.Enqueue(_sweepDataPnt)

                    If (tblIndex >= FormRfSensorMain.CommandedStepCount) Then

                        ' Phase 1 step count is always 700.
                        RaiseEvent SweepActiveValueEvent(_sweepData)
                        _dataPointsQueued = True
                    End If
                    FormRfSensorMain.SweepDataIndex = tblIndex
                    dataPointIndex = tblIndex
                Else
                    RaiseEvent LogErrorEvent(New StackTrace(), "Invalid scan data table index received: " & CStr(tblIndex))
                End If
        End Select
    End Sub

    Public Shared Sub ProcessDblResponse(ByVal rcvdMsgStruct As CAN_Services.RcvdCANMsg)

        Dim msgStruct As CanMsgRcvd = Nothing
        Dim msgOpcode = rcvdMsgStruct.CANMsg.DATA(0)

        SyncLock DblToolRespLock
            Select Case CType(msgOpcode, DblRcvOpCodes)
                Case DblRcvOpCodes.DblAttnResp, DblRcvOpCodes.DblInitWriteAck, DblRcvOpCodes.DblMemWriteDataAck,
                     DblRcvOpCodes.DblInitReadAck, DblRcvOpCodes.DblMemReadData, DblRcvOpCodes.DblChksumResult,
                     DblRcvOpCodes.DblSerialNumberResp, DblRcvOpCodes.DblWriteSerNumAck, DblRcvOpCodes.DblActiveSessionClrd,
                     DblRcvOpCodes.DblCloseSessionAck, DblRcvOpCodes.DblStartAppAck, DblRcvOpCodes.DblRestartAck,
                     DblRcvOpCodes.DblFaultResp

                    ' This message contains a valid DBL Op Code value.
                    If DblToolRespMsgs.ContainsKey(msgOpcode) Then
                        ' Remove previous message.
                        DblToolRespMsgs.TryRemove(msgOpcode, msgStruct)
                    End If

                    ' Store latest message.
                    msgStruct = New CanMsgRcvd(rcvdMsgStruct.CANMsg, rcvdMsgStruct.CANMsgTimeStamp)
                    DblToolRespMsgs.TryAdd(msgOpcode, msgStruct)

                    ' Notify interested parties that a DBL Tool Response message has been received.
                    RaiseEvent DblToolResponseMsgRcvdEvent(rcvdMsgStruct.CANMsg.DATA(0))
            End Select
        End SyncLock
    End Sub

    Public Shared Function GetToolFunctionMsg(ByVal opCode As ToolFunctionMsg, ByRef toolFuncMsg As CanMsgRcvd) As Boolean
        SyncLock ToolFuncRespLock
            Return ToolFuncResponseMsgs.TryRemove(CUShort(opCode), toolFuncMsg)
        End SyncLock
    End Function

    Public Shared Function GetToolParamRespMsg(ByVal msgID As UInteger, ByRef toolParamReadMsg As CanMsgRcvd) As Boolean
        Dim result = False

        toolParamReadMsg = New CanMsgRcvd(New TPCANMsg(), New CAN_Services.CAN_MsgTimestamp())
        SyncLock ToolParamReadLock
            If ToolParamReadRespMsgs.ContainsKey(msgID) Then
                result = ToolParamReadRespMsgs.TryRemove(msgID, toolParamReadMsg)
            ElseIf ToolParamReadRespMsgs.ContainsKey(ToolParamMsgInvalid) Then
                result = ToolParamReadRespMsgs.TryRemove(ToolParamMsgInvalid, toolParamReadMsg)
            End If
        End SyncLock
        Return result
    End Function

    Public Shared Function GetToolAppCalRespMsg(ByVal msgID As UInteger, ByRef toolAppCalReadMsg As CanMsgRcvd) As Boolean
        Dim result = False

        toolAppCalReadMsg = New CanMsgRcvd(New TPCANMsg(), New CAN_Services.CAN_MsgTimestamp())
        SyncLock ToolAppCalReadLock
            If ToolAppCalReadRespMsgs.ContainsKey(msgID) Then
                result = ToolAppCalReadRespMsgs.TryRemove(msgID, toolAppCalReadMsg)
            ElseIf ToolAppCalReadRespMsgs.ContainsKey(ToolAppCalMsgInvalid) Then
                result = ToolAppCalReadRespMsgs.TryRemove(ToolAppCalMsgInvalid, toolAppCalReadMsg)
            End If
        End SyncLock
        Return result
    End Function

    Public Shared Function GetDblToolResponseMsg(ByVal opCode As Byte, ByRef dfltDblToolRespMsg As CanMsgRcvd) As Boolean
        SyncLock DblToolRespLock
            If DblToolRespMsgs.TryRemove(opCode, dfltDblToolRespMsg) Then
                Return True
            Else
                Return DblToolRespMsgs.TryRemove(CByte(DblRcvOpCodes.DblFaultResp), dfltDblToolRespMsg)
            End If
        End SyncLock
    End Function

    Private Shared Sub CalculateMovingAverage(ByVal x As Integer, ByVal cnt As Integer, ByRef avg As Integer)
        avg = (x + ((cnt - 1) * avg)) \ cnt
    End Sub

#End Region ' methods
End Class
