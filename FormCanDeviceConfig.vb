﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports System.Windows.Forms

Partial Public Class FormCanDeviceConfig
    Inherits Form

    Private _currentCanCommunication As CanCommunication = Nothing

    ' May be changed to Yes to indicate to the caller that CAN
    ' Communications configuration(s) have changed.
    Private _configChanged As DialogResult = DialogResult.No

    Public Sub New()
        InitializeComponent()
    End Sub

    Private Sub FormCAN_DeviceConfiguration_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

        ' Load Descriptions of all currently available CAN Communication configurations.
        ListBoxCanCommunications.Items.Clear()
        For Each kvpCanCom As KeyValuePair(Of String, CanCommunication) In FormRfSensorMain.CanCommunications
            ListBoxCanCommunications.Items.Add(kvpCanCom.Value.Description)
        Next kvpCanCom

        ' By default, display the first CAN Communication in the control.
        If ListBoxCanCommunications.Items.Count > 0 Then
            ListBoxCanCommunications.SelectedIndex = 0
            DisplaySelectedCanCommunication()
        Else
            ' No existing CAN Communication so display default values.
            ListBoxCanCommunications.SelectedItem = Nothing
            InitializeForm()
        End If
    End Sub

    Private Sub FormCAN_DeviceConfiguration_FormClosing(ByVal sender As Object, ByVal e As FormClosingEventArgs) Handles Me.FormClosing
        DialogResult = _configChanged
    End Sub

    Private Sub Add_CAN_ConfigButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Add_CAN_ConfigButton.Click

        _currentCanCommunication = New CanCommunication()

        ' Display default values.
        InitializeForm()
        PanelDataFields.Enabled = True
        CAN_ConfigDescriptionTextBox.Focus()
    End Sub

    Private Sub Edit_CAN_ConfigButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Edit_CAN_ConfigButton.Click
        If _currentCanCommunication IsNot Nothing Then
            PanelDataFields.Enabled = True
            CAN_ConfigDescriptionTextBox.Focus()
        Else
            MessageBox.Show("Please select a CAN Communication configuration first.", "")
        End If
    End Sub

    Private Sub Update_CAN_ConfigButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Update_CAN_ConfigButton.Click

        If PanelDataFields.Enabled = True Then
            If Not (String.IsNullOrWhiteSpace(CAN_ConfigDescriptionTextBox.Text)) AndAlso Not (CAN_ConfigDescriptionTextBox.Text = "Description") Then

                ' If a new CAN Communication configuration was created or an
                ' existing one was modified then save the current
                ' CAN Communication configuration.
                If (Not FormRfSensorMain.CanCommunications.ContainsKey(_currentCanCommunication.Description)) Then

                    ' This is new CAN Communication configuration so add
                    ' it to the CAN Communications dictionary.
                    If FormRfSensorMain.CanCommunications.TryAdd(_currentCanCommunication.Description, _currentCanCommunication) Then

                        ' Added successfully.
                        ' Load Descriptions of new CAN Communication configuration.
                        ListBoxCanCommunications.Items.Add(_currentCanCommunication.Description)
                        If (_currentCanCommunication.Active) AndAlso (FormRfSensorMain.ActiveCanDevice?.Description IsNot Nothing) Then

                            ' Allow only one active CAN Communication
                            ' configuration at a time.
                            FormRfSensorMain.CanCommunications(FormRfSensorMain.ActiveCanDevice.Description).Active = False
                        End If
                    Else
                        MessageBox.Show("A failure prevented the new CAN Communication" & vbCrLf & "configuration from being created." & vbCrLf & "Please try again or press Cancel.", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Return
                    End If
                Else
                    ' If a CAN Communication configuration was modified then replace the previous one.
                    FormRfSensorMain.CanCommunications(_currentCanCommunication.Description) = _currentCanCommunication
                End If

                ' Reload the active CAN configuration.
                FormRfSensorMain.LoadActiveCanCommunication()

                ' Indicate to the caller, on return, that the CAN
                ' Communication configuration(s) have changed.
                _configChanged = DialogResult.Yes
                ' Disable form controls.
                PanelDataFields.Enabled = False
                _currentCanCommunication = Nothing
            Else
                MessageBox.Show("You must enter a Description first." & vbCrLf & "Or press Cancel to discard changes.", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        End If
    End Sub

    Private Sub Remove_CAN_ConfigButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Remove_CAN_ConfigButton.Click
        Dim removed As New CanCommunication()

        If PanelDataFields.Enabled <> True Then
            If FormRfSensorMain.CanCommunications.ContainsKey(_currentCanCommunication.Description) Then
                If FormRfSensorMain.CanCommunications.TryRemove(_currentCanCommunication.Description, removed) Then
                    ListBoxCanCommunications.Items.RemoveAt(ListBoxCanCommunications.FindString(_currentCanCommunication.Description))
                    _currentCanCommunication = Nothing

                    ' Display the first CAN Communication configuration
                    ' in the control.
                    If ListBoxCanCommunications.Items.Count > 0 Then
                        ListBoxCanCommunications.SelectedIndex = 0
                        DisplaySelectedCanCommunication()
                    Else
                        ' No existing CAN Communication configuration
                        ' so display default values.
                        InitializeForm()
                    End If
                    If removed.Active = True Then

                        ' The removed CAN Communication configuration
                        ' was the active one.
                        FormRfSensorMain.LoadActiveCanCommunication()
                    End If

                    ' Indicate to the caller, on return, that the CAN
                    ' Communication configuration(s) have changed.
                    _configChanged = DialogResult.Yes
                Else
                    MessageBox.Show("A failure prevented the selected CAN Communication" & vbCrLf & "configuration from being removed." & vbCrLf & "Please try again.", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            Else
                MessageBox.Show("Please select a CAN Communication configuration first.", "")
            End If
        End If
    End Sub

    Private Sub CAN_ConfigCancelButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles CAN_ConfigCancelButton.Click

        If PanelDataFields.Enabled = True Then

            ' Disable controls.
            PanelDataFields.Enabled = False
            _currentCanCommunication = Nothing

            ' The current edit or add has been canceled, so
            ' display the selected CAN Communication configuration if
            ' there is one, otherwise display the first CAN Communication
            ' configuration in the control.
            If ListBoxCanCommunications.Items.Count > 0 Then
                If ListBoxCanCommunications.SelectedItem Is Nothing Then
                    ListBoxCanCommunications.SelectedIndex = 0
                End If
                DisplaySelectedCanCommunication()
            Else

                ' No existing CAN Communication configuration so display
                ' default values.
                ListBoxCanCommunications.SelectedItem = Nothing
                InitializeForm()
            End If
        End If
    End Sub

    Private Sub Close_CAN_ConfigButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Close_CAN_ConfigButton.Click
        Close()
    End Sub

    Private Sub ListBoxCommunications_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        If ListBoxCanCommunications.SelectedItem IsNot Nothing Then
            PanelDataFields.Enabled = False
            DisplaySelectedCanCommunication()
        End If
    End Sub

    Private Sub DisplaySelectedCanCommunication()
        If Visible = True Then

            ' This form is visible (not loading).
            For Each kvpCanCom As KeyValuePair(Of String, CanCommunication) In FormRfSensorMain.CanCommunications
                If kvpCanCom.Value.Description = DirectCast(ListBoxCanCommunications.SelectedItem, String) Then

                    ' Get a copy of the selected CAN Communication
                    ' configuration.
                    _currentCanCommunication = New CanCommunication(kvpCanCom.Value)

                    ' Display the selected CAN Communication configuration.
                    CAN_ConfigDescriptionTextBox.Text = kvpCanCom.Value.Description
                    CAN_Device_ComboBox.SelectedIndex = CAN_Device_ComboBox.FindString(kvpCanCom.Value.DeviceType)
                    PCAN_DeviceDesignationComboBox.SelectedIndex = PCAN_DeviceDesignationComboBox.FindString(kvpCanCom.Value.PcanDesignation)
                    NICAN_DeviceDesignationComboBox.SelectedIndex = NICAN_DeviceDesignationComboBox.FindString(kvpCanCom.Value.NicanDesignation)
                    CAN_BitRate_ComboBox.SelectedIndex = CAN_BitRate_ComboBox.FindString(kvpCanCom.Value.BitRate.ToString())
                    CAN_ActiveCheckBox.Checked = kvpCanCom.Value.Active
                    Exit For
                End If
            Next kvpCanCom
        End If
    End Sub

    Private Sub InitializeForm()

        ' Display default values.
        CAN_ConfigDescriptionTextBox.Text = "Description"
        CAN_Device_ComboBox.SelectedText = "PCAN USB"
        PCAN_DeviceDesignationComboBox.SelectedText = "PCAN_USB1"
        NICAN_DeviceDesignationComboBox.SelectedText = "CAN0"
        CAN_BitRate_ComboBox.SelectedText = "250000"
        CAN_ActiveCheckBox.Checked = False
    End Sub

    Private Sub CAN_ActiveCheckBox_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CAN_ActiveCheckBox.CheckedChanged
        _currentCanCommunication.Active = CAN_ActiveCheckBox.Checked
    End Sub

    Private Sub PCAN_DeviceDesignationComboBox_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles PCAN_DeviceDesignationComboBox.SelectedIndexChanged
        _currentCanCommunication.PcanDesignation = DirectCast(PCAN_DeviceDesignationComboBox.SelectedItem, String)
    End Sub

    Private Sub NICAN_DeviceDesignationComboBox_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles NICAN_DeviceDesignationComboBox.SelectedIndexChanged
        _currentCanCommunication.NicanDesignation = DirectCast(NICAN_DeviceDesignationComboBox.SelectedItem, String)
    End Sub

    Private Sub CAN_BitRate_ComboBox_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CAN_BitRate_ComboBox.SelectedIndexChanged
        _currentCanCommunication.BitRate = UInteger.Parse(DirectCast(CAN_BitRate_ComboBox.SelectedItem, String))
    End Sub

    Private Sub ComboBox_CAN_Device_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CAN_Device_ComboBox.SelectedIndexChanged
        _currentCanCommunication.DeviceType = DirectCast(CAN_Device_ComboBox.SelectedItem, String)
        If DirectCast(CAN_Device_ComboBox.SelectedItem, String) = "PCAN USB" Then
            PCAN_DeviceDesignationComboBox.BringToFront()
        Else
            NICAN_DeviceDesignationComboBox.BringToFront()
        End If
    End Sub

    Private Sub CAN_ConfigDescriptionTextBox_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CAN_ConfigDescriptionTextBox.TextChanged
        If (_currentCanCommunication IsNot Nothing) Then
            _currentCanCommunication.Description = CAN_ConfigDescriptionTextBox.Text
        End If
    End Sub

    Private Sub ListBoxCanCommunications_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ListBoxCanCommunications.SelectedIndexChanged
        If ListBoxCanCommunications.SelectedItem IsNot Nothing Then
            PanelDataFields.Enabled = False
            DisplaySelectedCanCommunication()
        End If
    End Sub
End Class
