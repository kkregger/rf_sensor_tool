﻿Partial Public Class FormFileEncryption
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormFileEncryption))
        Me.OpenFileDialogEncrypt = New System.Windows.Forms.OpenFileDialog()
        Me.FolderBrowserDialogEncrypt = New System.Windows.Forms.FolderBrowserDialog()
        Me.ButtonEncrypt = New System.Windows.Forms.Button()
        Me.ButtonDecrypt = New System.Windows.Forms.Button()
        Me.ToolTipEncryption = New System.Windows.Forms.ToolTip(Me.components)
        Me.SuspendLayout()
        '
        'OpenFileDialogEncrypt
        '
        Me.OpenFileDialogEncrypt.Multiselect = True
        '
        'ButtonEncrypt
        '
        Me.ButtonEncrypt.BackColor = System.Drawing.Color.Cyan
        Me.ButtonEncrypt.Location = New System.Drawing.Point(27, 18)
        Me.ButtonEncrypt.Name = "ButtonEncrypt"
        Me.ButtonEncrypt.Size = New System.Drawing.Size(75, 23)
        Me.ButtonEncrypt.TabIndex = 0
        Me.ButtonEncrypt.Text = "Encrypt"
        Me.ToolTipEncryption.SetToolTip(Me.ButtonEncrypt, "Select file(s) to Encrypt")
        Me.ButtonEncrypt.UseVisualStyleBackColor = False
        '
        'ButtonDecrypt
        '
        Me.ButtonDecrypt.BackColor = System.Drawing.Color.Cyan
        Me.ButtonDecrypt.Location = New System.Drawing.Point(147, 18)
        Me.ButtonDecrypt.Name = "ButtonDecrypt"
        Me.ButtonDecrypt.Size = New System.Drawing.Size(75, 23)
        Me.ButtonDecrypt.TabIndex = 1
        Me.ButtonDecrypt.Text = "Decrypt"
        Me.ToolTipEncryption.SetToolTip(Me.ButtonDecrypt, "Select file(s) to Decrypt")
        Me.ButtonDecrypt.UseVisualStyleBackColor = False
        '
        'FormFileEncryption
        '
        Me.BackColor = System.Drawing.Color.DimGray
        Me.ClientSize = New System.Drawing.Size(248, 53)
        Me.Controls.Add(Me.ButtonDecrypt)
        Me.Controls.Add(Me.ButtonEncrypt)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "FormFileEncryption"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "File Encryption"
        Me.TopMost = True
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents OpenFileDialogEncrypt As OpenFileDialog
    Friend WithEvents FolderBrowserDialogEncrypt As FolderBrowserDialog
    Friend WithEvents ButtonEncrypt As Button
    Friend WithEvents ButtonDecrypt As Button
    Friend WithEvents ToolTipEncryption As ToolTip
End Class
