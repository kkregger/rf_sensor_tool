﻿Partial Public Class FormCanDeviceConfig

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim CAN_DeviceTypeLabel As System.Windows.Forms.Label
        Dim CAN_BitRateLabel As System.Windows.Forms.Label
        Dim CAN_DeviceDesinationLabel As System.Windows.Forms.Label
        Dim resources As New System.ComponentModel.ComponentResourceManager(GetType(FormCanDeviceConfig))
        Me.Edit_CAN_ConfigButton = New System.Windows.Forms.Button()
        Me.ActionPpanel = New System.Windows.Forms.Panel()
        Me.CAN_ConfigCancelButton = New System.Windows.Forms.Button()
        Me.Update_CAN_ConfigButton = New System.Windows.Forms.Button()
        Me.Add_CAN_ConfigButton = New System.Windows.Forms.Button()
        Me.Remove_CAN_ConfigButton = New System.Windows.Forms.Button()
        Me.Close_CAN_ConfigButton = New System.Windows.Forms.Button()
        Me.CAN_ConfigDescriptionTextBox = New System.Windows.Forms.TextBox()
        Me.CAN_ConfigDescriptionLabel = New System.Windows.Forms.Label()
        Me.ListBoxCanCommunications = New System.Windows.Forms.ListBox()
        Me.CAN_CommunicationListBoxLabel = New System.Windows.Forms.Label()
        Me.panel1 = New System.Windows.Forms.Panel()
        Me.CAN_BitRate_ComboBox = New System.Windows.Forms.ComboBox()
        Me.CAN_Device_ComboBox = New System.Windows.Forms.ComboBox()
        Me.PanelDataFields = New System.Windows.Forms.Panel()
        Me.CAN_ActiveCheckBox = New System.Windows.Forms.CheckBox()
        Me.PCAN_DeviceDesignationComboBox = New System.Windows.Forms.ComboBox()
        Me.NICAN_DeviceDesignationComboBox = New System.Windows.Forms.ComboBox()
        Me.toolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        CAN_DeviceTypeLabel = New System.Windows.Forms.Label()
        CAN_BitRateLabel = New System.Windows.Forms.Label()
        CAN_DeviceDesinationLabel = New System.Windows.Forms.Label()
        Me.ActionPpanel.SuspendLayout()
        Me.panel1.SuspendLayout()
        Me.PanelDataFields.SuspendLayout()
        Me.SuspendLayout()
        ' 
        ' CAN_DeviceTypeLabel
        ' 
        CAN_DeviceTypeLabel.AutoSize = True
        CAN_DeviceTypeLabel.Location = New System.Drawing.Point(147, 45)
        CAN_DeviceTypeLabel.Name = "CAN_DeviceTypeLabel"
        CAN_DeviceTypeLabel.Size = New System.Drawing.Size(68, 13)
        CAN_DeviceTypeLabel.TabIndex = 1
        CAN_DeviceTypeLabel.Text = "Device Type"
        CAN_DeviceTypeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        ' 
        ' CAN_BitRateLabel
        ' 
        CAN_BitRateLabel.AutoSize = True
        CAN_BitRateLabel.Location = New System.Drawing.Point(157, 86)
        CAN_BitRateLabel.Name = "CAN_BitRateLabel"
        CAN_BitRateLabel.Size = New System.Drawing.Size(45, 13)
        CAN_BitRateLabel.TabIndex = 3
        CAN_BitRateLabel.Text = "Bit Rate"
        CAN_BitRateLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        ' 
        ' CAN_DeviceDesinationLabel
        ' 
        CAN_DeviceDesinationLabel.AutoSize = True
        CAN_DeviceDesinationLabel.Location = New System.Drawing.Point(264, 44)
        CAN_DeviceDesinationLabel.Name = "CAN_DeviceDesinationLabel"
        CAN_DeviceDesinationLabel.Size = New System.Drawing.Size(100, 13)
        CAN_DeviceDesinationLabel.TabIndex = 12
        CAN_DeviceDesinationLabel.Text = "Device Designation"
        CAN_DeviceDesinationLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        ' 
        ' Edit_CAN_ConfigButton
        ' 
        Me.Edit_CAN_ConfigButton.BackColor = System.Drawing.Color.Cyan
        Me.Edit_CAN_ConfigButton.ForeColor = System.Drawing.Color.Black
        Me.Edit_CAN_ConfigButton.Location = New System.Drawing.Point(92, 7)
        Me.Edit_CAN_ConfigButton.Name = "Edit_CAN_ConfigButton"
        Me.Edit_CAN_ConfigButton.Size = New System.Drawing.Size(75, 23)
        Me.Edit_CAN_ConfigButton.TabIndex = 1
        Me.Edit_CAN_ConfigButton.Text = "Edit"
        Me.Edit_CAN_ConfigButton.UseVisualStyleBackColor = False
        'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
        'ORIGINAL LINE: this.Edit_CAN_ConfigButton.Click += new System.EventHandler(this.Edit_CAN_ConfigButton_Click);
        ' 
        ' ActionPpanel
        ' 
        Me.ActionPpanel.BackColor = System.Drawing.Color.DimGray
        Me.ActionPpanel.Controls.Add(Me.CAN_ConfigCancelButton)
        Me.ActionPpanel.Controls.Add(Me.Edit_CAN_ConfigButton)
        Me.ActionPpanel.Controls.Add(Me.Update_CAN_ConfigButton)
        Me.ActionPpanel.Controls.Add(Me.Add_CAN_ConfigButton)
        Me.ActionPpanel.Controls.Add(Me.Remove_CAN_ConfigButton)
        Me.ActionPpanel.Controls.Add(Me.Close_CAN_ConfigButton)
        Me.ActionPpanel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.ActionPpanel.ForeColor = System.Drawing.Color.White
        Me.ActionPpanel.Location = New System.Drawing.Point(122, 129)
        Me.ActionPpanel.Name = "ActionPpanel"
        Me.ActionPpanel.Size = New System.Drawing.Size(259, 65)
        Me.ActionPpanel.TabIndex = 19
        ' 
        ' CAN_ConfigCancelButton
        ' 
        Me.CAN_ConfigCancelButton.BackColor = System.Drawing.Color.Cyan
        Me.CAN_ConfigCancelButton.ForeColor = System.Drawing.Color.Black
        Me.CAN_ConfigCancelButton.Location = New System.Drawing.Point(11, 36)
        Me.CAN_ConfigCancelButton.Name = "CAN_ConfigCancelButton"
        Me.CAN_ConfigCancelButton.Size = New System.Drawing.Size(75, 23)
        Me.CAN_ConfigCancelButton.TabIndex = 3
        Me.CAN_ConfigCancelButton.Text = "Cancel"
        Me.CAN_ConfigCancelButton.UseVisualStyleBackColor = False
        'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
        'ORIGINAL LINE: this.CAN_ConfigCancelButton.Click += new System.EventHandler(this.CAN_ConfigCancelButton_Click);
        ' 
        ' Update_CAN_ConfigButton
        ' 
        Me.Update_CAN_ConfigButton.BackColor = System.Drawing.Color.Cyan
        Me.Update_CAN_ConfigButton.ForeColor = System.Drawing.Color.Black
        Me.Update_CAN_ConfigButton.Location = New System.Drawing.Point(173, 7)
        Me.Update_CAN_ConfigButton.Name = "Update_CAN_ConfigButton"
        Me.Update_CAN_ConfigButton.Size = New System.Drawing.Size(75, 23)
        Me.Update_CAN_ConfigButton.TabIndex = 2
        Me.Update_CAN_ConfigButton.Text = "Update"
        Me.Update_CAN_ConfigButton.UseVisualStyleBackColor = False
        'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
        'ORIGINAL LINE: this.Update_CAN_ConfigButton.Click += new System.EventHandler(this.Update_CAN_ConfigButton_Click);
        ' 
        ' Add_CAN_ConfigButton
        ' 
        Me.Add_CAN_ConfigButton.BackColor = System.Drawing.Color.Cyan
        Me.Add_CAN_ConfigButton.ForeColor = System.Drawing.Color.Black
        Me.Add_CAN_ConfigButton.Location = New System.Drawing.Point(11, 7)
        Me.Add_CAN_ConfigButton.Name = "Add_CAN_ConfigButton"
        Me.Add_CAN_ConfigButton.Size = New System.Drawing.Size(75, 23)
        Me.Add_CAN_ConfigButton.TabIndex = 0
        Me.Add_CAN_ConfigButton.Text = "Add"
        Me.Add_CAN_ConfigButton.UseVisualStyleBackColor = False
        'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
        'ORIGINAL LINE: this.Add_CAN_ConfigButton.Click += new System.EventHandler(this.Add_CAN_ConfigButton_Click);
        ' 
        ' Remove_CAN_ConfigButton
        ' 
        Me.Remove_CAN_ConfigButton.BackColor = System.Drawing.Color.Cyan
        Me.Remove_CAN_ConfigButton.ForeColor = System.Drawing.Color.Black
        Me.Remove_CAN_ConfigButton.Location = New System.Drawing.Point(92, 36)
        Me.Remove_CAN_ConfigButton.Name = "Remove_CAN_ConfigButton"
        Me.Remove_CAN_ConfigButton.Size = New System.Drawing.Size(75, 23)
        Me.Remove_CAN_ConfigButton.TabIndex = 4
        Me.Remove_CAN_ConfigButton.Text = "Remove"
        Me.Remove_CAN_ConfigButton.UseVisualStyleBackColor = False
        'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
        'ORIGINAL LINE: this.Remove_CAN_ConfigButton.Click += new System.EventHandler(this.Remove_CAN_ConfigButton_Click);
        ' 
        ' Close_CAN_ConfigButton
        ' 
        Me.Close_CAN_ConfigButton.BackColor = System.Drawing.Color.Gold
        Me.Close_CAN_ConfigButton.ForeColor = System.Drawing.Color.Black
        Me.Close_CAN_ConfigButton.Location = New System.Drawing.Point(173, 36)
        Me.Close_CAN_ConfigButton.Name = "Close_CAN_ConfigButton"
        Me.Close_CAN_ConfigButton.Size = New System.Drawing.Size(75, 23)
        Me.Close_CAN_ConfigButton.TabIndex = 5
        Me.Close_CAN_ConfigButton.Text = "Close"
        Me.Close_CAN_ConfigButton.UseVisualStyleBackColor = False
        'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
        'ORIGINAL LINE: this.Close_CAN_ConfigButton.Click += new System.EventHandler(this.Close_CAN_ConfigButton_Click);
        ' 
        ' CAN_ConfigDescriptionTextBox
        ' 
        Me.CAN_ConfigDescriptionTextBox.Location = New System.Drawing.Point(140, 20)
        Me.CAN_ConfigDescriptionTextBox.Name = "CAN_ConfigDescriptionTextBox"
        Me.CAN_ConfigDescriptionTextBox.Size = New System.Drawing.Size(218, 20)
        Me.CAN_ConfigDescriptionTextBox.TabIndex = 0
        'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
        'ORIGINAL LINE: this.CAN_ConfigDescriptionTextBox.TextChanged += new System.EventHandler(this.CAN_ConfigDescriptionTextBox_TextChanged);
        ' 
        ' CAN_ConfigDescriptionLabel
        ' 
        Me.CAN_ConfigDescriptionLabel.AutoSize = True
        Me.CAN_ConfigDescriptionLabel.Location = New System.Drawing.Point(218, 5)
        Me.CAN_ConfigDescriptionLabel.Name = "CAN_ConfigDescriptionLabel"
        Me.CAN_ConfigDescriptionLabel.Size = New System.Drawing.Size(63, 13)
        Me.CAN_ConfigDescriptionLabel.TabIndex = 10
        Me.CAN_ConfigDescriptionLabel.Text = "Description:"
        ' 
        ' ListBoxCanCommunications
        ' 
        Me.ListBoxCanCommunications.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ListBoxCanCommunications.FormattingEnabled = True
        Me.ListBoxCanCommunications.Location = New System.Drawing.Point(0, 17)
        Me.ListBoxCanCommunications.Name = "ListBoxCanCommunications"
        Me.ListBoxCanCommunications.Size = New System.Drawing.Size(118, 173)
        Me.ListBoxCanCommunications.TabIndex = 0
        'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
        'ORIGINAL LINE: this.ListBoxCanCommunications.SelectedIndexChanged += new System.EventHandler(this.ListBoxCanCommunications_SelectedIndexChanged);
        ' 
        ' CAN_CommunicationListBoxLabel
        ' 
        Me.CAN_CommunicationListBoxLabel.BackColor = System.Drawing.Color.DimGray
        Me.CAN_CommunicationListBoxLabel.Dock = System.Windows.Forms.DockStyle.Top
        Me.CAN_CommunicationListBoxLabel.ForeColor = System.Drawing.Color.White
        Me.CAN_CommunicationListBoxLabel.Location = New System.Drawing.Point(0, 0)
        Me.CAN_CommunicationListBoxLabel.Name = "CAN_CommunicationListBoxLabel"
        Me.CAN_CommunicationListBoxLabel.Size = New System.Drawing.Size(118, 17)
        Me.CAN_CommunicationListBoxLabel.TabIndex = 0
        Me.CAN_CommunicationListBoxLabel.Text = "CAN Communications"
        Me.CAN_CommunicationListBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        ' 
        ' panel1
        ' 
        Me.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.panel1.Controls.Add(Me.ListBoxCanCommunications)
        Me.panel1.Controls.Add(Me.CAN_CommunicationListBoxLabel)
        Me.panel1.Dock = System.Windows.Forms.DockStyle.Left
        Me.panel1.Location = New System.Drawing.Point(0, 0)
        Me.panel1.Name = "panel1"
        Me.panel1.Size = New System.Drawing.Size(122, 194)
        Me.panel1.TabIndex = 17
        ' 
        ' CAN_BitRate_ComboBox
        ' 
        Me.CAN_BitRate_ComboBox.BackColor = System.Drawing.Color.White
        Me.CAN_BitRate_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CAN_BitRate_ComboBox.FormattingEnabled = True
        Me.CAN_BitRate_ComboBox.Items.AddRange(New Object() {"250000", "500000"})
        Me.CAN_BitRate_ComboBox.Location = New System.Drawing.Point(140, 101)
        Me.CAN_BitRate_ComboBox.Name = "CAN_BitRate_ComboBox"
        Me.CAN_BitRate_ComboBox.Size = New System.Drawing.Size(80, 21)
        Me.CAN_BitRate_ComboBox.TabIndex = 3
        'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
        'ORIGINAL LINE: this.CAN_BitRate_ComboBox.SelectedIndexChanged += new System.EventHandler(this.CAN_BitRate_ComboBox_SelectedIndexChanged);
        ' 
        ' CAN_Device_ComboBox
        ' 
        Me.CAN_Device_ComboBox.FormattingEnabled = True
        Me.CAN_Device_ComboBox.Items.AddRange(New Object() {"PCAN USB", "NICAN USB"})
        Me.CAN_Device_ComboBox.Location = New System.Drawing.Point(141, 60)
        Me.CAN_Device_ComboBox.Name = "CAN_Device_ComboBox"
        Me.CAN_Device_ComboBox.Size = New System.Drawing.Size(80, 21)
        Me.CAN_Device_ComboBox.TabIndex = 1
        'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
        'ORIGINAL LINE: this.CAN_Device_ComboBox.SelectedIndexChanged += new System.EventHandler(this.ComboBox_CAN_Device_SelectedIndexChanged);
        ' 
        ' PanelDataFields
        ' 
        Me.PanelDataFields.BackColor = System.Drawing.Color.DimGray
        Me.PanelDataFields.Controls.Add(Me.CAN_ActiveCheckBox)
        Me.PanelDataFields.Controls.Add(CAN_DeviceDesinationLabel)
        Me.PanelDataFields.Controls.Add(Me.CAN_ConfigDescriptionTextBox)
        Me.PanelDataFields.Controls.Add(Me.CAN_ConfigDescriptionLabel)
        Me.PanelDataFields.Controls.Add(Me.CAN_BitRate_ComboBox)
        Me.PanelDataFields.Controls.Add(CAN_BitRateLabel)
        Me.PanelDataFields.Controls.Add(CAN_DeviceTypeLabel)
        Me.PanelDataFields.Controls.Add(Me.CAN_Device_ComboBox)
        Me.PanelDataFields.Controls.Add(Me.PCAN_DeviceDesignationComboBox)
        Me.PanelDataFields.Controls.Add(Me.NICAN_DeviceDesignationComboBox)
        Me.PanelDataFields.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelDataFields.Enabled = False
        Me.PanelDataFields.ForeColor = System.Drawing.Color.White
        Me.PanelDataFields.Location = New System.Drawing.Point(0, 0)
        Me.PanelDataFields.Name = "PanelDataFields"
        Me.PanelDataFields.Size = New System.Drawing.Size(381, 194)
        Me.PanelDataFields.TabIndex = 18
        ' 
        ' CAN_ActiveCheckBox
        ' 
        Me.CAN_ActiveCheckBox.AutoSize = True
        Me.CAN_ActiveCheckBox.Location = New System.Drawing.Point(302, 103)
        Me.CAN_ActiveCheckBox.Name = "CAN_ActiveCheckBox"
        Me.CAN_ActiveCheckBox.Size = New System.Drawing.Size(56, 17)
        Me.CAN_ActiveCheckBox.TabIndex = 14
        Me.CAN_ActiveCheckBox.Text = "Active"
        Me.CAN_ActiveCheckBox.UseVisualStyleBackColor = True
        'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
        'ORIGINAL LINE: this.CAN_ActiveCheckBox.CheckedChanged += new System.EventHandler(this.CAN_ActiveCheckBox_CheckedChanged);
        ' 
        ' PCAN_DeviceDesignationComboBox
        ' 
        Me.PCAN_DeviceDesignationComboBox.FormattingEnabled = True
        Me.PCAN_DeviceDesignationComboBox.Items.AddRange(New Object() {"PCAN_USB1", "PCAN_USB2", "PCAN_USB3", "PCAN_USB4", "PCAN_USB5", "PCAN_USB6", "PCAN_USB7", "PCAN_USB8"})
        Me.PCAN_DeviceDesignationComboBox.Location = New System.Drawing.Point(271, 59)
        Me.PCAN_DeviceDesignationComboBox.Name = "PCAN_DeviceDesignationComboBox"
        Me.PCAN_DeviceDesignationComboBox.Size = New System.Drawing.Size(87, 21)
        Me.PCAN_DeviceDesignationComboBox.TabIndex = 11
        'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
        'ORIGINAL LINE: this.PCAN_DeviceDesignationComboBox.SelectedIndexChanged += new System.EventHandler(this.PCAN_DeviceDesignationComboBox_SelectedIndexChanged);
        ' 
        ' NICAN_DeviceDesignationComboBox
        ' 
        Me.NICAN_DeviceDesignationComboBox.FormattingEnabled = True
        Me.NICAN_DeviceDesignationComboBox.Items.AddRange(New Object() {"CAN0", "CAN1", "CAN2", "CAN3", "CAN4", "CAN5", "CAN6", "CAN7", "CAN8", "CAN9", "CAN10", "CAN11", "CAN12", "CAN13", "CAN14", "CAN15", "CAN16", "CAN17", "CAN18", "CAN19", "CAN20", "CAN21", "CAN22", "CAN23", "CAN24", "CAN25", "CAN26", "CAN27", "CAN28", "CAN29", "CAN30", "CAN31", "CAN32", "CAN33", "CAN34", "CAN35", "CAN36", "CAN37", "CAN38", "CAN39", "CAN40", "CAN41", "CAN42", "CAN43", "CAN44", "CAN45", "CAN46", "CAN47", "CAN48", "CAN49", "CAN50", "CAN51", "CAN52", "CAN53", "CAN54", "CAN55", "CAN56", "CAN57", "CAN58", "CAN59", "CAN60", "CAN61", "CAN62", "CAN63", "CAN64", "CAN65", "CAN66", "CAN67", "CAN68", "CAN69", "CAN70", "CAN71", "CAN72", "CAN73", "CAN74", "CAN75", "CAN76", "CAN77", "CAN78", "CAN79", "CAN80", "CAN81", "CAN82", "CAN83", "CAN84", "CAN85", "CAN86", "CAN87", "CAN88", "CAN89", "CAN90", "CAN91", "CAN92", "CAN93", "CAN94", "CAN95", "CAN96", "CAN97", "CAN98", "CAN99", "CAN100", "CAN101", "CAN102", "CAN103", "CAN104", "CAN105", "CAN106", "CAN107", "CAN108", "CAN109", "CAN110", "CAN111", "CAN112", "CAN113", "CAN114", "CAN115", "CAN116", "CAN117", "CAN118", "CAN119", "CAN120", "CAN121", "CAN122", "CAN123", "CAN124", "CAN125", "CAN126", "CAN127", "CAN128", "CAN129", "CAN130", "CAN131", "CAN132", "CAN133", "CAN134", "CAN135", "CAN136", "CAN137", "CAN138", "CAN139", "CAN140", "CAN141", "CAN142", "CAN143", "CAN144", "CAN145", "CAN146", "CAN147", "CAN148", "CAN149", "CAN150", "CAN151", "CAN152", "CAN153", "CAN154", "CAN155", "CAN156", "CAN157", "CAN158", "CAN159", "CAN160", "CAN161", "CAN162", "CAN163", "CAN164", "CAN165", "CAN166", "CAN167", "CAN168", "CAN169", "CAN170", "CAN171", "CAN172", "CAN173", "CAN174", "CAN175", "CAN176", "CAN177", "CAN178", "CAN179", "CAN180", "CAN181", "CAN182", "CAN183", "CAN184", "CAN185", "CAN186", "CAN187", "CAN188", "CAN189", "CAN190", "CAN191", "CAN192", "CAN193", "CAN194", "CAN195", "CAN196", "CAN197", "CAN198", "CAN199", "CAN200", "CAN201", "CAN202", "CAN203", "CAN204", "CAN205", "CAN206", "CAN207", "CAN208", "CAN209", "CAN210", "CAN211", "CAN212", "CAN213", "CAN214", "CAN215", "CAN216", "CAN217", "CAN218", "CAN219", "CAN220", "CAN221", "CAN222", "CAN223", "CAN224", "CAN225", "CAN226", "CAN227", "CAN228", "CAN229", "CAN230", "CAN231", "CAN232", "CAN233", "CAN234", "CAN235", "CAN236", "CAN237", "CAN238", "CAN239", "CAN240", "CAN241", "CAN242", "CAN243", "CAN244", "CAN245", "CAN246", "CAN247", "CAN248", "CAN249", "CAN250", "CAN251", "CAN252", "CAN253", "CAN254"})
        Me.NICAN_DeviceDesignationComboBox.Location = New System.Drawing.Point(271, 59)
        Me.NICAN_DeviceDesignationComboBox.Name = "NICAN_DeviceDesignationComboBox"
        Me.NICAN_DeviceDesignationComboBox.Size = New System.Drawing.Size(87, 21)
        Me.NICAN_DeviceDesignationComboBox.TabIndex = 13
        'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
        'ORIGINAL LINE: this.NICAN_DeviceDesignationComboBox.SelectedIndexChanged += new System.EventHandler(this.NICAN_DeviceDesignationComboBox_SelectedIndexChanged);
        ' 
        ' FormCAN_DeviceConfiguration
        ' 
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0F, 13.0F)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(381, 194)
        Me.Controls.Add(Me.ActionPpanel)
        Me.Controls.Add(Me.panel1)
        Me.Controls.Add(Me.PanelDataFields)
        Me.Icon = (CType(resources.GetObject("$this.Icon"), System.Drawing.Icon))
        Me.MaximizeBox = False
        Me.Name = "FormCanDeviceConfiguration"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "CAN Device Selection & Configuration"
        'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
        'ORIGINAL LINE: this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormCAN_DeviceConfiguration_FormClosing);
        'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
        'ORIGINAL LINE: this.Load += new System.EventHandler(this.FormCAN_DeviceConfiguration_Load);
        Me.ActionPpanel.ResumeLayout(False)
        Me.panel1.ResumeLayout(False)
        Me.PanelDataFields.ResumeLayout(False)
        Me.PanelDataFields.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Private WithEvents Edit_CAN_ConfigButton As System.Windows.Forms.Button
    Private ActionPpanel As System.Windows.Forms.Panel
    Private WithEvents CAN_ConfigCancelButton As System.Windows.Forms.Button
    Private WithEvents Update_CAN_ConfigButton As System.Windows.Forms.Button
    Private WithEvents Add_CAN_ConfigButton As System.Windows.Forms.Button
    Private WithEvents Remove_CAN_ConfigButton As System.Windows.Forms.Button
    Private WithEvents Close_CAN_ConfigButton As System.Windows.Forms.Button
    Private WithEvents CAN_ConfigDescriptionTextBox As System.Windows.Forms.TextBox
    Private CAN_ConfigDescriptionLabel As System.Windows.Forms.Label
    Private WithEvents ListBoxCanCommunications As System.Windows.Forms.ListBox
    Private CAN_CommunicationListBoxLabel As System.Windows.Forms.Label
    Private panel1 As System.Windows.Forms.Panel
    Private WithEvents CAN_BitRate_ComboBox As System.Windows.Forms.ComboBox
    Private WithEvents CAN_Device_ComboBox As System.Windows.Forms.ComboBox
    Private PanelDataFields As System.Windows.Forms.Panel
    Private WithEvents PCAN_DeviceDesignationComboBox As System.Windows.Forms.ComboBox
    Private WithEvents NICAN_DeviceDesignationComboBox As System.Windows.Forms.ComboBox
    Private toolTip1 As System.Windows.Forms.ToolTip
    Private WithEvents CAN_ActiveCheckBox As System.Windows.Forms.CheckBox
End Class
