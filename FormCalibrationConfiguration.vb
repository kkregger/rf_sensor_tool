﻿Imports System
Imports System.Collections.Generic
Imports System.Globalization
Imports System.Linq
Imports System.Text
Imports System.Windows.Forms

Namespace CTS_RF_Sensor_Tool
    Partial Public Class FormCalibrationConfiguration
        Inherits Form

'        
'         0th Order:   f(x,y) = p00 
'
'         1st Order:   f(x,y) = 0th + p10*x + p01*y
'
'         2nd Order:   f(x,y) = 1st + p20*x^2 + p11*x*y + p02*y^2
'
'         3rd Order:   f(x,y) = 2nd + p30*x^3 + p21*x^2*y + p12*x*y^2 + p03*y^3
'         
'         4th Order:   f(x,y) = 3rd + p40*x^4 + p31*x^3*y + p22*x^2*y^2 + p13*x*y^3 + p04*y^4
'
'         5th Order:   f(x,y) = 4th + p50*x^5 + p41*x^4*y + p32*x^3*y^2 + p23*x2*y^3 + p14*x*y^4 + p05*y^5
'        

        Private _currentCalibration As Calibration
        ' Indicates the current add/edit state of this Form.
        Private _state As FormState = FormState.Ready
        ' May be changed to Yes to indicate to the caller that Measurement,
        ' and possibly RegionCalibration, configuration(s) have changed.
        Private _configChanged As DialogResult = DialogResult.No

        Public Sub New()
            InitializeComponent()
        End Sub

        Private Sub FormCalibrationConfiguration_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
            ' Load Descriptions of all currently available Calibration configurations.
            ListBoxCalibrations.Items.Clear()
            For Each kvpCal As KeyValuePair(Of Integer, Calibration) In FormRFSensorMain.Calibrations
                ListBoxCalibrations.Items.Add(kvpCal.Value.Description)
            Next kvpCal
            ' By default, display the first Calibration in the control.
            If ListBoxCalibrations.Items.Count > 0 Then
                ListBoxCalibrations.SelectedIndex = 0
                DisplaySelectedCalibration()
            Else
                ' No existing Calibrations so display default values.
                ListBoxCalibrations.SelectedItem = Nothing
                InitializeForm()
            End If
        End Sub

        Private Sub FormCalibrationConfiguration_FormClosing(ByVal sender As Object,
                                                             ByVal e As FormClosingEventArgs) Handles Me.FormClosing
            DialogResult = _configChanged
        End Sub

        Private Sub ConfigurationChangeEnable(ByVal enable As Boolean)
            ' Enabled/Disable controls based on whether were in the process
            ' of adding or changing Calibration configurations.
            ButtonAdd.Enabled = enable
            CopyThenEditButton.Enabled = enable
            ButtonEdit.Enabled = enable
            ButtonRemove.Enabled = enable
            ButtonUpdate.Enabled = Not enable
            ButtonCancel.Enabled = Not enable
            ListBoxCalibrations.Enabled = enable
        End Sub

        Private Sub DisableEdit()
            CopyThenEditButton.Enabled = False
            ButtonEdit.Enabled = False
        End Sub

        Private Sub DisplaySelectedCalibration()
            PanelDataFields.Enabled = False
            ' This form is visible (not loading).
            For Each kvpCal As KeyValuePair(Of Integer, Calibration) In
                FormRFSensorMain.Calibrations.Where(Function(kvp) kvp.Value.Description =
                DirectCast(ListBoxCalibrations.SelectedItem, String))

                ' Get a copy of the selected Calibration.
                _currentCalibration = New Calibration(kvpCal.Value)
                ' Display the selected Calibration.
                TextBoxDescription.Text = kvpCal.Value.Description
                NumericUpDownSootLoad.Value = CDec(kvpCal.Value.SootLoad)
                CheckBoxBinaryFunction.Checked = kvpCal.Value.Binary
                TextBoxCoefficents.Text = kvpCal.Value.Coefficients
                TextBoxFunction.Text = [Function](kvpCal.Value.Coefficients, kvpCal.Value.Binary)
                Exit For
            Next kvpCal
        End Sub

        Private Sub InitializeForm()
            ' Display default values.
            PanelDataFields.Enabled = False
            TextBoxDescription.Text = "Description"
            NumericUpDownSootLoad.Value = NumericUpDownSootLoad.Minimum
            CheckBoxBinaryFunction.Checked = False
            TextBoxCoefficents.Text = ""
            TextBoxFunction.Text = ""
        End Sub

        Private Shared Function [Function](ByVal source As String, ByVal binary As Boolean) As String
            Dim result = New StringBuilder()

            Dim coefficients = source.Split( { ","c }, StringSplitOptions.None)

            If binary Then
                Dim order = 0

                Dim coefficientsIndex = 0
                Do While coefficientsIndex < coefficients.Length
                    For tExponent = 0 To order
                        Dim xExponent = order - tExponent
                        If coefficientsIndex >= coefficients.Length Then
                            Exit For
                        End If

                        Dim coefficient As Single = Nothing
                        Dim tempVar As Boolean = String.IsNullOrWhiteSpace(coefficients(coefficientsIndex)) OrElse
                                                 Not Single.TryParse(coefficients(coefficientsIndex).Trim(), coefficient)
                        coefficientsIndex += 1
                        If tempVar Then
                            Continue For
                        End If
                        If coefficient > 0 AndAlso result.Length <> 0 Then
                            result.Append(" + ")
                        End If
                        ' Checking for 1 +/- 1 * 10^-9, rather than compare to
                        ' float literal.
                        If Not ((coefficient > 1.0 - 0.000000001) AndAlso
                                (coefficient < 1.0 + 0.000000001)) OrElse
                               coefficientsIndex = 1 Then
                            If xExponent > 0 Then
                                If xExponent = 1 Then
                                    result.Append("x")
                                Else
                                    result.Append("x^" & xExponent)
                                End If
                            End If
                        End If
                        If tExponent <= 0 Then
                            Continue For
                        End If
                        If tExponent = 1 Then
                            result.Append("t")
                        Else
                            result.Append("t^" & tExponent)
                        End If
                    Next tExponent
                    order += 1
                Loop
            Else
                For coefficientIndex = 0 To coefficients.Length - 1
                    Dim coefficient As Single = Nothing
                    If Not String.IsNullOrWhiteSpace(coefficients(coefficientIndex)) AndAlso
                       Single.TryParse(coefficients(coefficientIndex).Trim(), coefficient) Then
                        If coefficient > 0 AndAlso result.Length <> 0 Then
                            result.Append(" + ")
                        End If
                        ' Checking for 1 +/- 1 * 10^-9, rather than compare to
                        ' float literal.
                        If Not ((coefficient > 1.0 - .000000001) AndAlso (coefficient < 1.0 + .000000001)) OrElse coefficientIndex = 0 Then
                            result.Append(coefficient.ToString(CultureInfo.InvariantCulture))
                        End If
                        If coefficientIndex = 1 Then
                            result.Append("t")
                        ElseIf coefficientIndex <> 0 Then
                            result.Append("t^" & coefficientIndex)
                        End If
                    Else
                        Return "Error"
                    End If
                Next coefficientIndex
            End If
            Return result.ToString()
        End Function

        Private Sub ButtonCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonCancel.Click
            If PanelDataFields.Enabled Then
                ' Disable controls.
                PanelDataFields.Enabled = False
                _currentCalibration = Nothing
                ' Re-enable all change initiating controls.
                ConfigurationChangeEnable(True)
                ' The current edit or add has been canceled, so
                ' display the selected Calibration if there is one
                ' otherwise display the first Calibration in the control.
                If ListBoxCalibrations.Items.Count > 0 Then
                    If ListBoxCalibrations.SelectedItem Is Nothing Then
                        ListBoxCalibrations.SelectedIndex = 0
                    End If
                    DisplaySelectedCalibration()
                Else
                    ' No existing Calibrations so display default values.
                    ListBoxCalibrations.SelectedIndex = -1
                    ' Disable the edit buttons.
                    DisableEdit()
                    InitializeForm()
                End If
                _state = FormState.Ready
            End If
        End Sub

        Private Sub ButtonClose_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonClose.Click
            Close()
        End Sub

        Private Sub ButtonAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonAdd.Click
            Dim maxCalID As Integer = -1

            If _state <> FormState.Ready Then
                Select Case _state
                    Case FormState.CopyEditing
                        MessageBox.Show("Copy/Edit is in progress.", "Invalid Operation!")
                    Case FormState.Editing
                        MessageBox.Show("Edit is in progress.", "Invalid Operation!")
                End Select
                Return ' Operation invalid;
            End If
            _currentCalibration = New Calibration()
            ' Display default values.
            InitializeForm()
            For Each kvpCal As KeyValuePair(Of Integer, Calibration) In FormRFSensorMain.Calibrations
                ' Find the last Calibration ID.
                If (kvpCal.Value.CalibrationID IsNot Nothing) AndAlso (kvpCal.Value.CalibrationID > maxCalID) Then
                    maxCalID = CInt(kvpCal.Value.CalibrationID)
                End If
            Next kvpCal
            If maxCalID > 0 Then
                ' New Calibration ID = last Calibration ID + 1.
                _currentCalibration.CalibrationID = maxCalID + 1
            Else
                ' This is the first Calibration configuration so use the
                ' default Calibration ID value.
                _currentCalibration.CalibrationID = Common.DefaultID
            End If
            _state = FormState.Adding
            ' During an Add disable all change initiating controls.
            ConfigurationChangeEnable(False)
            InitializeForm()
            PanelDataFields.Enabled = True
            TextBoxDescription.Focus()
        End Sub

        Private Sub CopyThenEditButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles CopyThenEditButton.Click

            Dim lastCalID As Integer = -1

            If _state <> FormState.Ready Then
                Select Case _state
                    Case FormState.Adding
                        MessageBox.Show("Add is in progress.", "Invalid Operation!")
                    Case FormState.Editing
                        MessageBox.Show("Edit is in progress.", "Invalid Operation!")
                End Select
                Return ' Operation invalid;
            End If
            If _currentCalibration IsNot Nothing Then
                lastCalID = (
                    From calibration In FormRFSensorMain.Calibrations
                    Where calibration.Value.CalibrationID IsNot Nothing
                    Select CInt(calibration.Value.CalibrationID)).Concat({lastCalID}).Max()
                ' Assign a new Calibration ID value to a copy of the current
                ' Calibration configuration.
                If lastCalID <= 0 Then
                    MessageBox.Show("Copy/Edit operation failed." & vbcrlf & "No Calibration ID found.", "")
                    Return
                Else
                    _currentCalibration.CalibrationID = lastCalID + 1
                End If
                _state = FormState.CopyEditing
                ' Change the Calibration Description to the defualt value.
                TextBoxDescription.Text = "Description"
                _currentCalibration.Description = ""
                ' During an Copy/Edit disable all change initiating controls.
                ConfigurationChangeEnable(False)
                PanelDataFields.Enabled = True
                TextBoxDescription.Focus()
            Else
                MessageBox.Show("Please select a Calibration configuration first.", "")
            End If
        End Sub

        Private Sub ButtonEdit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonEdit.Click
            If _state <> FormState.Ready Then
                Select Case _state
                    Case FormState.Adding
                        MessageBox.Show("Add is in progress.", "Invalid Operation!")
                    Case FormState.CopyEditing
                        MessageBox.Show("Copy/Edit is in progress.", "Invalid Operation!")
                End Select
                Return ' Operation invalid;
            End If
            If _currentCalibration IsNot Nothing Then
                _state = FormState.Editing
                ' During an Edit disable all change initiating controls.
                ConfigurationChangeEnable(False)
                PanelDataFields.Enabled = True
                TextBoxDescription.Focus()
            Else
                MessageBox.Show("Please select a Calibration Configuration first.", "")
            End If
        End Sub

        Private Sub ButtonUpdate_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonUpdate.Click
            Dim removed As Calibration = Nothing

            If (PanelDataFields.Enabled <> True) OrElse (_state = FormState.Ready) Then
                Return ' Shouldn't be here.
            End If

            If (_currentCalibration?.CalibrationID) Is Nothing Then
                ' Shouldn't happen, get ready to try again.
                _state = FormState.Ready
                _currentCalibration = Nothing
                ' Re-enable all change initiating controls.
                ConfigurationChangeEnable(True)
                ' Display the first Calibration in the control.
                If ListBoxCalibrations.Items.Count > 0 Then
                    ListBoxCalibrations.SelectedIndex = 0
                    DisplaySelectedCalibration()
                Else
                    ' No existing Calibrations so display default values.
                    ListBoxCalibrations.SelectedIndex = -1
                    ' Disable the edit buttons.
                    DisableEdit()
                    InitializeForm()
                End If
                MessageBox.Show("A failure prevented the new Calibration" & vbcrlf &
                                "configuration from being created." & vbcrlf &
                                "Please try again.", "Error!",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Exclamation)
                Return
            End If
            If Not String.IsNullOrWhiteSpace(TextBoxDescription.Text) AndAlso
               TextBoxDescription.Text <> "Description" Then
                If _state <> FormState.Editing Then
                    If ListBoxCalibrations.Items.Cast(Of String)().Any(Function(t) _
                       String.Equals(_currentCalibration.Description, t, StringComparison.CurrentCultureIgnoreCase)) Then
                        MessageBox.Show("First enter a unique Description string." & vbcrlf &
                                        "The comparison is case insensitive.", "",
                                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Return
                    End If
                End If
                If _state = FormState.Adding Then
                    ' This is a new Calibration configuration.
                    ' Save the new Calibration configuration.
                    If Not FormRFSensorMain.Calibrations.TryAdd(CInt(_currentCalibration.CalibrationID), _currentCalibration) Then
                        MessageBox.Show("A failure prevented the new Calibration" & vbcrlf &
                                        "configuration from being created." & vbcrlf &
                                        "Please try again or press Cancel.", "Error!",
                                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Return
                    End If
                Else
                    For Each kvpCal In (
                        From calibration In FormRFSensorMain.Calibrations
                        Where _currentCalibration.CalibrationID.Equals(calibration.Value.CalibrationID)
                        Select calibration).Where(Function(kvp) kvp.Value.Description <> _currentCalibration.Description)
                        ' The Calibration Description has changed so remove
                        ' the previous Calibration configuration from the
                        ' Master Calibration configuration dictionary.
                        If FormRFSensorMain.Calibrations.TryRemove(kvpCal.Key, removed) Then
                            ListBoxCalibrations.Items.RemoveAt(ListBoxCalibrations.FindString(String.Copy(kvpCal.Value.Description)))
                        Else
                            MessageBox.Show("A failure prevented the edited Calibration" & vbcrlf &
                                            "configuration from being saved." & vbcrlf &
                                            "Please try again or press Cancel.", "Error!",
                                            MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            Return
                        End If
                        ' Now add the current Calibration configuration to the
                        ' Master Calibration configuration dictionary.
                        If FormRFSensorMain.Calibrations.TryAdd(CInt(_currentCalibration.CalibrationID),
                                                                New Calibration(_currentCalibration)) Then
                            ListBoxCalibrations.Items.Add(String.Copy(_currentCalibration.Description))
                            Exit For
                        End If
                        MessageBox.Show("Failure to update the Calibrations ListBox" & vbcrlf &
                                        "prevented the edited Calibration" & vbcrlf &
                                        "configuration from being saved." & vbcrlf &
                                        "Please try again or press Cancel.", "Error!",
                                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Return
                    Next kvpCal
                    If removed Is Nothing Then
                        If _state = FormState.Editing Then
                            ' This is an edited Calibration configuration,
                            ' so replace the previous in the Master Calibration
                            ' configuration dictionary.
                            FormRFSensorMain.Calibrations(CInt(_currentCalibration.CalibrationID)) = _currentCalibration
                        ElseIf _state = FormState.CopyEditing Then
                            ' This is a Calibration configuration that has
                            ' copied and edited. It is new, so add it to the
                            ' Master Calibration configuration dictionary.
                            If Not FormRFSensorMain.Calibrations.TryAdd(CInt(_currentCalibration.CalibrationID), _currentCalibration) Then
                                MessageBox.Show("A failure prevented the copied and edited" & vbcrlf &
                                                "Calibration configuration from being saved." & vbcrlf &
                                                "Please try again or press Cancel.", "Error!",
                                                MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                Return
                            End If
                        End If
                    End If
                End If
                If (_state = FormState.Adding) OrElse (_state = FormState.CopyEditing) Then
                    ListBoxCalibrations.Items.Add(String.Copy(_currentCalibration.Description))
                End If
                ListBoxCalibrations.SelectedIndex = ListBoxCalibrations.FindString(_currentCalibration.Description)
                ' Indicate to the caller, on return, that the Calibration
                ' configuration(s) have changed.
                _configChanged = System.Windows.Forms.DialogResult.Yes
                _currentCalibration = Nothing
                _state = FormState.Ready
                ' Re-initialize change initiating controls.
                ConfigurationChangeEnable(True)
                ' Display the currently selected Calibration configuration
                ' and reload _currentCalibration.
                DisplaySelectedCalibration()
                ' Disable form controls.
                PanelDataFields.Enabled = False
            Else
                MessageBox.Show("You must enter a Description first." & vbcrlf &
                                "Or press Cancel to discard changes.", "",
                                MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        End Sub

        Private Sub ButtonRemove_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonRemove.Click
            If _state <> FormState.Ready Then
                Select Case _state
                    Case FormState.Adding
                        MessageBox.Show("Add is in progress.", "Invalid Operation!")
                    Case FormState.CopyEditing
                        MessageBox.Show("Copy/Edit is in progress.", "Invalid Operation!")
                    Case FormState.Editing
                        MessageBox.Show("Edit is in progress.", "Invalid Operation!")
                End Select
                Return ' Operation invalid;
            End If
            If _currentCalibration Is Nothing Then
                MessageBox.Show("Please select a Measurement configuration first.", "")
                Return
            End If
            ' Null if _currentCalibration is null or if CalibrationID is null.
            ' Otherwise it should be valid.
            Dim calID? As Integer = _currentCalibration?.CalibrationID
            If (calID IsNot Nothing) AndAlso FormRFSensorMain.Calibrations.ContainsKey(CInt(calID)) Then
                Dim removed As Calibration = Nothing
                If FormRFSensorMain.Calibrations.TryRemove(CInt(calID), removed) Then
                    ' Disable controls.
                    PanelDataFields.Enabled = False
                    ' Remove the current Calibration from the Calibrations List Box.
                    ListBoxCalibrations.Items.RemoveAt(ListBoxCalibrations.FindString(_currentCalibration.Description))
                    _currentCalibration = Nothing
                    ' Display the first Calibration in the control.
                    If ListBoxCalibrations.Items.Count > 0 Then
                        ListBoxCalibrations.SelectedIndex = 0
                        DisplaySelectedCalibration()
                    Else
                        ' No existing Calibrations so display default values.
                        InitializeForm()
                    End If
                    ' Indicate to the caller, on return, that the Calibration
                    ' configuration(s) have changed.
                    _configChanged = System.Windows.Forms.DialogResult.Yes
                Else
                    MessageBox.Show("A failure prevented the selected Calibration from being removed." & vbcrlf &
                                    "Please try again.", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
                ' Re-enable all change initiating controls.
                ConfigurationChangeEnable(True)
                If ListBoxCalibrations.Items.Count = 0 Then
                    ' There are no more Calibration configurations, so disable
                    ' the edit buttons.
                    DisableEdit()
                End If
            Else
                ' It looks like an Add might have been in progress when the
                ' user pressed remove so discard it.
                ' Disable controls.
                PanelDataFields.Enabled = False
                _currentCalibration = Nothing
                ' Re-enable all change initiating controls.
                ConfigurationChangeEnable(True)
                ' Display the first Calibration in the control.
                If ListBoxCalibrations.Items.Count > 0 Then
                    ListBoxCalibrations.SelectedIndex = 0
                    DisplaySelectedCalibration()
                Else
                    ' No existing Calibrations so display default values.
                    ListBoxCalibrations.SelectedIndex = -1
                    ' Disable the edit buttons.
                    DisableEdit()
                    InitializeForm()
                End If
            End If
        End Sub

        Private Sub ListBoxCalibrations_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ListBoxCalibrations.SelectedIndexChanged
            If _state = FormState.Ready Then
                ConfigurationChangeEnable(True)
            End If
            DisplaySelectedCalibration()
        End Sub

        Private Sub TextBoxCoefficents_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TextBoxCoefficents.TextChanged
            If PanelDataFields.Enabled Then
                TextBoxFunction.Text = [Function](TextBoxCoefficents.Text, CheckBoxBinaryFunction.Checked)
            End If
        End Sub

        Private Sub CheckBoxBinaryFunction_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles CheckBoxBinaryFunction.CheckedChanged
            If PanelDataFields.Enabled Then
                TextBoxFunction.Text = [Function](TextBoxCoefficents.Text, CheckBoxBinaryFunction.Checked)
            End If
        End Sub

        Private Sub NumericUpDownSootLoad_ValueChanged(ByVal sender As Object, ByVal e As EventArgs) Handles NumericUpDownSootLoad.ValueChanged
            If (PanelDataFields.Enabled) AndAlso (_currentCalibration IsNot Nothing) Then
                _currentCalibration.SootLoad = CDbl(NumericUpDownSootLoad.Value)
            End If
        End Sub

        Private Sub TextBoxDescription_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TextBoxDescription.TextChanged
            If (PanelDataFields.Enabled) AndAlso (_currentCalibration IsNot Nothing) Then
                _currentCalibration.Description = TextBoxDescription.Text
            End If
        End Sub
    End Class
End Namespace