﻿Partial Public Class FormFlashDevice

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As New System.ComponentModel.ComponentResourceManager(GetType(FormFlashDevice))
        Me.OpenHexFileDialog = New System.Windows.Forms.OpenFileDialog()
        Me.SaveHexFileDialog = New System.Windows.Forms.SaveFileDialog()
        Me.OutputFolderBrowserDialog = New System.Windows.Forms.FolderBrowserDialog()
        Me.ButtonDownload = New System.Windows.Forms.Button()
        Me.ProgressBarUpload = New System.Windows.Forms.ProgressBar()
        Me.LabelInputFile = New System.Windows.Forms.Label()
        Me.TextBoxInputFile = New System.Windows.Forms.TextBox()
        Me.LabelOutputFile = New System.Windows.Forms.Label()
        Me.TextBoxOutputFile = New System.Windows.Forms.TextBox()
        Me.ButtonProgramApplication = New System.Windows.Forms.Button()
        Me.ButtonProgramCcbl = New System.Windows.Forms.Button()
        Me.ButtonProgramEeprom = New System.Windows.Forms.Button()
        Me.ButtonUpload = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        ' 
        ' OpenHexFileDialog
        ' 
        Me.OpenHexFileDialog.DefaultExt = "hex"
        Me.OpenHexFileDialog.Filter = "Hex|*.hex| All Files|*.*"
        Me.OpenHexFileDialog.RestoreDirectory = True
        Me.OpenHexFileDialog.Title = "Open Hex File"
        ' 
        ' SaveHexFileDialog
        ' 
        Me.SaveHexFileDialog.DefaultExt = "hex"
        Me.SaveHexFileDialog.Filter = "Hex|*.hex|All Files|*.*"
        Me.SaveHexFileDialog.Title = "Save Hex File"
        ' 
        ' OutputFolderBrowserDialog
        ' 
        Me.OutputFolderBrowserDialog.Description = "Download File(s) To:"
        ' 
        ' ButtonDownload
        ' 
        Me.ButtonDownload.Anchor = (CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
        Me.ButtonDownload.AutoSize = True
        Me.ButtonDownload.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ButtonDownload.Enabled = False
        Me.ButtonDownload.Location = New System.Drawing.Point(441, 141)
        Me.ButtonDownload.Name = "ButtonDownload"
        Me.ButtonDownload.Size = New System.Drawing.Size(65, 23)
        Me.ButtonDownload.TabIndex = 26
        Me.ButtonDownload.Text = "Download"
        Me.ButtonDownload.UseVisualStyleBackColor = True
        'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
        'ORIGINAL LINE: this.ButtonDownload.Click += new System.EventHandler(this.ButtonDownload_Click);
        ' 
        ' ProgressBarUpload
        ' 
        Me.ProgressBarUpload.Location = New System.Drawing.Point(14, 98)
        Me.ProgressBarUpload.Name = "ProgressBarUpload"
        Me.ProgressBarUpload.Size = New System.Drawing.Size(554, 23)
        Me.ProgressBarUpload.TabIndex = 18
        ' 
        ' LabelInputFile
        ' 
        Me.LabelInputFile.AutoSize = True
        Me.LabelInputFile.Location = New System.Drawing.Point(264, 48)
        Me.LabelInputFile.MinimumSize = New System.Drawing.Size(53, 13)
        Me.LabelInputFile.Name = "LabelInputFile"
        Me.LabelInputFile.Size = New System.Drawing.Size(53, 13)
        Me.LabelInputFile.TabIndex = 19
        Me.LabelInputFile.Text = "Input File:"
        Me.LabelInputFile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        ' 
        ' TextBoxInputFile
        ' 
        Me.TextBoxInputFile.Location = New System.Drawing.Point(14, 67)
        Me.TextBoxInputFile.Name = "TextBoxInputFile"
        Me.TextBoxInputFile.ReadOnly = True
        Me.TextBoxInputFile.Size = New System.Drawing.Size(554, 20)
        Me.TextBoxInputFile.TabIndex = 0
        Me.TextBoxInputFile.WordWrap = False
        ' 
        ' LabelOutputFile
        ' 
        Me.LabelOutputFile.AutoSize = True
        Me.LabelOutputFile.Location = New System.Drawing.Point(260, 190)
        Me.LabelOutputFile.MinimumSize = New System.Drawing.Size(61, 13)
        Me.LabelOutputFile.Name = "LabelOutputFile"
        Me.LabelOutputFile.Size = New System.Drawing.Size(61, 13)
        Me.LabelOutputFile.TabIndex = 24
        Me.LabelOutputFile.Text = "Output File:"
        Me.LabelOutputFile.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        ' 
        ' TextBoxOutputFile
        ' 
        Me.TextBoxOutputFile.Location = New System.Drawing.Point(14, 209)
        Me.TextBoxOutputFile.Name = "TextBoxOutputFile"
        Me.TextBoxOutputFile.ReadOnly = True
        Me.TextBoxOutputFile.Size = New System.Drawing.Size(554, 20)
        Me.TextBoxOutputFile.TabIndex = 8
        ' 
        ' ButtonProgramApplication
        ' 
        Me.ButtonProgramApplication.AutoSize = True
        Me.ButtonProgramApplication.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ButtonProgramApplication.Location = New System.Drawing.Point(14, 14)
        Me.ButtonProgramApplication.Name = "ButtonProgramApplication"
        Me.ButtonProgramApplication.Size = New System.Drawing.Size(69, 23)
        Me.ButtonProgramApplication.TabIndex = 5
        Me.ButtonProgramApplication.Text = "Application"
        Me.ButtonProgramApplication.UseVisualStyleBackColor = True
        'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
        'ORIGINAL LINE: this.ButtonProgramApplication.Click += new System.EventHandler(this.ButtonProgramApplication_Click);
        ' 
        ' ButtonProgramCcbl
        ' 
        Me.ButtonProgramCcbl.Anchor = (CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles))
        Me.ButtonProgramCcbl.AutoSize = True
        Me.ButtonProgramCcbl.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ButtonProgramCcbl.Location = New System.Drawing.Point(506, 14)
        Me.ButtonProgramCcbl.Name = "ButtonProgramCcbl"
        Me.ButtonProgramCcbl.Size = New System.Drawing.Size(62, 23)
        Me.ButtonProgramCcbl.TabIndex = 7
        Me.ButtonProgramCcbl.Text = "   CCBL   "
        Me.ButtonProgramCcbl.UseVisualStyleBackColor = True
        'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
        'ORIGINAL LINE: this.ButtonProgramCcbl.Click += new System.EventHandler(this.ButtonProgramCcbl_Click);
        ' 
        ' ButtonProgramEeprom
        ' 
        Me.ButtonProgramEeprom.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.ButtonProgramEeprom.AutoSize = True
        Me.ButtonProgramEeprom.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ButtonProgramEeprom.Location = New System.Drawing.Point(259, 14)
        Me.ButtonProgramEeprom.Name = "ButtonProgramEeprom"
        Me.ButtonProgramEeprom.Size = New System.Drawing.Size(63, 23)
        Me.ButtonProgramEeprom.TabIndex = 6
        Me.ButtonProgramEeprom.Text = "EEPROM"
        Me.ButtonProgramEeprom.UseVisualStyleBackColor = True
        'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
        'ORIGINAL LINE: this.ButtonProgramEeprom.Click += new System.EventHandler(this.ButtonProgramEeprom_Click);
        ' 
        ' ButtonUpload
        ' 
        Me.ButtonUpload.AutoSize = True
        Me.ButtonUpload.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ButtonUpload.Enabled = False
        Me.ButtonUpload.Location = New System.Drawing.Point(56, 141)
        Me.ButtonUpload.Name = "ButtonUpload"
        Me.ButtonUpload.Size = New System.Drawing.Size(51, 23)
        Me.ButtonUpload.TabIndex = 25
        Me.ButtonUpload.Text = "Upload"
        Me.ButtonUpload.UseVisualStyleBackColor = True
        'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
        'ORIGINAL LINE: this.ButtonUpload.Click += new System.EventHandler(this.ButtonUpload_Click);
        ' 
        ' FormFlashDevice
        ' 
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0F, 13.0F)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(580, 339)
        Me.Controls.Add(Me.ButtonDownload)
        Me.Controls.Add(Me.ButtonUpload)
        Me.Controls.Add(Me.ButtonProgramEeprom)
        Me.Controls.Add(Me.ButtonProgramCcbl)
        Me.Controls.Add(Me.ButtonProgramApplication)
        Me.Controls.Add(Me.TextBoxOutputFile)
        Me.Controls.Add(Me.LabelOutputFile)
        Me.Controls.Add(Me.TextBoxInputFile)
        Me.Controls.Add(Me.LabelInputFile)
        Me.Controls.Add(Me.ProgressBarUpload)
        Me.Icon = (CType(resources.GetObject("$this.Icon"), System.Drawing.Icon))
        Me.Name = "FormFlashDevice"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Flash Device"
        'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
        'ORIGINAL LINE: this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormFlashDevice_FormClosing);
        'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
        'ORIGINAL LINE: this.Load += new System.EventHandler(this.FormFlashDevice_Load);
        'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
        'ORIGINAL LINE: this.Shown += new System.EventHandler(this.FormFlashDevice_Shown);
        'INSTANT VB NOTE: The following InitializeComponent event wireup was converted to a 'Handles' clause:
        'ORIGINAL LINE: this.Resize += new System.EventHandler(this.FormFlashDevice_Resize);
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Private OpenHexFileDialog As System.Windows.Forms.OpenFileDialog
    Private SaveHexFileDialog As System.Windows.Forms.SaveFileDialog
    Private OutputFolderBrowserDialog As System.Windows.Forms.FolderBrowserDialog
    Private WithEvents ButtonDownload As System.Windows.Forms.Button
    Private ProgressBarUpload As System.Windows.Forms.ProgressBar
    Private LabelInputFile As System.Windows.Forms.Label
    Private TextBoxInputFile As System.Windows.Forms.TextBox
    Private LabelOutputFile As System.Windows.Forms.Label
    Private TextBoxOutputFile As System.Windows.Forms.TextBox
    Private WithEvents ButtonProgramApplication As System.Windows.Forms.Button
    Private WithEvents ButtonProgramCcbl As System.Windows.Forms.Button
    Private WithEvents ButtonProgramEeprom As System.Windows.Forms.Button
    Private WithEvents ButtonUpload As System.Windows.Forms.Button
End Class
