﻿Partial Public Class FormRfSensorMain

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ChartArea2 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Series2 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormRfSensorMain))
        Me.TextBoxTestName = New System.Windows.Forms.TextBox()
        Me.labelSweeping = New System.Windows.Forms.Label()
        Me.TextBoxDataOutputFolder = New System.Windows.Forms.TextBox()
        Me.TextBoxCurrentOutputFilename = New System.Windows.Forms.TextBox()
        Me.ProgressBarSweepProgress = New System.Windows.Forms.ProgressBar()
        Me.TextBoxRunCount = New System.Windows.Forms.TextBox()
        Me.label10 = New System.Windows.Forms.Label()
        Me.label11 = New System.Windows.Forms.Label()
        Me.PanelRFSensorDisplay = New System.Windows.Forms.Panel()
        Me.panel2 = New System.Windows.Forms.Panel()
        Me.PanelChartAdjustments = New System.Windows.Forms.Panel()
        Me.ButtonCompressPlot = New System.Windows.Forms.Button()
        Me.ButtonExpandPlot = New System.Windows.Forms.Button()
        Me.ButtonScrollPlotRight = New System.Windows.Forms.Button()
        Me.ButtonScrollPlotLeft = New System.Windows.Forms.Button()
        Me.ButtonClearPlot = New System.Windows.Forms.Button()
        Me.ChartSootLoad = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.panel3 = New System.Windows.Forms.Panel()
        Me.label30 = New System.Windows.Forms.Label()
        Me.TextBoxStatus = New System.Windows.Forms.TextBox()
        Me.label29 = New System.Windows.Forms.Label()
        Me.label26 = New System.Windows.Forms.Label()
        Me.PictureBoxTempLow = New System.Windows.Forms.PictureBox()
        Me.label25 = New System.Windows.Forms.Label()
        Me.PictureBoxTempHigh = New System.Windows.Forms.PictureBox()
        Me.labelBelowMinDetectable = New System.Windows.Forms.Label()
        Me.PictureBoxSootLow = New System.Windows.Forms.PictureBox()
        Me.labelAboveMaxDetectable = New System.Windows.Forms.Label()
        Me.PictureBoxSootHigh = New System.Windows.Forms.PictureBox()
        Me.label22 = New System.Windows.Forms.Label()
        Me.PictureBoxCalibrationOff = New System.Windows.Forms.PictureBox()
        Me.PanelFilterImage = New System.Windows.Forms.Panel()
        Me.label12 = New System.Windows.Forms.Label()
        Me.label21 = New System.Windows.Forms.Label()
        Me.label20 = New System.Windows.Forms.Label()
        Me.TextBoxOutletTemperature = New System.Windows.Forms.TextBox()
        Me.TextBoxInletTemperature = New System.Windows.Forms.TextBox()
        Me.PanelLoadBar = New System.Windows.Forms.Panel()
        Me.PanelLoadBarPercentage = New System.Windows.Forms.Panel()
        Me.label15 = New System.Windows.Forms.Label()
        Me.SecondaryMeasurementTextBox = New System.Windows.Forms.TextBox()
        Me.PrimaryMeasurementTextBox = New System.Windows.Forms.TextBox()
        Me.TextBoxSootLoad = New System.Windows.Forms.TextBox()
        Me.TextBoxSensorTemperature = New System.Windows.Forms.TextBox()
        Me.FolderBrowserDialogOutputDirectory = New System.Windows.Forms.FolderBrowserDialog()
        Me.TextBoxSweepDuration = New System.Windows.Forms.TextBox()
        Me.label28 = New System.Windows.Forms.Label()
        Me.toolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ButtonCanConfig = New System.Windows.Forms.Button()
        Me.ButtonCanEnable = New System.Windows.Forms.Button()
        Me.ButtonSelectOutputFolder = New System.Windows.Forms.Button()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.PictureBoxIdle = New System.Windows.Forms.PictureBox()
        Me.GroupBoxCanControls = New System.Windows.Forms.GroupBox()
        Me.LabelCanActivity = New System.Windows.Forms.Label()
        Me.PictureBoxCanActivity = New System.Windows.Forms.PictureBox()
        Me.LabelTestName = New System.Windows.Forms.Label()
        Me.LabelOutputFileFolder = New System.Windows.Forms.Label()
        Me.LabelOutputFileName = New System.Windows.Forms.Label()
        Me.LabelSweepProgress = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.LabelSweepTime = New System.Windows.Forms.Label()
        Me.LabelCtsWebAddress = New System.Windows.Forms.Label()
        Me.LabelCTSProprietary = New System.Windows.Forms.Label()
        Me.LabelCTSCopyright = New System.Windows.Forms.Label()
        Me.MenuStripRFSensorMain = New System.Windows.Forms.MenuStrip()
        Me.ToolStripMenuItemFile = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItemEncryption = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItemSweepSettings = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItemOperatingMode = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripComboBoxOperatingMode = New System.Windows.Forms.ToolStripComboBox()
        Me.ToolStripMenuItemOutputPower = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripComboBoxOutputPower = New System.Windows.Forms.ToolStripComboBox()
        Me.ToolStripMenuItemOutputPowerAux = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripComboBoxOutputPowerAux = New System.Windows.Forms.ToolStripComboBox()
        Me.ToolStripMenuItemAtoDReadingsForAvg = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripTextBoxAtoDReadingsForAvgValue = New System.Windows.Forms.ToolStripTextBox()
        Me.ToolStripMenuItemLockDelay = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripTextBoxLockDelayValue = New System.Windows.Forms.ToolStripTextBox()
        Me.ToolStripMenuItemMinDetectable = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripTextBoxMinDetectableValue = New System.Windows.Forms.ToolStripTextBox()
        Me.ToolStripMenuItemMaxDetectable = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripTextBoxMaxDetectableValue = New System.Windows.Forms.ToolStripTextBox()
        Me.ToolStripMenuItemSweepInterval = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripTextBoxSweepIntervalValue = New System.Windows.Forms.ToolStripTextBox()
        Me.ToolStripMenuItemStepDelay = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripTextBoxStepDelayValue = New System.Windows.Forms.ToolStripTextBox()
        Me.ToolStripMenuItemSweepRate = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripTextBoxSweepRateValue = New System.Windows.Forms.ToolStripTextBox()
        Me.ToolStripMenuItemDisableCalibration = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItemConfigure = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItemMeasurements = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItemRegionsOfInterest = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItemAccess = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItemCustomer = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItemAppEngineer = New System.Windows.Forms.ToolStripMenuItem()
        Me.PanelLogo = New System.Windows.Forms.Panel()
        Me.ButtonStartStopScan = New System.Windows.Forms.Button()
        Me.PictureBoxSweeping = New System.Windows.Forms.PictureBox()
        Me.LabelStatus = New System.Windows.Forms.Label()
        Me.TimerUpdateProgressBar = New System.Windows.Forms.Timer(Me.components)
        Me.LabelSelect = New System.Windows.Forms.Label()
        Me.ButtonWaitSweep = New System.Windows.Forms.Button()
        Me.PanelRFSensorDisplay.SuspendLayout
        Me.panel2.SuspendLayout
        Me.PanelChartAdjustments.SuspendLayout
        CType(Me.ChartSootLoad,System.ComponentModel.ISupportInitialize).BeginInit
        Me.panel3.SuspendLayout
        CType(Me.PictureBoxTempLow,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.PictureBoxTempHigh,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.PictureBoxSootLow,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.PictureBoxSootHigh,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.PictureBoxCalibrationOff,System.ComponentModel.ISupportInitialize).BeginInit
        Me.PanelFilterImage.SuspendLayout
        Me.PanelLoadBar.SuspendLayout
        CType(Me.PictureBoxIdle,System.ComponentModel.ISupportInitialize).BeginInit
        Me.GroupBoxCanControls.SuspendLayout
        CType(Me.PictureBoxCanActivity,System.ComponentModel.ISupportInitialize).BeginInit
        Me.MenuStripRFSensorMain.SuspendLayout
        CType(Me.PictureBoxSweeping,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SuspendLayout
        '
        'TextBoxTestName
        '
        Me.TextBoxTestName.Location = New System.Drawing.Point(322, 141)
        Me.TextBoxTestName.Name = "TextBoxTestName"
        Me.TextBoxTestName.Size = New System.Drawing.Size(268, 20)
        Me.TextBoxTestName.TabIndex = 1
        '
        'labelSweeping
        '
        Me.labelSweeping.AutoSize = true
        Me.labelSweeping.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.labelSweeping.ForeColor = System.Drawing.Color.White
        Me.labelSweeping.Location = New System.Drawing.Point(86, 56)
        Me.labelSweeping.Name = "labelSweeping"
        Me.labelSweeping.Size = New System.Drawing.Size(54, 13)
        Me.labelSweeping.TabIndex = 3
        Me.labelSweeping.Text = "Sweeping"
        '
        'TextBoxDataOutputFolder
        '
        Me.TextBoxDataOutputFolder.BackColor = System.Drawing.Color.White
        Me.TextBoxDataOutputFolder.Location = New System.Drawing.Point(47, 141)
        Me.TextBoxDataOutputFolder.Name = "TextBoxDataOutputFolder"
        Me.TextBoxDataOutputFolder.ReadOnly = true
        Me.TextBoxDataOutputFolder.Size = New System.Drawing.Size(268, 20)
        Me.TextBoxDataOutputFolder.TabIndex = 5
        '
        'TextBoxCurrentOutputFilename
        '
        Me.TextBoxCurrentOutputFilename.BackColor = System.Drawing.Color.White
        Me.TextBoxCurrentOutputFilename.Location = New System.Drawing.Point(597, 141)
        Me.TextBoxCurrentOutputFilename.Name = "TextBoxCurrentOutputFilename"
        Me.TextBoxCurrentOutputFilename.ReadOnly = true
        Me.TextBoxCurrentOutputFilename.Size = New System.Drawing.Size(269, 20)
        Me.TextBoxCurrentOutputFilename.TabIndex = 7
        '
        'ProgressBarSweepProgress
        '
        Me.ProgressBarSweepProgress.BackColor = System.Drawing.SystemColors.Window
        Me.ProgressBarSweepProgress.ForeColor = System.Drawing.SystemColors.Window
        Me.ProgressBarSweepProgress.Location = New System.Drawing.Point(150, 53)
        Me.ProgressBarSweepProgress.MarqueeAnimationSpeed = 0
        Me.ProgressBarSweepProgress.Maximum = 2025
        Me.ProgressBarSweepProgress.Name = "ProgressBarSweepProgress"
        Me.ProgressBarSweepProgress.Size = New System.Drawing.Size(167, 20)
        Me.ProgressBarSweepProgress.TabIndex = 11
        '
        'TextBoxRunCount
        '
        Me.TextBoxRunCount.BackColor = System.Drawing.Color.White
        Me.TextBoxRunCount.Location = New System.Drawing.Point(407, 53)
        Me.TextBoxRunCount.Name = "TextBoxRunCount"
        Me.TextBoxRunCount.ReadOnly = true
        Me.TextBoxRunCount.Size = New System.Drawing.Size(64, 20)
        Me.TextBoxRunCount.TabIndex = 17
        Me.TextBoxRunCount.Text = "0"
        Me.TextBoxRunCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'label10
        '
        Me.label10.AutoSize = true
        Me.label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.label10.Location = New System.Drawing.Point(86, 90)
        Me.label10.Name = "label10"
        Me.label10.Size = New System.Drawing.Size(24, 13)
        Me.label10.TabIndex = 20
        Me.label10.Text = "Idle"
        '
        'label11
        '
        Me.label11.AutoSize = true
        Me.label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.label11.ForeColor = System.Drawing.Color.White
        Me.label11.Location = New System.Drawing.Point(10, 37)
        Me.label11.Name = "label11"
        Me.label11.Size = New System.Drawing.Size(56, 13)
        Me.label11.TabIndex = 22
        Me.label11.Text = "Start/Stop"
        '
        'PanelRFSensorDisplay
        '
        Me.PanelRFSensorDisplay.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PanelRFSensorDisplay.BackColor = System.Drawing.Color.DimGray
        Me.PanelRFSensorDisplay.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PanelRFSensorDisplay.Controls.Add(Me.panel2)
        Me.PanelRFSensorDisplay.Controls.Add(Me.panel3)
        Me.PanelRFSensorDisplay.Controls.Add(Me.PanelFilterImage)
        Me.PanelRFSensorDisplay.Controls.Add(Me.SecondaryMeasurementTextBox)
        Me.PanelRFSensorDisplay.Controls.Add(Me.PrimaryMeasurementTextBox)
        Me.PanelRFSensorDisplay.Controls.Add(Me.TextBoxSootLoad)
        Me.PanelRFSensorDisplay.Controls.Add(Me.TextBoxSensorTemperature)
        Me.PanelRFSensorDisplay.Location = New System.Drawing.Point(12, 167)
        Me.PanelRFSensorDisplay.Name = "PanelRFSensorDisplay"
        Me.PanelRFSensorDisplay.Size = New System.Drawing.Size(854, 315)
        Me.PanelRFSensorDisplay.TabIndex = 23
        '
        'panel2
        '
        Me.panel2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.panel2.Controls.Add(Me.PanelChartAdjustments)
        Me.panel2.Controls.Add(Me.ChartSootLoad)
        Me.panel2.Location = New System.Drawing.Point(298, 5)
        Me.panel2.Name = "panel2"
        Me.panel2.Size = New System.Drawing.Size(397, 301)
        Me.panel2.TabIndex = 4
        '
        'PanelChartAdjustments
        '
        Me.PanelChartAdjustments.BackColor = System.Drawing.Color.White
        Me.PanelChartAdjustments.Controls.Add(Me.ButtonCompressPlot)
        Me.PanelChartAdjustments.Controls.Add(Me.ButtonExpandPlot)
        Me.PanelChartAdjustments.Controls.Add(Me.ButtonScrollPlotRight)
        Me.PanelChartAdjustments.Controls.Add(Me.ButtonScrollPlotLeft)
        Me.PanelChartAdjustments.Controls.Add(Me.ButtonClearPlot)
        Me.PanelChartAdjustments.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanelChartAdjustments.Location = New System.Drawing.Point(0, 261)
        Me.PanelChartAdjustments.Name = "PanelChartAdjustments"
        Me.PanelChartAdjustments.Size = New System.Drawing.Size(395, 38)
        Me.PanelChartAdjustments.TabIndex = 3
        '
        'ButtonCompressPlot
        '
        Me.ButtonCompressPlot.BackColor = System.Drawing.Color.MidnightBlue
        Me.ButtonCompressPlot.Font = New System.Drawing.Font("Microsoft Sans Serif", 7!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ButtonCompressPlot.ForeColor = System.Drawing.Color.Orange
        Me.ButtonCompressPlot.Location = New System.Drawing.Point(236, 8)
        Me.ButtonCompressPlot.Name = "ButtonCompressPlot"
        Me.ButtonCompressPlot.Size = New System.Drawing.Size(26, 23)
        Me.ButtonCompressPlot.TabIndex = 4
        Me.ButtonCompressPlot.Text = "-"
        Me.ButtonCompressPlot.UseVisualStyleBackColor = false
        '
        'ButtonExpandPlot
        '
        Me.ButtonExpandPlot.BackColor = System.Drawing.Color.MidnightBlue
        Me.ButtonExpandPlot.Font = New System.Drawing.Font("Microsoft Sans Serif", 7!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ButtonExpandPlot.ForeColor = System.Drawing.Color.Orange
        Me.ButtonExpandPlot.Location = New System.Drawing.Point(127, 8)
        Me.ButtonExpandPlot.Name = "ButtonExpandPlot"
        Me.ButtonExpandPlot.Size = New System.Drawing.Size(26, 23)
        Me.ButtonExpandPlot.TabIndex = 3
        Me.ButtonExpandPlot.Text = "+"
        Me.ButtonExpandPlot.UseVisualStyleBackColor = false
        '
        'ButtonScrollPlotRight
        '
        Me.ButtonScrollPlotRight.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.ButtonScrollPlotRight.BackColor = System.Drawing.Color.MidnightBlue
        Me.ButtonScrollPlotRight.Font = New System.Drawing.Font("Microsoft Sans Serif", 7!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ButtonScrollPlotRight.ForeColor = System.Drawing.Color.Orange
        Me.ButtonScrollPlotRight.Location = New System.Drawing.Point(365, 8)
        Me.ButtonScrollPlotRight.Name = "ButtonScrollPlotRight"
        Me.ButtonScrollPlotRight.Size = New System.Drawing.Size(26, 23)
        Me.ButtonScrollPlotRight.TabIndex = 2
        Me.ButtonScrollPlotRight.Text = ">"
        Me.ButtonScrollPlotRight.UseVisualStyleBackColor = false
        '
        'ButtonScrollPlotLeft
        '
        Me.ButtonScrollPlotLeft.BackColor = System.Drawing.Color.MidnightBlue
        Me.ButtonScrollPlotLeft.Font = New System.Drawing.Font("Microsoft Sans Serif", 7!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ButtonScrollPlotLeft.ForeColor = System.Drawing.Color.Orange
        Me.ButtonScrollPlotLeft.Location = New System.Drawing.Point(3, 8)
        Me.ButtonScrollPlotLeft.Name = "ButtonScrollPlotLeft"
        Me.ButtonScrollPlotLeft.Size = New System.Drawing.Size(26, 23)
        Me.ButtonScrollPlotLeft.TabIndex = 1
        Me.ButtonScrollPlotLeft.Text = "<"
        Me.ButtonScrollPlotLeft.UseVisualStyleBackColor = false
        '
        'ButtonClearPlot
        '
        Me.ButtonClearPlot.BackColor = System.Drawing.Color.MidnightBlue
        Me.ButtonClearPlot.Font = New System.Drawing.Font("Microsoft Sans Serif", 7!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ButtonClearPlot.ForeColor = System.Drawing.Color.Orange
        Me.ButtonClearPlot.Location = New System.Drawing.Point(160, 8)
        Me.ButtonClearPlot.Name = "ButtonClearPlot"
        Me.ButtonClearPlot.Size = New System.Drawing.Size(69, 23)
        Me.ButtonClearPlot.TabIndex = 0
        Me.ButtonClearPlot.Text = "Clear Plot"
        Me.ButtonClearPlot.UseVisualStyleBackColor = false
        '
        'ChartSootLoad
        '
        Me.ChartSootLoad.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.ChartSootLoad.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.ChartSootLoad.BackImageWrapMode = System.Windows.Forms.DataVisualization.Charting.ChartImageWrapMode.TileFlipX
        ChartArea2.AxisX.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Hours
        ChartArea2.AxisX.IsLabelAutoFit = false
        ChartArea2.AxisX.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        ChartArea2.AxisX.LabelStyle.Format = "hh:mm"
        ChartArea2.AxisX.LineColor = System.Drawing.Color.Silver
        ChartArea2.AxisX.MajorGrid.LineColor = System.Drawing.Color.Silver
        ChartArea2.AxisX.MajorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dash
        ChartArea2.AxisX.MinorGrid.LineColor = System.Drawing.Color.Silver
        ChartArea2.AxisX.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot
        ChartArea2.AxisX.ScaleView.SizeType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Hours
        ChartArea2.AxisX.Title = "Time"
        ChartArea2.AxisX.TitleFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        ChartArea2.AxisY.IsLabelAutoFit = false
        ChartArea2.AxisY.IsStartedFromZero = false
        ChartArea2.AxisY.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        ChartArea2.AxisY.LabelStyle.Format = "{0:F4}"
        ChartArea2.AxisY.LineColor = System.Drawing.Color.Silver
        ChartArea2.AxisY.MajorGrid.LineColor = System.Drawing.Color.Silver
        ChartArea2.AxisY.MajorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dash
        ChartArea2.AxisY.MinorGrid.LineColor = System.Drawing.Color.Silver
        ChartArea2.AxisY.MinorGrid.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot
        ChartArea2.AxisY.Title = " Mass Load [g/L] "
        ChartArea2.AxisY.TitleFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        ChartArea2.Name = "ChartArea1"
        Me.ChartSootLoad.ChartAreas.Add(ChartArea2)
        Me.ChartSootLoad.Location = New System.Drawing.Point(0, -5)
        Me.ChartSootLoad.Margin = New System.Windows.Forms.Padding(0)
        Me.ChartSootLoad.Name = "ChartSootLoad"
        Me.ChartSootLoad.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Bright
        Series2.BorderWidth = 2
        Series2.ChartArea = "ChartArea1"
        Series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line
        Series2.Color = System.Drawing.Color.Blue
        Series2.Name = "Specie"
        Series2.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Time
        Me.ChartSootLoad.Series.Add(Series2)
        Me.ChartSootLoad.Size = New System.Drawing.Size(400, 260)
        Me.ChartSootLoad.TabIndex = 2
        Me.ChartSootLoad.Text = "chart1"
        '
        'panel3
        '
        Me.panel3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.panel3.BackColor = System.Drawing.Color.White
        Me.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.panel3.Controls.Add(Me.label30)
        Me.panel3.Controls.Add(Me.TextBoxStatus)
        Me.panel3.Controls.Add(Me.label29)
        Me.panel3.Controls.Add(Me.label26)
        Me.panel3.Controls.Add(Me.PictureBoxTempLow)
        Me.panel3.Controls.Add(Me.label25)
        Me.panel3.Controls.Add(Me.PictureBoxTempHigh)
        Me.panel3.Controls.Add(Me.labelBelowMinDetectable)
        Me.panel3.Controls.Add(Me.PictureBoxSootLow)
        Me.panel3.Controls.Add(Me.labelAboveMaxDetectable)
        Me.panel3.Controls.Add(Me.PictureBoxSootHigh)
        Me.panel3.Controls.Add(Me.label22)
        Me.panel3.Controls.Add(Me.PictureBoxCalibrationOff)
        Me.panel3.Location = New System.Drawing.Point(701, 5)
        Me.panel3.Name = "panel3"
        Me.panel3.Size = New System.Drawing.Size(142, 301)
        Me.panel3.TabIndex = 5
        '
        'label30
        '
        Me.label30.AutoSize = true
        Me.label30.BackColor = System.Drawing.Color.White
        Me.label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.label30.ForeColor = System.Drawing.Color.Black
        Me.label30.Location = New System.Drawing.Point(79, 208)
        Me.label30.Name = "label30"
        Me.label30.Size = New System.Drawing.Size(37, 13)
        Me.label30.TabIndex = 24
        Me.label30.Text = "Status"
        '
        'TextBoxStatus
        '
        Me.TextBoxStatus.BackColor = System.Drawing.Color.White
        Me.TextBoxStatus.Location = New System.Drawing.Point(20, 205)
        Me.TextBoxStatus.Name = "TextBoxStatus"
        Me.TextBoxStatus.ReadOnly = true
        Me.TextBoxStatus.Size = New System.Drawing.Size(53, 20)
        Me.TextBoxStatus.TabIndex = 23
        Me.TextBoxStatus.Text = "0"
        Me.TextBoxStatus.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'label29
        '
        Me.label29.Dock = System.Windows.Forms.DockStyle.Top
        Me.label29.Font = New System.Drawing.Font("Arial Narrow", 12!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.label29.ForeColor = System.Drawing.Color.Black
        Me.label29.Location = New System.Drawing.Point(0, 0)
        Me.label29.Name = "label29"
        Me.label29.Size = New System.Drawing.Size(140, 28)
        Me.label29.TabIndex = 18
        Me.label29.Text = "Diagnostics"
        Me.label29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'label26
        '
        Me.label26.BackColor = System.Drawing.Color.White
        Me.label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.label26.ForeColor = System.Drawing.Color.Black
        Me.label26.Location = New System.Drawing.Point(29, 174)
        Me.label26.Name = "label26"
        Me.label26.Size = New System.Drawing.Size(102, 13)
        Me.label26.TabIndex = 13
        Me.label26.Text = "Temp. Low"
        '
        'PictureBoxTempLow
        '
        Me.PictureBoxTempLow.BackColor = System.Drawing.Color.Transparent
        Me.PictureBoxTempLow.BackgroundImage = CType(resources.GetObject("PictureBoxTempLow.BackgroundImage"),System.Drawing.Image)
        Me.PictureBoxTempLow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBoxTempLow.InitialImage = Nothing
        Me.PictureBoxTempLow.Location = New System.Drawing.Point(6, 172)
        Me.PictureBoxTempLow.Name = "PictureBoxTempLow"
        Me.PictureBoxTempLow.Size = New System.Drawing.Size(18, 18)
        Me.PictureBoxTempLow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBoxTempLow.TabIndex = 12
        Me.PictureBoxTempLow.TabStop = false
        '
        'label25
        '
        Me.label25.BackColor = System.Drawing.Color.White
        Me.label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.label25.ForeColor = System.Drawing.Color.Black
        Me.label25.Location = New System.Drawing.Point(29, 143)
        Me.label25.Name = "label25"
        Me.label25.Size = New System.Drawing.Size(102, 13)
        Me.label25.TabIndex = 11
        Me.label25.Text = "Temp. High"
        '
        'PictureBoxTempHigh
        '
        Me.PictureBoxTempHigh.BackColor = System.Drawing.Color.Transparent
        Me.PictureBoxTempHigh.BackgroundImage = CType(resources.GetObject("PictureBoxTempHigh.BackgroundImage"),System.Drawing.Image)
        Me.PictureBoxTempHigh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBoxTempHigh.InitialImage = Nothing
        Me.PictureBoxTempHigh.Location = New System.Drawing.Point(6, 141)
        Me.PictureBoxTempHigh.Name = "PictureBoxTempHigh"
        Me.PictureBoxTempHigh.Size = New System.Drawing.Size(18, 18)
        Me.PictureBoxTempHigh.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBoxTempHigh.TabIndex = 10
        Me.PictureBoxTempHigh.TabStop = false
        '
        'labelBelowMinDetectable
        '
        Me.labelBelowMinDetectable.BackColor = System.Drawing.Color.White
        Me.labelBelowMinDetectable.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.labelBelowMinDetectable.ForeColor = System.Drawing.Color.Black
        Me.labelBelowMinDetectable.Location = New System.Drawing.Point(29, 112)
        Me.labelBelowMinDetectable.Name = "labelBelowMinDetectable"
        Me.labelBelowMinDetectable.Size = New System.Drawing.Size(102, 13)
        Me.labelBelowMinDetectable.TabIndex = 9
        Me.labelBelowMinDetectable.Text = "Below Min Detect"
        '
        'PictureBoxSootLow
        '
        Me.PictureBoxSootLow.BackColor = System.Drawing.Color.Transparent
        Me.PictureBoxSootLow.BackgroundImage = CType(resources.GetObject("PictureBoxSootLow.BackgroundImage"),System.Drawing.Image)
        Me.PictureBoxSootLow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBoxSootLow.InitialImage = Nothing
        Me.PictureBoxSootLow.Location = New System.Drawing.Point(6, 110)
        Me.PictureBoxSootLow.Name = "PictureBoxSootLow"
        Me.PictureBoxSootLow.Size = New System.Drawing.Size(18, 18)
        Me.PictureBoxSootLow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBoxSootLow.TabIndex = 8
        Me.PictureBoxSootLow.TabStop = false
        '
        'labelAboveMaxDetectable
        '
        Me.labelAboveMaxDetectable.BackColor = System.Drawing.Color.White
        Me.labelAboveMaxDetectable.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.labelAboveMaxDetectable.ForeColor = System.Drawing.Color.Black
        Me.labelAboveMaxDetectable.Location = New System.Drawing.Point(29, 81)
        Me.labelAboveMaxDetectable.Name = "labelAboveMaxDetectable"
        Me.labelAboveMaxDetectable.Size = New System.Drawing.Size(102, 13)
        Me.labelAboveMaxDetectable.TabIndex = 7
        Me.labelAboveMaxDetectable.Text = "Above Max Detect"
        '
        'PictureBoxSootHigh
        '
        Me.PictureBoxSootHigh.BackColor = System.Drawing.Color.Transparent
        Me.PictureBoxSootHigh.BackgroundImage = CType(resources.GetObject("PictureBoxSootHigh.BackgroundImage"),System.Drawing.Image)
        Me.PictureBoxSootHigh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBoxSootHigh.InitialImage = Nothing
        Me.PictureBoxSootHigh.Location = New System.Drawing.Point(6, 79)
        Me.PictureBoxSootHigh.Name = "PictureBoxSootHigh"
        Me.PictureBoxSootHigh.Size = New System.Drawing.Size(18, 18)
        Me.PictureBoxSootHigh.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBoxSootHigh.TabIndex = 6
        Me.PictureBoxSootHigh.TabStop = false
        '
        'label22
        '
        Me.label22.BackColor = System.Drawing.Color.White
        Me.label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.label22.ForeColor = System.Drawing.Color.Black
        Me.label22.Location = New System.Drawing.Point(29, 50)
        Me.label22.Name = "label22"
        Me.label22.Size = New System.Drawing.Size(102, 13)
        Me.label22.TabIndex = 5
        Me.label22.Text = "Calibration Off"
        '
        'PictureBoxCalibrationOff
        '
        Me.PictureBoxCalibrationOff.BackColor = System.Drawing.Color.Transparent
        Me.PictureBoxCalibrationOff.BackgroundImage = CType(resources.GetObject("PictureBoxCalibrationOff.BackgroundImage"),System.Drawing.Image)
        Me.PictureBoxCalibrationOff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBoxCalibrationOff.InitialImage = Nothing
        Me.PictureBoxCalibrationOff.Location = New System.Drawing.Point(6, 48)
        Me.PictureBoxCalibrationOff.Name = "PictureBoxCalibrationOff"
        Me.PictureBoxCalibrationOff.Size = New System.Drawing.Size(18, 18)
        Me.PictureBoxCalibrationOff.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBoxCalibrationOff.TabIndex = 4
        Me.PictureBoxCalibrationOff.TabStop = false
        '
        'PanelFilterImage
        '
        Me.PanelFilterImage.BackColor = System.Drawing.Color.White
        Me.PanelFilterImage.BackgroundImage = CType(resources.GetObject("PanelFilterImage.BackgroundImage"),System.Drawing.Image)
        Me.PanelFilterImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PanelFilterImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanelFilterImage.Controls.Add(Me.label12)
        Me.PanelFilterImage.Controls.Add(Me.label21)
        Me.PanelFilterImage.Controls.Add(Me.label20)
        Me.PanelFilterImage.Controls.Add(Me.TextBoxOutletTemperature)
        Me.PanelFilterImage.Controls.Add(Me.TextBoxInletTemperature)
        Me.PanelFilterImage.Controls.Add(Me.PanelLoadBar)
        Me.PanelFilterImage.Controls.Add(Me.label15)
        Me.PanelFilterImage.Location = New System.Drawing.Point(7, 5)
        Me.PanelFilterImage.Name = "PanelFilterImage"
        Me.PanelFilterImage.Size = New System.Drawing.Size(285, 257)
        Me.PanelFilterImage.TabIndex = 0
        '
        'label12
        '
        Me.label12.BackColor = System.Drawing.Color.Transparent
        Me.label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.label12.Location = New System.Drawing.Point(2, 187)
        Me.label12.Name = "label12"
        Me.label12.Size = New System.Drawing.Size(50, 18)
        Me.label12.TabIndex = 8
        Me.label12.Text = "Tin (°C)"
        Me.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'label21
        '
        Me.label21.AutoSize = true
        Me.label21.BackColor = System.Drawing.Color.White
        Me.label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.label21.Location = New System.Drawing.Point(88, 59)
        Me.label21.Name = "label21"
        Me.label21.Size = New System.Drawing.Size(33, 13)
        Me.label21.TabIndex = 5
        Me.label21.Text = "100%"
        '
        'label20
        '
        Me.label20.AutoSize = true
        Me.label20.BackColor = System.Drawing.Color.White
        Me.label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.label20.Location = New System.Drawing.Point(100, 183)
        Me.label20.Name = "label20"
        Me.label20.Size = New System.Drawing.Size(21, 13)
        Me.label20.TabIndex = 4
        Me.label20.Text = "0%"
        '
        'TextBoxOutletTemperature
        '
        Me.TextBoxOutletTemperature.BackColor = System.Drawing.Color.White
        Me.TextBoxOutletTemperature.Location = New System.Drawing.Point(236, 164)
        Me.TextBoxOutletTemperature.Name = "TextBoxOutletTemperature"
        Me.TextBoxOutletTemperature.ReadOnly = true
        Me.TextBoxOutletTemperature.Size = New System.Drawing.Size(41, 20)
        Me.TextBoxOutletTemperature.TabIndex = 1
        Me.TextBoxOutletTemperature.TabStop = false
        Me.TextBoxOutletTemperature.Text = "0"
        Me.TextBoxOutletTemperature.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBoxInletTemperature
        '
        Me.TextBoxInletTemperature.BackColor = System.Drawing.Color.White
        Me.TextBoxInletTemperature.Location = New System.Drawing.Point(6, 164)
        Me.TextBoxInletTemperature.Name = "TextBoxInletTemperature"
        Me.TextBoxInletTemperature.ReadOnly = true
        Me.TextBoxInletTemperature.Size = New System.Drawing.Size(41, 20)
        Me.TextBoxInletTemperature.TabIndex = 0
        Me.TextBoxInletTemperature.TabStop = false
        Me.TextBoxInletTemperature.Text = "0"
        Me.TextBoxInletTemperature.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'PanelLoadBar
        '
        Me.PanelLoadBar.BackColor = System.Drawing.SystemColors.Control
        Me.PanelLoadBar.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PanelLoadBar.Controls.Add(Me.PanelLoadBarPercentage)
        Me.PanelLoadBar.Location = New System.Drawing.Point(125, 64)
        Me.PanelLoadBar.Name = "PanelLoadBar"
        Me.PanelLoadBar.Size = New System.Drawing.Size(38, 129)
        Me.PanelLoadBar.TabIndex = 1
        '
        'PanelLoadBarPercentage
        '
        Me.PanelLoadBarPercentage.BackColor = System.Drawing.SystemColors.HotTrack
        Me.PanelLoadBarPercentage.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanelLoadBarPercentage.Location = New System.Drawing.Point(0, 123)
        Me.PanelLoadBarPercentage.Name = "PanelLoadBarPercentage"
        Me.PanelLoadBarPercentage.Size = New System.Drawing.Size(34, 2)
        Me.PanelLoadBarPercentage.TabIndex = 0
        '
        'label15
        '
        Me.label15.BackColor = System.Drawing.Color.Transparent
        Me.label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.label15.Location = New System.Drawing.Point(232, 187)
        Me.label15.Name = "label15"
        Me.label15.Size = New System.Drawing.Size(50, 18)
        Me.label15.TabIndex = 4
        Me.label15.Text = "Tout (°C)"
        Me.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'SecondaryMeasurementTextBox
        '
        Me.SecondaryMeasurementTextBox.BackColor = System.Drawing.Color.White
        Me.SecondaryMeasurementTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SecondaryMeasurementTextBox.Location = New System.Drawing.Point(7, 286)
        Me.SecondaryMeasurementTextBox.Name = "SecondaryMeasurementTextBox"
        Me.SecondaryMeasurementTextBox.ReadOnly = true
        Me.SecondaryMeasurementTextBox.Size = New System.Drawing.Size(229, 20)
        Me.SecondaryMeasurementTextBox.TabIndex = 8
        Me.SecondaryMeasurementTextBox.Text = " Average Temp (°C):"
        '
        'PrimaryMeasurementTextBox
        '
        Me.PrimaryMeasurementTextBox.BackColor = System.Drawing.Color.White
        Me.PrimaryMeasurementTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PrimaryMeasurementTextBox.ForeColor = System.Drawing.Color.Black
        Me.PrimaryMeasurementTextBox.Location = New System.Drawing.Point(7, 266)
        Me.PrimaryMeasurementTextBox.Name = "PrimaryMeasurementTextBox"
        Me.PrimaryMeasurementTextBox.ReadOnly = true
        Me.PrimaryMeasurementTextBox.Size = New System.Drawing.Size(229, 20)
        Me.PrimaryMeasurementTextBox.TabIndex = 7
        Me.PrimaryMeasurementTextBox.Text = " Mass Load [g/L]:"
        '
        'TextBoxSootLoad
        '
        Me.TextBoxSootLoad.BackColor = System.Drawing.Color.White
        Me.TextBoxSootLoad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxSootLoad.Location = New System.Drawing.Point(236, 266)
        Me.TextBoxSootLoad.Name = "TextBoxSootLoad"
        Me.TextBoxSootLoad.ReadOnly = true
        Me.TextBoxSootLoad.Size = New System.Drawing.Size(56, 20)
        Me.TextBoxSootLoad.TabIndex = 2
        Me.TextBoxSootLoad.Text = "0"
        Me.TextBoxSootLoad.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBoxSensorTemperature
        '
        Me.TextBoxSensorTemperature.BackColor = System.Drawing.Color.White
        Me.TextBoxSensorTemperature.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxSensorTemperature.Location = New System.Drawing.Point(236, 286)
        Me.TextBoxSensorTemperature.Name = "TextBoxSensorTemperature"
        Me.TextBoxSensorTemperature.ReadOnly = true
        Me.TextBoxSensorTemperature.Size = New System.Drawing.Size(56, 20)
        Me.TextBoxSensorTemperature.TabIndex = 5
        Me.TextBoxSensorTemperature.Text = "0"
        Me.TextBoxSensorTemperature.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'FolderBrowserDialogOutputDirectory
        '
        Me.FolderBrowserDialogOutputDirectory.Description = "Select the Output File Destination Folder"
        '
        'TextBoxSweepDuration
        '
        Me.TextBoxSweepDuration.BackColor = System.Drawing.SystemColors.Window
        Me.TextBoxSweepDuration.Location = New System.Drawing.Point(330, 53)
        Me.TextBoxSweepDuration.Name = "TextBoxSweepDuration"
        Me.TextBoxSweepDuration.ReadOnly = true
        Me.TextBoxSweepDuration.Size = New System.Drawing.Size(64, 20)
        Me.TextBoxSweepDuration.TabIndex = 27
        Me.TextBoxSweepDuration.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'label28
        '
        Me.label28.AutoSize = true
        Me.label28.Location = New System.Drawing.Point(330, 36)
        Me.label28.Name = "label28"
        Me.label28.Size = New System.Drawing.Size(66, 13)
        Me.label28.TabIndex = 28
        Me.label28.Text = "Sweep Time"
        Me.label28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'toolTip1
        '
        Me.toolTip1.AutomaticDelay = 250
        Me.toolTip1.AutoPopDelay = 10000
        Me.toolTip1.InitialDelay = 250
        Me.toolTip1.ReshowDelay = 50
        '
        'ButtonCanConfig
        '
        Me.ButtonCanConfig.BackColor = System.Drawing.Color.MidnightBlue
        Me.ButtonCanConfig.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ButtonCanConfig.ForeColor = System.Drawing.Color.Chartreuse
        Me.ButtonCanConfig.Location = New System.Drawing.Point(12, 14)
        Me.ButtonCanConfig.Name = "ButtonCanConfig"
        Me.ButtonCanConfig.Size = New System.Drawing.Size(70, 25)
        Me.ButtonCanConfig.TabIndex = 46
        Me.ButtonCanConfig.Text = "Configure"
        Me.toolTip1.SetToolTip(Me.ButtonCanConfig, "Configure CAN Device")
        Me.ButtonCanConfig.UseVisualStyleBackColor = false
        '
        'ButtonCanEnable
        '
        Me.ButtonCanEnable.BackColor = System.Drawing.Color.MidnightBlue
        Me.ButtonCanEnable.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.ButtonCanEnable.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ButtonCanEnable.ForeColor = System.Drawing.Color.Chartreuse
        Me.ButtonCanEnable.Location = New System.Drawing.Point(91, 14)
        Me.ButtonCanEnable.Name = "ButtonCanEnable"
        Me.ButtonCanEnable.Size = New System.Drawing.Size(70, 25)
        Me.ButtonCanEnable.TabIndex = 48
        Me.ButtonCanEnable.Text = "Connect"
        Me.toolTip1.SetToolTip(Me.ButtonCanEnable, "Connect/Disconnect CAN Device")
        Me.ButtonCanEnable.UseVisualStyleBackColor = false
        '
        'ButtonSelectOutputFolder
        '
        Me.ButtonSelectOutputFolder.BackgroundImage = CType(resources.GetObject("ButtonSelectOutputFolder.BackgroundImage"),System.Drawing.Image)
        Me.ButtonSelectOutputFolder.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.ButtonSelectOutputFolder.Location = New System.Drawing.Point(12, 137)
        Me.ButtonSelectOutputFolder.Name = "ButtonSelectOutputFolder"
        Me.ButtonSelectOutputFolder.Size = New System.Drawing.Size(30, 26)
        Me.ButtonSelectOutputFolder.TabIndex = 6
        Me.toolTip1.SetToolTip(Me.ButtonSelectOutputFolder, "Select Output File Destination Folder")
        Me.ButtonSelectOutputFolder.UseVisualStyleBackColor = true
        '
        'Label18
        '
        Me.Label18.AutoSize = true
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label18.ForeColor = System.Drawing.Color.White
        Me.Label18.Location = New System.Drawing.Point(87, 90)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(24, 13)
        Me.Label18.TabIndex = 20
        Me.Label18.Text = "Idle"
        Me.toolTip1.SetToolTip(Me.Label18, "Indicates the period between sweeps"&Global.Microsoft.VisualBasic.ChrW(13)&Global.Microsoft.VisualBasic.ChrW(10)&"when running continuous sweeps.")
        '
        'PictureBoxIdle
        '
        Me.PictureBoxIdle.BackColor = System.Drawing.Color.Transparent
        Me.PictureBoxIdle.BackgroundImage = CType(resources.GetObject("PictureBoxIdle.BackgroundImage"),System.Drawing.Image)
        Me.PictureBoxIdle.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBoxIdle.InitialImage = Nothing
        Me.PictureBoxIdle.Location = New System.Drawing.Point(66, 87)
        Me.PictureBoxIdle.Name = "PictureBoxIdle"
        Me.PictureBoxIdle.Size = New System.Drawing.Size(18, 18)
        Me.PictureBoxIdle.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBoxIdle.TabIndex = 19
        Me.PictureBoxIdle.TabStop = false
        Me.toolTip1.SetToolTip(Me.PictureBoxIdle, "Indicates the period between sweeps"&Global.Microsoft.VisualBasic.ChrW(13)&Global.Microsoft.VisualBasic.ChrW(10)&"when running continuous sweeps.")
        '
        'GroupBoxCanControls
        '
        Me.GroupBoxCanControls.Controls.Add(Me.LabelCanActivity)
        Me.GroupBoxCanControls.Controls.Add(Me.PictureBoxCanActivity)
        Me.GroupBoxCanControls.Controls.Add(Me.ButtonCanEnable)
        Me.GroupBoxCanControls.Controls.Add(Me.ButtonCanConfig)
        Me.GroupBoxCanControls.ForeColor = System.Drawing.Color.White
        Me.GroupBoxCanControls.Location = New System.Drawing.Point(485, 37)
        Me.GroupBoxCanControls.Name = "GroupBoxCanControls"
        Me.GroupBoxCanControls.Size = New System.Drawing.Size(245, 46)
        Me.GroupBoxCanControls.TabIndex = 49
        Me.GroupBoxCanControls.TabStop = false
        Me.GroupBoxCanControls.Text = "CAN"
        '
        'LabelCanActivity
        '
        Me.LabelCanActivity.AutoSize = true
        Me.LabelCanActivity.ForeColor = System.Drawing.Color.White
        Me.LabelCanActivity.Location = New System.Drawing.Point(193, 20)
        Me.LabelCanActivity.Name = "LabelCanActivity"
        Me.LabelCanActivity.Size = New System.Drawing.Size(41, 13)
        Me.LabelCanActivity.TabIndex = 51
        Me.LabelCanActivity.Text = "Activity"
        '
        'PictureBoxCanActivity
        '
        Me.PictureBoxCanActivity.BackColor = System.Drawing.Color.Transparent
        Me.PictureBoxCanActivity.BackgroundImage = CType(resources.GetObject("PictureBoxCanActivity.BackgroundImage"),System.Drawing.Image)
        Me.PictureBoxCanActivity.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBoxCanActivity.InitialImage = Nothing
        Me.PictureBoxCanActivity.Location = New System.Drawing.Point(173, 18)
        Me.PictureBoxCanActivity.Name = "PictureBoxCanActivity"
        Me.PictureBoxCanActivity.Size = New System.Drawing.Size(18, 18)
        Me.PictureBoxCanActivity.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBoxCanActivity.TabIndex = 50
        Me.PictureBoxCanActivity.TabStop = false
        '
        'LabelTestName
        '
        Me.LabelTestName.AutoSize = true
        Me.LabelTestName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.LabelTestName.ForeColor = System.Drawing.Color.White
        Me.LabelTestName.Location = New System.Drawing.Point(344, 125)
        Me.LabelTestName.Name = "LabelTestName"
        Me.LabelTestName.Size = New System.Drawing.Size(224, 13)
        Me.LabelTestName.TabIndex = 0
        Me.LabelTestName.Text = "<Enter> Test Name ( required for file creation )"
        '
        'LabelOutputFileFolder
        '
        Me.LabelOutputFileFolder.AutoSize = true
        Me.LabelOutputFileFolder.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.LabelOutputFileFolder.ForeColor = System.Drawing.Color.White
        Me.LabelOutputFileFolder.Location = New System.Drawing.Point(110, 125)
        Me.LabelOutputFileFolder.Name = "LabelOutputFileFolder"
        Me.LabelOutputFileFolder.Size = New System.Drawing.Size(135, 13)
        Me.LabelOutputFileFolder.TabIndex = 4
        Me.LabelOutputFileFolder.Text = "Selected Output File Folder"
        '
        'LabelOutputFileName
        '
        Me.LabelOutputFileName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.LabelOutputFileName.ForeColor = System.Drawing.Color.White
        Me.LabelOutputFileName.Location = New System.Drawing.Point(597, 125)
        Me.LabelOutputFileName.Name = "LabelOutputFileName"
        Me.LabelOutputFileName.Size = New System.Drawing.Size(270, 13)
        Me.LabelOutputFileName.TabIndex = 8
        Me.LabelOutputFileName.Text = "Current Output File Name"
        Me.LabelOutputFileName.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'LabelSweepProgress
        '
        Me.LabelSweepProgress.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.LabelSweepProgress.ForeColor = System.Drawing.Color.White
        Me.LabelSweepProgress.Location = New System.Drawing.Point(150, 36)
        Me.LabelSweepProgress.Name = "LabelSweepProgress"
        Me.LabelSweepProgress.Size = New System.Drawing.Size(167, 15)
        Me.LabelSweepProgress.TabIndex = 12
        Me.LabelSweepProgress.Text = "Sweep Progress"
        Me.LabelSweepProgress.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label17
        '
        Me.Label17.AutoSize = true
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label17.ForeColor = System.Drawing.Color.White
        Me.Label17.Location = New System.Drawing.Point(409, 36)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(59, 13)
        Me.Label17.TabIndex = 18
        Me.Label17.Text = "Total Runs"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LabelSweepTime
        '
        Me.LabelSweepTime.AutoSize = true
        Me.LabelSweepTime.ForeColor = System.Drawing.Color.White
        Me.LabelSweepTime.Location = New System.Drawing.Point(323, 36)
        Me.LabelSweepTime.Name = "LabelSweepTime"
        Me.LabelSweepTime.Size = New System.Drawing.Size(80, 13)
        Me.LabelSweepTime.TabIndex = 28
        Me.LabelSweepTime.Text = "Sweep Time (s)"
        Me.LabelSweepTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LabelCtsWebAddress
        '
        Me.LabelCtsWebAddress.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelCtsWebAddress.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LabelCtsWebAddress.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.LabelCtsWebAddress.ForeColor = System.Drawing.Color.Blue
        Me.LabelCtsWebAddress.Location = New System.Drawing.Point(296, 516)
        Me.LabelCtsWebAddress.Name = "LabelCtsWebAddress"
        Me.LabelCtsWebAddress.Size = New System.Drawing.Size(284, 19)
        Me.LabelCtsWebAddress.TabIndex = 51
        Me.LabelCtsWebAddress.Tag = "http://www.ctscorp.com/products/sensors-2/rf-dpf-sensor/"
        Me.LabelCtsWebAddress.Text = "http://www.ctscorp.com/"
        Me.LabelCtsWebAddress.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'LabelCTSProprietary
        '
        Me.LabelCTSProprietary.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelCTSProprietary.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.LabelCTSProprietary.ForeColor = System.Drawing.Color.White
        Me.LabelCTSProprietary.Location = New System.Drawing.Point(12, 516)
        Me.LabelCTSProprietary.Name = "LabelCTSProprietary"
        Me.LabelCTSProprietary.Size = New System.Drawing.Size(285, 19)
        Me.LabelCTSProprietary.TabIndex = 52
        Me.LabelCTSProprietary.Text = "CTS Corporation Proprietary Software"
        Me.LabelCTSProprietary.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LabelCTSCopyright
        '
        Me.LabelCTSCopyright.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LabelCTSCopyright.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.LabelCTSCopyright.ForeColor = System.Drawing.Color.White
        Me.LabelCTSCopyright.Location = New System.Drawing.Point(580, 516)
        Me.LabelCTSCopyright.Name = "LabelCTSCopyright"
        Me.LabelCTSCopyright.Size = New System.Drawing.Size(286, 19)
        Me.LabelCTSCopyright.TabIndex = 53
        Me.LabelCTSCopyright.Text = "© CTS Corporation 2016"
        Me.LabelCTSCopyright.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'MenuStripRFSensorMain
        '
        Me.MenuStripRFSensorMain.BackColor = System.Drawing.Color.MidnightBlue
        Me.MenuStripRFSensorMain.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.MenuStripRFSensorMain.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemFile, Me.ToolStripMenuItemSweepSettings, Me.ToolStripMenuItemConfigure, Me.ToolStripMenuItemAccess})
        Me.MenuStripRFSensorMain.Location = New System.Drawing.Point(0, 0)
        Me.MenuStripRFSensorMain.Name = "MenuStripRFSensorMain"
        Me.MenuStripRFSensorMain.Size = New System.Drawing.Size(877, 24)
        Me.MenuStripRFSensorMain.TabIndex = 61
        Me.MenuStripRFSensorMain.Text = "MenuStripRFSensorMain"
        '
        'ToolStripMenuItemFile
        '
        Me.ToolStripMenuItemFile.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemEncryption})
        Me.ToolStripMenuItemFile.ForeColor = System.Drawing.Color.Chartreuse
        Me.ToolStripMenuItemFile.Name = "ToolStripMenuItemFile"
        Me.ToolStripMenuItemFile.Size = New System.Drawing.Size(37, 20)
        Me.ToolStripMenuItemFile.Text = "File"
        Me.ToolStripMenuItemFile.Visible = false
        '
        'ToolStripMenuItemEncryption
        '
        Me.ToolStripMenuItemEncryption.BackColor = System.Drawing.Color.MidnightBlue
        Me.ToolStripMenuItemEncryption.ForeColor = System.Drawing.Color.Chartreuse
        Me.ToolStripMenuItemEncryption.Name = "ToolStripMenuItemEncryption"
        Me.ToolStripMenuItemEncryption.Size = New System.Drawing.Size(160, 22)
        Me.ToolStripMenuItemEncryption.Text = "Encrypt/Decrypt"
        '
        'ToolStripMenuItemSweepSettings
        '
        Me.ToolStripMenuItemSweepSettings.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripMenuItemSweepSettings.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemOperatingMode, Me.ToolStripMenuItemOutputPower, Me.ToolStripMenuItemOutputPowerAux, Me.ToolStripMenuItemAtoDReadingsForAvg, Me.ToolStripMenuItemLockDelay, Me.ToolStripMenuItemMinDetectable, Me.ToolStripMenuItemMaxDetectable, Me.ToolStripMenuItemSweepInterval, Me.ToolStripMenuItemStepDelay, Me.ToolStripMenuItemSweepRate, Me.ToolStripMenuItemDisableCalibration})
        Me.ToolStripMenuItemSweepSettings.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ToolStripMenuItemSweepSettings.ForeColor = System.Drawing.Color.Chartreuse
        Me.ToolStripMenuItemSweepSettings.Name = "ToolStripMenuItemSweepSettings"
        Me.ToolStripMenuItemSweepSettings.Size = New System.Drawing.Size(89, 20)
        Me.ToolStripMenuItemSweepSettings.Text = "Scan Settings"
        Me.ToolStripMenuItemSweepSettings.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.ToolStripMenuItemSweepSettings.Visible = false
        '
        'ToolStripMenuItemOperatingMode
        '
        Me.ToolStripMenuItemOperatingMode.BackColor = System.Drawing.Color.MidnightBlue
        Me.ToolStripMenuItemOperatingMode.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripComboBoxOperatingMode})
        Me.ToolStripMenuItemOperatingMode.ForeColor = System.Drawing.Color.Chartreuse
        Me.ToolStripMenuItemOperatingMode.Name = "ToolStripMenuItemOperatingMode"
        Me.ToolStripMenuItemOperatingMode.Size = New System.Drawing.Size(207, 22)
        Me.ToolStripMenuItemOperatingMode.Text = "Operating Mode"
        Me.ToolStripMenuItemOperatingMode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ToolStripMenuItemOperatingMode.Visible = false
        '
        'ToolStripComboBoxOperatingMode
        '
        Me.ToolStripComboBoxOperatingMode.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold)
        Me.ToolStripComboBoxOperatingMode.Name = "ToolStripComboBoxOperatingMode"
        Me.ToolStripComboBoxOperatingMode.Size = New System.Drawing.Size(90, 23)
        '
        'ToolStripMenuItemOutputPower
        '
        Me.ToolStripMenuItemOutputPower.BackColor = System.Drawing.Color.MidnightBlue
        Me.ToolStripMenuItemOutputPower.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripComboBoxOutputPower})
        Me.ToolStripMenuItemOutputPower.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ToolStripMenuItemOutputPower.ForeColor = System.Drawing.Color.Chartreuse
        Me.ToolStripMenuItemOutputPower.Name = "ToolStripMenuItemOutputPower"
        Me.ToolStripMenuItemOutputPower.Size = New System.Drawing.Size(207, 22)
        Me.ToolStripMenuItemOutputPower.Text = "Output Power"
        Me.ToolStripMenuItemOutputPower.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'ToolStripComboBoxOutputPower
        '
        Me.ToolStripComboBoxOutputPower.AutoSize = false
        Me.ToolStripComboBoxOutputPower.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ToolStripComboBoxOutputPower.Name = "ToolStripComboBoxOutputPower"
        Me.ToolStripComboBoxOutputPower.Size = New System.Drawing.Size(65, 23)
        '
        'ToolStripMenuItemOutputPowerAux
        '
        Me.ToolStripMenuItemOutputPowerAux.BackColor = System.Drawing.Color.MidnightBlue
        Me.ToolStripMenuItemOutputPowerAux.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripComboBoxOutputPowerAux})
        Me.ToolStripMenuItemOutputPowerAux.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ToolStripMenuItemOutputPowerAux.ForeColor = System.Drawing.Color.Chartreuse
        Me.ToolStripMenuItemOutputPowerAux.Name = "ToolStripMenuItemOutputPowerAux"
        Me.ToolStripMenuItemOutputPowerAux.Size = New System.Drawing.Size(207, 22)
        Me.ToolStripMenuItemOutputPowerAux.Text = "Aux Output Power"
        Me.ToolStripMenuItemOutputPowerAux.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'ToolStripComboBoxOutputPowerAux
        '
        Me.ToolStripComboBoxOutputPowerAux.AutoSize = false
        Me.ToolStripComboBoxOutputPowerAux.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ToolStripComboBoxOutputPowerAux.Name = "ToolStripComboBoxOutputPowerAux"
        Me.ToolStripComboBoxOutputPowerAux.Size = New System.Drawing.Size(65, 23)
        '
        'ToolStripMenuItemAtoDReadingsForAvg
        '
        Me.ToolStripMenuItemAtoDReadingsForAvg.BackColor = System.Drawing.Color.MidnightBlue
        Me.ToolStripMenuItemAtoDReadingsForAvg.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripTextBoxAtoDReadingsForAvgValue})
        Me.ToolStripMenuItemAtoDReadingsForAvg.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ToolStripMenuItemAtoDReadingsForAvg.ForeColor = System.Drawing.Color.Chartreuse
        Me.ToolStripMenuItemAtoDReadingsForAvg.Name = "ToolStripMenuItemAtoDReadingsForAvg"
        Me.ToolStripMenuItemAtoDReadingsForAvg.Size = New System.Drawing.Size(207, 22)
        Me.ToolStripMenuItemAtoDReadingsForAvg.Text = "A/D Readings For Avg."
        Me.ToolStripMenuItemAtoDReadingsForAvg.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'ToolStripTextBoxAtoDReadingsForAvgValue
        '
        Me.ToolStripTextBoxAtoDReadingsForAvgValue.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ToolStripTextBoxAtoDReadingsForAvgValue.Name = "ToolStripTextBoxAtoDReadingsForAvgValue"
        Me.ToolStripTextBoxAtoDReadingsForAvgValue.Size = New System.Drawing.Size(45, 23)
        Me.ToolStripTextBoxAtoDReadingsForAvgValue.Text = "1"
        Me.ToolStripTextBoxAtoDReadingsForAvgValue.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'ToolStripMenuItemLockDelay
        '
        Me.ToolStripMenuItemLockDelay.BackColor = System.Drawing.Color.MidnightBlue
        Me.ToolStripMenuItemLockDelay.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripTextBoxLockDelayValue})
        Me.ToolStripMenuItemLockDelay.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ToolStripMenuItemLockDelay.ForeColor = System.Drawing.Color.Chartreuse
        Me.ToolStripMenuItemLockDelay.Name = "ToolStripMenuItemLockDelay"
        Me.ToolStripMenuItemLockDelay.Size = New System.Drawing.Size(207, 22)
        Me.ToolStripMenuItemLockDelay.Text = "Lock Delay (μs)"
        Me.ToolStripMenuItemLockDelay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'ToolStripTextBoxLockDelayValue
        '
        Me.ToolStripTextBoxLockDelayValue.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ToolStripTextBoxLockDelayValue.Name = "ToolStripTextBoxLockDelayValue"
        Me.ToolStripTextBoxLockDelayValue.Size = New System.Drawing.Size(45, 23)
        Me.ToolStripTextBoxLockDelayValue.Text = "0"
        Me.ToolStripTextBoxLockDelayValue.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'ToolStripMenuItemMinDetectable
        '
        Me.ToolStripMenuItemMinDetectable.BackColor = System.Drawing.Color.MidnightBlue
        Me.ToolStripMenuItemMinDetectable.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripTextBoxMinDetectableValue})
        Me.ToolStripMenuItemMinDetectable.ForeColor = System.Drawing.Color.Chartreuse
        Me.ToolStripMenuItemMinDetectable.Name = "ToolStripMenuItemMinDetectable"
        Me.ToolStripMenuItemMinDetectable.Size = New System.Drawing.Size(207, 22)
        Me.ToolStripMenuItemMinDetectable.Text = "Min Detectable"
        Me.ToolStripMenuItemMinDetectable.Visible = false
        '
        'ToolStripTextBoxMinDetectableValue
        '
        Me.ToolStripTextBoxMinDetectableValue.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold)
        Me.ToolStripTextBoxMinDetectableValue.Name = "ToolStripTextBoxMinDetectableValue"
        Me.ToolStripTextBoxMinDetectableValue.Size = New System.Drawing.Size(65, 23)
        '
        'ToolStripMenuItemMaxDetectable
        '
        Me.ToolStripMenuItemMaxDetectable.BackColor = System.Drawing.Color.MidnightBlue
        Me.ToolStripMenuItemMaxDetectable.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripTextBoxMaxDetectableValue})
        Me.ToolStripMenuItemMaxDetectable.ForeColor = System.Drawing.Color.Chartreuse
        Me.ToolStripMenuItemMaxDetectable.Name = "ToolStripMenuItemMaxDetectable"
        Me.ToolStripMenuItemMaxDetectable.Size = New System.Drawing.Size(207, 22)
        Me.ToolStripMenuItemMaxDetectable.Text = "Max Detectable"
        Me.ToolStripMenuItemMaxDetectable.Visible = false
        '
        'ToolStripTextBoxMaxDetectableValue
        '
        Me.ToolStripTextBoxMaxDetectableValue.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold)
        Me.ToolStripTextBoxMaxDetectableValue.Name = "ToolStripTextBoxMaxDetectableValue"
        Me.ToolStripTextBoxMaxDetectableValue.Size = New System.Drawing.Size(65, 23)
        '
        'ToolStripMenuItemSweepInterval
        '
        Me.ToolStripMenuItemSweepInterval.BackColor = System.Drawing.Color.MidnightBlue
        Me.ToolStripMenuItemSweepInterval.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripTextBoxSweepIntervalValue})
        Me.ToolStripMenuItemSweepInterval.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ToolStripMenuItemSweepInterval.ForeColor = System.Drawing.Color.Chartreuse
        Me.ToolStripMenuItemSweepInterval.Name = "ToolStripMenuItemSweepInterval"
        Me.ToolStripMenuItemSweepInterval.Size = New System.Drawing.Size(207, 22)
        Me.ToolStripMenuItemSweepInterval.Text = "Sweep Interval (ms)"
        Me.ToolStripMenuItemSweepInterval.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'ToolStripTextBoxSweepIntervalValue
        '
        Me.ToolStripTextBoxSweepIntervalValue.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ToolStripTextBoxSweepIntervalValue.Name = "ToolStripTextBoxSweepIntervalValue"
        Me.ToolStripTextBoxSweepIntervalValue.Size = New System.Drawing.Size(45, 23)
        Me.ToolStripTextBoxSweepIntervalValue.Text = "2"
        Me.ToolStripTextBoxSweepIntervalValue.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'ToolStripMenuItemStepDelay
        '
        Me.ToolStripMenuItemStepDelay.BackColor = System.Drawing.Color.MidnightBlue
        Me.ToolStripMenuItemStepDelay.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripTextBoxStepDelayValue})
        Me.ToolStripMenuItemStepDelay.ForeColor = System.Drawing.Color.Chartreuse
        Me.ToolStripMenuItemStepDelay.Name = "ToolStripMenuItemStepDelay"
        Me.ToolStripMenuItemStepDelay.Size = New System.Drawing.Size(207, 22)
        Me.ToolStripMenuItemStepDelay.Text = "Time Between Steps (ms)"
        '
        'ToolStripTextBoxStepDelayValue
        '
        Me.ToolStripTextBoxStepDelayValue.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold)
        Me.ToolStripTextBoxStepDelayValue.Name = "ToolStripTextBoxStepDelayValue"
        Me.ToolStripTextBoxStepDelayValue.Size = New System.Drawing.Size(65, 23)
        '
        'ToolStripMenuItemSweepRate
        '
        Me.ToolStripMenuItemSweepRate.AutoToolTip = true
        Me.ToolStripMenuItemSweepRate.BackColor = System.Drawing.Color.MidnightBlue
        Me.ToolStripMenuItemSweepRate.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripTextBoxSweepRateValue})
        Me.ToolStripMenuItemSweepRate.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ToolStripMenuItemSweepRate.ForeColor = System.Drawing.Color.Chartreuse
        Me.ToolStripMenuItemSweepRate.Name = "ToolStripMenuItemSweepRate"
        Me.ToolStripMenuItemSweepRate.Size = New System.Drawing.Size(207, 22)
        Me.ToolStripMenuItemSweepRate.Tag = ""
        Me.ToolStripMenuItemSweepRate.Text = "Sweep Rate (sec)"
        Me.ToolStripMenuItemSweepRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ToolStripMenuItemSweepRate.ToolTipText = "Used to determine rate.Sweep repeats every n seconds."
        '
        'ToolStripTextBoxSweepRateValue
        '
        Me.ToolStripTextBoxSweepRateValue.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold)
        Me.ToolStripTextBoxSweepRateValue.Name = "ToolStripTextBoxSweepRateValue"
        Me.ToolStripTextBoxSweepRateValue.Size = New System.Drawing.Size(50, 23)
        Me.ToolStripTextBoxSweepRateValue.Text = "10"
        '
        'ToolStripMenuItemDisableCalibration
        '
        Me.ToolStripMenuItemDisableCalibration.BackColor = System.Drawing.Color.MidnightBlue
        Me.ToolStripMenuItemDisableCalibration.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ToolStripMenuItemDisableCalibration.ForeColor = System.Drawing.Color.Chartreuse
        Me.ToolStripMenuItemDisableCalibration.Name = "ToolStripMenuItemDisableCalibration"
        Me.ToolStripMenuItemDisableCalibration.Size = New System.Drawing.Size(207, 22)
        Me.ToolStripMenuItemDisableCalibration.Text = "Disable Calibration"
        Me.ToolStripMenuItemDisableCalibration.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ToolStripMenuItemDisableCalibration.Visible = false
        '
        'ToolStripMenuItemConfigure
        '
        Me.ToolStripMenuItemConfigure.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemMeasurements, Me.ToolStripMenuItemRegionsOfInterest})
        Me.ToolStripMenuItemConfigure.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ToolStripMenuItemConfigure.ForeColor = System.Drawing.Color.Chartreuse
        Me.ToolStripMenuItemConfigure.Name = "ToolStripMenuItemConfigure"
        Me.ToolStripMenuItemConfigure.Size = New System.Drawing.Size(93, 20)
        Me.ToolStripMenuItemConfigure.Text = "Configuration"
        Me.ToolStripMenuItemConfigure.Visible = false
        '
        'ToolStripMenuItemMeasurements
        '
        Me.ToolStripMenuItemMeasurements.BackColor = System.Drawing.Color.MidnightBlue
        Me.ToolStripMenuItemMeasurements.ForeColor = System.Drawing.Color.Chartreuse
        Me.ToolStripMenuItemMeasurements.Name = "ToolStripMenuItemMeasurements"
        Me.ToolStripMenuItemMeasurements.Size = New System.Drawing.Size(199, 22)
        Me.ToolStripMenuItemMeasurements.Text = "Measurement"
        '
        'ToolStripMenuItemRegionsOfInterest
        '
        Me.ToolStripMenuItemRegionsOfInterest.BackColor = System.Drawing.Color.MidnightBlue
        Me.ToolStripMenuItemRegionsOfInterest.ForeColor = System.Drawing.Color.Chartreuse
        Me.ToolStripMenuItemRegionsOfInterest.Name = "ToolStripMenuItemRegionsOfInterest"
        Me.ToolStripMenuItemRegionsOfInterest.Size = New System.Drawing.Size(199, 22)
        Me.ToolStripMenuItemRegionsOfInterest.Text = "Region Of Interest (ROI)"
        '
        'ToolStripMenuItemAccess
        '
        Me.ToolStripMenuItemAccess.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemCustomer, Me.ToolStripMenuItemAppEngineer})
        Me.ToolStripMenuItemAccess.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ToolStripMenuItemAccess.ForeColor = System.Drawing.Color.Chartreuse
        Me.ToolStripMenuItemAccess.Name = "ToolStripMenuItemAccess"
        Me.ToolStripMenuItemAccess.Size = New System.Drawing.Size(55, 20)
        Me.ToolStripMenuItemAccess.Text = "Access"
        Me.ToolStripMenuItemAccess.Visible = false
        '
        'ToolStripMenuItemCustomer
        '
        Me.ToolStripMenuItemCustomer.BackColor = System.Drawing.Color.MidnightBlue
        Me.ToolStripMenuItemCustomer.ForeColor = System.Drawing.Color.Chartreuse
        Me.ToolStripMenuItemCustomer.Name = "ToolStripMenuItemCustomer"
        Me.ToolStripMenuItemCustomer.Size = New System.Drawing.Size(184, 22)
        Me.ToolStripMenuItemCustomer.Text = "Customer"
        '
        'ToolStripMenuItemAppEngineer
        '
        Me.ToolStripMenuItemAppEngineer.BackColor = System.Drawing.Color.MidnightBlue
        Me.ToolStripMenuItemAppEngineer.ForeColor = System.Drawing.Color.Chartreuse
        Me.ToolStripMenuItemAppEngineer.Name = "ToolStripMenuItemAppEngineer"
        Me.ToolStripMenuItemAppEngineer.Size = New System.Drawing.Size(184, 22)
        Me.ToolStripMenuItemAppEngineer.Text = "Application Engineer"
        '
        'PanelLogo
        '
        Me.PanelLogo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.PanelLogo.BackgroundImage = Global.CTS_RF_Sensor_Tool.My.Resources.Resources.CTS_RF_Sensing_logo
        Me.PanelLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PanelLogo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PanelLogo.Location = New System.Drawing.Point(742, 32)
        Me.PanelLogo.Name = "PanelLogo"
        Me.PanelLogo.Size = New System.Drawing.Size(123, 85)
        Me.PanelLogo.TabIndex = 29
        '
        'ButtonStartStopScan
        '
        Me.ButtonStartStopScan.BackgroundImage = CType(resources.GetObject("ButtonStartStopScan.BackgroundImage"),System.Drawing.Image)
        Me.ButtonStartStopScan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ButtonStartStopScan.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ButtonStartStopScan.ForeColor = System.Drawing.Color.Black
        Me.ButtonStartStopScan.Location = New System.Drawing.Point(10, 52)
        Me.ButtonStartStopScan.Name = "ButtonStartStopScan"
        Me.ButtonStartStopScan.Padding = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.ButtonStartStopScan.Size = New System.Drawing.Size(55, 55)
        Me.ButtonStartStopScan.TabIndex = 21
        Me.ButtonStartStopScan.Tag = ""
        Me.ButtonStartStopScan.Text = "Start"
        Me.ButtonStartStopScan.UseVisualStyleBackColor = true
        '
        'PictureBoxSweeping
        '
        Me.PictureBoxSweeping.BackColor = System.Drawing.Color.Transparent
        Me.PictureBoxSweeping.BackgroundImage = CType(resources.GetObject("PictureBoxSweeping.BackgroundImage"),System.Drawing.Image)
        Me.PictureBoxSweeping.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBoxSweeping.InitialImage = Nothing
        Me.PictureBoxSweeping.Location = New System.Drawing.Point(66, 54)
        Me.PictureBoxSweeping.Name = "PictureBoxSweeping"
        Me.PictureBoxSweeping.Size = New System.Drawing.Size(18, 18)
        Me.PictureBoxSweeping.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBoxSweeping.TabIndex = 2
        Me.PictureBoxSweeping.TabStop = false
        '
        'LabelStatus
        '
        Me.LabelStatus.BackColor = System.Drawing.Color.MidnightBlue
        Me.LabelStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LabelStatus.Font = New System.Drawing.Font("MS Reference Sans Serif", 9!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.LabelStatus.ForeColor = System.Drawing.Color.Chartreuse
        Me.LabelStatus.Location = New System.Drawing.Point(11, 488)
        Me.LabelStatus.Name = "LabelStatus"
        Me.LabelStatus.Size = New System.Drawing.Size(856, 23)
        Me.LabelStatus.TabIndex = 62
        Me.LabelStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TimerUpdateProgressBar
        '
        '
        'LabelSelect
        '
        Me.LabelSelect.AutoSize = true
        Me.LabelSelect.ForeColor = System.Drawing.Color.White
        Me.LabelSelect.Location = New System.Drawing.Point(3, 123)
        Me.LabelSelect.Name = "LabelSelect"
        Me.LabelSelect.Size = New System.Drawing.Size(49, 13)
        Me.LabelSelect.TabIndex = 63
        Me.LabelSelect.Text = "<Select>"
        '
        'ButtonWaitSweep
        '
        Me.ButtonWaitSweep.BackgroundImage = Global.CTS_RF_Sensor_Tool.My.Resources.Resources.GreenLEDOn
        Me.ButtonWaitSweep.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ButtonWaitSweep.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ButtonWaitSweep.ForeColor = System.Drawing.Color.Black
        Me.ButtonWaitSweep.Location = New System.Drawing.Point(10, 52)
        Me.ButtonWaitSweep.Name = "ButtonWaitSweep"
        Me.ButtonWaitSweep.Padding = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.ButtonWaitSweep.Size = New System.Drawing.Size(55, 55)
        Me.ButtonWaitSweep.TabIndex = 64
        Me.ButtonWaitSweep.Tag = ""
        Me.ButtonWaitSweep.Text = "Wait"
        Me.ButtonWaitSweep.UseVisualStyleBackColor = true
        '
        'FormRfSensorMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.DimGray
        Me.ClientSize = New System.Drawing.Size(877, 542)
        Me.Controls.Add(Me.LabelSelect)
        Me.Controls.Add(Me.LabelStatus)
        Me.Controls.Add(Me.MenuStripRFSensorMain)
        Me.Controls.Add(Me.LabelCTSCopyright)
        Me.Controls.Add(Me.LabelCTSProprietary)
        Me.Controls.Add(Me.LabelCtsWebAddress)
        Me.Controls.Add(Me.GroupBoxCanControls)
        Me.Controls.Add(Me.PanelLogo)
        Me.Controls.Add(Me.LabelSweepTime)
        Me.Controls.Add(Me.label28)
        Me.Controls.Add(Me.TextBoxSweepDuration)
        Me.Controls.Add(Me.label11)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.label10)
        Me.Controls.Add(Me.PictureBoxIdle)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.TextBoxRunCount)
        Me.Controls.Add(Me.LabelSweepProgress)
        Me.Controls.Add(Me.ProgressBarSweepProgress)
        Me.Controls.Add(Me.LabelOutputFileName)
        Me.Controls.Add(Me.TextBoxCurrentOutputFilename)
        Me.Controls.Add(Me.ButtonSelectOutputFolder)
        Me.Controls.Add(Me.TextBoxDataOutputFolder)
        Me.Controls.Add(Me.LabelOutputFileFolder)
        Me.Controls.Add(Me.labelSweeping)
        Me.Controls.Add(Me.PictureBoxSweeping)
        Me.Controls.Add(Me.LabelTestName)
        Me.Controls.Add(Me.TextBoxTestName)
        Me.Controls.Add(Me.PanelRFSensorDisplay)
        Me.Controls.Add(Me.ButtonStartStopScan)
        Me.Controls.Add(Me.ButtonWaitSweep)
        Me.Icon = CType(resources.GetObject("$this.Icon"),System.Drawing.Icon)
        Me.KeyPreview = true
        Me.MainMenuStrip = Me.MenuStripRFSensorMain
        Me.MaximizeBox = false
        Me.Name = "FormRfSensorMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "RF Sensor Tool"
        Me.PanelRFSensorDisplay.ResumeLayout(false)
        Me.PanelRFSensorDisplay.PerformLayout
        Me.panel2.ResumeLayout(false)
        Me.PanelChartAdjustments.ResumeLayout(false)
        CType(Me.ChartSootLoad,System.ComponentModel.ISupportInitialize).EndInit
        Me.panel3.ResumeLayout(false)
        Me.panel3.PerformLayout
        CType(Me.PictureBoxTempLow,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.PictureBoxTempHigh,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.PictureBoxSootLow,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.PictureBoxSootHigh,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.PictureBoxCalibrationOff,System.ComponentModel.ISupportInitialize).EndInit
        Me.PanelFilterImage.ResumeLayout(false)
        Me.PanelFilterImage.PerformLayout
        Me.PanelLoadBar.ResumeLayout(false)
        CType(Me.PictureBoxIdle,System.ComponentModel.ISupportInitialize).EndInit
        Me.GroupBoxCanControls.ResumeLayout(false)
        Me.GroupBoxCanControls.PerformLayout
        CType(Me.PictureBoxCanActivity,System.ComponentModel.ISupportInitialize).EndInit
        Me.MenuStripRFSensorMain.ResumeLayout(false)
        Me.MenuStripRFSensorMain.PerformLayout
        CType(Me.PictureBoxSweeping,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub

    Private WithEvents TextBoxTestName As System.Windows.Forms.TextBox
    Private PictureBoxSweeping As System.Windows.Forms.PictureBox
    Private labelSweeping As System.Windows.Forms.Label
    Private WithEvents TextBoxDataOutputFolder As System.Windows.Forms.TextBox
    Private WithEvents ButtonSelectOutputFolder As System.Windows.Forms.Button
    Private ProgressBarSweepProgress As System.Windows.Forms.ProgressBar
    Private TextBoxRunCount As System.Windows.Forms.TextBox
    Private label10 As System.Windows.Forms.Label
    Private PictureBoxIdle As System.Windows.Forms.PictureBox
    Private WithEvents ButtonStartStopScan As System.Windows.Forms.Button
    Private label11 As System.Windows.Forms.Label
    Private PanelRFSensorDisplay As System.Windows.Forms.Panel
    Private PanelFilterImage As System.Windows.Forms.Panel
    Private TextBoxSensorTemperature As System.Windows.Forms.TextBox
    Private TextBoxOutletTemperature As System.Windows.Forms.TextBox
    Private TextBoxInletTemperature As System.Windows.Forms.TextBox
    Private PanelLoadBar As System.Windows.Forms.Panel
    Private label15 As System.Windows.Forms.Label
    Private TextBoxSootLoad As System.Windows.Forms.TextBox
    Private label21 As System.Windows.Forms.Label
    Private label20 As System.Windows.Forms.Label
    Private panel3 As System.Windows.Forms.Panel
    Private panel2 As System.Windows.Forms.Panel
    Private label26 As System.Windows.Forms.Label
    Private PictureBoxTempLow As System.Windows.Forms.PictureBox
    Private label25 As System.Windows.Forms.Label
    Private PictureBoxTempHigh As System.Windows.Forms.PictureBox
    Private labelBelowMinDetectable As System.Windows.Forms.Label
    Private PictureBoxSootLow As System.Windows.Forms.PictureBox
    Private labelAboveMaxDetectable As System.Windows.Forms.Label
    Private PictureBoxSootHigh As System.Windows.Forms.PictureBox
    Private label22 As System.Windows.Forms.Label
    Private PictureBoxCalibrationOff As System.Windows.Forms.PictureBox
    Private label29 As System.Windows.Forms.Label
    Private FolderBrowserDialogOutputDirectory As System.Windows.Forms.FolderBrowserDialog
    Private PanelLoadBarPercentage As System.Windows.Forms.Panel
    Private TextBoxSweepDuration As System.Windows.Forms.TextBox
    Private label28 As System.Windows.Forms.Label
    Private label30 As System.Windows.Forms.Label
    Private TextBoxStatus As System.Windows.Forms.TextBox
    Private label12 As System.Windows.Forms.Label
    Private ChartSootLoad As System.Windows.Forms.DataVisualization.Charting.Chart
    Private WithEvents SecondaryMeasurementTextBox As System.Windows.Forms.TextBox
    Private WithEvents PrimaryMeasurementTextBox As System.Windows.Forms.TextBox
    Private WithEvents PanelChartAdjustments As Panel
    Private WithEvents ButtonCompressPlot As Button
    Private WithEvents ButtonExpandPlot As Button
    Private WithEvents ButtonScrollPlotRight As Button
    Private WithEvents ButtonScrollPlotLeft As Button
    Private WithEvents ButtonClearPlot As Button
    Friend WithEvents ButtonCanConfig As Button
    Friend WithEvents ButtonCanEnable As Button
    Friend WithEvents GroupBoxCanControls As GroupBox
    Friend WithEvents LabelCanActivity As Label
    Private WithEvents PictureBoxCanActivity As PictureBox
    Private WithEvents LabelTestName As Label
    Private WithEvents LabelOutputFileFolder As Label
    Private WithEvents LabelOutputFileName As Label
    Private WithEvents LabelSweepProgress As Label
    Private WithEvents Label17 As Label
    Private WithEvents Label18 As Label
    Private WithEvents LabelSweepTime As Label
    Friend WithEvents LabelCtsWebAddress As Label
    Friend WithEvents LabelCTSProprietary As Label
    Friend WithEvents LabelCTSCopyright As Label
    Friend WithEvents MenuStripRFSensorMain As MenuStrip
    Friend WithEvents ToolStripMenuItemSweepSettings As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItemConfigure As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItemAtoDReadingsForAvg As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItemSweepInterval As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItemLockDelay As ToolStripMenuItem
    Friend WithEvents ToolStripTextBoxAtoDReadingsForAvgValue As ToolStripTextBox
    Friend WithEvents ToolStripTextBoxSweepIntervalValue As ToolStripTextBox
    Friend WithEvents ToolStripTextBoxLockDelayValue As ToolStripTextBox
    Friend WithEvents ToolStripMenuItemRegionsOfInterest As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItemMeasurements As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItemDisableCalibration As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItemAccess As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItemAppEngineer As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItemCustomer As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItemOutputPower As ToolStripMenuItem
    Friend WithEvents ToolStripComboBoxOutputPower As ToolStripComboBox
    Friend WithEvents ToolStripMenuItemOutputPowerAux As ToolStripMenuItem
    Friend WithEvents ToolStripComboBoxOutputPowerAux As ToolStripComboBox
    Friend WithEvents ToolStripMenuItemFile As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItemEncryption As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItemSweepRate As ToolStripMenuItem
    Friend WithEvents ToolStripTextBoxSweepRateValue As ToolStripTextBox
    Friend WithEvents ToolStripMenuItemOperatingMode As ToolStripMenuItem
    Friend WithEvents ToolStripComboBoxOperatingMode As ToolStripComboBox
    Private WithEvents PanelLogo As Panel
    Friend WithEvents ToolStripMenuItemMinDetectable As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItemMaxDetectable As ToolStripMenuItem
    Friend WithEvents ToolStripTextBoxMinDetectableValue As ToolStripTextBox
    Friend WithEvents ToolStripTextBoxMaxDetectableValue As ToolStripTextBox
    Friend WithEvents ToolStripMenuItemStepDelay As ToolStripMenuItem
    Friend WithEvents ToolStripTextBoxStepDelayValue As ToolStripTextBox
    Friend WithEvents LabelStatus As Label
    Friend WithEvents TextBoxCurrentOutputFilename As TextBox
    Friend WithEvents TimerUpdateProgressBar As Timer
    Friend WithEvents toolTip1 As ToolTip
    Friend WithEvents LabelSelect As Label
    Private WithEvents ButtonWaitSweep As Button
End Class

