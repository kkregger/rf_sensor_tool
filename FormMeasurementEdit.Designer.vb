﻿Partial Class FormMeasurementEdit
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormMeasurementEdit))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.LabelMinMass = New System.Windows.Forms.Label()
        Me.LabelMaxTemp = New System.Windows.Forms.Label()
        Me.LabelMinTemp = New System.Windows.Forms.Label()
        Me.NumericUpDownMaxDetectable = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDownMinDetectable = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDownMaxTemp = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDownMinTemp = New System.Windows.Forms.NumericUpDown()
        Me.TextBoxMeasurementName = New System.Windows.Forms.TextBox()
        Me.LabelMeasurementName = New System.Windows.Forms.Label()
        Me.ContextMenuStripROIDisplay = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ToolStripMenuItemAddExistingROI = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItemCreateROI = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItemRemoveROI = New System.Windows.Forms.ToolStripMenuItem()
        Me.ButtonSave = New System.Windows.Forms.Button()
        Me.ButtonClose = New System.Windows.Forms.Button()
        Me.ToolTipMeasurementEdit = New System.Windows.Forms.ToolTip(Me.components)
        Me.DataGridViewAssociatedROIs = New System.Windows.Forms.DataGridView()
        Me.PanelMeasurement = New System.Windows.Forms.Panel()
        Me.CheckBoxActive = New System.Windows.Forms.CheckBox()
        Me.MenuStripMeasurementEdit = New System.Windows.Forms.MenuStrip()
        Me.ActionsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RevertConfigurationToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ROIName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UpdatePeriod = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RFStatistics = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ROIWeights = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.NumericUpDownMaxDetectable,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.NumericUpDownMinDetectable,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.NumericUpDownMaxTemp,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.NumericUpDownMinTemp,System.ComponentModel.ISupportInitialize).BeginInit
        Me.ContextMenuStripROIDisplay.SuspendLayout
        CType(Me.DataGridViewAssociatedROIs,System.ComponentModel.ISupportInitialize).BeginInit
        Me.PanelMeasurement.SuspendLayout
        Me.MenuStripMeasurementEdit.SuspendLayout
        Me.SuspendLayout
        '
        'Label1
        '
        Me.Label1.AutoSize = true
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(340, 90)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(107, 15)
        Me.Label1.TabIndex = 114
        Me.Label1.Text = "Max Detectable"
        '
        'LabelMinMass
        '
        Me.LabelMinMass.AutoSize = true
        Me.LabelMinMass.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.LabelMinMass.ForeColor = System.Drawing.Color.White
        Me.LabelMinMass.Location = New System.Drawing.Point(340, 57)
        Me.LabelMinMass.Name = "LabelMinMass"
        Me.LabelMinMass.Size = New System.Drawing.Size(104, 15)
        Me.LabelMinMass.TabIndex = 113
        Me.LabelMinMass.Text = "Min Detectable"
        '
        'LabelMaxTemp
        '
        Me.LabelMaxTemp.AutoSize = true
        Me.LabelMaxTemp.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.LabelMaxTemp.ForeColor = System.Drawing.Color.White
        Me.LabelMaxTemp.Location = New System.Drawing.Point(103, 90)
        Me.LabelMaxTemp.Name = "LabelMaxTemp"
        Me.LabelMaxTemp.Size = New System.Drawing.Size(120, 15)
        Me.LabelMaxTemp.TabIndex = 112
        Me.LabelMaxTemp.Text = "Max Temperature"
        '
        'LabelMinTemp
        '
        Me.LabelMinTemp.AutoSize = true
        Me.LabelMinTemp.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.LabelMinTemp.ForeColor = System.Drawing.Color.White
        Me.LabelMinTemp.Location = New System.Drawing.Point(103, 57)
        Me.LabelMinTemp.Name = "LabelMinTemp"
        Me.LabelMinTemp.Size = New System.Drawing.Size(117, 15)
        Me.LabelMinTemp.TabIndex = 111
        Me.LabelMinTemp.Text = "Min Temperature"
        '
        'NumericUpDownMaxDetectable
        '
        Me.NumericUpDownMaxDetectable.BackColor = System.Drawing.SystemColors.Window
        Me.NumericUpDownMaxDetectable.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.NumericUpDownMaxDetectable.Location = New System.Drawing.Point(247, 88)
        Me.NumericUpDownMaxDetectable.Name = "NumericUpDownMaxDetectable"
        Me.NumericUpDownMaxDetectable.Size = New System.Drawing.Size(90, 21)
        Me.NumericUpDownMaxDetectable.TabIndex = 110
        '
        'NumericUpDownMinDetectable
        '
        Me.NumericUpDownMinDetectable.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.NumericUpDownMinDetectable.Location = New System.Drawing.Point(247, 55)
        Me.NumericUpDownMinDetectable.Name = "NumericUpDownMinDetectable"
        Me.NumericUpDownMinDetectable.Size = New System.Drawing.Size(90, 21)
        Me.NumericUpDownMinDetectable.TabIndex = 109
        '
        'NumericUpDownMaxTemp
        '
        Me.NumericUpDownMaxTemp.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.NumericUpDownMaxTemp.Location = New System.Drawing.Point(10, 88)
        Me.NumericUpDownMaxTemp.Maximum = New Decimal(New Integer() {1500, 0, 0, 0})
        Me.NumericUpDownMaxTemp.Name = "NumericUpDownMaxTemp"
        Me.NumericUpDownMaxTemp.Size = New System.Drawing.Size(90, 21)
        Me.NumericUpDownMaxTemp.TabIndex = 108
        '
        'NumericUpDownMinTemp
        '
        Me.NumericUpDownMinTemp.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.NumericUpDownMinTemp.Location = New System.Drawing.Point(10, 55)
        Me.NumericUpDownMinTemp.Maximum = New Decimal(New Integer() {1500, 0, 0, 0})
        Me.NumericUpDownMinTemp.Name = "NumericUpDownMinTemp"
        Me.NumericUpDownMinTemp.Size = New System.Drawing.Size(90, 21)
        Me.NumericUpDownMinTemp.TabIndex = 107
        '
        'TextBoxMeasurementName
        '
        Me.TextBoxMeasurementName.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.TextBoxMeasurementName.Location = New System.Drawing.Point(10, 22)
        Me.TextBoxMeasurementName.Name = "TextBoxMeasurementName"
        Me.TextBoxMeasurementName.Size = New System.Drawing.Size(622, 21)
        Me.TextBoxMeasurementName.TabIndex = 115
        Me.TextBoxMeasurementName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LabelMeasurementName
        '
        Me.LabelMeasurementName.AutoSize = true
        Me.LabelMeasurementName.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.LabelMeasurementName.Location = New System.Drawing.Point(253, 4)
        Me.LabelMeasurementName.Name = "LabelMeasurementName"
        Me.LabelMeasurementName.Size = New System.Drawing.Size(137, 15)
        Me.LabelMeasurementName.TabIndex = 116
        Me.LabelMeasurementName.Text = "Measurement Name"
        '
        'ContextMenuStripROIDisplay
        '
        Me.ContextMenuStripROIDisplay.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ContextMenuStripROIDisplay.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItemAddExistingROI, Me.ToolStripMenuItemCreateROI, Me.ToolStripMenuItemRemoveROI})
        Me.ContextMenuStripROIDisplay.Name = "ContextMenuStripROIDisplay"
        Me.ContextMenuStripROIDisplay.Size = New System.Drawing.Size(182, 70)
        '
        'ToolStripMenuItemAddExistingROI
        '
        Me.ToolStripMenuItemAddExistingROI.Name = "ToolStripMenuItemAddExistingROI"
        Me.ToolStripMenuItemAddExistingROI.Size = New System.Drawing.Size(181, 22)
        Me.ToolStripMenuItemAddExistingROI.Text = "Add Existing ROI"
        '
        'ToolStripMenuItemCreateROI
        '
        Me.ToolStripMenuItemCreateROI.Name = "ToolStripMenuItemCreateROI"
        Me.ToolStripMenuItemCreateROI.Size = New System.Drawing.Size(181, 22)
        Me.ToolStripMenuItemCreateROI.Text = "Create ROI"
        '
        'ToolStripMenuItemRemoveROI
        '
        Me.ToolStripMenuItemRemoveROI.Name = "ToolStripMenuItemRemoveROI"
        Me.ToolStripMenuItemRemoveROI.Size = New System.Drawing.Size(181, 22)
        Me.ToolStripMenuItemRemoveROI.Text = "Remove ROI"
        '
        'ButtonSave
        '
        Me.ButtonSave.BackColor = System.Drawing.Color.Cyan
        Me.ButtonSave.Enabled = false
        Me.ButtonSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ButtonSave.ForeColor = System.Drawing.Color.Black
        Me.ButtonSave.Location = New System.Drawing.Point(558, 56)
        Me.ButtonSave.Name = "ButtonSave"
        Me.ButtonSave.Size = New System.Drawing.Size(75, 23)
        Me.ButtonSave.TabIndex = 145
        Me.ButtonSave.Text = "Save"
        Me.ButtonSave.UseVisualStyleBackColor = false
        '
        'ButtonClose
        '
        Me.ButtonClose.BackColor = System.Drawing.Color.Gold
        Me.ButtonClose.Enabled = false
        Me.ButtonClose.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ButtonClose.ForeColor = System.Drawing.Color.Black
        Me.ButtonClose.Location = New System.Drawing.Point(558, 87)
        Me.ButtonClose.Name = "ButtonClose"
        Me.ButtonClose.Size = New System.Drawing.Size(75, 23)
        Me.ButtonClose.TabIndex = 146
        Me.ButtonClose.Text = "Close"
        Me.ButtonClose.UseVisualStyleBackColor = false
        '
        'DataGridViewAssociatedROIs
        '
        Me.DataGridViewAssociatedROIs.AllowUserToAddRows = false
        Me.DataGridViewAssociatedROIs.AllowUserToDeleteRows = false
        Me.DataGridViewAssociatedROIs.AllowUserToResizeColumns = false
        Me.DataGridViewAssociatedROIs.AllowUserToResizeRows = false
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.LightGray
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewAssociatedROIs.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridViewAssociatedROIs.BackgroundColor = System.Drawing.Color.MidnightBlue
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.LightGray
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewAssociatedROIs.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridViewAssociatedROIs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridViewAssociatedROIs.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ROIName, Me.UpdatePeriod, Me.RFStatistics, Me.ROIWeights})
        Me.DataGridViewAssociatedROIs.ContextMenuStrip = Me.ContextMenuStripROIDisplay
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewAssociatedROIs.DefaultCellStyle = DataGridViewCellStyle7
        Me.DataGridViewAssociatedROIs.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke
        Me.DataGridViewAssociatedROIs.GridColor = System.Drawing.Color.Chartreuse
        Me.DataGridViewAssociatedROIs.Location = New System.Drawing.Point(11, 145)
        Me.DataGridViewAssociatedROIs.MultiSelect = false
        Me.DataGridViewAssociatedROIs.Name = "DataGridViewAssociatedROIs"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle8.BackColor = System.Drawing.Color.LightGray
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewAssociatedROIs.RowHeadersDefaultCellStyle = DataGridViewCellStyle8
        Me.DataGridViewAssociatedROIs.RowHeadersVisible = false
        Me.DataGridViewAssociatedROIs.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle9.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewAssociatedROIs.RowsDefaultCellStyle = DataGridViewCellStyle9
        Me.DataGridViewAssociatedROIs.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.White
        Me.DataGridViewAssociatedROIs.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black
        Me.DataGridViewAssociatedROIs.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.SystemColors.Highlight
        Me.DataGridViewAssociatedROIs.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        Me.DataGridViewAssociatedROIs.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.DataGridViewAssociatedROIs.Size = New System.Drawing.Size(623, 501)
        Me.DataGridViewAssociatedROIs.TabIndex = 151
        Me.DataGridViewAssociatedROIs.TabStop = false
        Me.ToolTipMeasurementEdit.SetToolTip(Me.DataGridViewAssociatedROIs, "Right-click to see menu.")
        '
        'PanelMeasurement
        '
        Me.PanelMeasurement.Controls.Add(Me.CheckBoxActive)
        Me.PanelMeasurement.Controls.Add(Me.LabelMeasurementName)
        Me.PanelMeasurement.Controls.Add(Me.NumericUpDownMinTemp)
        Me.PanelMeasurement.Controls.Add(Me.ButtonSave)
        Me.PanelMeasurement.Controls.Add(Me.NumericUpDownMaxTemp)
        Me.PanelMeasurement.Controls.Add(Me.ButtonClose)
        Me.PanelMeasurement.Controls.Add(Me.NumericUpDownMinDetectable)
        Me.PanelMeasurement.Controls.Add(Me.NumericUpDownMaxDetectable)
        Me.PanelMeasurement.Controls.Add(Me.TextBoxMeasurementName)
        Me.PanelMeasurement.Controls.Add(Me.LabelMinTemp)
        Me.PanelMeasurement.Controls.Add(Me.Label1)
        Me.PanelMeasurement.Controls.Add(Me.LabelMaxTemp)
        Me.PanelMeasurement.Controls.Add(Me.LabelMinMass)
        Me.PanelMeasurement.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.PanelMeasurement.Location = New System.Drawing.Point(1, 25)
        Me.PanelMeasurement.Name = "PanelMeasurement"
        Me.PanelMeasurement.Size = New System.Drawing.Size(643, 120)
        Me.PanelMeasurement.TabIndex = 152
        '
        'CheckBoxActive
        '
        Me.CheckBoxActive.AutoCheck = false
        Me.CheckBoxActive.AutoSize = true
        Me.CheckBoxActive.Location = New System.Drawing.Point(472, 90)
        Me.CheckBoxActive.Name = "CheckBoxActive"
        Me.CheckBoxActive.Size = New System.Drawing.Size(63, 19)
        Me.CheckBoxActive.TabIndex = 147
        Me.CheckBoxActive.Text = "Active"
        Me.CheckBoxActive.UseVisualStyleBackColor = true
        '
        'MenuStripMeasurementEdit
        '
        Me.MenuStripMeasurementEdit.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ActionsToolStripMenuItem})
        Me.MenuStripMeasurementEdit.Location = New System.Drawing.Point(0, 0)
        Me.MenuStripMeasurementEdit.Name = "MenuStripMeasurementEdit"
        Me.MenuStripMeasurementEdit.Size = New System.Drawing.Size(644, 24)
        Me.MenuStripMeasurementEdit.TabIndex = 154
        Me.MenuStripMeasurementEdit.Text = "MenuStrip1"
        '
        'ActionsToolStripMenuItem
        '
        Me.ActionsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RevertConfigurationToolStripMenuItem})
        Me.ActionsToolStripMenuItem.Font = New System.Drawing.Font("Segoe UI", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ActionsToolStripMenuItem.Name = "ActionsToolStripMenuItem"
        Me.ActionsToolStripMenuItem.Size = New System.Drawing.Size(60, 20)
        Me.ActionsToolStripMenuItem.Text = "Actions"
        '
        'RevertConfigurationToolStripMenuItem
        '
        Me.RevertConfigurationToolStripMenuItem.Name = "RevertConfigurationToolStripMenuItem"
        Me.RevertConfigurationToolStripMenuItem.Size = New System.Drawing.Size(192, 22)
        Me.RevertConfigurationToolStripMenuItem.Text = "Revert Configuration"
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn1.DefaultCellStyle = DataGridViewCellStyle10
        Me.DataGridViewTextBoxColumn1.HeaderText = "Region Of Interest (ROI) Name"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 235
        '
        'DataGridViewTextBoxColumn2
        '
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle11.Format = "N0"
        DataGridViewCellStyle11.NullValue = "0"
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        Me.DataGridViewTextBoxColumn2.DefaultCellStyle = DataGridViewCellStyle11
        Me.DataGridViewTextBoxColumn2.HeaderText = "Period (ms)"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn2.Width = 90
        '
        'DataGridViewTextBoxColumn3
        '
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle12.Format = "N0"
        DataGridViewCellStyle12.NullValue = "0"
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        Me.DataGridViewTextBoxColumn3.DefaultCellStyle = DataGridViewCellStyle12
        Me.DataGridViewTextBoxColumn3.HeaderText = "Weights"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn3.Width = 70
        '
        'DataGridViewTextBoxColumn4
        '
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle13.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle13.Format = "N0"
        DataGridViewCellStyle13.NullValue = "0"
        DataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        Me.DataGridViewTextBoxColumn4.DefaultCellStyle = DataGridViewCellStyle13
        Me.DataGridViewTextBoxColumn4.HeaderText = "Weights"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn4.Width = 70
        '
        'ROIName
        '
        Me.ROIName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.Chartreuse
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.ROIName.DefaultCellStyle = DataGridViewCellStyle3
        Me.ROIName.HeaderText = "Region Of Interest (ROI) Name"
        Me.ROIName.Name = "ROIName"
        Me.ROIName.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.ROIName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.ROIName.Width = 235
        '
        'UpdatePeriod
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.Chartreuse
        DataGridViewCellStyle4.Format = "N0"
        DataGridViewCellStyle4.NullValue = "0"
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        Me.UpdatePeriod.DefaultCellStyle = DataGridViewCellStyle4
        Me.UpdatePeriod.HeaderText = "Period (ms)"
        Me.UpdatePeriod.Name = "UpdatePeriod"
        Me.UpdatePeriod.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.UpdatePeriod.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.UpdatePeriod.Width = 90
        '
        'RFStatistics
        '
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.Chartreuse
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        Me.RFStatistics.DefaultCellStyle = DataGridViewCellStyle5
        Me.RFStatistics.HeaderText = "RF Statistics"
        Me.RFStatistics.Name = "RFStatistics"
        Me.RFStatistics.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.RFStatistics.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.RFStatistics.Width = 225
        '
        'ROIWeights
        '
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.Chartreuse
        DataGridViewCellStyle6.Format = "N0"
        DataGridViewCellStyle6.NullValue = "0"
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        Me.ROIWeights.DefaultCellStyle = DataGridViewCellStyle6
        Me.ROIWeights.HeaderText = "Weights"
        Me.ROIWeights.Name = "ROIWeights"
        Me.ROIWeights.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.ROIWeights.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.ROIWeights.Width = 70
        '
        'FormMeasurementEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.DimGray
        Me.ClientSize = New System.Drawing.Size(644, 656)
        Me.Controls.Add(Me.MenuStripMeasurementEdit)
        Me.Controls.Add(Me.PanelMeasurement)
        Me.Controls.Add(Me.DataGridViewAssociatedROIs)
        Me.ForeColor = System.Drawing.Color.White
        Me.Icon = CType(resources.GetObject("$this.Icon"),System.Drawing.Icon)
        Me.Name = "FormMeasurementEdit"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Measurement Edit"
        CType(Me.NumericUpDownMaxDetectable,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.NumericUpDownMinDetectable,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.NumericUpDownMaxTemp,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.NumericUpDownMinTemp,System.ComponentModel.ISupportInitialize).EndInit
        Me.ContextMenuStripROIDisplay.ResumeLayout(false)
        CType(Me.DataGridViewAssociatedROIs,System.ComponentModel.ISupportInitialize).EndInit
        Me.PanelMeasurement.ResumeLayout(false)
        Me.PanelMeasurement.PerformLayout
        Me.MenuStripMeasurementEdit.ResumeLayout(false)
        Me.MenuStripMeasurementEdit.PerformLayout
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents LabelMinMass As Label
    Friend WithEvents LabelMaxTemp As Label
    Friend WithEvents LabelMinTemp As Label
    Friend WithEvents NumericUpDownMaxDetectable As NumericUpDown
    Friend WithEvents NumericUpDownMinDetectable As NumericUpDown
    Friend WithEvents NumericUpDownMaxTemp As NumericUpDown
    Friend WithEvents NumericUpDownMinTemp As NumericUpDown
    Friend WithEvents TextBoxMeasurementName As TextBox
    Friend WithEvents LabelMeasurementName As Label
    Private WithEvents ButtonSave As Button
    Private WithEvents ButtonClose As Button
    Friend WithEvents ContextMenuStripROIDisplay As ContextMenuStrip
    Friend WithEvents ToolStripMenuItemCreateROI As ToolStripMenuItem
    Friend WithEvents ToolTipMeasurementEdit As ToolTip
    Friend WithEvents ToolStripMenuItemRemoveROI As ToolStripMenuItem
    Friend WithEvents DataGridViewAssociatedROIs As DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As DataGridViewTextBoxColumn
    Friend WithEvents PanelMeasurement As Panel
    Friend WithEvents ToolStripMenuItemAddExistingROI As ToolStripMenuItem
    Friend WithEvents MenuStripMeasurementEdit As MenuStrip
    Friend WithEvents ActionsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RevertConfigurationToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CheckBoxActive As CheckBox
    Friend WithEvents ROIName As DataGridViewTextBoxColumn
    Friend WithEvents UpdatePeriod As DataGridViewTextBoxColumn
    Friend WithEvents RFStatistics As DataGridViewTextBoxColumn
    Friend WithEvents ROIWeights As DataGridViewTextBoxColumn
End Class
