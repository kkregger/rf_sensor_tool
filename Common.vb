﻿Imports System.Numerics
Imports System.Collections.Concurrent
Imports System.Runtime.InteropServices

Public Class Common '==================================================================================================

#Region "imported functions"

    <DllImport("Kernel32.dll")>
    Private Shared Function QueryPerformanceCounter(ByRef lpPerformanceCount As Long) As Boolean
    End Function

    <DllImport("Kernel32.dll")>
    Private Shared Function QueryPerformanceFrequency(ByRef lpFrequency As Long) As Boolean
    End Function

#End Region ' imported functions

    ' Used to calculate elapsed time.
    Private Shared _counterPeriod As Double

    Shared Sub New()

        ' Used to derive the high performance counter period.
        Dim counterFreq As Long

        ' Get the frequency of the Performance Counter
        QueryPerformanceFrequency(counterFreq)

        ' Store the period of the Performance Counter * 1000 to scale to msecs.
        _counterPeriod = (1.0 / counterFreq) * 1000

    End Sub

    ' Default scan frequency ranges.
    Public Const DefaultStartFreqRange1   As Double   = 400000   ' kHZ
    Public Const DefaultStartFreqRange2   As Double   = 1100000  ' kHZ
    Public Const DefaultStartFreqRange3   As Double   = 1800000  ' kHZ
    Public Const DefaultFrequencyStepSize As Double   = 1000     ' kHz
    Public Const DefaultStepCount         As UShort   = 699      ' total = 700 (first data point index = 0)
    Public Const DefaultStepCountFinal    As UShort   = 700      ' total = 701 (first data point index = 0)
    Public Const TotalStepCount           As UShort   = 2101     ' (700 * 2) + 701
    Public Const DefaultSweepCount        As Byte     = 3

    ' Sweep setting maximums.
    Public Const SweepRateMax             As UInteger = 86400 ' Sweep repeats every 86400 seconds (24 hours).
    Public Const DetectableMax            As UInteger = 65535
    Public Const StepDelayMax             As Byte     = 255
    Public Const ReadingsForAvgMax        As Byte     = 255
    Public Const LockDelayMax             As UShort   = 65535 ' microseconds

    ' Sweep setting defaults.
    Public Const ReadingsForAvgDefault    As Byte     = 3
    Public Const LockDelayDefault         As UShort   = 200    ' microseconds
    Public Const SweepIntervalDefault     As UShort   = 2      ' milliseconds
    Public Const StepDelayDefault         As Byte     = 5      ' milliseconds
    Public Const StartFreqDefault         As Double   = 400000 ' kHz
    Public Const FreqStepSizeDefault      As Double   = 1.0    ' KHz
    Public Const FreqStepCountDefault     As UShort   = 512
    Public Const ScanDataMsgRateDefault   As Byte     = 1      ' milliseconds, actual rate = 1/ScanDataMsgRate
    Public Const SweepRateDefault         As UInteger = 10     ' Sweep repeats every 5 seconds.

    ' Delay Times.
    Public Const SweepDataMsgTimerInterval   As Integer = 100    ' milliseconds
    Public Const SweepRateMin                As Integer = 500    ' milliseconds
    Public Const ThreadJoinTimeout           As Long    = 1000000' .1s / 100ns "tick" = 1000000
    Public Const TimerUnlockKeyPressTimeout  As Integer = 5000   ' milliseconds
    Public Const TimerCanActivityTimeout     As Integer = 1000   ' milliseconds
    Public Const TimerReadSensorStateTimeout As Integer = 50     ' milliseconds

    ' Constants used in RF measurement processing.
    Public Const ZeroDbMillivolts         As Double   = 900
    Public Const MillivoltToDbSlope       As Double   = 30
    Public Const MillivoltToPhaseSlope    As Double   = 10

    ' Default value for Configuration IDs.
    Public Const DefaultID                As Integer  = 1000

    ' Binary weighted Measurement Limits flag values.
    Public Const TemperatureLow           As Byte     = &H01
    Public Const TemperatureHigh          As Byte     = &H02
    Public Const MeasuredLevelLow         As Byte     = &H04
    Public Const MeasuredLevelHigh        As Byte     = &H08

    ' ROI Edit NumericUpDown SweepInterval and Statistic Weight maximums.
    Public Const SweepIntervalMax         As UShort   = 65535 ' Milliseconds
    Public Const MaxStatisticWeight       As Decimal  = 255

    ' ROI Edit DataGridView Column numbers.
    Public Const RoiNameColumn            As Integer  = 0
    Public Const RoiSweepIntervalColumn   As Integer  = 1
    Public Const RoiRfStatisticColumn     As Integer  = 2
    Public Const RoiStatisticWeightColumn As Integer  = 3

    ' HRESULT Error Codes
    Public Const ErrorSharingViolation    As Integer  = &H20
    Public Const ErrorLockViolation       As Integer  = &H21

    ' Microcontroller ID, used in Cal file selection.
    Public Shared MicroIdStr As String() = {"PLACEHOLDER",
                                            "AM64_",
                                            "MD64_",
                                            "MD128_",
                                            "MD256_",
                                            "MD512_",
                                            "MP512_",
                                            "MD128_",
                                            "MD256_",
                                            "MD512_"}

    ' Dictionary of RF Statistic name strings.
    Public Shared ReadOnly StatisticDict As ConcurrentDictionary(Of String, RfStatistic) =
        New ConcurrentDictionary(Of String, RfStatistic) _
        (
            New Dictionary(Of String, RfStatistic) From
            {
                {"None"                    , RfStatistic.None},
                {"SummedPhase"             , RfStatistic.SummedPhase},
                {"AveragePhase"            , RfStatistic.AveragePhase},
                {"SumMagnitudes"           , RfStatistic.SumMagnitudes},
                {"AverageMagnitude"        , RfStatistic.AverageMagnitude},
                {"PeakMagnitude"           , RfStatistic.PeakMagnitude},
                {"FreqAtPeakMagnitude"     , RfStatistic.FreqAtPeakMagnitude},
                {"PhaseAtPeakMagnitude"    , RfStatistic.PhaseAtPeakMagnitude},
                {"MinMagnitude"            , RfStatistic.MinMagnitude},
                {"FreqAtMinMagnitude"      , RfStatistic.FreqAtMinMagnitude},
                {"PhaseAtMinMagnitude"     , RfStatistic.PhaseAtMinMagnitude},
                {"Degree90CrossoverFreq"   , RfStatistic.Degree90CrossoverFreq},
                {"MagnitudeAtCrossoverFreq", RfStatistic.MagnitudeAtCrossoverFreq},
                {"HalfWidthIfMagnitude"    , RfStatistic.HalfWidthIfMagnitude},
                {"QualityOfMagnitude"      , RfStatistic.QualityOfMagnitude}
            }
        )

    ' Dictionary of RF Statistic name strings.
    Public Readonly Shared StatisticNameDict As ConcurrentDictionary(Of RfStatistic, String) =
        New ConcurrentDictionary(Of RfStatistic, String) _
        (
            New Dictionary(Of RfStatistic, String) From _
            {
                {RfStatistic.None                    , String.Empty},
                {RfStatistic.SummedPhase             , "Summed Phase"},
                {RfStatistic.AveragePhase            , "Average Phase"},
                {RfStatistic.SumMagnitudes           , "Sum Magnitudes"},
                {RfStatistic.AverageMagnitude        , "Average Magnitude"},
                {RfStatistic.PeakMagnitude           , "Peak Magnitude"},
                {RfStatistic.FreqAtPeakMagnitude     , "Frequency At Peak Magnitude"},
                {RfStatistic.PhaseAtPeakMagnitude    , "Phase At Peak Magnitude"},
                {RfStatistic.MinMagnitude            , "Min Magnitude"},
                {RfStatistic.FreqAtMinMagnitude      , "Frequency At Min Magnitude"},
                {RfStatistic.PhaseAtMinMagnitude     , "Phase At Min Magnitude"},
                {RfStatistic.Degree90CrossoverFreq   , "90 Deg Crossover Frequency"},
                {RfStatistic.MagnitudeAtCrossoverFreq, "Magnitude At Crossover Freq"},
                {RfStatistic.HalfWidthIfMagnitude    , "Half Width If Magnitude"},
                {RfStatistic.QualityOfMagnitude      , "Quality Of Magnitude"}
            }
        )

    ' Dictionary of RF Statistic acronym strings.
    Public Readonly Shared StatisticAcronymDict As ConcurrentDictionary(Of RfStatistic, String) =
        New ConcurrentDictionary(Of RfStatistic, String) _
        (
            New Dictionary(Of RfStatistic, String) From _
            {
                {RfStatistic.None                    , String.Empty},
                {RfStatistic.SummedPhase             , "SP"},
                {RfStatistic.AveragePhase            , "AP"},
                {RfStatistic.SumMagnitudes           , "SM"},
                {RfStatistic.AverageMagnitude        , "AM"},
                {RfStatistic.PeakMagnitude           , "PM"},
                {RfStatistic.FreqAtPeakMagnitude     , "FAPM"},
                {RfStatistic.PhaseAtPeakMagnitude    , "PAPM"},
                {RfStatistic.MinMagnitude            , "MM"},
                {RfStatistic.FreqAtMinMagnitude      , "FAMM"},
                {RfStatistic.PhaseAtMinMagnitude     , "PAMM"},
                {RfStatistic.Degree90CrossoverFreq   , "NDCF"},
                {RfStatistic.MagnitudeAtCrossoverFreq, "MACF"},
                {RfStatistic.HalfWidthIfMagnitude    , "HWIM"},
                {RfStatistic.QualityOfMagnitude      , "QOM"}
            }
        )

    Public Shared ReadOnly Property HighResTime() As Double
        Get
            ' Returns the high resolution elapsed time in msec.
            Dim hpCounterValue As Long
            QueryPerformanceCounter(hpCounterValue)
            Return hpCounterValue * _counterPeriod
        End Get
    End Property

End Class

Public Class MyRenderer
    Inherits ToolStripProfessionalRenderer

    Protected Overloads Overrides Sub OnRenderMenuItemBackground(ByVal e As ToolStripItemRenderEventArgs)
        Dim rc As New Rectangle(Point.Empty, e.Item.Size)
        'Dim c As Color = IIf(e.Item.Selected, Color.While, Color.DarkBlue)
        Using brush As New SolidBrush(Color.MidnightBlue)
            e.Graphics.FillRectangle(brush, rc)
        End Using
    End Sub

    protected Overrides Sub OnRenderArrow (ByVal e As ToolStripArrowRenderEventArgs )
        Dim tsMenuItem = CType(e.Item, ToolStripMenuItem)
        if (tsMenuItem IsNot Nothing) Then
            e.ArrowColor = Color.White
        End If
        MyBase.OnRenderArrow(e)
    End Sub
End Class

Public Enum SweepSequenceState As Byte
    First
    Second
    Final
    Complete
End Enum
