﻿Imports System.Collections.Concurrent

Partial Public Class FormRoiEdit
    Inherits Form

    Private _roiOriginal     As RegionOfInterest
    Private _editResult      As DialogResult = DialogResult.Cancel
    Private _measurementName As String

    ' Event to raise when there is an error to be logged.
    Private Shared Event LogErrorEvent As ActivityLog.LogErrorStackDelegate
    Private Shared Event LogExceptionEvent As ActivityLog.LogExceptionDelegate

    Public Sub New(ByVal roi As RegionOfInterest)
        Me.New(roi, Nothing)
    End Sub

    Public Sub New(ByVal roi As RegionOfInterest, ByVal measureName As String)

        ' Called directly when editing a Measurement Associated ROI.

        ' This call is required by the designer.
        InitializeComponent()

        Try
            AddHandler LogErrorEvent, AddressOf ActivityLog.LogError
            AddHandler LogExceptionEvent, AddressOf ActivityLog.LogError

            ' Unmodified copy of the ROI that was passed in.
            _roiOriginal   = New RegionOfInterest(roi)

            ' Make working copy. Constructor does a deep copy.
            RoiWorkingCopy = New RegionOfInterest(roi)

            ' Name of the Measurement that the ROI belongs to, if associated.
            _measurementName = measureName

            If (RoiWorkingCopy IsNot Nothing) Then

                ' Initialize the form control values.
                TextBoxROIName.Text                   = RoiWorkingCopy.Name
                ComboBoxROIOpModeSelect.SelectedIndex = ComboBoxROIOpModeSelect.FindStringExact(RoiWorkingCopy.OperatingMode)
                ROI_StartFreqNumericUpDown.Value      = RoiWorkingCopy.StartFrequency
                ROI_StepSizeNumericUpDown.Value       = RoiWorkingCopy.FreqStepSize
                ROI_FreqStepsNumericUpDown.Value      = RoiWorkingCopy.StepCount
                NumericUpDownROISweepInterval.Value   = RoiWorkingCopy.SweepInterval

                ' Initialize the RF Statistic checkboxes.
                InitROIStatisticControls(RoiWorkingCopy.StatisticWeights)
                GenerateAndDisplayROINameString()
            End If

            ' Now that the NumericUpDown controls are intialized, add their ValuesChanged eventhandlers.
            ' The event handlers were causing exceptions during initialization.
            AddHandler NumericUpDownSummedPhaseWeight.ValueChanged       , AddressOf NumericUpDownWeight_ValueChanged
            AddHandler NumericUpDownAveragePhaseWeight.ValueChanged      , AddressOf NumericUpDownWeight_ValueChanged
            AddHandler NumericUpDownSumMagnitudesWeight.ValueChanged     , AddressOf NumericUpDownWeight_ValueChanged
            AddHandler NumericUpDownAverageMagWeight.ValueChanged        , AddressOf NumericUpDownWeight_ValueChanged
            AddHandler NumericUpDownPeakMagnitudeWeight.ValueChanged     , AddressOf NumericUpDownWeight_ValueChanged
            AddHandler NumericUpDownFreqAtPeakMagWeight.ValueChanged     , AddressOf NumericUpDownWeight_ValueChanged
            AddHandler NumericUpDownPhaseAtPeakMagWeight.ValueChanged    , AddressOf NumericUpDownWeight_ValueChanged
            AddHandler NumericUpDownMinMagnitudeWeight.ValueChanged      , AddressOf NumericUpDownWeight_ValueChanged
            AddHandler NumericUpDownFreqAtMinMagWeight.ValueChanged      , AddressOf NumericUpDownWeight_ValueChanged
            AddHandler NumericUpDownPhaseAtMinMagWeight.ValueChanged     , AddressOf NumericUpDownWeight_ValueChanged
            AddHandler NumericUpDownDeg90CrossoverFreqWeight.ValueChanged, AddressOf NumericUpDownWeight_ValueChanged
            AddHandler NumericUpDownMagAtCrossoverFreq.ValueChanged      , AddressOf NumericUpDownWeight_ValueChanged
            AddHandler NumericUpDownHalfWidthAtMagWeight.ValueChanged    , AddressOf NumericUpDownWeight_ValueChanged
            AddHandler NumericUpDownQualityOfMagnitudeWeight.ValueChanged, AddressOf NumericUpDownWeight_ValueChanged

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Public Shared Property RoiWorkingCopy As RegionOfInterest

    Private Sub FormROIEdit_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        AddHandler ComboBoxROIOpModeSelect.SelectedIndexChanged, AddressOf ComboBoxROIOpModeSelect_SelectedIndexChanged
        AddHandler NumericUpDownROISweepInterval.ValueChanged  , AddressOf NumericUpDownROISweepInterval_ValueChanged
        AddHandler ROI_StartFreqNumericUpDown.ValueChanged     , AddressOf ROI_StartFreqNumericUpDown_ValueChanged
        AddHandler ROI_StepSizeNumericUpDown.ValueChanged      , AddressOf ROI_StepSizeNumericUpDown_ValueChanged
        AddHandler ROI_FreqStepsNumericUpDown.ValueChanged     , AddressOf ROI_FreqStepsNumericUpDown_ValueChanged
    End Sub
    
    Private Sub FormROIEdit_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing

        Try
            If (_roiOriginal <> RoiWorkingCopy) Then
                Dim mbResult = MsgBox("Are You Sure?" & vbCrLf &
                                      "All changes will be discarded.",
                                      MsgBoxStyle.SystemModal + MsgBoxStyle.YesNo)
                If (mbResult = MsgBoxResult.No) Then
                    e.Cancel = True
                Else
                    DialogResult = _editResult
                End If
            End If

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Private Sub GenerateAndDisplayROINameString()

        Try
            Dim endFreq As Double = ROI_StartFreqNumericUpDown.Value

            TextBoxROIName.Text  = ComboBoxROIOpModeSelect.SelectedItem & "," &
                                   NumericUpDownROISweepInterval.Value.ToString() & "," &
                                   ROI_StartFreqNumericUpDown.Value.ToString() & "-"
            endFreq             += ROI_StepSizeNumericUpDown.Value * ROI_FreqStepsNumericUpDown.Value
            TextBoxROIName.Text &= endFreq.ToString() & "," & ROI_FreqStepsNumericUpDown.Value.ToString() & ","
            TextBoxROIName.Text &= ROI_StepSizeNumericUpDown.Value.ToString() & "," & GenerateStatAcronymString()
            TextBoxROIName.Text  = TextBoxROIName.Text.TrimEnd(","c)
            RoiWorkingCopy.Name  = TextBoxROIName.Text
            ButtonSave.Enabled   = _roiOriginal <> RoiWorkingCopy

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Private Function GenerateStatAcronymString() As String

        Try
            Dim statAcronyms = String.Empty

            ' Generate the statistic acronym sting part of the ROI name.
            For Each statWeight In RoiWorkingCopy.StatisticWeights
                statAcronyms &= Common.StatisticAcronymDict(statWeight.Statistic)
                statAcronyms &= "-" & statWeight.Weight.ToString() & ","
            Next

            Return statAcronyms.TrimEnd("'"c)

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Function

    Private Function GetROIStatisticValue(ByVal cb As CheckBox) As UInteger

        Try
            Dim statisticValue As RfStatistic = RfStatistic.None

            If (ReferenceEquals(cb, CheckBoxSummedPhase)) Then
                statisticValue = RfStatistic.SummedPhase
            ElseIf (ReferenceEquals(cb, CheckBoxAvgPhase)) Then
                statisticValue = RfStatistic.AveragePhase
            ElseIf (ReferenceEquals(cb, CheckBoxSumMagnitude)) Then
                statisticValue = RfStatistic.SumMagnitudes
            ElseIf (ReferenceEquals(cb, CheckBoxAvgMagnitude)) Then
                statisticValue = RfStatistic.AverageMagnitude
            ElseIf (ReferenceEquals(cb, CheckBoxPeakMagnitude)) Then
                statisticValue = RfStatistic.PeakMagnitude
            ElseIf (ReferenceEquals(cb, CheckBoxFreqAtPeakMagnitude)) Then
                statisticValue = RfStatistic.FreqAtPeakMagnitude
            ElseIf (ReferenceEquals(cb, CheckBoxPhaseAtPeakMagnitude)) Then
                statisticValue = RfStatistic.PhaseAtPeakMagnitude
            ElseIf (ReferenceEquals(cb, CheckBoxMinMagnitude)) Then
                statisticValue = RfStatistic.MinMagnitude
            ElseIf (ReferenceEquals(cb, CheckBoxFreqAtMinMagnitude)) Then
                statisticValue = RfStatistic.FreqAtMinMagnitude
            ElseIf (ReferenceEquals(cb, CheckBoxPhaseAtMinMagnitude)) Then
                statisticValue = RfStatistic.PhaseAtMinMagnitude
            ElseIf (ReferenceEquals(cb, CheckBox90DegCrossoverFreq)) Then
                statisticValue = RfStatistic.Degree90CrossoverFreq
            ElseIf (ReferenceEquals(cb, CheckBoxMagnitudeAtCrossoverFreq)) Then
                statisticValue = RfStatistic.MagnitudeAtCrossoverFreq
            ElseIf (ReferenceEquals(cb, CheckBoxHalfWidthIfMagnitude)) Then
                statisticValue = RfStatistic.HalfWidthIfMagnitude
            ElseIf (ReferenceEquals(cb, CheckBoxQualityOfMagnitude)) Then
                statisticValue = RfStatistic.QualityOfMagnitude
            End If
            Return CType(statisticValue, UInteger)

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Function

    Private Function GetROIStatisticValue(ByVal numericCtrl As Object) As UInteger

        Try
            Dim statisticValue As UInteger = 0

            If (ReferenceEquals(numericCtrl    , NumericUpDownSummedPhaseWeight)) Then
                statisticValue = RfStatistic.SummedPhase
            ElseIf (ReferenceEquals(numericCtrl, NumericUpDownAveragePhaseWeight)) Then
                statisticValue = RfStatistic.AveragePhase
            ElseIf (ReferenceEquals(numericCtrl, NumericUpDownSumMagnitudesWeight)) Then
                statisticValue = RfStatistic.SumMagnitudes
            ElseIf (ReferenceEquals(numericCtrl, NumericUpDownAverageMagWeight)) Then
                statisticValue = RfStatistic.AverageMagnitude
            ElseIf (ReferenceEquals(numericCtrl, NumericUpDownPeakMagnitudeWeight)) Then
                statisticValue = RfStatistic.PeakMagnitude
            ElseIf (ReferenceEquals(numericCtrl, NumericUpDownFreqAtPeakMagWeight)) Then
                statisticValue = RfStatistic.FreqAtPeakMagnitude
            ElseIf (ReferenceEquals(numericCtrl, NumericUpDownPhaseAtPeakMagWeight)) Then
                statisticValue = RfStatistic.PhaseAtPeakMagnitude
            ElseIf (ReferenceEquals(numericCtrl, NumericUpDownMinMagnitudeWeight)) Then
                statisticValue = RfStatistic.MinMagnitude
            ElseIf (ReferenceEquals(numericCtrl, NumericUpDownFreqAtMinMagWeight)) Then
                statisticValue = RfStatistic.FreqAtMinMagnitude
            ElseIf (ReferenceEquals(numericCtrl, NumericUpDownPhaseAtMinMagWeight)) Then
                statisticValue = RfStatistic.PhaseAtMinMagnitude
            ElseIf (ReferenceEquals(numericCtrl, NumericUpDownDeg90CrossoverFreqWeight)) Then
                statisticValue = RfStatistic.Degree90CrossoverFreq
            ElseIf (ReferenceEquals(numericCtrl, NumericUpDownMagAtCrossoverFreq)) Then
                statisticValue = RfStatistic.MagnitudeAtCrossoverFreq
            ElseIf (ReferenceEquals(numericCtrl, NumericUpDownHalfWidthAtMagWeight)) Then
                statisticValue = RfStatistic.HalfWidthIfMagnitude
            ElseIf (ReferenceEquals(numericCtrl, NumericUpDownQualityOfMagnitudeWeight)) Then
                statisticValue = RfStatistic.QualityOfMagnitude
            End If
            Return statisticValue

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Function

    
    Private Function GetROIStatisticsValue() As UInteger

        Try
            Dim statisticsValue As UInteger

            statisticsValue = If(CheckBoxSummedPhase.Checked,
                             statisticsValue Or RfStatistic.SummedPhase,
                             statisticsValue)
            statisticsValue = If(CheckBoxAvgPhase.Checked,
                             statisticsValue Or RfStatistic.AveragePhase,
                             statisticsValue)
            statisticsValue = If(CheckBoxSumMagnitude.Checked,
                             statisticsValue Or RfStatistic.SumMagnitudes,
                             statisticsValue)
            statisticsValue = If(CheckBoxAvgMagnitude.Checked,
                             statisticsValue Or RfStatistic.AverageMagnitude,
                             statisticsValue)
            statisticsValue = If(CheckBoxPeakMagnitude.Checked,
                             statisticsValue Or RfStatistic.PeakMagnitude,
                             statisticsValue)
            statisticsValue = If(CheckBoxFreqAtPeakMagnitude.Checked,
                             statisticsValue Or RfStatistic.FreqAtPeakMagnitude,
                             statisticsValue)
            statisticsValue = If(CheckBoxPhaseAtPeakMagnitude.Checked,
                             statisticsValue Or RfStatistic.PhaseAtPeakMagnitude,
                             statisticsValue)
            statisticsValue = If(CheckBoxMinMagnitude.Checked,
                             statisticsValue Or RfStatistic.MinMagnitude,
                             statisticsValue)
            statisticsValue = If(CheckBoxFreqAtMinMagnitude.Checked,
                             statisticsValue Or RfStatistic.FreqAtMinMagnitude,
                             statisticsValue)
            statisticsValue = If(CheckBoxPhaseAtMinMagnitude.Checked,
                             statisticsValue Or RfStatistic.PhaseAtMinMagnitude,
                             statisticsValue)
            statisticsValue = If(CheckBox90DegCrossoverFreq.Checked,
                             statisticsValue Or RfStatistic.Degree90CrossoverFreq,
                             statisticsValue)
            statisticsValue = If(CheckBoxMagnitudeAtCrossoverFreq.Checked,
                             statisticsValue Or RfStatistic.MagnitudeAtCrossoverFreq,
                             statisticsValue)
            statisticsValue = If(CheckBoxHalfWidthIfMagnitude.Checked,
                             statisticsValue Or RfStatistic.HalfWidthIfMagnitude,
                             statisticsValue)
            statisticsValue = If(CheckBoxQualityOfMagnitude.Checked,
                             statisticsValue Or RfStatistic.QualityOfMagnitude,
                             statisticsValue)
            Return statisticsValue

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Function

    Private Function GetStatisticWeightNumericUpDown(ByVal statistic As RfStatistic) As NumericUpDown

        Try
            Dim statisticWeightCtrl As NumericUpDown = Nothing

            Select Case statistic
                Case RfStatistic.SummedPhase
                    statisticWeightCtrl = NumericUpDownSummedPhaseWeight
                Case RfStatistic.AveragePhase
                    statisticWeightCtrl = NumericUpDownAveragePhaseWeight
                Case RfStatistic.SumMagnitudes
                    statisticWeightCtrl = NumericUpDownSumMagnitudesWeight
                Case RfStatistic.AverageMagnitude
                    statisticWeightCtrl = NumericUpDownAverageMagWeight
                Case RfStatistic.PeakMagnitude
                    statisticWeightCtrl = NumericUpDownPeakMagnitudeWeight
                Case RfStatistic.FreqAtPeakMagnitude
                    statisticWeightCtrl = NumericUpDownFreqAtPeakMagWeight
                Case RfStatistic.PhaseAtPeakMagnitude
                    statisticWeightCtrl = NumericUpDownPhaseAtPeakMagWeight
                Case RfStatistic.MinMagnitude
                    statisticWeightCtrl = NumericUpDownMinMagnitudeWeight
                Case RfStatistic.FreqAtMinMagnitude
                    statisticWeightCtrl = NumericUpDownFreqAtMinMagWeight
                Case RfStatistic.PhaseAtMinMagnitude
                    statisticWeightCtrl = NumericUpDownPhaseAtMinMagWeight
                Case RfStatistic.Degree90CrossoverFreq
                    statisticWeightCtrl = NumericUpDownDeg90CrossoverFreqWeight
                Case RfStatistic.MagnitudeAtCrossoverFreq
                    statisticWeightCtrl = NumericUpDownMagAtCrossoverFreq
                Case RfStatistic.HalfWidthIfMagnitude
                    statisticWeightCtrl = NumericUpDownHalfWidthAtMagWeight
                Case RfStatistic.QualityOfMagnitude
                    statisticWeightCtrl = NumericUpDownQualityOfMagnitudeWeight
            End Select
            Return statisticWeightCtrl

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Function

    Private Sub InitROIStatisticControls(ByVal statWeightList As List(Of ROIStatisticWeight))

        Try
            Dim statWeightNumericUpDownCtrl As NumericUpDown = Nothing

            For Each statWeight In statWeightList
                Select Case statWeight.Statistic
                    Case RfStatistic.SummedPhase
                        CheckBoxSummedPhase.Checked              = True
                        statWeightNumericUpDownCtrl              = NumericUpDownSummedPhaseWeight
                    Case RfStatistic.AveragePhase
                        CheckBoxAvgPhase.Checked                 = True
                        statWeightNumericUpDownCtrl              = NumericUpDownAveragePhaseWeight
                    Case RfStatistic.SumMagnitudes
                        CheckBoxSumMagnitude.Checked             = True
                        statWeightNumericUpDownCtrl              = NumericUpDownSumMagnitudesWeight
                    Case RfStatistic.AverageMagnitude
                        CheckBoxAvgMagnitude.Checked             = True
                        statWeightNumericUpDownCtrl              = NumericUpDownAverageMagWeight
                    Case RfStatistic.PeakMagnitude
                        CheckBoxPeakMagnitude.Checked            = True
                        statWeightNumericUpDownCtrl              = NumericUpDownPeakMagnitudeWeight
                    Case RfStatistic.FreqAtPeakMagnitude
                        CheckBoxFreqAtPeakMagnitude.Checked      = True
                        statWeightNumericUpDownCtrl              = NumericUpDownFreqAtPeakMagWeight
                    Case RfStatistic.PhaseAtPeakMagnitude
                        CheckBoxPhaseAtPeakMagnitude.Checked     = True
                        statWeightNumericUpDownCtrl              = NumericUpDownPhaseAtPeakMagWeight
                    Case RfStatistic.MinMagnitude
                        CheckBoxMinMagnitude.Checked             = True
                        statWeightNumericUpDownCtrl              = NumericUpDownMinMagnitudeWeight
                    Case RfStatistic.FreqAtMinMagnitude
                        CheckBoxFreqAtMinMagnitude.Checked       = True
                        statWeightNumericUpDownCtrl              = NumericUpDownFreqAtMinMagWeight
                    Case RfStatistic.PhaseAtMinMagnitude
                        CheckBoxPhaseAtMinMagnitude.Checked      = True
                        statWeightNumericUpDownCtrl              = NumericUpDownPhaseAtMinMagWeight
                    Case RfStatistic.Degree90CrossoverFreq
                        CheckBox90DegCrossoverFreq.Checked       = True
                        statWeightNumericUpDownCtrl              = NumericUpDownDeg90CrossoverFreqWeight
                    Case RfStatistic.MagnitudeAtCrossoverFreq
                        CheckBoxMagnitudeAtCrossoverFreq.Checked = True
                        statWeightNumericUpDownCtrl              = NumericUpDownMagAtCrossoverFreq
                    Case RfStatistic.HalfWidthIfMagnitude
                        CheckBoxHalfWidthIfMagnitude.Checked     = True
                        statWeightNumericUpDownCtrl              = NumericUpDownHalfWidthAtMagWeight
                    Case RfStatistic.QualityOfMagnitude
                        CheckBoxQualityOfMagnitude.Checked       = True
                        statWeightNumericUpDownCtrl              = NumericUpDownQualityOfMagnitudeWeight
                End Select

                If ((RoiWorkingCopy.ContainsStatistic(statWeight.Statistic)) AndAlso
                    (statWeightNumericUpDownCtrl IsNot Nothing)) Then
                    statWeightNumericUpDownCtrl.Value   = RoiWorkingCopy.GetWeight(statWeight.Statistic)
                End If
            Next

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Private Sub CheckBox_CheckedChanged(sender As Object, e As EventArgs) _
        Handles CheckBoxSummedPhase.CheckedChanged, CheckBoxAvgPhase.CheckedChanged,
                CheckBoxSumMagnitude.CheckedChanged, CheckBoxAvgMagnitude.CheckedChanged,
                CheckBoxPeakMagnitude.CheckedChanged, CheckBoxFreqAtPeakMagnitude.CheckedChanged,
                CheckBoxPhaseAtPeakMagnitude.CheckedChanged, CheckBoxMinMagnitude.CheckedChanged,
                CheckBoxFreqAtMinMagnitude.CheckedChanged, CheckBoxPhaseAtMinMagnitude.CheckedChanged,
                CheckBox90DegCrossoverFreq.CheckedChanged, CheckBoxMagnitudeAtCrossoverFreq.CheckedChanged,
                CheckBoxQualityOfMagnitude.CheckedChanged, CheckBoxHalfWidthIfMagnitude.CheckedChanged

        Try
            Dim value As UInteger = GetROIStatisticValue(CType(sender, CheckBox))

            If (value > 0) Then

                ' Enable/Disable the corresponding Weight numeric up-down control.
                If (ReferenceEquals(sender, CheckBoxSummedPhase)) Then
                    NumericUpDownSummedPhaseWeight.Value          = 0
                    NumericUpDownSummedPhaseWeight.Enabled        = CheckBoxSummedPhase.Checked
                ElseIf (ReferenceEquals(sender, CheckBoxAvgPhase)) Then
                    NumericUpDownAveragePhaseWeight.Value         = 0
                    NumericUpDownAveragePhaseWeight.Enabled       = CheckBoxAvgPhase.Checked
                ElseIf (ReferenceEquals(sender, CheckBoxSumMagnitude)) Then
                    NumericUpDownSumMagnitudesWeight.Value        = 0
                    NumericUpDownSumMagnitudesWeight.Enabled      = CheckBoxSumMagnitude.Checked
                ElseIf (ReferenceEquals(sender, CheckBoxAvgMagnitude)) Then
                    NumericUpDownAverageMagWeight.Value           = 0
                    NumericUpDownAverageMagWeight.Enabled         = CheckBoxAvgMagnitude.Checked
                ElseIf (ReferenceEquals(sender, CheckBoxPeakMagnitude)) Then
                    NumericUpDownPeakMagnitudeWeight.Value        = 0
                    NumericUpDownPeakMagnitudeWeight.Enabled      = CheckBoxPeakMagnitude.Checked
                ElseIf (ReferenceEquals(sender, CheckBoxFreqAtPeakMagnitude)) Then
                    NumericUpDownFreqAtPeakMagWeight.Value        = 0
                    NumericUpDownFreqAtPeakMagWeight.Enabled      = CheckBoxFreqAtPeakMagnitude.Checked
                ElseIf (ReferenceEquals(sender, CheckBoxPhaseAtPeakMagnitude)) Then
                    NumericUpDownPhaseAtPeakMagWeight.Value       = 0
                    NumericUpDownPhaseAtPeakMagWeight.Enabled     = CheckBoxPhaseAtPeakMagnitude.Checked
                ElseIf (ReferenceEquals(sender, CheckBoxMinMagnitude)) Then
                    NumericUpDownMinMagnitudeWeight.Value         = 0
                    NumericUpDownMinMagnitudeWeight.Enabled       = CheckBoxMinMagnitude.Checked
                ElseIf (ReferenceEquals(sender, CheckBoxFreqAtMinMagnitude)) Then
                    NumericUpDownFreqAtMinMagWeight.Value         = 0
                    NumericUpDownFreqAtMinMagWeight.Enabled       = CheckBoxFreqAtMinMagnitude.Checked
                ElseIf (ReferenceEquals(sender, CheckBoxPhaseAtMinMagnitude)) Then
                    NumericUpDownPhaseAtMinMagWeight.Value        = 0
                    NumericUpDownPhaseAtMinMagWeight.Enabled      = CheckBoxPhaseAtMinMagnitude.Checked
                ElseIf (ReferenceEquals(sender, CheckBox90DegCrossoverFreq)) Then
                    NumericUpDownDeg90CrossoverFreqWeight.Value   = 0
                    NumericUpDownDeg90CrossoverFreqWeight.Enabled = CheckBox90DegCrossoverFreq.Checked
                ElseIf (ReferenceEquals(sender, CheckBoxMagnitudeAtCrossoverFreq)) Then
                    NumericUpDownMagAtCrossoverFreq.Value         = 0
                    NumericUpDownMagAtCrossoverFreq.Enabled       = CheckBoxMagnitudeAtCrossoverFreq.Checked
                ElseIf (ReferenceEquals(sender, CheckBoxHalfWidthIfMagnitude)) Then
                    NumericUpDownHalfWidthAtMagWeight.Value       = 0
                    NumericUpDownHalfWidthAtMagWeight.Enabled     = CheckBoxHalfWidthIfMagnitude.Checked
                ElseIf (ReferenceEquals(sender, CheckBoxQualityOfMagnitude)) Then
                    NumericUpDownQualityOfMagnitudeWeight.Value   = 0
                    NumericUpDownQualityOfMagnitudeWeight.Enabled = CheckBoxQualityOfMagnitude.Checked
                End If
            End If
            GenerateAndDisplayROINameString()

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Private Sub NumericUpDownWeight_ValueChanged(sender As Object, e As EventArgs) _

        Try
            Dim statistic As UInteger = GetROIStatisticValue(sender)
            Dim statisticWeight As Decimal = CType(sender, NumericUpDown).Value

            If ((statistic > 0) AndAlso (statisticWeight <= Common.MaxStatisticWeight)) Then
                ' Replace existing StatisticWeight.
                If (RoiWorkingCopy.ContainsStatistic(statistic)) Then
                    If (RoiWorkingCopy.RemoveStatistic(statistic)) Then

                        RoiWorkingCopy.StatisticWeights.Add(New ROIStatisticWeight(statistic, statisticWeight))
                    Else
                        MsgBox("Failed to Remove the selected Statistic/Weight: " &
                                Common.StatisticNameDict(statistic),
                                MsgBoxStyle.SystemModal + MsgBoxStyle.Exclamation)
                        RaiseEvent LogErrorEvent(New StackTrace, 
                                                 "FormROIEdit.NumericUpDownWeight_ValueChanged() failed to " &
                                                 "remove the selected Statistic/Weight: " &
                                                 Common.StatisticNameDict(statistic))
                    End If
                Else
                    RoiWorkingCopy.StatisticWeights.Add(New ROIStatisticWeight(statistic, statisticWeight))
                End If
                GenerateAndDisplayROINameString()
            End If

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub

    Private Sub ComboBoxROIOpModeSelect_SelectedIndexChanged(sender As Object, e As EventArgs)
        RoiWorkingCopy.OperatingMode = ComboBoxROIOpModeSelect.SelectedItem
        GenerateAndDisplayROINameString()
    End Sub

    Private Sub NumericUpDownROISweepInterval_ValueChanged(sender As Object, e As EventArgs)
        RoiWorkingCopy.SweepInterval = NumericUpDownROISweepInterval.Value
        GenerateAndDisplayROINameString()
    End Sub

    Private Sub ROI_StartFreqNumericUpDown_ValueChanged(sender As Object, e As EventArgs)
        RoiWorkingCopy.StartFrequency = ROI_StartFreqNumericUpDown.Value
        GenerateAndDisplayROINameString()
    End Sub

    Private Sub ROI_StepSizeNumericUpDown_ValueChanged(sender As Object, e As EventArgs)
        RoiWorkingCopy.FreqStepSize = ROI_StepSizeNumericUpDown.Value
        GenerateAndDisplayROINameString()
    End Sub

    Private Sub ROI_FreqStepsNumericUpDown_ValueChanged(sender As Object, e As EventArgs)
        RoiWorkingCopy.StepCount = ROI_FreqStepsNumericUpDown.Value
        GenerateAndDisplayROINameString()
    End Sub

    Private Sub ButtonClose_Click(sender As Object, e As EventArgs) Handles ButtonClose.Click

        Close()
    End Sub

    Private Sub ButtonSave_Click(sender As Object, e As EventArgs) Handles ButtonSave.Click

        Try
            If (_roiOriginal = RoiWorkingCopy) Then
                If (Not FormRfSensorMain.RegionsOfInterest.ContainsKey(_roiOriginal.Name)) Then

                    ' This looks like an unmodified (New) Region Of Interest containing default values.
                    ' I'll ask the user if they want to save it anyway.
                    Dim mbResult = MsgBox("The ROI is new and unmodified." & vbCrLf &
                                          "Would you like to save it anyway?",
                                          MsgBoxStyle.SystemModal + MsgBoxStyle.YesNo)
                    If (mbResult = MsgBoxResult.Yes) Then
                        If (Not FormRfSensorMain.RegionsOfInterest.TryAdd(RoiWorkingCopy.Name, RoiWorkingCopy)) Then
                            MsgBox("Failed to save the new ROI!", MsgBoxStyle.SystemModal + MsgBoxStyle.Exclamation)
                            RaiseEvent LogErrorEvent(New StackTrace, 
                                                     "ButtonSave_Click() failed to save the new ROI: " & RoiWorkingCopy.Name)
                        Else
                            _roiOriginal       = New RegionOfInterest(RoiWorkingCopy)
                            ButtonSave.Enabled = False
                            _editResult = DialogResult.OK
                        End If
                    End If
                End If
            Else
                'This name and therefore the contents of this ROI have been changed.
                If (RoiWorkingCopy.Independent) Then
                    If (Not FormRfSensorMain.RegionsOfInterest.ContainsKey(RoiWorkingCopy.Name)) Then

                        ' This ROI's name does not exist in the ROI dictionary.
                        If (FormRfSensorMain.RegionsOfInterest().TryAdd(RoiWorkingCopy.Name,
                                                                        New RegionOfInterest(RoiWorkingCopy))) Then
                            _roiOriginal = New RegionOfInterest(RoiWorkingCopy)
                            ButtonSave.Enabled = False
                            _editResult = DialogResult.OK
                        Else

                            MsgBox("Failed to save the modified ROI!", MsgBoxStyle.SystemModal + MsgBoxStyle.Exclamation)
                            RaiseEvent LogErrorEvent(New StackTrace, 
                                                     "ButtonSave_Click() failed to save the modified ROI: " &
                                                     RoiWorkingCopy.Name & " to FormRFSensorMain.RegionsOfInterest")
                        End If
                    Else
                        ' An ROI by this name already exists!
                        Dim mbResult = MsgBox("This ROI duplicates an existing ROI and will not be saved.",
                                               MsgBoxStyle.SystemModal)
                    End If
                Else
                    ' This is a measurement associated ROI.
                    If (_measurementName IsNot Nothing) Then
                        If (FormRfSensorMain.Measurements.ContainsKey(_measurementName)) Then
                            If (Not FormRfSensorMain.Measurements(_measurementName).ContainsROI(RoiWorkingCopy.Name)) Then
                                ' This ROI's name does not exist in the ROI dictionary.
                                If (Not FormRfSensorMain.RegionsOfInterest.TryAdd(RoiWorkingCopy.Name, RoiWorkingCopy)) Then
                                    MsgBox("Failed to save the modified ROI!", MsgBoxStyle.SystemModal + MsgBoxStyle.Exclamation)
                                    RaiseEvent LogErrorEvent(New StackTrace, 
                                                             "ButtonSave_Click() failed to save the modified ROI: " &
                                                             RoiWorkingCopy.Name)
                                Else
                                    _roiOriginal = New RegionOfInterest(RoiWorkingCopy)
                                    ButtonSave.Enabled = False
                                    _editResult = DialogResult.OK
                                End If
                            Else
                                ' An ROI by this name already exists!
                                Dim mbResult = MsgBox("This ROI duplicates an existing ROI and will not be saved.",
                                                       MsgBoxStyle.SystemModal)
                            End If
                        Else
                            ' A Measurement by this name was not found!
                            Dim mbResult = MsgBox("Failed to save the modified ROI!" & vbCrLf &
                                                   MsgBoxStyle.SystemModal + MsgBoxStyle.Exclamation)
                            RaiseEvent LogErrorEvent(New StackTrace, 
                                                     "FormROIEdit.ButtonSave_Click() Failed to save the modified ROI!" & vbCrLf &
                                                     "Because the name of the associated Measurement: " & _measurementName &
                                                     " was not found in FormRFSensorMain.Measurements.")
                        End If
                    Else
                        ' A Measurement by this name was not found!
                        Dim mbResult = MsgBox("Failed to save the modified ROI!",
                                               MsgBoxStyle.SystemModal + MsgBoxStyle.Exclamation)
                        RaiseEvent LogErrorEvent(New StackTrace, 
                                                 "FormROIEdit.ButtonSave_Click() Failed to save the modified ROI!" & vbCrLf &
                                                 "Because the name of the associated Measurement is null")
                    End If
                End If
            End If

        Catch ex As Exception
            RaiseEvent LogExceptionEvent(New StackTrace, ex)
        End Try
    End Sub
End Class
