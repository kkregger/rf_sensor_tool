﻿Imports System
Imports System.Collections.Generic
Imports System.Collections.Concurrent
Imports System.Data
Imports System.Linq
Imports System.Numerics
Imports System.Windows.Forms
Imports System.Threading

Namespace CTS_RF_Sensor_Tool
    Partial Public Class FormRunOSLCalibration
        Inherits Form

        Private Shared _standardOpen As New Complex(0.9999, -0.0001)
        Private Shared _standardShort As New Complex(-0.9999, 0.0001)
        Private Shared _standardLoad As New Complex(0.0001, -0.0001)
        Private Shared _calReadingsProcessingThread As Thread
        Private Shared _terminateCalReadingsProcessThread As Boolean
        Private Shared _thisOSLCalibration As New List(Of OSLCalibration)()
        Private Shared _thisTransNorm As New List(Of TransmissionNormalization)()
        Private Shared _readings As List(Of Reading)
        Private Shared _sweepCount As Integer
        Private Shared _calSweepStarted As Boolean
        Private Shared _calMode As TransmissionModes
        Private Shared _thisInstance As FormRunOSLCalibration
        Private Shared _calDataPoints As ConcurrentDictionary(Of TransmissionModes, List(Of List(Of CalDataPoint)))
        Private Shared _calSweepStartData As ConcurrentDictionary(Of TransmissionModes, List(Of CalSweepStartData))
        Private Shared _calSweepEndData As ConcurrentDictionary(Of TransmissionModes, List(Of CalSweepEndData))
        Private Shared _calSweepStepData As ConcurrentDictionary(Of TransmissionModes, List(Of CalSweepStepData))
        Private Shared _calSweepCmdData As ConcurrentDictionary(Of TransmissionModes, List(Of CalSweepCmdData))
        ' Event raised when an error is being logged.
        Private Shared Event LogErrorEvent As ActivityLog.ErrorLogDelegate

        Public Sub New()
            InitializeComponent()
            _thisInstance = Me
            CanMessageLib.AddStateChangeDelegate(AddressOf StateChangeHandler)
        End Sub

        Private Sub FormRunOSLCalibration_Shown(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Shown
            ' Register error logging event handler.
            AddHandler LogErrorEvent, AddressOf ActivityLog.LogError
            If Not FormRFSensorMain.CanConnected Then
                FormRFSensorMain.LoadActiveCAN_Communication()
                If FormRFSensorMain.ActiveMeasurement IsNot Nothing Then
                    FormRFSensorMain.CanConnect()
                    If Not FormRFSensorMain.CanConnected Then
                        LabelMessage.Text = ""
                        Close()
                    End If
                Else
                    MessageBox.Show("No active CAN configuration found." & vbcrlf &
                                    "Unable to access an RF Sensing Unit.", "")
                    LabelMessage.Text = ""
                    Close()
                End If
            End If
            StartCalibration()
        End Sub

        Private Sub FormRunOSLCalibration_FormClosing(ByVal sender As Object, ByVal e As FormClosingEventArgs)

            CanMessageLib.SendCommandMessage(CommandMsgType.NormalOperation)
            ' Unregister error logging event handler.
            RemoveHandler LogErrorEvent, AddressOf ActivityLog.LogError
            CanMessageLib.RemoveStateChangeDelegate(AddressOf StateChangeHandler)
            _terminateCalReadingsProcessThread = True
            ' These are static , so make sweep data collections available for GC.
            _calDataPoints = Nothing
            _calSweepStartData = Nothing
            _calSweepEndData = Nothing
            _calSweepStepData = Nothing
            _calSweepCmdData = Nothing
        End Sub

        ' Allows the main form to display OSL Cal message.
        Public ReadOnly Property Message() As String
            Get
                Return LabelMessage.Text
            End Get
        End Property

        ' Active measurement ID.
        Public Shared Property MeasurementID() As Integer

        Private Sub StartCalibration()

            Dim result = False
            Dim canCom As CanCommunication = FormRFSensorMain.ActiveCanDevice

            If (canCom IsNot Nothing) AndAlso (canCom.Active) Then
                If FormRFSensorMain.ActiveMeasurement IsNot Nothing Then
                    Dim actSweep As Sweep = FormRFSensorMain.ActiveSweep
                    If actSweep IsNot Nothing Then
                        If FormRFSensorMain.ActiveMeasurement.OperatingMode = "S11" Then
                            ' Reflection type measurement selected.
                            LabelMessage.Text = "Running Opened Calibration"
                            _calMode = TransmissionModes.S11Open
                        Else
                            If FormRFSensorMain.ActiveMeasurement.NormalizeViaBypass Then
                                LabelMessage.Text = "Running S12 Normalization Bypassed"
                                _calMode = TransmissionModes.S12Bypass6Db
                            Else
                                LabelMessage.Text = "Running S12 Normalization"
                                _calMode = TransmissionModes.S12NormalOperation
                            End If
                        End If
                        ' Initialize calibration data lists and dictionarys used to
                        ' store calibration/normalization sweep results.
                        _calDataPoints = New ConcurrentDictionary(Of TransmissionModes, List(Of List(Of CalDataPoint)))()
                        _calSweepStartData = New ConcurrentDictionary(Of TransmissionModes, List(Of CalSweepStartData))()
                        _calSweepEndData = New ConcurrentDictionary(Of TransmissionModes, List(Of CalSweepEndData))()
                        _calSweepStepData = New ConcurrentDictionary(Of TransmissionModes, List(Of CalSweepStepData))()
                        _calSweepCmdData = New ConcurrentDictionary(Of TransmissionModes, List(Of CalSweepCmdData))()

                        ' Send the commands to setup and start the calibration/normalization sweeps.
                        ' Delays are added here to enforce message spacing.
                        result = CanMessageLib.SendCommandMessage(CommandMsgType.ConfigStepParams)
                        Thread.Sleep(20)
                        result = result And CanMessageLib.SendCommandMessage(CommandMsgType.ConfigureVCO1)
                        Thread.Sleep(20)
                        result = result And CanMessageLib.SendCommandMessage(CommandMsgType.ConfigureVCO2)
                        Thread.Sleep(20)
                        result = result And StartCalSweep()
                        _sweepCount = 0
                        _readings = New List(Of Reading)()
                    End If
                End If
            End If
            If result Then
                ButtonCancel.Text = "Close"
            Else
                DialogResult = System.Windows.Forms.DialogResult.Cancel
                Close()
            End If
        End Sub

        Private Function StartCalSweep() As Boolean

            _calSweepStarted = False
            CalSweepStartTimer.Enabled = True
            Return CanMessageLib.SendCommandMessage(CommandMsgType.RunSingleScan, _calMode)
        End Function

        Private Function NextSweep() As Boolean

            Dim result = False

            _sweepCount += 1
            If _sweepCount < FormRFSensorMain.RunConfigurations.CalSweepsForAvg Then
                ' Run the calibration/normalization sweep the user selected number of times.
                ' Send the command to start the next calibration sweep.
                ' false will be returned if this function call is unsuccessful.
                result = StartCalSweep()
                If Not result Then
                    LabelMessage.Text = "Calibration Failed"
                End If
            Else
                If FormRFSensorMain.ActiveMeasurement.OperatingMode = "S11" Then
                    Select Case _calMode
                        Case TransmissionModes.S11Open
                            ' Run shorted cal.
                            _calMode = TransmissionModes.S11Short
                            result = StartCalSweep()
                            LabelMessage.Text = If(result, "Running Shorted Calibration", "Calibration Failed")
                        Case TransmissionModes.S11Short
                            ' Run loaded cal.
                            _calMode = TransmissionModes.S11Load
                            result = StartCalSweep()
                            LabelMessage.Text = If(result, "Running Loaded Calibration", "Calibration Failed")
                        Case TransmissionModes.S11Load
                            ' OSL calibration is complete, now run normalization.
                            _calMode = TransmissionModes.S11NormalOperation
                            result = StartCalSweep()
                            LabelMessage.Text = If(result, "Running S11 Normalization", "Calibration Failed")
                        Case TransmissionModes.S11NormalOperation
                            LabelMessage.Text = "S11 Normalization Sweep Complete"
                            ' Normalization complete, return false.
                    End Select
                Else
                    LabelMessage.Text = "S12 Normalization Sweep Complete"
                End If
            End If
            Return result
        End Function

        Public Shared Sub StateChangeHandler(ByVal state As AppStates)

            ' Called upon RF Sensor state change.
            Select Case state
                Case AppStates.RunSingleScan
                    ' The rf sensor has entered the calibration measurement state.
                    _calSweepStarted = True

                Case AppStates.WaitingForCommand
                    ' The rf sensor has transitioned out of the calibration measurement state.
                    ' Process the calibration readings data received during the sweep.
                    _calReadingsProcessingThread = New Thread(AddressOf ProcessCalReadings)
                    _calReadingsProcessingThread.Priority = ThreadPriority.AboveNormal
                    _calReadingsProcessingThread.Start()

                Case Else
                    If _calSweepStarted = True Then
                        _thisInstance?.Invoke(CType(Sub()
                            MessageBox.Show("An unexpected RF Sensor state change occurred" & vbcrlf &
                                            "during the Calibration/Normalization measurement sweep!")
                            _thisInstance.DialogResult = DialogResult.Cancel
                            _thisInstance.Close()
                        End Sub, MethodInvoker))
                    End If
            End Select
        End Sub

        Private Shared Sub ProcessCalReadings()

            ' Store the results of the previous calibration/normalization measurement sweep.
            ' Expected modes include: S11, S11 Opened, S11 Shorted, S11 Loaded, S12, and S12 Bypass6Db.
            If _calDataPoints.ContainsKey(_calMode) Then
                _calDataPoints(_calMode).Add(CanMessageLib.GetCalDataPoints())
            Else
                _calDataPoints.TryAdd(_calMode, New List(Of List(Of CalDataPoint)) From {CanMessageLib.GetCalDataPoints()})
            End If

            If _calSweepStartData.ContainsKey(_calMode) Then
                _calSweepStartData(_calMode).Add(CanMessageLib.GetCalSweepStartData())
            Else
                _calSweepStartData.TryAdd(_calMode, New List(Of CalSweepStartData) From {CanMessageLib.GetCalSweepStartData()})
            End If

            If _calSweepEndData.ContainsKey(_calMode) Then
                _calSweepEndData(_calMode).Add(CanMessageLib.GetCalSweepEndData())
            Else
                _calSweepEndData.TryAdd(_calMode, New List(Of CalSweepEndData) From {CanMessageLib.GetCalSweepEndData()})
            End If

            If _calSweepStepData.ContainsKey(_calMode) Then
                _calSweepStepData(_calMode).Add(CanMessageLib.GetCalSweepStepData())
            Else
                _calSweepStepData.TryAdd(_calMode, New List(Of CalSweepStepData) From {CanMessageLib.GetCalSweepStepData()})
            End If

            If _calSweepCmdData.ContainsKey(_calMode) Then
                _calSweepCmdData(_calMode).Add(CanMessageLib.GetCalSweepCmdData())
            Else
                _calSweepCmdData.TryAdd(_calMode, New List(Of CalSweepCmdData) From {CanMessageLib.GetCalSweepCmdData()})
            End If
            If _thisInstance Is Nothing Then
                Return
            End If
            If _terminateCalReadingsProcessThread Then
                Return
            End If
            ' Start the next calibration/normalization measurement sweep.
            If Not _thisInstance.NextSweep() Then
                If _sweepCount < FormRFSensorMain.RunConfigurations.CalSweepsForAvg Then
                    ' The user selected number of calibration/measurement
                    ' sweeps were not completed due to a failure.
                    _thisInstance.Invoke(CType(Sub()
                        MessageBox.Show("Failed to start the next Calibration/" & vbcrlf & "Normalization measurement sweep!")
                        _thisInstance.DialogResult = System.Windows.Forms.DialogResult.Cancel
                        _thisInstance.Close()
                    End Sub, MethodInvoker))
                Else
                    ' The last Normalization sweep is complete.
                    ' Now process the OSL Calibration/Normalization sweep results.
                    If _calMode = TransmissionModes.S12NormalOperation OrElse _calMode = TransmissionModes.S12Bypass6Db Then
                        _thisInstance.Invoke(CType(Sub()
                            _thisInstance.LabelMessage.Text = "Calculating S12 Normalization."
                        End Sub, MethodInvoker))

                        ' Calculate S12 or S12 Bypassed normalization.
                        If Not CalculateNormalization(False) Then
                            _thisInstance.Invoke(CType(Sub()
                                MessageBox.Show("A failure occurred while calculating S12 Normalization.")
                                _thisInstance.DialogResult = System.Windows.Forms.DialogResult.Cancel
                                _thisInstance.Close()
                            End Sub, MethodInvoker))
                        End If
                        _thisInstance.Invoke(CType(Sub()
                            _thisInstance.LabelMessage.Text = "S12 Normalization Complete."
                        End Sub, MethodInvoker))
                    Else
                        _thisInstance.Invoke(CType(Sub()
                            _thisInstance.LabelMessage.Text = "Calculating S11 OSL Calibration."
                        End Sub, MethodInvoker))

                        ' Calculate S11 OSL calibration.
                        If CalculateOSLCalibration() Then
                            _thisInstance.Invoke(CType(Sub()
                                _thisInstance.LabelMessage.Text = "Calculating S11 Normalization."
                            End Sub, MethodInvoker))

                            ' Calculate S11 normalization.
                            If Not CalculateNormalization(True) Then
                                _thisInstance.Invoke(CType(Sub()
                                    MessageBox.Show("A failure occurred while calculating S11 Normalization.")
                                    _thisInstance.DialogResult = System.Windows.Forms.DialogResult.Cancel
                                    _thisInstance.Close()
                                End Sub, MethodInvoker))
                            End If
                            _thisInstance.Invoke(CType(Sub()
                                _thisInstance.LabelMessage.Text = "S11 OSL Calibration and Normalization Complete."
                            End Sub, MethodInvoker))
                        Else
                            _thisInstance.Invoke(CType(Sub()
                                MessageBox.Show("A failure occurred while calculating the OSL Calibration.")
                                _thisInstance.DialogResult = System.Windows.Forms.DialogResult.Cancel
                                _thisInstance.Close()
                            End Sub, MethodInvoker))
                        End If
                    End If
                End If
            End If
        End Sub

        Private Shared Function FindCalibrationEntry(ByVal Frequency As ULong) As OSLCalibration
            Dim oslCalibration = CType((From  OSL_cal In _thisOSLCalibration
                                        Where OSL_cal.Frequency = Frequency
                                        Select OSL_cal), OSLCalibration)
            Return oslCalibration
        End Function

        Private Shared Sub ApplyCalibration(ByRef Magnitude As Double, ByRef Phase As Double,
                                            ByVal Coordinates As Complex, ByVal oslCalibration As OSLCalibration)
            Dim complexA = New Complex(oslCalibration.RealA, oslCalibration.ImaginaryA)
            Dim complexB = New Complex(oslCalibration.RealB, oslCalibration.ImaginaryB)
            Dim complexC = New Complex(oslCalibration.RealC, oslCalibration.ImaginaryC)

            Coordinates = (Coordinates - complexB) / (complexA - complexC)
            Phase = (Coordinates.Phase * 180) / Math.PI
            Magnitude = Math.Log10(Coordinates.Magnitude) * 20
        End Sub

        Private Shared Function CalculateOSLCalibration() As Boolean

            Dim result = False
            Dim openedDataPoints = New List(Of List(Of CalDataPoint))()
            Dim shortedDataPoints = New List(Of List(Of CalDataPoint))()
            Dim loadedDataPoints = New List(Of List(Of CalDataPoint))()

            _calDataPoints.TryGetValue(TransmissionModes.S11Open, openedDataPoints)
            _calDataPoints.TryGetValue(TransmissionModes.S11Short, shortedDataPoints)
            _calDataPoints.TryGetValue(TransmissionModes.S11Load, loadedDataPoints)
            If openedDataPoints Is Nothing OrElse shortedDataPoints Is Nothing OrElse
               loadedDataPoints Is Nothing OrElse openedDataPoints?.Count <> shortedDataPoints?.Count OrElse
               shortedDataPoints?.Count <> loadedDataPoints?.Count Then

                MessageBox.Show("The OSL Calibration measurement count," & vbcrlf & "Opened " &
                                openedDataPoints.Count & " vs Shorted " & shortedDataPoints.Count &
                                " vs Loaded " & loadedDataPoints.Count & ", is inconsistent!" & vbcrlf &
                                "The OSL Calibration calculation could not be completed!.")
                Return result
            End If
            If _terminateCalReadingsProcessThread Then
                Return False
            End If

            Dim openedAverages = OslAverage(openedDataPoints)

            If _terminateCalReadingsProcessThread Then
                Return False
            End If

            Dim shortedAverages = OslAverage(shortedDataPoints)

            If _terminateCalReadingsProcessThread Then
                Return False
            End If

            Dim loadedAverages = OslAverage(openedDataPoints)

            If openedAverages Is Nothing OrElse shortedAverages IsNot Nothing OrElse loadedAverages IsNot Nothing Then
                MessageBox.Show("Failed to calculate one of the OSL Calibration measurement averages!" & vbcrlf &
                                "The OSL Calibration calculation could not be completed!")
                Return result
            End If

            If _terminateCalReadingsProcessThread Then
                Return False
            End If

            Dim calibrationDataSets = From openedAverage In openedAverages
                Join shortedAverage In shortedAverages On openedAverage.frequency Equals shortedAverage.frequency
                Join loadedAverage In loadedAverages On openedAverage.frequency Equals loadedAverage.frequency
                Select New With {
                    Key .frequency = openedAverage.frequency,
                    Key .openedMagnitude = openedAverage.Magnitude,
                    Key .openedPhase = openedAverage.Phase,
                    Key .shortedMagnitude = shortedAverage.Magnitude,
                    Key .shortedPhase = shortedAverage.Phase,
                    Key .loadedMagnitude = loadedAverage.Magnitude,
                    Key .loadedPhase = loadedAverage.Phase
                }

            If _terminateCalReadingsProcessThread Then
                Return False
            End If

            For Each calDataSet In calibrationDataSets
                Dim openedPhase As Double
                Dim openedMagnitudeRC As Double
                Dim openedCoordinates As Complex
                Dim shortedPhase As Double
                Dim shortedMagnitudeRC As Double
                Dim shortedCoordinates As Complex
                Dim loadedPhase As Double
                Dim loadedMagnitudeRC As Double
                Dim loadedCoordinates As Complex
                Dim a As Complex = Nothing
                Dim b As Complex = Nothing
                Dim c As Complex = Nothing

                openedPhase = CULng(calDataSet.openedPhase) / Common.MillivoltToPhaseSlope
                Dim openPhase180 = openedPhase * Math.PI / 180.0
                openedMagnitudeRC = Common.MagnitudeInRC(CULng(calDataSet.openedMagnitude))
                openedCoordinates = Complex.FromPolarCoordinates(openedMagnitudeRC, openPhase180)

                shortedPhase = CULng(calDataSet.shortedPhase) / Common.MillivoltToPhaseSlope
                Dim shortedPhase180 = shortedPhase * Math.PI / 180.0
                shortedMagnitudeRC = Common.MagnitudeInRC(CULng(calDataSet.shortedMagnitude))
                shortedCoordinates = Complex.FromPolarCoordinates(shortedMagnitudeRC, shortedPhase180)

                loadedPhase = CULng(calDataSet.loadedPhase) / Common.MillivoltToPhaseSlope
                Dim loadedPhase180 = loadedPhase * Math.PI / 180.0
                loadedMagnitudeRC = Common.MagnitudeInRC(CULng(calDataSet.loadedMagnitude))
                loadedCoordinates = Complex.FromPolarCoordinates(loadedMagnitudeRC, loadedPhase180)

                CalculateCoefficients(openPhase180, openedMagnitudeRC, shortedPhase180, shortedMagnitudeRC,
                                      loadedPhase180, loadedMagnitudeRC, a, b, c)

                ' Fill an OSLCalibration list with entries for this calibration.
                _thisOSLCalibration.Add(New OSLCalibration(MeasurementID, calDataSet.frequency, a.Real, a.Imaginary,
                                                           b.Real, b.Imaginary, c.Real, c.Imaginary))

                If _terminateCalReadingsProcessThread Then
                    Return False
                End If
            Next calDataSet
            If _thisOSLCalibration.Count = Enumerable.Count(calibrationDataSets) Then
                ' if there is a previous OSL calibration corresponding to the
                ' measurement ID then remove it.
                Dim idx As Integer = 0
                Do While idx < FormRFSensorMain.OslCalibrations.Count
                    If FormRFSensorMain.OslCalibrations(idx)(0).MeasurementID.Count = 1 Then
                        ' The MeasurementID list contains one value, so we can
                        ' just remove the corresponding OSL calibration list.
                        FormRFSensorMain.OslCalibrations.RemoveAt(idx)
                        Exit Do
                    ElseIf FormRFSensorMain.OslCalibrations(idx)(0).MeasurementID.Count > 1 Then
                        For Each oslCal In FormRFSensorMain.OslCalibrations(idx)
                            ' The MeasurementID list contains more that one value
                            ' so we can't just remove the OSL calibration list.
                            ' Instead remove the matching MeasurementID from
                            ' all of the OSL calibration Measurement ID lists.
                            oslCal.MeasurementID.Remove(MeasurementID)
                        Next oslCal
                        Exit Do
                    End If
                    If _terminateCalReadingsProcessThread Then
                        Return False
                    End If
                    idx += 1
                Loop
                ' Add this OSL calibration to the OSLCalibrations list.
                FormRFSensorMain.OslCalibrations.Add(_thisOSLCalibration)
                result = True
            End If
            Return result
        End Function

        Private Shared Function CalculateNormalization(ByVal applyOslCal As Boolean) As Boolean
            Dim result = False
            Dim normalizeDataPoints = New List(Of List(Of CalDataPoint))()

            _calDataPoints.TryGetValue(_calMode, normalizeDataPoints)

            Dim AveragedDataPoints = OslAverage(normalizeDataPoints)

            If AveragedDataPoints Is Nothing Then
                Return result ' Failed to calculate normalization average.
            End If

            If _terminateCalReadingsProcessThread Then
                Return False
            End If

            For Each avgDataPoint In AveragedDataPoints
                Dim phase As Double = Nothing
                Dim magnitude As Double = Nothing
                Dim magnitudeRC As Double
                Dim coordinates As Complex

                phase = CULng(avgDataPoint.Phase) / Common.MillivoltToPhaseSlope
                Dim phase180 = phase * Math.PI / 180.0
                magnitude = Common.MagnitudeInDB(CULng(avgDataPoint.Magnitude))
                magnitudeRC = Common.MagnitudeInRC(magnitude)
                coordinates = Complex.FromPolarCoordinates(magnitudeRC, phase180)

                If applyOslCal Then ' If OSL Cal was run (S11 mode), apply it to the normalization.
                    Dim oslCalibration As OSLCalibration

                    oslCalibration = FindCalibrationEntry(avgDataPoint.frequency)
                    If oslCalibration IsNot Nothing Then
                        ApplyCalibration(magnitude, phase, coordinates, oslCalibration)
                    End If
                End If
                ' Fill a TransmissionNormalization list with entries for this normalization.
                _thisTransNorm.Add(New TransmissionNormalization(MeasurementID, avgDataPoint.frequency, magnitude, phase))
                If _terminateCalReadingsProcessThread Then
                    Return False
                End If
            Next avgDataPoint

            If _thisTransNorm.Count = AveragedDataPoints.Count Then
                ' if there is a previous normalization corresponding to the
                ' measurement ID then remove it.
                Dim idx As Integer = 0
                Do While idx < FormRFSensorMain.TransmissionNormalizations.Count
                    If FormRFSensorMain.TransmissionNormalizations(idx)(0).MeasurementID.Count = 1 Then
                        FormRFSensorMain.TransmissionNormalizations.RemoveAt(idx)
                        Exit Do
                    ElseIf FormRFSensorMain.TransmissionNormalizations(idx)(0).MeasurementID.Count > 1 Then
                        For Each transNorm In FormRFSensorMain.TransmissionNormalizations(idx)
                            transNorm.MeasurementID.Remove(MeasurementID)
                        Next transNorm
                        Exit Do
                    End If
                    If _terminateCalReadingsProcessThread Then
                        Return False
                    End If
                    idx += 1
                Loop
                ' Add this normalization to the TransmissionNormalizations list.
                FormRFSensorMain.TransmissionNormalizations.Add(_thisTransNorm)
                result = True
            End If

            Return result
        End Function

        Private Shared Function OslAverage(ByVal listCaldataPntLists As List(Of List(Of CalDataPoint))) As List(Of PhaseMagnitude)
            Return (
                From dataPointList In listCaldataPntLists , dataPoint In dataPointList
                Group dataPoint By dataPoint.Frequency Into averages = Group
                Select New PhaseMagnitude() With {
                    .frequency = Frequency,
                    .Magnitude = averages.Average((Function(F) CSng(F.MagnitudeCalc))),
                    .Phase = averages.Average((Function(F) CSng(F.PhaseCalc)))
                }).ToList()
        End Function

        Private Shared Sub CalculateCoefficients(ByVal openPhase As Double, ByVal openMagnitude As Double,
                                                 ByVal shortPhase As Double, ByVal shortMagnitude As Double,
                                                 ByVal loadPhase As Double, ByVal loadMagnitude As Double,
                                                 ByRef a As Complex, ByRef b As Complex, ByRef c As Complex)
            a = 0
            b = 0
            c = 0

            Dim measuredOpen As Complex = Complex.FromPolarCoordinates(openMagnitude, openPhase)
            Dim measuredShort As Complex = Complex.FromPolarCoordinates(shortMagnitude, shortPhase)
            Dim measuredLoad As Complex = Complex.FromPolarCoordinates(loadMagnitude, loadPhase)

            Dim k1 As Complex = measuredLoad - measuredShort
            Dim k2 As Complex = measuredShort - measuredOpen
            Dim k3 As Complex = measuredOpen - measuredLoad
            Dim k4 As Complex = _standardLoad * _standardShort * k1
            Dim k5 As Complex = _standardOpen * _standardShort * k2
            Dim k6 As Complex = _standardLoad * _standardOpen * k3
            Dim k7 As Complex = _standardOpen * k1
            Dim k8 As Complex = _standardLoad * k2
            Dim k9 As Complex = _standardShort * k3
            Dim d As Complex = k4 + k5 + k6

            If Math.Abs(d.Magnitude) > Single.Epsilon Then
                a = (measuredOpen * k7 + measuredLoad * k8 + measuredShort * k9) / d
                b = (measuredOpen * k4 + measuredLoad * k5 + measuredShort * k6) / d
                c = (k7 + k8 + k9) / d
            End If
        End Sub

        Private Sub ButtonCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonCancel.Click
            LabelMessage.Text = "Calibration Cancelled."
            CanMessageLib.SendCommandMessage(CommandMsgType.NormalOperation)
        End Sub

        Private Sub CalSweepStartTimer_Tick(ByVal sender As Object, ByVal e As EventArgs) Handles CalSweepStartTimer.Tick
            If Not _calSweepStarted Then
                ' Failed to start the next calibration/normalization measurement sweep.
                LabelMessage.Text = "Calibration Start Timed-Out."
                MessageBox.Show("Timed-out waiting for the Calibration/" & vbcrlf & "Normalization measurement sweep to start!")
                _thisInstance.DialogResult = System.Windows.Forms.DialogResult.Cancel
                _thisInstance.Close()
            End If
        End Sub
    End Class

    Public Structure PhaseMagnitude
        Public frequency As UInteger
        Public Magnitude As Single
        Public Phase As Single
    End Structure
End Namespace
