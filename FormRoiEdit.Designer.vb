﻿
Partial Class FormRoiEdit
    Inherits Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormRoiEdit))
        Me.PanelROIConfig = New System.Windows.Forms.Panel()
        Me.ButtonSave = New System.Windows.Forms.Button()
        Me.TextBoxROIName = New System.Windows.Forms.TextBox()
        Me.GroupBoxStatistic = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.NumericUpDownQualityOfMagnitudeWeight = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDownPhaseAtPeakMagWeight = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDownHalfWidthAtMagWeight = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDownFreqAtPeakMagWeight = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDownMagAtCrossoverFreq = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDownPeakMagnitudeWeight = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDownDeg90CrossoverFreqWeight = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDownAverageMagWeight = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDownPhaseAtMinMagWeight = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDownSumMagnitudesWeight = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDownFreqAtMinMagWeight = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDownAveragePhaseWeight = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDownMinMagnitudeWeight = New System.Windows.Forms.NumericUpDown()
        Me.NumericUpDownSummedPhaseWeight = New System.Windows.Forms.NumericUpDown()
        Me.CheckBoxMagnitudeAtCrossoverFreq = New System.Windows.Forms.CheckBox()
        Me.CheckBoxPhaseAtMinMagnitude = New System.Windows.Forms.CheckBox()
        Me.CheckBoxPhaseAtPeakMagnitude = New System.Windows.Forms.CheckBox()
        Me.CheckBoxSummedPhase = New System.Windows.Forms.CheckBox()
        Me.CheckBox90DegCrossoverFreq = New System.Windows.Forms.CheckBox()
        Me.CheckBoxHalfWidthIfMagnitude = New System.Windows.Forms.CheckBox()
        Me.CheckBoxQualityOfMagnitude = New System.Windows.Forms.CheckBox()
        Me.CheckBoxMinMagnitude = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFreqAtMinMagnitude = New System.Windows.Forms.CheckBox()
        Me.CheckBoxFreqAtPeakMagnitude = New System.Windows.Forms.CheckBox()
        Me.CheckBoxPeakMagnitude = New System.Windows.Forms.CheckBox()
        Me.CheckBoxSumMagnitude = New System.Windows.Forms.CheckBox()
        Me.CheckBoxAvgMagnitude = New System.Windows.Forms.CheckBox()
        Me.CheckBoxAvgPhase = New System.Windows.Forms.CheckBox()
        Me.LabelROISweepInterval = New System.Windows.Forms.Label()
        Me.NumericUpDownROISweepInterval = New System.Windows.Forms.NumericUpDown()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.ROI_FreqStepsNumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.LabelName = New System.Windows.Forms.Label()
        Me.ROI_StepSizeNumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me.ComboBoxROIOpModeSelect = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.ROI_StartFreqNumericUpDown = New System.Windows.Forms.NumericUpDown()
        Me.ButtonClose = New System.Windows.Forms.Button()
        Me.ToolTipROI = New System.Windows.Forms.ToolTip(Me.components)
        Me.ContextMenuStripROI = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.EditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditCopyToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NewToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RemoveToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.PanelROIConfig.SuspendLayout
        Me.GroupBoxStatistic.SuspendLayout
        CType(Me.NumericUpDownQualityOfMagnitudeWeight,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.NumericUpDownPhaseAtPeakMagWeight,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.NumericUpDownHalfWidthAtMagWeight,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.NumericUpDownFreqAtPeakMagWeight,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.NumericUpDownMagAtCrossoverFreq,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.NumericUpDownPeakMagnitudeWeight,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.NumericUpDownDeg90CrossoverFreqWeight,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.NumericUpDownAverageMagWeight,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.NumericUpDownPhaseAtMinMagWeight,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.NumericUpDownSumMagnitudesWeight,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.NumericUpDownFreqAtMinMagWeight,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.NumericUpDownAveragePhaseWeight,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.NumericUpDownMinMagnitudeWeight,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.NumericUpDownSummedPhaseWeight,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.NumericUpDownROISweepInterval,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.ROI_FreqStepsNumericUpDown,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.ROI_StepSizeNumericUpDown,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.ROI_StartFreqNumericUpDown,System.ComponentModel.ISupportInitialize).BeginInit
        Me.ContextMenuStripROI.SuspendLayout
        Me.SuspendLayout
        '
        'PanelROIConfig
        '
        Me.PanelROIConfig.BackColor = System.Drawing.Color.DimGray
        Me.PanelROIConfig.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PanelROIConfig.Controls.Add(Me.ButtonSave)
        Me.PanelROIConfig.Controls.Add(Me.TextBoxROIName)
        Me.PanelROIConfig.Controls.Add(Me.GroupBoxStatistic)
        Me.PanelROIConfig.Controls.Add(Me.LabelROISweepInterval)
        Me.PanelROIConfig.Controls.Add(Me.NumericUpDownROISweepInterval)
        Me.PanelROIConfig.Controls.Add(Me.Label11)
        Me.PanelROIConfig.Controls.Add(Me.ROI_FreqStepsNumericUpDown)
        Me.PanelROIConfig.Controls.Add(Me.Label10)
        Me.PanelROIConfig.Controls.Add(Me.LabelName)
        Me.PanelROIConfig.Controls.Add(Me.ROI_StepSizeNumericUpDown)
        Me.PanelROIConfig.Controls.Add(Me.ComboBoxROIOpModeSelect)
        Me.PanelROIConfig.Controls.Add(Me.Label7)
        Me.PanelROIConfig.Controls.Add(Me.Label9)
        Me.PanelROIConfig.Controls.Add(Me.ROI_StartFreqNumericUpDown)
        Me.PanelROIConfig.Controls.Add(Me.ButtonClose)
        Me.PanelROIConfig.Dock = System.Windows.Forms.DockStyle.Right
        Me.PanelROIConfig.ForeColor = System.Drawing.Color.White
        Me.PanelROIConfig.Location = New System.Drawing.Point(0, 0)
        Me.PanelROIConfig.Name = "PanelROIConfig"
        Me.PanelROIConfig.Size = New System.Drawing.Size(482, 505)
        Me.PanelROIConfig.TabIndex = 0
        '
        'ButtonSave
        '
        Me.ButtonSave.BackColor = System.Drawing.Color.Cyan
        Me.ButtonSave.Enabled = false
        Me.ButtonSave.ForeColor = System.Drawing.Color.Black
        Me.ButtonSave.Location = New System.Drawing.Point(307, 122)
        Me.ButtonSave.Name = "ButtonSave"
        Me.ButtonSave.Size = New System.Drawing.Size(75, 23)
        Me.ButtonSave.TabIndex = 143
        Me.ButtonSave.Text = "Save"
        Me.ButtonSave.UseVisualStyleBackColor = false
        '
        'TextBoxROIName
        '
        Me.TextBoxROIName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxROIName.Font = New System.Drawing.Font("MS Reference Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.TextBoxROIName.Location = New System.Drawing.Point(11, 22)
        Me.TextBoxROIName.Name = "TextBoxROIName"
        Me.TextBoxROIName.ReadOnly = true
        Me.TextBoxROIName.Size = New System.Drawing.Size(454, 23)
        Me.TextBoxROIName.TabIndex = 141
        '
        'GroupBoxStatistic
        '
        Me.GroupBoxStatistic.Controls.Add(Me.Label2)
        Me.GroupBoxStatistic.Controls.Add(Me.Label1)
        Me.GroupBoxStatistic.Controls.Add(Me.NumericUpDownQualityOfMagnitudeWeight)
        Me.GroupBoxStatistic.Controls.Add(Me.NumericUpDownPhaseAtPeakMagWeight)
        Me.GroupBoxStatistic.Controls.Add(Me.NumericUpDownHalfWidthAtMagWeight)
        Me.GroupBoxStatistic.Controls.Add(Me.NumericUpDownFreqAtPeakMagWeight)
        Me.GroupBoxStatistic.Controls.Add(Me.NumericUpDownMagAtCrossoverFreq)
        Me.GroupBoxStatistic.Controls.Add(Me.NumericUpDownPeakMagnitudeWeight)
        Me.GroupBoxStatistic.Controls.Add(Me.NumericUpDownDeg90CrossoverFreqWeight)
        Me.GroupBoxStatistic.Controls.Add(Me.NumericUpDownAverageMagWeight)
        Me.GroupBoxStatistic.Controls.Add(Me.NumericUpDownPhaseAtMinMagWeight)
        Me.GroupBoxStatistic.Controls.Add(Me.NumericUpDownSumMagnitudesWeight)
        Me.GroupBoxStatistic.Controls.Add(Me.NumericUpDownFreqAtMinMagWeight)
        Me.GroupBoxStatistic.Controls.Add(Me.NumericUpDownAveragePhaseWeight)
        Me.GroupBoxStatistic.Controls.Add(Me.NumericUpDownMinMagnitudeWeight)
        Me.GroupBoxStatistic.Controls.Add(Me.NumericUpDownSummedPhaseWeight)
        Me.GroupBoxStatistic.Controls.Add(Me.CheckBoxMagnitudeAtCrossoverFreq)
        Me.GroupBoxStatistic.Controls.Add(Me.CheckBoxPhaseAtMinMagnitude)
        Me.GroupBoxStatistic.Controls.Add(Me.CheckBoxPhaseAtPeakMagnitude)
        Me.GroupBoxStatistic.Controls.Add(Me.CheckBoxSummedPhase)
        Me.GroupBoxStatistic.Controls.Add(Me.CheckBox90DegCrossoverFreq)
        Me.GroupBoxStatistic.Controls.Add(Me.CheckBoxHalfWidthIfMagnitude)
        Me.GroupBoxStatistic.Controls.Add(Me.CheckBoxQualityOfMagnitude)
        Me.GroupBoxStatistic.Controls.Add(Me.CheckBoxMinMagnitude)
        Me.GroupBoxStatistic.Controls.Add(Me.CheckBoxFreqAtMinMagnitude)
        Me.GroupBoxStatistic.Controls.Add(Me.CheckBoxFreqAtPeakMagnitude)
        Me.GroupBoxStatistic.Controls.Add(Me.CheckBoxPeakMagnitude)
        Me.GroupBoxStatistic.Controls.Add(Me.CheckBoxSumMagnitude)
        Me.GroupBoxStatistic.Controls.Add(Me.CheckBoxAvgMagnitude)
        Me.GroupBoxStatistic.Controls.Add(Me.CheckBoxAvgPhase)
        Me.GroupBoxStatistic.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.GroupBoxStatistic.ForeColor = System.Drawing.Color.White
        Me.GroupBoxStatistic.Location = New System.Drawing.Point(11, 146)
        Me.GroupBoxStatistic.Name = "GroupBoxStatistic"
        Me.GroupBoxStatistic.Size = New System.Drawing.Size(455, 342)
        Me.GroupBoxStatistic.TabIndex = 134
        Me.GroupBoxStatistic.TabStop = false
        Me.GroupBoxStatistic.Text = "Statistic(s) Selection"
        '
        'Label2
        '
        Me.Label2.AutoSize = true
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline),System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label2.Location = New System.Drawing.Point(383, 17)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(51, 15)
        Me.Label2.TabIndex = 159
        Me.Label2.Text = "Weight"
        '
        'Label1
        '
        Me.Label1.AutoSize = true
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline),System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label1.Location = New System.Drawing.Point(160, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(51, 15)
        Me.Label1.TabIndex = 145
        Me.Label1.Text = "Weight"
        '
        'NumericUpDownQualityOfMagnitudeWeight
        '
        Me.NumericUpDownQualityOfMagnitudeWeight.Enabled = false
        Me.NumericUpDownQualityOfMagnitudeWeight.Location = New System.Drawing.Point(384, 307)
        Me.NumericUpDownQualityOfMagnitudeWeight.Maximum = New Decimal(New Integer() {255, 0, 0, 0})
        Me.NumericUpDownQualityOfMagnitudeWeight.Name = "NumericUpDownQualityOfMagnitudeWeight"
        Me.NumericUpDownQualityOfMagnitudeWeight.Size = New System.Drawing.Size(50, 21)
        Me.NumericUpDownQualityOfMagnitudeWeight.TabIndex = 143
        '
        'NumericUpDownPhaseAtPeakMagWeight
        '
        Me.NumericUpDownPhaseAtPeakMagWeight.Enabled = false
        Me.NumericUpDownPhaseAtPeakMagWeight.Location = New System.Drawing.Point(161, 307)
        Me.NumericUpDownPhaseAtPeakMagWeight.Maximum = New Decimal(New Integer() {255, 0, 0, 0})
        Me.NumericUpDownPhaseAtPeakMagWeight.Name = "NumericUpDownPhaseAtPeakMagWeight"
        Me.NumericUpDownPhaseAtPeakMagWeight.Size = New System.Drawing.Size(50, 21)
        Me.NumericUpDownPhaseAtPeakMagWeight.TabIndex = 142
        '
        'NumericUpDownHalfWidthAtMagWeight
        '
        Me.NumericUpDownHalfWidthAtMagWeight.Enabled = false
        Me.NumericUpDownHalfWidthAtMagWeight.Location = New System.Drawing.Point(384, 263)
        Me.NumericUpDownHalfWidthAtMagWeight.Maximum = New Decimal(New Integer() {255, 0, 0, 0})
        Me.NumericUpDownHalfWidthAtMagWeight.Name = "NumericUpDownHalfWidthAtMagWeight"
        Me.NumericUpDownHalfWidthAtMagWeight.Size = New System.Drawing.Size(50, 21)
        Me.NumericUpDownHalfWidthAtMagWeight.TabIndex = 141
        '
        'NumericUpDownFreqAtPeakMagWeight
        '
        Me.NumericUpDownFreqAtPeakMagWeight.Enabled = false
        Me.NumericUpDownFreqAtPeakMagWeight.Location = New System.Drawing.Point(161, 263)
        Me.NumericUpDownFreqAtPeakMagWeight.Maximum = New Decimal(New Integer() {255, 0, 0, 0})
        Me.NumericUpDownFreqAtPeakMagWeight.Name = "NumericUpDownFreqAtPeakMagWeight"
        Me.NumericUpDownFreqAtPeakMagWeight.Size = New System.Drawing.Size(50, 21)
        Me.NumericUpDownFreqAtPeakMagWeight.TabIndex = 140
        '
        'NumericUpDownMagAtCrossoverFreq
        '
        Me.NumericUpDownMagAtCrossoverFreq.Enabled = false
        Me.NumericUpDownMagAtCrossoverFreq.Location = New System.Drawing.Point(384, 219)
        Me.NumericUpDownMagAtCrossoverFreq.Maximum = New Decimal(New Integer() {255, 0, 0, 0})
        Me.NumericUpDownMagAtCrossoverFreq.Name = "NumericUpDownMagAtCrossoverFreq"
        Me.NumericUpDownMagAtCrossoverFreq.Size = New System.Drawing.Size(50, 21)
        Me.NumericUpDownMagAtCrossoverFreq.TabIndex = 139
        '
        'NumericUpDownPeakMagnitudeWeight
        '
        Me.NumericUpDownPeakMagnitudeWeight.Enabled = false
        Me.NumericUpDownPeakMagnitudeWeight.Location = New System.Drawing.Point(162, 220)
        Me.NumericUpDownPeakMagnitudeWeight.Maximum = New Decimal(New Integer() {255, 0, 0, 0})
        Me.NumericUpDownPeakMagnitudeWeight.Name = "NumericUpDownPeakMagnitudeWeight"
        Me.NumericUpDownPeakMagnitudeWeight.Size = New System.Drawing.Size(50, 21)
        Me.NumericUpDownPeakMagnitudeWeight.TabIndex = 138
        '
        'NumericUpDownDeg90CrossoverFreqWeight
        '
        Me.NumericUpDownDeg90CrossoverFreqWeight.Enabled = false
        Me.NumericUpDownDeg90CrossoverFreqWeight.Location = New System.Drawing.Point(384, 175)
        Me.NumericUpDownDeg90CrossoverFreqWeight.Maximum = New Decimal(New Integer() {255, 0, 0, 0})
        Me.NumericUpDownDeg90CrossoverFreqWeight.Name = "NumericUpDownDeg90CrossoverFreqWeight"
        Me.NumericUpDownDeg90CrossoverFreqWeight.Size = New System.Drawing.Size(50, 21)
        Me.NumericUpDownDeg90CrossoverFreqWeight.TabIndex = 137
        '
        'NumericUpDownAverageMagWeight
        '
        Me.NumericUpDownAverageMagWeight.Enabled = false
        Me.NumericUpDownAverageMagWeight.Location = New System.Drawing.Point(161, 176)
        Me.NumericUpDownAverageMagWeight.Maximum = New Decimal(New Integer() {255, 0, 0, 0})
        Me.NumericUpDownAverageMagWeight.Name = "NumericUpDownAverageMagWeight"
        Me.NumericUpDownAverageMagWeight.Size = New System.Drawing.Size(50, 21)
        Me.NumericUpDownAverageMagWeight.TabIndex = 136
        '
        'NumericUpDownPhaseAtMinMagWeight
        '
        Me.NumericUpDownPhaseAtMinMagWeight.Enabled = false
        Me.NumericUpDownPhaseAtMinMagWeight.Location = New System.Drawing.Point(384, 131)
        Me.NumericUpDownPhaseAtMinMagWeight.Maximum = New Decimal(New Integer() {255, 0, 0, 0})
        Me.NumericUpDownPhaseAtMinMagWeight.Name = "NumericUpDownPhaseAtMinMagWeight"
        Me.NumericUpDownPhaseAtMinMagWeight.Size = New System.Drawing.Size(50, 21)
        Me.NumericUpDownPhaseAtMinMagWeight.TabIndex = 135
        '
        'NumericUpDownSumMagnitudesWeight
        '
        Me.NumericUpDownSumMagnitudesWeight.Enabled = false
        Me.NumericUpDownSumMagnitudesWeight.Location = New System.Drawing.Point(161, 132)
        Me.NumericUpDownSumMagnitudesWeight.Maximum = New Decimal(New Integer() {255, 0, 0, 0})
        Me.NumericUpDownSumMagnitudesWeight.Name = "NumericUpDownSumMagnitudesWeight"
        Me.NumericUpDownSumMagnitudesWeight.Size = New System.Drawing.Size(50, 21)
        Me.NumericUpDownSumMagnitudesWeight.TabIndex = 134
        '
        'NumericUpDownFreqAtMinMagWeight
        '
        Me.NumericUpDownFreqAtMinMagWeight.Enabled = false
        Me.NumericUpDownFreqAtMinMagWeight.Location = New System.Drawing.Point(384, 88)
        Me.NumericUpDownFreqAtMinMagWeight.Maximum = New Decimal(New Integer() {255, 0, 0, 0})
        Me.NumericUpDownFreqAtMinMagWeight.Name = "NumericUpDownFreqAtMinMagWeight"
        Me.NumericUpDownFreqAtMinMagWeight.Size = New System.Drawing.Size(50, 21)
        Me.NumericUpDownFreqAtMinMagWeight.TabIndex = 133
        '
        'NumericUpDownAveragePhaseWeight
        '
        Me.NumericUpDownAveragePhaseWeight.Enabled = false
        Me.NumericUpDownAveragePhaseWeight.Location = New System.Drawing.Point(161, 88)
        Me.NumericUpDownAveragePhaseWeight.Maximum = New Decimal(New Integer() {255, 0, 0, 0})
        Me.NumericUpDownAveragePhaseWeight.Name = "NumericUpDownAveragePhaseWeight"
        Me.NumericUpDownAveragePhaseWeight.Size = New System.Drawing.Size(50, 21)
        Me.NumericUpDownAveragePhaseWeight.TabIndex = 132
        '
        'NumericUpDownMinMagnitudeWeight
        '
        Me.NumericUpDownMinMagnitudeWeight.Enabled = false
        Me.NumericUpDownMinMagnitudeWeight.Location = New System.Drawing.Point(384, 44)
        Me.NumericUpDownMinMagnitudeWeight.Maximum = New Decimal(New Integer() {255, 0, 0, 0})
        Me.NumericUpDownMinMagnitudeWeight.Name = "NumericUpDownMinMagnitudeWeight"
        Me.NumericUpDownMinMagnitudeWeight.Size = New System.Drawing.Size(50, 21)
        Me.NumericUpDownMinMagnitudeWeight.TabIndex = 131
        '
        'NumericUpDownSummedPhaseWeight
        '
        Me.NumericUpDownSummedPhaseWeight.Enabled = false
        Me.NumericUpDownSummedPhaseWeight.Location = New System.Drawing.Point(161, 44)
        Me.NumericUpDownSummedPhaseWeight.Maximum = New Decimal(New Integer() {255, 0, 0, 0})
        Me.NumericUpDownSummedPhaseWeight.Name = "NumericUpDownSummedPhaseWeight"
        Me.NumericUpDownSummedPhaseWeight.Size = New System.Drawing.Size(50, 21)
        Me.NumericUpDownSummedPhaseWeight.TabIndex = 130
        '
        'CheckBoxMagnitudeAtCrossoverFreq
        '
        Me.CheckBoxMagnitudeAtCrossoverFreq.AutoSize = true
        Me.CheckBoxMagnitudeAtCrossoverFreq.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.CheckBoxMagnitudeAtCrossoverFreq.Location = New System.Drawing.Point(239, 199)
        Me.CheckBoxMagnitudeAtCrossoverFreq.Name = "CheckBoxMagnitudeAtCrossoverFreq"
        Me.CheckBoxMagnitudeAtCrossoverFreq.Size = New System.Drawing.Size(210, 19)
        Me.CheckBoxMagnitudeAtCrossoverFreq.TabIndex = 129
        Me.CheckBoxMagnitudeAtCrossoverFreq.Text = "Magnitude At Crossover Freq"
        Me.CheckBoxMagnitudeAtCrossoverFreq.UseVisualStyleBackColor = true
        '
        'CheckBoxPhaseAtMinMagnitude
        '
        Me.CheckBoxPhaseAtMinMagnitude.AutoSize = true
        Me.CheckBoxPhaseAtMinMagnitude.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.CheckBoxPhaseAtMinMagnitude.Location = New System.Drawing.Point(239, 111)
        Me.CheckBoxPhaseAtMinMagnitude.Name = "CheckBoxPhaseAtMinMagnitude"
        Me.CheckBoxPhaseAtMinMagnitude.Size = New System.Drawing.Size(186, 19)
        Me.CheckBoxPhaseAtMinMagnitude.TabIndex = 128
        Me.CheckBoxPhaseAtMinMagnitude.Text = "Phase At Min Magnitiude"
        Me.CheckBoxPhaseAtMinMagnitude.UseVisualStyleBackColor = true
        '
        'CheckBoxPhaseAtPeakMagnitude
        '
        Me.CheckBoxPhaseAtPeakMagnitude.AutoSize = true
        Me.CheckBoxPhaseAtPeakMagnitude.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.CheckBoxPhaseAtPeakMagnitude.Location = New System.Drawing.Point(12, 287)
        Me.CheckBoxPhaseAtPeakMagnitude.Name = "CheckBoxPhaseAtPeakMagnitude"
        Me.CheckBoxPhaseAtPeakMagnitude.Size = New System.Drawing.Size(190, 19)
        Me.CheckBoxPhaseAtPeakMagnitude.TabIndex = 127
        Me.CheckBoxPhaseAtPeakMagnitude.Text = "Phase At Peak Magnitude"
        Me.CheckBoxPhaseAtPeakMagnitude.UseVisualStyleBackColor = true
        '
        'CheckBoxSummedPhase
        '
        Me.CheckBoxSummedPhase.AutoSize = true
        Me.CheckBoxSummedPhase.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.CheckBoxSummedPhase.ForeColor = System.Drawing.Color.White
        Me.CheckBoxSummedPhase.Location = New System.Drawing.Point(12, 23)
        Me.CheckBoxSummedPhase.Name = "CheckBoxSummedPhase"
        Me.CheckBoxSummedPhase.Size = New System.Drawing.Size(127, 19)
        Me.CheckBoxSummedPhase.TabIndex = 126
        Me.CheckBoxSummedPhase.Text = "Summed Phase"
        Me.CheckBoxSummedPhase.UseVisualStyleBackColor = true
        '
        'CheckBox90DegCrossoverFreq
        '
        Me.CheckBox90DegCrossoverFreq.AutoSize = true
        Me.CheckBox90DegCrossoverFreq.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.CheckBox90DegCrossoverFreq.Location = New System.Drawing.Point(239, 155)
        Me.CheckBox90DegCrossoverFreq.Name = "CheckBox90DegCrossoverFreq"
        Me.CheckBox90DegCrossoverFreq.Size = New System.Drawing.Size(209, 19)
        Me.CheckBox90DegCrossoverFreq.TabIndex = 125
        Me.CheckBox90DegCrossoverFreq.Text = "90 Deg Crossover Frequency"
        Me.CheckBox90DegCrossoverFreq.UseVisualStyleBackColor = true
        '
        'CheckBoxHalfWidthIfMagnitude
        '
        Me.CheckBoxHalfWidthIfMagnitude.AutoSize = true
        Me.CheckBoxHalfWidthIfMagnitude.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.CheckBoxHalfWidthIfMagnitude.Location = New System.Drawing.Point(239, 243)
        Me.CheckBoxHalfWidthIfMagnitude.Name = "CheckBoxHalfWidthIfMagnitude"
        Me.CheckBoxHalfWidthIfMagnitude.Size = New System.Drawing.Size(176, 19)
        Me.CheckBoxHalfWidthIfMagnitude.TabIndex = 122
        Me.CheckBoxHalfWidthIfMagnitude.Text = "Half Width If Magnitude"
        Me.CheckBoxHalfWidthIfMagnitude.UseVisualStyleBackColor = true
        '
        'CheckBoxQualityOfMagnitude
        '
        Me.CheckBoxQualityOfMagnitude.AutoSize = true
        Me.CheckBoxQualityOfMagnitude.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.CheckBoxQualityOfMagnitude.Location = New System.Drawing.Point(239, 287)
        Me.CheckBoxQualityOfMagnitude.Name = "CheckBoxQualityOfMagnitude"
        Me.CheckBoxQualityOfMagnitude.Size = New System.Drawing.Size(160, 19)
        Me.CheckBoxQualityOfMagnitude.TabIndex = 121
        Me.CheckBoxQualityOfMagnitude.Text = "Quality Of Magnitude"
        Me.CheckBoxQualityOfMagnitude.UseVisualStyleBackColor = true
        '
        'CheckBoxMinMagnitude
        '
        Me.CheckBoxMinMagnitude.AutoSize = true
        Me.CheckBoxMinMagnitude.BackColor = System.Drawing.Color.DimGray
        Me.CheckBoxMinMagnitude.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.CheckBoxMinMagnitude.ForeColor = System.Drawing.Color.White
        Me.CheckBoxMinMagnitude.Location = New System.Drawing.Point(239, 23)
        Me.CheckBoxMinMagnitude.Name = "CheckBoxMinMagnitude"
        Me.CheckBoxMinMagnitude.Size = New System.Drawing.Size(122, 19)
        Me.CheckBoxMinMagnitude.TabIndex = 120
        Me.CheckBoxMinMagnitude.Text = "Min Magnitude"
        Me.CheckBoxMinMagnitude.UseVisualStyleBackColor = false
        '
        'CheckBoxFreqAtMinMagnitude
        '
        Me.CheckBoxFreqAtMinMagnitude.AutoSize = true
        Me.CheckBoxFreqAtMinMagnitude.Cursor = System.Windows.Forms.Cursors.Default
        Me.CheckBoxFreqAtMinMagnitude.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.CheckBoxFreqAtMinMagnitude.Location = New System.Drawing.Point(239, 67)
        Me.CheckBoxFreqAtMinMagnitude.Name = "CheckBoxFreqAtMinMagnitude"
        Me.CheckBoxFreqAtMinMagnitude.Size = New System.Drawing.Size(208, 19)
        Me.CheckBoxFreqAtMinMagnitude.TabIndex = 119
        Me.CheckBoxFreqAtMinMagnitude.Text = "Frequency At Min Magnitude"
        Me.CheckBoxFreqAtMinMagnitude.UseVisualStyleBackColor = true
        '
        'CheckBoxFreqAtPeakMagnitude
        '
        Me.CheckBoxFreqAtPeakMagnitude.AutoSize = true
        Me.CheckBoxFreqAtPeakMagnitude.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.CheckBoxFreqAtPeakMagnitude.Location = New System.Drawing.Point(12, 243)
        Me.CheckBoxFreqAtPeakMagnitude.Name = "CheckBoxFreqAtPeakMagnitude"
        Me.CheckBoxFreqAtPeakMagnitude.Size = New System.Drawing.Size(179, 19)
        Me.CheckBoxFreqAtPeakMagnitude.TabIndex = 118
        Me.CheckBoxFreqAtPeakMagnitude.Text = "Freq At Peak Magnitude"
        Me.CheckBoxFreqAtPeakMagnitude.UseVisualStyleBackColor = true
        '
        'CheckBoxPeakMagnitude
        '
        Me.CheckBoxPeakMagnitude.AutoSize = true
        Me.CheckBoxPeakMagnitude.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.CheckBoxPeakMagnitude.Location = New System.Drawing.Point(12, 199)
        Me.CheckBoxPeakMagnitude.Name = "CheckBoxPeakMagnitude"
        Me.CheckBoxPeakMagnitude.Size = New System.Drawing.Size(130, 19)
        Me.CheckBoxPeakMagnitude.TabIndex = 117
        Me.CheckBoxPeakMagnitude.Text = "Peak Magnitude"
        Me.CheckBoxPeakMagnitude.UseVisualStyleBackColor = true
        '
        'CheckBoxSumMagnitude
        '
        Me.CheckBoxSumMagnitude.AutoSize = true
        Me.CheckBoxSumMagnitude.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.CheckBoxSumMagnitude.Location = New System.Drawing.Point(12, 111)
        Me.CheckBoxSumMagnitude.Name = "CheckBoxSumMagnitude"
        Me.CheckBoxSumMagnitude.Size = New System.Drawing.Size(134, 19)
        Me.CheckBoxSumMagnitude.TabIndex = 116
        Me.CheckBoxSumMagnitude.Text = "Sum Magnitudes"
        Me.CheckBoxSumMagnitude.UseVisualStyleBackColor = true
        '
        'CheckBoxAvgMagnitude
        '
        Me.CheckBoxAvgMagnitude.AutoSize = true
        Me.CheckBoxAvgMagnitude.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.CheckBoxAvgMagnitude.Location = New System.Drawing.Point(12, 155)
        Me.CheckBoxAvgMagnitude.Name = "CheckBoxAvgMagnitude"
        Me.CheckBoxAvgMagnitude.Size = New System.Drawing.Size(149, 19)
        Me.CheckBoxAvgMagnitude.TabIndex = 115
        Me.CheckBoxAvgMagnitude.Text = "Average Magnitude"
        Me.CheckBoxAvgMagnitude.UseVisualStyleBackColor = true
        '
        'CheckBoxAvgPhase
        '
        Me.CheckBoxAvgPhase.AutoSize = true
        Me.CheckBoxAvgPhase.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.CheckBoxAvgPhase.Location = New System.Drawing.Point(12, 67)
        Me.CheckBoxAvgPhase.Name = "CheckBoxAvgPhase"
        Me.CheckBoxAvgPhase.Size = New System.Drawing.Size(121, 19)
        Me.CheckBoxAvgPhase.TabIndex = 114
        Me.CheckBoxAvgPhase.Text = "Average Phase"
        Me.CheckBoxAvgPhase.UseVisualStyleBackColor = true
        '
        'LabelROISweepInterval
        '
        Me.LabelROISweepInterval.AutoSize = true
        Me.LabelROISweepInterval.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.LabelROISweepInterval.Location = New System.Drawing.Point(335, 57)
        Me.LabelROISweepInterval.Name = "LabelROISweepInterval"
        Me.LabelROISweepInterval.Size = New System.Drawing.Size(134, 15)
        Me.LabelROISweepInterval.TabIndex = 135
        Me.LabelROISweepInterval.Text = "Sweep Interval (ms)"
        '
        'NumericUpDownROISweepInterval
        '
        Me.NumericUpDownROISweepInterval.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.NumericUpDownROISweepInterval.Location = New System.Drawing.Point(242, 55)
        Me.NumericUpDownROISweepInterval.Maximum = New Decimal(New Integer() {65534, 0, 0, 0})
        Me.NumericUpDownROISweepInterval.Name = "NumericUpDownROISweepInterval"
        Me.NumericUpDownROISweepInterval.Size = New System.Drawing.Size(90, 21)
        Me.NumericUpDownROISweepInterval.TabIndex = 133
        Me.NumericUpDownROISweepInterval.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.NumericUpDownROISweepInterval.Value = New Decimal(New Integer() {100, 0, 0, 0})
        '
        'Label11
        '
        Me.Label11.AutoSize = true
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label11.Location = New System.Drawing.Point(104, 119)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(76, 15)
        Me.Label11.TabIndex = 132
        Me.Label11.Text = "Freq Steps"
        '
        'ROI_FreqStepsNumericUpDown
        '
        Me.ROI_FreqStepsNumericUpDown.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ROI_FreqStepsNumericUpDown.Location = New System.Drawing.Point(11, 117)
        Me.ROI_FreqStepsNumericUpDown.Maximum = New Decimal(New Integer() {1024, 0, 0, 0})
        Me.ROI_FreqStepsNumericUpDown.Name = "ROI_FreqStepsNumericUpDown"
        Me.ROI_FreqStepsNumericUpDown.Size = New System.Drawing.Size(90, 21)
        Me.ROI_FreqStepsNumericUpDown.TabIndex = 129
        Me.ROI_FreqStepsNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ROI_FreqStepsNumericUpDown.Value = New Decimal(New Integer() {512, 0, 0, 0})
        '
        'Label10
        '
        Me.Label10.AutoSize = true
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label10.Location = New System.Drawing.Point(335, 88)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(110, 15)
        Me.Label10.TabIndex = 131
        Me.Label10.Text = "Step Size  (kHz)"
        '
        'LabelName
        '
        Me.LabelName.AutoSize = true
        Me.LabelName.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.LabelName.Location = New System.Drawing.Point(216, 3)
        Me.LabelName.Name = "LabelName"
        Me.LabelName.Size = New System.Drawing.Size(45, 15)
        Me.LabelName.TabIndex = 123
        Me.LabelName.Text = "Name"
        '
        'ROI_StepSizeNumericUpDown
        '
        Me.ROI_StepSizeNumericUpDown.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ROI_StepSizeNumericUpDown.Location = New System.Drawing.Point(242, 86)
        Me.ROI_StepSizeNumericUpDown.Margin = New System.Windows.Forms.Padding(2)
        Me.ROI_StepSizeNumericUpDown.Maximum = New Decimal(New Integer() {2700000, 0, 0, 0})
        Me.ROI_StepSizeNumericUpDown.Name = "ROI_StepSizeNumericUpDown"
        Me.ROI_StepSizeNumericUpDown.Size = New System.Drawing.Size(90, 21)
        Me.ROI_StepSizeNumericUpDown.TabIndex = 130
        Me.ROI_StepSizeNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ROI_StepSizeNumericUpDown.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'ComboBoxROIOpModeSelect
        '
        Me.ComboBoxROIOpModeSelect.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ComboBoxROIOpModeSelect.FormattingEnabled = true
        Me.ComboBoxROIOpModeSelect.Items.AddRange(New Object() {"S12", "S11", "Bypass", "S11 Load", "S11 Open", "S11 Zero"})
        Me.ComboBoxROIOpModeSelect.Location = New System.Drawing.Point(11, 55)
        Me.ComboBoxROIOpModeSelect.Margin = New System.Windows.Forms.Padding(2)
        Me.ComboBoxROIOpModeSelect.Name = "ComboBoxROIOpModeSelect"
        Me.ComboBoxROIOpModeSelect.Size = New System.Drawing.Size(90, 21)
        Me.ComboBoxROIOpModeSelect.TabIndex = 124
        Me.ComboBoxROIOpModeSelect.Text = "S12"
        '
        'Label7
        '
        Me.Label7.AutoSize = true
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label7.Location = New System.Drawing.Point(104, 59)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(114, 15)
        Me.Label7.TabIndex = 125
        Me.Label7.Text = "Operating  Mode"
        '
        'Label9
        '
        Me.Label9.AutoSize = true
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label9.Location = New System.Drawing.Point(104, 88)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(113, 15)
        Me.Label9.TabIndex = 128
        Me.Label9.Text = "Start Freq (MHz)"
        '
        'ROI_StartFreqNumericUpDown
        '
        Me.ROI_StartFreqNumericUpDown.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ROI_StartFreqNumericUpDown.Location = New System.Drawing.Point(11, 86)
        Me.ROI_StartFreqNumericUpDown.Maximum = New Decimal(New Integer() {2700000, 0, 0, 0})
        Me.ROI_StartFreqNumericUpDown.Minimum = New Decimal(New Integer() {350000, 0, 0, 0})
        Me.ROI_StartFreqNumericUpDown.Name = "ROI_StartFreqNumericUpDown"
        Me.ROI_StartFreqNumericUpDown.Size = New System.Drawing.Size(90, 21)
        Me.ROI_StartFreqNumericUpDown.TabIndex = 127
        Me.ROI_StartFreqNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ROI_StartFreqNumericUpDown.Value = New Decimal(New Integer() {400000, 0, 0, 0})
        '
        'ButtonClose
        '
        Me.ButtonClose.BackColor = System.Drawing.Color.Gold
        Me.ButtonClose.ForeColor = System.Drawing.Color.Black
        Me.ButtonClose.Location = New System.Drawing.Point(391, 122)
        Me.ButtonClose.Name = "ButtonClose"
        Me.ButtonClose.Size = New System.Drawing.Size(75, 23)
        Me.ButtonClose.TabIndex = 144
        Me.ButtonClose.Text = "Close"
        Me.ButtonClose.UseVisualStyleBackColor = false
        '
        'ContextMenuStripROI
        '
        Me.ContextMenuStripROI.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EditToolStripMenuItem, Me.EditCopyToolStripMenuItem, Me.NewToolStripMenuItem, Me.RemoveToolStripMenuItem1})
        Me.ContextMenuStripROI.Name = "ContextMenuStripROI"
        Me.ContextMenuStripROI.Size = New System.Drawing.Size(126, 92)
        '
        'EditToolStripMenuItem
        '
        Me.EditToolStripMenuItem.Enabled = false
        Me.EditToolStripMenuItem.Name = "EditToolStripMenuItem"
        Me.EditToolStripMenuItem.Size = New System.Drawing.Size(125, 22)
        Me.EditToolStripMenuItem.Text = "Edit"
        '
        'EditCopyToolStripMenuItem
        '
        Me.EditCopyToolStripMenuItem.Enabled = false
        Me.EditCopyToolStripMenuItem.Name = "EditCopyToolStripMenuItem"
        Me.EditCopyToolStripMenuItem.Size = New System.Drawing.Size(125, 22)
        Me.EditCopyToolStripMenuItem.Text = "Edit Copy"
        '
        'NewToolStripMenuItem
        '
        Me.NewToolStripMenuItem.Name = "NewToolStripMenuItem"
        Me.NewToolStripMenuItem.Size = New System.Drawing.Size(125, 22)
        Me.NewToolStripMenuItem.Text = "New"
        '
        'RemoveToolStripMenuItem1
        '
        Me.RemoveToolStripMenuItem1.Enabled = false
        Me.RemoveToolStripMenuItem1.Name = "RemoveToolStripMenuItem1"
        Me.RemoveToolStripMenuItem1.Size = New System.Drawing.Size(125, 22)
        Me.RemoveToolStripMenuItem1.Text = "Remove"
        '
        'FormRoiEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.DimGray
        Me.ClientSize = New System.Drawing.Size(482, 505)
        Me.Controls.Add(Me.PanelROIConfig)
        Me.ForeColor = System.Drawing.Color.White
        Me.Icon = CType(resources.GetObject("$this.Icon"),System.Drawing.Icon)
        Me.Name = "FormRoiEdit"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Region Of Interest Edit"
        Me.PanelROIConfig.ResumeLayout(false)
        Me.PanelROIConfig.PerformLayout
        Me.GroupBoxStatistic.ResumeLayout(false)
        Me.GroupBoxStatistic.PerformLayout
        CType(Me.NumericUpDownQualityOfMagnitudeWeight,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.NumericUpDownPhaseAtPeakMagWeight,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.NumericUpDownHalfWidthAtMagWeight,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.NumericUpDownFreqAtPeakMagWeight,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.NumericUpDownMagAtCrossoverFreq,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.NumericUpDownPeakMagnitudeWeight,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.NumericUpDownDeg90CrossoverFreqWeight,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.NumericUpDownAverageMagWeight,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.NumericUpDownPhaseAtMinMagWeight,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.NumericUpDownSumMagnitudesWeight,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.NumericUpDownFreqAtMinMagWeight,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.NumericUpDownAveragePhaseWeight,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.NumericUpDownMinMagnitudeWeight,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.NumericUpDownSummedPhaseWeight,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.NumericUpDownROISweepInterval,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.ROI_FreqStepsNumericUpDown,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.ROI_StepSizeNumericUpDown,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.ROI_StartFreqNumericUpDown,System.ComponentModel.ISupportInitialize).EndInit
        Me.ContextMenuStripROI.ResumeLayout(false)
        Me.ResumeLayout(false)

End Sub

    Friend WithEvents PanelROIConfig As Panel
    Friend WithEvents ToolTipROI As ToolTip
    Friend WithEvents GroupBoxStatistic As GroupBox
    Friend WithEvents CheckBoxMagnitudeAtCrossoverFreq As CheckBox
    Friend WithEvents CheckBoxPhaseAtMinMagnitude As CheckBox
    Friend WithEvents CheckBoxPhaseAtPeakMagnitude As CheckBox
    Friend WithEvents CheckBoxSummedPhase As CheckBox
    Friend WithEvents CheckBox90DegCrossoverFreq As CheckBox
    Friend WithEvents CheckBoxHalfWidthIfMagnitude As CheckBox
    Friend WithEvents CheckBoxQualityOfMagnitude As CheckBox
    Friend WithEvents CheckBoxMinMagnitude As CheckBox
    Friend WithEvents CheckBoxFreqAtMinMagnitude As CheckBox
    Friend WithEvents CheckBoxFreqAtPeakMagnitude As CheckBox
    Friend WithEvents CheckBoxPeakMagnitude As CheckBox
    Friend WithEvents CheckBoxSumMagnitude As CheckBox
    Friend WithEvents CheckBoxAvgMagnitude As CheckBox
    Friend WithEvents CheckBoxAvgPhase As CheckBox
    Friend WithEvents LabelROISweepInterval As Label
    Friend WithEvents NumericUpDownROISweepInterval As NumericUpDown
    Friend WithEvents Label11 As Label
    Friend WithEvents ROI_FreqStepsNumericUpDown As NumericUpDown
    Friend WithEvents Label10 As Label
    Friend WithEvents LabelName As Label
    Public WithEvents ROI_StepSizeNumericUpDown As NumericUpDown
    Friend WithEvents ComboBoxROIOpModeSelect As ComboBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents ROI_StartFreqNumericUpDown As NumericUpDown
    Friend WithEvents ContextMenuStripROI As ContextMenuStrip
    Friend WithEvents EditToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents NewToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TextBoxROIName As TextBox
    Friend WithEvents RemoveToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents EditCopyToolStripMenuItem As ToolStripMenuItem
    Private WithEvents ButtonSave As Button
    Private WithEvents ButtonClose As Button
    Friend WithEvents NumericUpDownMinMagnitudeWeight As NumericUpDown
    Friend WithEvents NumericUpDownSummedPhaseWeight As NumericUpDown
    Friend WithEvents NumericUpDownQualityOfMagnitudeWeight As NumericUpDown
    Friend WithEvents NumericUpDownPhaseAtPeakMagWeight As NumericUpDown
    Friend WithEvents NumericUpDownHalfWidthAtMagWeight As NumericUpDown
    Friend WithEvents NumericUpDownFreqAtPeakMagWeight As NumericUpDown
    Friend WithEvents NumericUpDownMagAtCrossoverFreq As NumericUpDown
    Friend WithEvents NumericUpDownPeakMagnitudeWeight As NumericUpDown
    Friend WithEvents NumericUpDownDeg90CrossoverFreqWeight As NumericUpDown
    Friend WithEvents NumericUpDownAverageMagWeight As NumericUpDown
    Friend WithEvents NumericUpDownPhaseAtMinMagWeight As NumericUpDown
    Friend WithEvents NumericUpDownSumMagnitudesWeight As NumericUpDown
    Friend WithEvents NumericUpDownFreqAtMinMagWeight As NumericUpDown
    Friend WithEvents NumericUpDownAveragePhaseWeight As NumericUpDown
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
End Class
