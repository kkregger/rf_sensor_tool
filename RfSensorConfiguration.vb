﻿Imports System.Threading
Imports System.Collections.Concurrent


Public NotInheritable Class RfSensorConfiguration

#Region "Constants"

    ' Parameter Tool Names.
    Public Const ActiveState         As String = "Active_State"
    Public Const ApplicationDevLevel As String = "App_DevLev"
    Public Const ApplicationID       As String = "App_ID"
    Public Const ApplicationLetter   As String = "AppLetter"
    Public Const ApplicationRevLev   As String = "App_RevLev"
    Public Const SoftwareDevLevel    As String = "SW_DevRev"
    Public Const SoftwareID          As String = "SW_ID"
    Public Const SoftwareRevLevel    As String = "SW_MajorRev"
    Public Const AppVoldDirLetter    As String = "VoltDirLetter"
    Public Const CpuTemperature      As String = "InternalTemperature"

    ' Calibration ID numbers
    Public Const CalibrationVersion As UShort = 1
    Public Const RoiStartFreq1      As UShort = 42
    Public Const RoiStartFreq2      As UShort = 43
    Public Const RoiStartFreq3      As UShort = 44
    Public Const RoiFreqSteps1      As UShort = 58
    Public Const RoiFreqSteps2      As UShort = 59
    Public Const RoiFreqSteps3      As UShort = 60
    Public Const InfoMessageID      As UShort = 197
    Public Const ToolConnectTimeout As UShort = 241
    Public Const RoiFreqStepSize1   As UShort = 368
    Public Const RoiFreqStepSize2   As UShort = 369
    Public Const RoiFreqStepSize3   As UShort = 370

#End Region     ' Constants

#Region "Fields"

    ' Devide ID array.
    Private Shared _deviceID As Byte()

    ' DBL software version array.
    Private Shared _dblSoftwareVersion As Byte()

    ' Application Cals dictionary keyed by CalVariable name.
    Public Shared Property AppCalDict As ConcurrentDictionary(Of UShort, AppCalibration)

    ' Usage data dictionary keyed by VarName.
    Public Shared Property UsageDataDict As ConcurrentDictionary(Of String, UsageData)

    ' E2 map dictionary keyed by vname.
    Public Shared Property E2MapDict As ConcurrentDictionary(Of String, E2MapEntry)

    ' Parameter dictionary keyed by VariableName.
    Public Shared Property ParamTblDict As ConcurrentDictionary(Of String, ParamTblEntry)

    ' Dictionary of calibration values read from the RF Sensor.
    Public Shared Property CalibrationValueDict As New ConcurrentDictionary(Of UShort, Uinteger)

    ' Event to raise when there is an error to be logged.
    Private Shared Event LogErrorEvent As ActivityLog.LogErrorStackDelegate

#End Region     ' Fields

#Region "Properties"

    Public Shared Property MicroID() As Byte

    Public Shared Property CalConfig() As Byte

    Public Shared Property EepromConfig() As Byte

    Public Shared Property ParameterIdLevel() As Byte

    Public Shared Property BaseTime() As Byte

    Public Shared Property UsageTime() As UInteger

    Public Shared Property DeviceID() As Byte()
        Get
            ' Arrays are reference type so return copy.
            Dim id As Byte() = Nothing

            If (_deviceID IsNot Nothing) Then

                id = New Byte(_deviceID.Length - 1) {}
                Array.Copy(_deviceID, id, id.Length)

            End If

            Return If(id, New Byte(CanMessageLib.DeviceIdLength - 1){})
        End Get
        Set(value As Byte())
            _deviceID = value
        End Set
    End Property

    Public Shared Property MicroCfg() As Byte

    Public Shared Property MicroSize() As Byte

    Public Shared Property HardwareID() As Byte

    Public Shared Property MajorRev() As Byte

    Public Shared Property MinorRev() As Byte

    Public Shared Property DevelopRev() As Byte

    ' Number of Calibration table entries being retrieved.
    Public Shared Property CalibrationEntries() As UInteger

    ' Transmission/Operating mode that Calibration was performed in.
    Public Shared Property OperatingMode() As Byte

    ' Calibration beginning board temperature.
    Public Shared Property BoardTemp() As Short

    ' Production HW Line words.
    Public Shared Property HwLineWord1() As UShort

    Public Shared Property HwLineWord2() As UShort

    Public Shared Property DblSoftwareVersion() As Byte()
        Get
            ' Arrays are reference type so return copy.
            Dim version As Byte() = Nothing

            If (_dblSoftwareVersion IsNot Nothing) Then

                version = New Byte(_dblSoftwareVersion.Length - 1) {}
                Array.Copy(_dblSoftwareVersion, version, version.Length)

            End If

            Return If(version, New Byte(CanMessageLib.DblSwVersionLength - 1){})
        End Get
        Set(value As Byte())
            _dblSoftwareVersion = value
        End Set
    End Property

#End Region     ' Properties

#Region "Methods"

    Shared Sub New()

        ' Add error logging delegate.
        AddHandler LogErrorEvent, AddressOf ActivityLog.LogError

    End Sub

    Public Shared Function ReadCalibrationValues() As Boolean

        Dim _delayWaitHandle = New EventWaitHandle(False, EventResetMode.AutoReset)

        Try
            Dim result    As Boolean
            Dim calValue  As UInteger
            Dim failCount As Integer

            CalibrationValueDict.Clear()
            If (AppCalDict IsNot Nothing) Then
                For Each appCal In AppCalDict
                    If (CanMessageLib.SendAppCalReadMsg(appCal.Key, calValue) OrElse
                        CanMessageLib.SendAppCalReadMsg(appCal.Key, calValue)) Then

                        If (Not CalibrationValueDict.TryAdd(appCal.Key, calValue)) Then
                            RaiseEvent LogErrorEvent(New StackTrace,
                                                     "ReadCalibrationValues() Failed to save received sensor " &
                                                     "calibration. ID = " & appCal.Key)
                            failCount += 1
                        End If
                    Else
                        ' No need to log every failed attempt if sensor isn't responding.
                        If (failCount < 5) Then
                            RaiseEvent LogErrorEvent(New StackTrace,
                                                     "ReadCalibrationValues() Failed to read sensor calibration." &
                                                     " ID = " & appCal.Key)
                            failCount += 1
                        End If

                        ' Last command failed, delay briefly.
                        _delayWaitHandle.WaitOne(10)
                    End If
                Next
                If (failCount = 0) Then
                    result = True
                End If
            End If
            Return result
        Finally
            _delayWaitHandle.Close()
        End Try
        CalibrationValueDict.TryGetValue(InfoMessageID, CanMessageLib.InfoMessageID)
    End Function

#End Region     ' Methods

End Class
