﻿Imports System.Threading
Imports CTS_CAN_Services.Peak.Can.Basic

Partial Public NotInheritable Class CanMessageLib

    ' Functions for Sending CAN messages.

    ' Arrays are reference types. So in cases where arrays are retrieved
    ' from dictionaries make copies!

    Public Shared Function SendPgnRequestMsg(ByVal pgn As PgnRequest) As Boolean

        ' Call when using the Global ID.
        Return SendPgnRequestMsg(pgn, True)
    End Function

    Public Shared Function SendPgnRequestMsg(ByVal pgn As PgnRequest, ByVal globalID As Boolean) As Boolean

        ' Call with Global ID = False when using the application CAN Node ID.

        Dim result = False
        Dim requestMsg = New TPCANMsg()

        ' Check for value that was cast as Pgn but doesn't exist in enum.
        If [Enum].IsDefined(GetType(PgnRequest), pgn) Then
            If globalID Then
                ' Use global ID.
                requestMsg.ID = PgnRequestMsgID + &HFF00
            Else
                ' Use CAN Node ID.
                requestMsg.ID = PgnRequestMsgID + (CUInt(AppCanNodeID) << 8)
            End If

            ' Load the request PGN.
            requestMsg.DATA = New Byte() {CByte(pgn), CByte(CUInt(pgn) >> 8), CByte(CUInt(pgn) >> 16),
                                               &HFF, &HFF, &HFF, &HFF, &HFF}
            requestMsg.LEN = ToolCmdDataLength
            requestMsg.MSGTYPE = TPCANMessageType.PCAN_MESSAGE_EXTENDED

            ' Send the message.
            LoadCanMsgQueue(requestMsg)
            result = True
        End If
        Return result
    End Function

    Public Shared Function SendCommandMessage(ByVal cmdMsgType As CommandMsgType) As Boolean
        Return SendCommandMessage(cmdMsgType, Nothing)
    End Function

    Public Shared Function SendCommandMessage(ByVal cmdMsgType As CommandMsgType,
                                              ByVal param As Object) As Boolean

        Dim startFreq     As Double
        Dim numberOfSteps As UShort
        Dim mode          As OperatingMode
        Dim result        As Boolean
        Dim paramType     As Type


        ' Check for a command message type value that was cast as a CommandMsgType
        ' but doesn't exist in the enum.
        If (Not [Enum].IsDefined(GetType(CommandMsgType), cmdMsgType)) Then

            Return result ' Exit tout de suite.
        End If

        If (param IsNot Nothing) Then

            paramType = param.GetType()

            Select (cmdMsgType)

                Case CommandMsgType.ConfigureVCO

                    If (paramType = GetType(Double)) Then

                        startFreq = CDbl(param)
                    End If
                Case CommandMsgType.ConfigStepParams

                    If (paramType = GetType(UShort)) Then

                        numberOfSteps = CUShort(param)
                    End If
                Case CommandMsgType.RunSingleScan, CommandMsgType.RunContinuousScan

                    If (paramType = GetType(OperatingMode)) Then

                        mode = CType(param, OperatingMode)
                    End If
            End Select
        End If

        Dim cmdMsg = New TPCANMsg With {
                .ID = CommandMsgID,
                .DATA = New Byte() {CByte(cmdMsgType), &HFF, &HFF, &HFF, &HFF, &HFF, &HFF, &HFF},
                .LEN = ToolCmdDataLength,
                .MSGTYPE = TPCANMessageType.PCAN_MESSAGE_EXTENDED
            }

        Select Case cmdMsgType

            Case CommandMsgType.NormalOperation

                ' Disable scan data transmission in NormalOperation state.
                cmdMsg.DATA(3) = &H0
                result = True

            Case CommandMsgType.StopMeasurement

                ' The data byte array is ready to send.
                result = True

            Case CommandMsgType.PowerOffPending, CommandMsgType.ClearCodes

                ' The data byte array is ready to send.
                result = True


            ' For the remaining commands, insert configurable data into
            ' the data byte array before sending.

            Case CommandMsgType.RunSingleScan

                'If FormRFSensorMain.ActiveMeasurement Is Nothing Then  ' XXX KTK access setting in active measurement/
                '    Exit Select                                        ' current ROI? when setting up CAN messages.
                'End If
                'RegionOfInterest.OperatingModeStrToEnum(FormRFSensorMain.RunConfigurations.OperatingMode)
                cmdMsg.DATA(1) = mode
                cmdMsg.DATA(2) = &HFF ' Reserved.
                cmdMsg.DATA(3) = &HC8 ' Send sweep data, send during sweep, use byte #1 op mode and use byte #5 power.
                cmdMsg.DATA(4) = FormRfSensorMain.RunConfigurations.ScanDataMsgRate

                ' AuxOutputPower value = 0 to 3. Shift into bits 6 & 7.
                Dim outPwr = CUShort(FormRfSensorMain.RunConfigurations.AuxOutputPower) << 3

                ' OutputPower value = 0 to 3. Shift into bits 3 & 4.
                outPwr += CUShort(FormRfSensorMain.RunConfigurations.OutputPower)
                cmdMsg.DATA(5) = CByte((outPwr << 3) And &HFF)
                cmdMsg.DATA(6) = 0 ' ROI to Run
                cmdMsg.DATA(7) = 1 ' Number of Scans.
                result = True 

            Case CommandMsgType.RunContinuousScan

                'If FormRFSensorMain.ActiveMeasurement Is Nothing Then  ' XXX KTK access setting in active measurement/
                '    Exit Select                                        ' current ROI? when setting up CAN messages.
                'End If
                'RegionOfInterest.OperatingModeStrToEnum(FormRFSensorMain.RunConfigurations.OperatingMode)
                cmdMsg.DATA(1) = mode
                cmdMsg.DATA(2) = &HFF ' Reserved.
                cmdMsg.DATA(3) = &HC8 ' Send sweep data, send during sweep, use byte #1 op mode and use byte #5 power.
                cmdMsg.DATA(4) = FormRfSensorMain.RunConfigurations.ScanDataMsgRate

                ' AuxOutputPower value = 0 to 3. Shift into bits 6 & 7.
                Dim outPwr = CUShort(FormRfSensorMain.RunConfigurations.AuxOutputPower) << 3

                ' OutputPower value = 0 to 3. Shift into bits 3 & 4.
                outPwr += CUShort(FormRfSensorMain.RunConfigurations.OutputPower)
                cmdMsg.DATA(5) = CByte((outPwr << 3) And &HFF)
                cmdMsg.DATA(6) = 0 'ROI to Run
                result = True

            Case CommandMsgType.ConfigureVCO

                If (startFreq < 1.0) Then
                    Exit Select
                End If
                cmdMsg.DATA(1) = CByte(CLng(startFreq) And &HFF)                                 ' kHz
                cmdMsg.DATA(2) = CByte((CLng(startFreq) >> 8) And &HFF)
                cmdMsg.DATA(3) = CByte((CLng(startFreq) >> 16) And &HFF)
                cmdMsg.DATA(4) = FormRfSensorMain.RunConfigurations.TimeBetweenSteps
                cmdMsg.DATA(5) = CByte(CLng(Common.DefaultFrequencyStepSize) And &HFF)           ' kHz
                cmdMsg.DATA(6) = CByte((CLng(Common.DefaultFrequencyStepSize) >> 8) And &HFF)
                cmdMsg.DATA(7) = CByte((CLng(Common.DefaultFrequencyStepSize) >> 16) And &HFF)
                result = True

            Case CommandMsgType.ConfigStepParams

                If (numberOfSteps = 0) Then
                    Exit Select
                End If
                cmdMsg.DATA(1) = CByte(FormRfSensorMain.RunConfigurations.LockDelay And &HFF)
                cmdMsg.DATA(2) = CByte((FormRfSensorMain.RunConfigurations.LockDelay >> 8) And &HFF)
                cmdMsg.DATA(3) = CByte(FormRfSensorMain.RunConfigurations.SweepInterval And &HFF)
                cmdMsg.DATA(4) = CByte((FormRfSensorMain.RunConfigurations.SweepInterval >> 8) And &HFF)
                cmdMsg.DATA(5) = CByte(numberOfSteps And &HFF)
                cmdMsg.DATA(6) = CByte((numberOfSteps >> 8) And &HFF)
                cmdMsg.DATA(7) = FormRfSensorMain.RunConfigurations.ReadingsForAvg
                result = True

        End Select
        If result = True Then
            LoadCanMsgQueue(cmdMsg)
        End If
        Return result
    End Function

    Public Shared Function SendParamReadMsg(ByVal paramToolNames() As String, ByRef values() As Byte) As Boolean

        ' Read multiple Params by Paramameter Variable Name.
        Dim result = False

        values = New Byte() {}
        If paramToolNames Is Nothing Then

            ' Requested Param Variable Name array is invalid. Returns false.
            Return result
        End If

        Dim readMsg = New TPCANMsg() With {
                .DATA = InitialMsgData,
                .ID = ToolCmdMsgID,
                .LEN = ToolCmdDataLength,
                .MSGTYPE = TPCANMessageType.PCAN_MESSAGE_EXTENDED
            }
        Dim byteCnt    As Byte
        Dim paramIdMsb As Byte
        Dim dataIdx    As Byte = ReadMsgDataOffset
        Dim parameter  As New ParamTblEntry()
        Dim paramID    As UShort
        Dim paramBytes As Byte

        ' How many Param ID bytes are being sent total?
        Dim idx As Integer = 0
        Do While idx < paramToolNames.Length AndAlso idx < ReadParamIdsMax

            ' These three param IDs are unlikely to change, but you never know. . .
            If (paramToolNames(idx) = SoftwareID) Then
                byteCnt += 1
                paramBytes = 1
                paramID = 287
            ElseIf (paramToolNames(idx) = SoftwareRevLevel) Then
                byteCnt += 1
                paramID = 296
                paramBytes = 1
            ElseIf (paramToolNames(idx) = SoftwareDevLevel) Then
                byteCnt += 1
                paramID = 297
                paramBytes = 1
            Else
                ' Get the Param length in bytes.
                If ((RfSensorConfiguration.ParamTblDict IsNot Nothing) AndAlso
                    (RfSensorConfiguration.ParamTblDict.TryGetValue(paramToolNames(idx), parameter))) Then

                    byteCnt += parameter.Bytes
                    paramBytes = parameter.Bytes
                    paramID = parameter.ParamID
                Else
                    ' A Param ID was not found.
                    byteCnt = 0
                    Exit Do
                End If
            End If
            paramIdMsb += CByte(parameter.ParamID >> 8)

            ' Load the requested PID into the command message DATA array.
            Dim paramIdLsb = CByte(paramID And &HFF)
            readMsg.DATA(dataIdx) = paramIdLsb
            paramIdLsb += 1
            dataIdx += 1
            Dim pidIdx As Integer = 1
            Do While pidIdx < paramBytes AndAlso dataIdx < ToolCmdDataLength

                ' Allows for multiple byte Params.
                readMsg.DATA(dataIdx) = paramIdLsb
                paramIdLsb += 1
                dataIdx += 1
                pidIdx += 1
            Loop
            idx += 1
        Loop
        If ((paramIdMsb <> 0 AndAlso paramIdMsb <> paramToolNames.Length) OrElse
            (byteCnt = 0 OrElse byteCnt > ReadParamBytesMax)) Then

            ' The upper byte of the param IDs did not match or
            ' the requested byte count is invalid. Returns false.
            Return result
        End If

        ' Combine Message Type ID with the MSB of the address (page identifier).
        Dim byte0 As Byte = CByte(MessageTypeID.ParamID) + CByte((paramID >> 8) And &HFF)
        readMsg.DATA(0) = CByte(byte0)
        readMsg.DATA(1) = byteCnt
        LoadCanMsgQueue(readMsg)

        ' Get the response message using the Message Type ID, param ID MSB and
        ' the requested byte count.
        Dim rcvdMsgData = GetToolParamReponseData((CUInt(byte0) << 8) + byteCnt)
        If rcvdMsgData IsNot Nothing AndAlso
           rcvdMsgData.Length = RespMsgDataLength AndAlso
           rcvdMsgData(1) <> ToolMsgInvalid Then

            ' Write the received message data byte array, minus the
            ' first 2 bytes, to the out param.
            values = New Byte(byteCnt - 1) {}
            Array.Copy(rcvdMsgData, RespMsgDataOffset, values, 0, values.Length)
            result = True
        ElseIf rcvdMsgData IsNot Nothing AndAlso rcvdMsgData.Length > 0 AndAlso rcvdMsgData(1) = ToolMsgInvalid Then
            ToolFunctionReturnCode = rcvdMsgData(3)
        End If

        Return result
    End Function

    Public Shared Function SendParamReadMsg(ByVal paramToolName As String, ByRef value As UInteger) As Boolean

        ' Read a single Param by Param Variable Name.

        Dim result = False
        Dim readMsg = New TPCANMsg()
        Dim msgID As UInteger
        Dim parameter As New ParamTblEntry()

        value = 0
        If ((RfSensorConfiguration.ParamTblDict IsNot Nothing) AndAlso 
            (RfSensorConfiguration.ParamTblDict.TryGetValue(paramToolName, parameter) AndAlso parameter.Bytes <> 0)) Then

            readMsg.DATA = InitialMsgData
            readMsg.DATA(0) = CByte(CUInt(MessageTypeID.ParamID) + (CUShort(parameter.ParamID) >> 8))
            msgID = CUInt(readMsg.DATA(0)) << 8
            readMsg.DATA(1) = parameter.Bytes
            msgID += readMsg.DATA(1)
            readMsg.DATA(2) = CByte(CUInt(parameter.ParamID) And &HFF)
            Dim paramLsb = CUInt(parameter.ParamID) And &HFF
            Dim idx As Byte = ReadMsgDataOffset
            Dim incr As Byte = 0
            Do While idx < RespMsgDataLength AndAlso incr < CUInt(parameter.Bytes)

                readMsg.DATA(idx) = CByte(paramLsb + incr)
                idx += 1
                incr += 1
            Loop
            readMsg.ID = ToolCmdMsgID
            readMsg.LEN = ToolCmdDataLength
            readMsg.MSGTYPE = TPCANMessageType.PCAN_MESSAGE_EXTENDED
            LoadCanMsgQueue(readMsg)

            ' Get the response message.
            Dim rcvdMsgData = GetToolParamReponseData(msgID)
            If rcvdMsgData IsNot Nothing AndAlso
               rcvdMsgData.Length = RespMsgDataLength AndAlso
               rcvdMsgData(1) <> ToolMsgInvalid Then

                idx = RespMsgDataOffset
                Dim shift = CSByte(parameter.Bytes - 1)
                Do While shift > 0 AndAlso idx < RespMsgDataLength

                    ' Extract big endian Param value.
                    value += CUInt(rcvdMsgData(idx)) << (8 * shift)
                    idx   += 1
                    shift -= 1
                Loop
                value += rcvdMsgData(idx)
                result = True
            ElseIf ((rcvdMsgData IsNot Nothing) AndAlso
                     rcvdMsgData.Length > 0     AndAlso
                     rcvdMsgData(1) = ToolMsgInvalid) Then

                ToolFunctionReturnCode = rcvdMsgData(3)
            End If
        End If
        Return result
    End Function

    ' ADD FUNCTION TO WRITE MULTIPLE PARAMS???????? XXX KTK

    Public Shared Function SendParamWriteMsg(ByVal paramToolName As String, ByVal value As ULong) As Boolean

        ' Write a single Param by Parameter Variable Name.

        Dim result = False
        Dim msgID As UInteger
        Dim parameter As New ParamTblEntry()

        If ((RfSensorConfiguration.ParamTblDict IsNot Nothing) AndAlso 
            (RfSensorConfiguration.ParamTblDict.TryGetValue(paramToolName, parameter) AndAlso parameter.Bytes <> 0)) Then

            ' This is a valid Param ID.
            Dim writeMsg = New TPCANMsg With {
                    .DATA = InitialMsgData,
                    .LEN = ToolCmdDataLength,
                    .ID = ToolCmdMsgID,
                    .MSGTYPE = TPCANMessageType.PCAN_MESSAGE_EXTENDED
                }
            writeMsg.DATA(0) = CByte(CUInt(MessageTypeID.ParamID) + (CUInt(parameter.ParamID) >> 8))
            msgID = CUInt(writeMsg.DATA(0)) << 8 ' Don't include write bit.
            writeMsg.DATA(0) += ParamWriteBit

            ' Maximum of 3 value bytes per write message.
            writeMsg.DATA(1) = If(CByte(parameter.Bytes) > WriteParamBytesMax, CByte(3), CByte(parameter.Bytes))
            msgID += writeMsg.DATA(1)
            writeMsg.DATA(2) = CByte(CUShort(parameter.ParamID) And &HFF)
            Dim idx As Byte = WriteMsgDataOffset
            Dim incr As Byte = 0
            Dim shft = CSByte(parameter.Bytes)
            Do While (idx < ToolCmdDataLength AndAlso incr < parameter.Bytes AndAlso shft >= 0)

                writeMsg.DATA(idx) = CByte((CUInt(parameter.ParamID) And &HFF) + incr)
                writeMsg.DATA(idx + 1) = CByte(value >> 8 * shft)
                idx += 2   ' 2 bytes at a time.
                incr += 1
                shft -= 1
            Loop
            writeMsg.ID = ToolCmdMsgID
            writeMsg.MSGTYPE = TPCANMessageType.PCAN_MESSAGE_EXTENDED

            ' Send the write message.
            LoadCanMsgQueue(writeMsg)

            ' Get the response message.
            Dim rcvdMsgData = GetToolParamReponseData(msgID)
            If rcvdMsgData IsNot Nothing AndAlso
                   rcvdMsgData.Length = RespMsgDataLength AndAlso
                   rcvdMsgData(1) <> ToolMsgInvalid Then

                ' Response message is valid.
                result = True
            ElseIf rcvdMsgData IsNot Nothing AndAlso
                       rcvdMsgData.Length > 0 AndAlso
                       rcvdMsgData(1) = ToolMsgInvalid Then

                ToolFunctionReturnCode = rcvdMsgData(3)
            End If
            If result AndAlso CUInt(parameter.Bytes) > WriteParamBytesMax Then

                ' The first write message was successful.
                ' Now send the remaining Param value bytes.
                writeMsg = New TPCANMsg With {
                        .DATA = InitialMsgData,
                        .LEN = ToolCmdDataLength
                    }
                writeMsg.DATA(0) = CByte(CUInt(MessageTypeID.ParamID) + (CUInt(parameter.ParamID) >> 8))
                msgID = CUInt(writeMsg.DATA(0)) << 8 ' Don't include write bit.
                writeMsg.DATA(0) += ParamWriteBit

                ' Maximum of 3 value bytes per write message.
                writeMsg.DATA(1) = CByte(parameter.Bytes - WriteParamBytesMax)
                msgID += writeMsg.DATA(1)
                idx = ReadMsgDataOffset
                Do While idx < ToolCmdDataLength AndAlso incr < parameter.Bytes AndAlso shft >= 0
                    writeMsg.DATA(idx) = CByte((CUInt(parameter.ParamID) And &HFF) + incr)
                    writeMsg.DATA(idx + 1) = CByte(value >> 8 * shft)
                    idx += 2
                    incr += 1
                    shft -= 1
                Loop
                writeMsg.ID = ToolCmdMsgID
                writeMsg.MSGTYPE = TPCANMessageType.PCAN_MESSAGE_EXTENDED

                ' Send the second write message.
                LoadCanMsgQueue(writeMsg)

                ' Get the response message.
                rcvdMsgData = GetToolParamReponseData(msgID)
                If rcvdMsgData Is Nothing OrElse rcvdMsgData(1) = ToolMsgInvalid OrElse
                       rcvdMsgData.Length <> RespMsgDataLength Then

                    ' The second write message response was invalid.
                    result = False
                    If rcvdMsgData IsNot Nothing AndAlso
                           rcvdMsgData.Length > 0 AndAlso
                           rcvdMsgData(1) = ToolMsgInvalid Then

                        ToolFunctionReturnCode = rcvdMsgData(3)
                    End If
                End If
            End If
        End If
        Return result
    End Function

    Public Shared Function SendAppCalReadMsg(ByVal appCalIdNumbers() As UShort, ByRef values() As Byte) As Boolean

        Dim result = False

        values = New Byte() {}
        If appCalIdNumbers Is Nothing OrElse appCalIdNumbers.Length > ReadAppCalIdxsMax Then

            ' Requested Application Cal IDs array is invalid. Returns false.
            Return result
        End If

        Dim readMsg = New TPCANMsg() With {
                .DATA = InitialMsgData,
                .LEN = ToolCmdDataLength,
                .MSGTYPE = TPCANMessageType.PCAN_MESSAGE_EXTENDED
            }
        Dim byteCnt As Byte = 0
        Dim dataIdx As Byte = ReadMsgDataOffset
        Dim appCalData As New AppCalibration()

        For Each calID In appCalIdNumbers
            Dim length = 0
            If ((RfSensorConfiguration.AppCalDict IsNot Nothing) AndAlso
                (RfSensorConfiguration.AppCalDict.TryGetValue(calID, appCalData))) Then

                byteCnt += appCalData.Bytes
                If dataIdx < readMsg.DATA.Length Then
                    If dataIdx > ReadMsgDataOffset AndAlso length <> appCalData.Bytes Then

                        ' Requested app calibration values are not all the same length.
                        byteCnt = 0
                        Exit For
                    End If

                    ' Load the app cal index values into the read message.
                    readMsg.DATA(dataIdx) = appCalData.CanAccessIndex
                    dataIdx += 1
                    length = appCalData.Bytes
                Else
                    ' Too many data bytes requested.
                    byteCnt = 0
                    Exit For
                End If
            Else
                ' Invalid Cal Variable Name.
                byteCnt = 0
                Exit For
            End If
        Next calID
        If byteCnt > 0 AndAlso byteCnt <= ReadAppCalBytesMax Then

            ' Combine Message Type ID with the number of bytes per app cal ID.
            ' All app cal ID lengths should be the same within one message.
            Dim byte0 = CUInt(CByte(MessageTypeID.CalibrationRam) + appCalData.Bytes)
            readMsg.DATA(0) = CByte(byte0)
            readMsg.DATA(1) = byteCnt
            LoadCanMsgQueue(readMsg)

            ' Get the response message by the first app cal index.
            Dim rcvdMsgData = GetToolAppCalReponse((byte0 << 8) + byteCnt)

            If rcvdMsgData IsNot Nothing AndAlso
               rcvdMsgData.Length = RespMsgDataLength AndAlso
               rcvdMsgData(1) <> ToolMsgInvalid Then

                ' Write the received message data byte array, minus the
                ' first 2 bytes, to the values array.
                values = New Byte(byteCnt - 1) {}
                Array.Copy(rcvdMsgData, RespMsgDataOffset, values, 0, values.Length)
                result = True
            ElseIf rcvdMsgData IsNot Nothing AndAlso rcvdMsgData.Length > 0 AndAlso rcvdMsgData(1) = ToolMsgInvalid Then
                ToolFunctionReturnCode = rcvdMsgData(3)
            End If
        End If
        Return result
    End Function

    Public Shared Function SendAppCalReadMsg(ByVal appCalIdNumber As UShort,
                                             ByRef value As UInteger) As Boolean

        ' Read a single application calibration value by application ID.
        Dim result = False
        Dim readMsg = New TPCANMsg() With {
                .DATA = InitialMsgData,
                .ID = ToolCmdMsgID,
                .LEN = ToolCmdDataLength,
                .MSGTYPE = TPCANMessageType.PCAN_MESSAGE_EXTENDED
            }
        Dim appCalData As New AppCalibration()

        value = 0
        If ((RfSensorConfiguration.AppCalDict IsNot Nothing) AndAlso
            (RfSensorConfiguration.AppCalDict.TryGetValue(appCalIdNumber, appCalData))) Then

            ' Combine message type ID with cal type (1, 2 or 4 byte).
            Dim byte0 = CUInt(CByte(MessageTypeID.CalibrationRam) + appCalData.Bytes)
            readMsg.DATA(0) = CByte(byte0)

            ' Read one calibration.
            readMsg.DATA(1) = 1
            readMsg.DATA(2) = appCalData.CanAccessIndex ' Application Cal Index.
            LoadCanMsgQueue(readMsg)

            ' Get the response message.
            Dim rcvdMsgData = GetToolAppCalReponse((byte0 << 8) + 1)
            If rcvdMsgData IsNot Nothing AndAlso
               rcvdMsgData.Length = RespMsgDataLength AndAlso
               rcvdMsgData(1) <> ToolMsgInvalid Then

                Dim idx As Byte = RespMsgDataOffset
                Dim shift = CSByte(appCalData.Bytes - 1)
                Do While shift > 0 AndAlso idx < RespMsgDataLength

                    ' Extract big endian Cal value.
                    value += CUInt(rcvdMsgData(idx)) << 8 * shift
                    idx += 1
                    shift -= 1
                Loop
                value += CUInt(rcvdMsgData(idx))
                result = True
            ElseIf rcvdMsgData IsNot Nothing AndAlso rcvdMsgData.Length > 0 AndAlso rcvdMsgData(1) = ToolMsgInvalid Then
                ToolFunctionReturnCode = rcvdMsgData(3)
            End If
        End If
        Return result
    End Function

    Public Shared Function SendAppCalWriteMsg(ByVal appCalIdNumbers() As UShort,
                                              ByVal values() As UInteger) As Boolean

        ' Read multiple application cal;ibration values by application ID.

        Dim result = False

        If (appCalIdNumbers Is Nothing OrElse appCalIdNumbers.Length = 0 OrElse
            appCalIdNumbers.Length > WriteAppCalIdxMax) Then

            ' Requested Application Cal IDs array is invalid. Returns false.
            Return result
        End If

        Dim writeMsg = New TPCANMsg() With {
                .DATA = New Byte() {&HFF, CByte(appCalIdNumbers.Length), &HFF, &HFF, &HFF, &HFF, &HFF, &HFF},
                .ID = ToolCmdMsgID,
                .LEN = ToolCmdDataLength,
                .MSGTYPE = TPCANMessageType.PCAN_MESSAGE_EXTENDED
            }
        Dim byteCnt = New Byte(appCalIdNumbers.Length - 1) {}
        Dim totalCnt = 0
        Dim dataIdx As Byte = WriteMsgDataOffset

        ' Load the app cal write data into the write message data array.
        Dim idx As Byte = 0
        Do While idx < appCalIdNumbers.Length AndAlso dataIdx < ToolCmdDataLength

            If ((RfSensorConfiguration.AppCalDict IsNot Nothing) AndAlso
                (RfSensorConfiguration.AppCalDict.ContainsKey(appCalIdNumbers(idx)))) Then

                byteCnt(idx) = RfSensorConfiguration.AppCalDict(appCalIdNumbers(idx)).Bytes
                If idx > 0 AndAlso byteCnt(idx) <> byteCnt(idx - 1) Then

                    ' Mixed calibration types (lengths) found.
                    totalCnt = 0
                    Exit Do
                Else
                    totalCnt += byteCnt(idx)

                    ' Load the current app cal index.
                    writeMsg.DATA(dataIdx) = RfSensorConfiguration.AppCalDict(appCalIdNumbers(idx)).CanAccessIndex
                    dataIdx += 1
                    For iidx = CSByte(byteCnt(idx) - 1) To 0 Step -1

                        ' Load the current data value (big endian).
                        If dataIdx < ToolCmdDataLength Then
                            writeMsg.DATA(dataIdx) = CByte(values(idx) >> (8 * iidx))
                            dataIdx += 1
                        Else
                            ' Too many data bytes.
                            totalCnt = 0
                            Exit For
                        End If
                    Next iidx
                End If
            Else
                ' Invalid app cal ID.
                totalCnt = 0
                Exit Do
            End If
            idx += 1
        Loop
        ' Total byte count must be <= WriteAppCalBytesMax in all cases.
        ' In the case where requested app cals are one byte long, the
        ' total count must be <= WriteAppCalBytesMax - 1.
        If totalCnt > 0 AndAlso totalCnt <= WriteAppCalBytesMax AndAlso
               Not (byteCnt(0) = 1 AndAlso totalCnt > (WriteAppCalBytesMax - 1)) Then

            ' Combine message type ID with cal type (1, 2 or 4 byte).
            Dim byte0 = CUInt(CByte(MessageTypeID.CalibrationRam) + byteCnt(0))
            writeMsg.DATA(0) = CByte(byte0)
            writeMsg.DATA(0) = writeMsg.DATA(0) Or AppCalWriteBit
            LoadCanMsgQueue(writeMsg)

            ' Get the response message.
            Dim rcvdMsgData = GetToolAppCalReponse((byte0 << 8) + byteCnt(0))
            If rcvdMsgData IsNot Nothing AndAlso rcvdMsgData.Length = RespMsgDataLength AndAlso
                   rcvdMsgData(1) <> ToolMsgInvalid Then

                result = True
            ElseIf rcvdMsgData IsNot Nothing AndAlso rcvdMsgData.Length > 0 AndAlso rcvdMsgData(1) = ToolMsgInvalid Then
                ToolFunctionReturnCode = rcvdMsgData(3)
            End If
        End If
        Return result
    End Function

    ' Tool Function command messages.
    Public Shared Function SendToolFunctionCmd(ByVal toolFuncMsg As ToolFunctionMsg,
                                               Optional ByVal data() As Byte = Nothing) As Boolean
        Dim result = False
        Dim cmdMsg = New TPCANMsg()

        If ToolFuncReqDict.ContainsKey(toolFuncMsg) Then
            ToolFunctionReturnCode = 0
            cmdMsg.DATA = New Byte(RespMsgDataLength - 1) {}
            If data Is Nothing Then
                Array.Copy(ToolFuncReqDict(toolFuncMsg), cmdMsg.DATA, ToolFuncReqDict(toolFuncMsg).Length)
            Else
                Array.Copy(data, cmdMsg.DATA, data.Length)
            End If
            LastToolFuncOpCode = toolFuncMsg
            cmdMsg.ID = ToolCmdMsgID
            cmdMsg.LEN = RespMsgDataLength
            cmdMsg.MSGTYPE = TPCANMessageType.PCAN_MESSAGE_EXTENDED
            LoadCanMsgQueue(cmdMsg)
            result = True
        End If
        Return result
    End Function

    Public Shared Function JumpToDbl() As Boolean

        Dim result = False
        Dim dblVersion As Byte() = New Byte(DblSwVersionLength - 1){}

        If SendToolFunctionCmd(ToolFunctionMsg.JumpToDbl) Then

            ' Get the DBL response message. DBL response messages contain
            ' an opcode in the first byte.
            Dim rcvdMsgData = New Byte() {}
            If GetDblToolMsgReponse(CByte(DblRcvOpCodes.DblAttnResp), rcvdMsgData) Then

                ' Success. Store the DBL software version returned in the response.
                For idx = 0 To dblVersion.Length - 1
                    dblVersion(idx) = rcvdMsgData(idx + 1)
                Next
                RfSensorConfiguration.DblSoftwareVersion = dblVersion
                result = True
            ElseIf rcvdMsgData IsNot Nothing AndAlso rcvdMsgData.Length > 0 AndAlso rcvdMsgData(1) = ToolMsgInvalid Then
                ToolFunctionReturnCode = rcvdMsgData(3)
            End If
        End If
        Return result
    End Function

    Public Shared Function ResetHwLine1() As Boolean
        Return AccessHwLineWord(True, False, HwLineErase)
    End Function

    Public Shared Function ResetHwLine2() As Boolean
        Return AccessHwLineWord(True, True, HwLineErase)
    End Function

    Public Shared Function AccessHwLineWord(ByVal rw As Boolean, ByVal word As Boolean, ByVal bitNum As Byte) As Boolean

        ' rw = true for write, otherwise read. word = true for word 1, otherwise word 0.
        Dim result = False
        Dim xmtMsgData() As Byte = Nothing

        If ToolFuncReqDict.TryGetValue(ToolFunctionMsg.AccessProdHwLineWord, xmtMsgData) Then
            xmtMsgData(3) = CByte(If(rw, &H80, &H1))
            xmtMsgData(4) = CByte(If(word, 1, 0))
            xmtMsgData(5) = CByte(If(word, bitNum, 0))
            If SendToolFunctionCmd(ToolFunctionMsg.AccessProdHwLineWord, xmtMsgData) Then

                ' Get the response message.
                Dim rcvdMsgData = GetToolFuncMsgReponse(ToolFunctionMsg.AccessProdHwLineWord)
                If rcvdMsgData IsNot Nothing AndAlso rcvdMsgData.Length = RespMsgDataLength AndAlso rcvdMsgData(1) <> ToolMsgInvalid Then
                    ' Success.
                    If rcvdMsgData(3) = xmtMsgData(3) AndAlso rcvdMsgData(4) = xmtMsgData(4) Then

                        ' Read/write and word selection values were echoed.
                        If rcvdMsgData(3) = HwLineRead AndAlso rcvdMsgData(1) = &H2 Then

                            ' The return data was correctly formatted.
                            ' Store the read hardware line word value.
                            If word Then
                                RfSensorConfiguration.HwLineWord2 = CUShort(rcvdMsgData(5) + CUInt(rcvdMsgData(6)) << 8)
                            Else
                                RfSensorConfiguration.HwLineWord1 = CUShort(rcvdMsgData(5) + CUInt(rcvdMsgData(6)) << 8)
                            End If
                            result = True
                        ElseIf rcvdMsgData(3) = HwLineWrite AndAlso rcvdMsgData(1) = &H3 Then
                            If bitNum = HwLineErase Then
                                If rcvdMsgData(5) = HwLineErase Then

                                    ' The return data was correctly formatted.
                                    result = True
                                End If
                            Else
                                ' The return data was correctly formatted.
                                ' Store the new hardware line word value.
                                If word Then
                                    RfSensorConfiguration.HwLineWord2 = CUShort(rcvdMsgData(5) + CUInt(rcvdMsgData(6)) << 8)
                                Else
                                    RfSensorConfiguration.HwLineWord1 = CUShort(rcvdMsgData(5) + CUInt(rcvdMsgData(6)) << 8)
                                End If
                                result = True
                            End If
                        End If
                    End If
                ElseIf rcvdMsgData IsNot Nothing AndAlso rcvdMsgData.Length > 0 AndAlso rcvdMsgData(1) = ToolMsgInvalid Then
                    ToolFunctionReturnCode = rcvdMsgData(3)
                End If
            End If
        End If
        Return result
    End Function

    Public Shared Function AllowToolCmdFunctions() As Boolean

        Dim result = False
        Dim cmdMsg = New TPCANMsg()
        Dim xmtMsgData() As Byte = Nothing
        Dim expectedRespData As Byte() = Nothing

        If Not ToolFuncReqDict.TryGetValue(ToolFunctionMsg.AllowToolCmdFunctions, xmtMsgData) Then
            Return result
        End If

        If ToolFuncReqDict.ContainsKey(ToolFunctionMsg.AllowToolCmdFunctions) Then
            ToolFunctionReturnCode = 0
            cmdMsg.DATA = New Byte(RespMsgDataLength - 1) {}
                Array.Copy(ToolFuncReqDict(ToolFunctionMsg.AllowToolCmdFunctions), cmdMsg.DATA,
                           ToolFuncReqDict(ToolFunctionMsg.AllowToolCmdFunctions).Length)
            LastToolFuncOpCode = ToolFunctionMsg.AllowToolCmdFunctions
            cmdMsg.ID = ToolCmdMsgID
            cmdMsg.LEN = RespMsgDataLength
            cmdMsg.MSGTYPE = TPCANMessageType.PCAN_MESSAGE_EXTENDED
            LoadCanMsgQueue(cmdMsg)

            ' Get the response message.
            Dim rcvdMsgData = GetToolFuncMsgReponse(ToolFunctionMsg.AllowToolCmdFunctions)
            If rcvdMsgData IsNot Nothing AndAlso rcvdMsgData.Length = RespMsgDataLength AndAlso rcvdMsgData(1) <> ToolMsgInvalid Then
                If rcvdMsgData(1) = xmtMsgData(1) AndAlso rcvdMsgData(3) = xmtMsgData(3) Then

                    ' The value of byte 3 in the corresponding command
                    ' message was echoed.
                    result = True
                End If
            ElseIf rcvdMsgData IsNot Nothing AndAlso rcvdMsgData.Length > 0 AndAlso rcvdMsgData(1) = ToolMsgInvalid Then
                ToolFunctionReturnCode = rcvdMsgData(3)
            End If
        End If
        Return result
    End Function

    Public Shared Function ReadAppSoftwareConfigs() As Boolean

        Dim result = False

        If SendToolFunctionCmd(ToolFunctionMsg.ReadAppSoftwareConfigs) Then

            ' Get the response message.
            Dim rcvdMsgData = GetToolFuncMsgReponse(ToolFunctionMsg.ReadAppSoftwareConfigs)
            If rcvdMsgData IsNot Nothing AndAlso rcvdMsgData.Length = RespMsgDataLength AndAlso rcvdMsgData(1) <> ToolMsgInvalid Then
                RfSensorConfiguration.MicroCfg = rcvdMsgData(1)
                RfSensorConfiguration.CalConfig = rcvdMsgData(2)
                RfSensorConfiguration.EepromConfig = rcvdMsgData(3)
                RfSensorConfiguration.ParameterIdLevel = rcvdMsgData(4)
                RfSensorConfiguration.BaseTime = rcvdMsgData(5)
                RfSensorConfiguration.UsageTime = CUInt(rcvdMsgData(6)) << 8 ' MSB
                RfSensorConfiguration.UsageTime += rcvdMsgData(7) ' LSB
                result = True
            ElseIf rcvdMsgData IsNot Nothing AndAlso rcvdMsgData.Length > 0 AndAlso rcvdMsgData(1) = ToolMsgInvalid Then
                ToolFunctionReturnCode = rcvdMsgData(3)
            End If
        End If
        Return result
    End Function

    Public Shared Function ReadSoftwareVersion() As Boolean

        Dim result = False

        If SendToolFunctionCmd(ToolFunctionMsg.ReadSoftwareVersion) Then

            ' Get the response message.
            Dim rcvdMsgData = GetToolFuncMsgReponse(ToolFunctionMsg.ReadSoftwareVersion)
            If rcvdMsgData IsNot Nothing AndAlso rcvdMsgData.Length = RespMsgDataLength AndAlso rcvdMsgData(1) <> ToolMsgInvalid Then
                RfSensorConfiguration.MicroID = rcvdMsgData(1)
                RfSensorConfiguration.MicroSize = rcvdMsgData(2)
                RfSensorConfiguration.HardwareID = rcvdMsgData(3)
                RfSensorConfiguration.MajorRev = rcvdMsgData(4)
                RfSensorConfiguration.MinorRev = rcvdMsgData(5)
                RfSensorConfiguration.DevelopRev = rcvdMsgData(6)
                result = True
            ElseIf rcvdMsgData IsNot Nothing AndAlso rcvdMsgData.Length > 0 AndAlso rcvdMsgData(1) = ToolMsgInvalid Then
                ToolFunctionReturnCode = rcvdMsgData(3)
            End If
        End If
        Return result
    End Function

    Public Shared Function ReadDeviceID() As Boolean

        Dim result = False
        Dim id As Byte() = New Byte(DeviceIdLength - 1) {}

        If SendToolFunctionCmd(ToolFunctionMsg.ReadDeviceID) Then

            ' Get the response message.
            Dim rcvdMsgData = GetToolFuncMsgReponse(ToolFunctionMsg.ReadDeviceID)
            If rcvdMsgData IsNot Nothing AndAlso rcvdMsgData.Length = RespMsgDataLength AndAlso rcvdMsgData(1) <> ToolMsgInvalid Then
                For idx = 0 To id.Length - 1
                    id(idx) = rcvdMsgData(idx + 1)
                Next
                RfSensorConfiguration.DeviceID = id
                result = True
            ElseIf rcvdMsgData IsNot Nothing AndAlso rcvdMsgData.Length > 0 AndAlso rcvdMsgData(1) = ToolMsgInvalid Then
                ToolFunctionReturnCode = rcvdMsgData(3)
            End If
        End If
        Return result
    End Function

    Public Shared Function ReadSerialNumFromFlash() As Byte()

        Dim serialNumber = New Byte(CanMessageLib.SerialNumberLength - 1){}

        If SendToolFunctionCmd(ToolFunctionMsg.ReadSerialNumFromFlash) Then

            ' Get the response message.
            Dim rcvdMsgData = GetToolFuncMsgReponse(ToolFunctionMsg.ReadSerialNumFromFlash)
            If rcvdMsgData IsNot Nothing AndAlso
               rcvdMsgData.Length = RespMsgDataLength AndAlso
               rcvdMsgData(1) <> ToolMsgInvalid Then

                serialNumber(0) = rcvdMsgData(5)
                serialNumber(1) = rcvdMsgData(4)
                serialNumber(2) = rcvdMsgData(2)
                serialNumber(3) = rcvdMsgData(1)
            ElseIf rcvdMsgData IsNot Nothing AndAlso rcvdMsgData.Length > 0 AndAlso rcvdMsgData(1) = ToolMsgInvalid Then
                ToolFunctionReturnCode = rcvdMsgData(3)
                serialNumber = Nothing
            End If
        End If
        Return serialNumber
    End Function

    Public Shared Function EepromErase() As Boolean

        Dim result = False
        Dim msgData() As Byte = Nothing

        If ToolFuncReqDict.TryGetValue(ToolFunctionMsg.EraseEepromRequest, msgData) Then

            ' Add serial number to byte array retrieved from dictionary.
            msgData(4) = _serialNumber(0)
            msgData(5) = _serialNumber(1)
            msgData(6) = _serialNumber(2)
            msgData(7) = _serialNumber(3)
            result = SendToolFunctionCmd(ToolFunctionMsg.EraseEepromRequest, msgData)

            ' Get the response message.
            Dim rcvdMsgData = GetToolFuncMsgReponse(ToolFunctionMsg.EraseEepromRequest)
            If rcvdMsgData IsNot Nothing AndAlso rcvdMsgData.Length = RespMsgDataLength AndAlso rcvdMsgData(1) <> ToolMsgInvalid Then
                result = True
            ElseIf rcvdMsgData IsNot Nothing AndAlso rcvdMsgData.Length > 0 AndAlso rcvdMsgData(1) = ToolMsgInvalid Then
                ToolFunctionReturnCode = rcvdMsgData(3)
            End If
        End If
        Return result
    End Function

    Public Shared Function InitializeUsageData() As Boolean

        Dim result = False
        Dim msgData() As Byte = Nothing

        If ToolFuncReqDict.TryGetValue(ToolFunctionMsg.InitializeUsageData, msgData) Then

            ' Add serial number to byte array retrieved from dictionary.
            msgData(4) = _serialNumber(0)
            msgData(5) = _serialNumber(1)
            msgData(6) = _serialNumber(2)
            msgData(7) = _serialNumber(3)

            ' Send the EraseEepromRequest ToolFunctionMsg value.
            result = SendToolFunctionCmd(ToolFunctionMsg.EraseEepromRequest, msgData)

            ' Get the response message.
            Dim rcvdMsgData = GetToolFuncMsgReponse(ToolFunctionMsg.EraseEepromRequest)
            If rcvdMsgData IsNot Nothing AndAlso rcvdMsgData.Length = RespMsgDataLength AndAlso rcvdMsgData(1) <> ToolMsgInvalid Then
                result = True
            ElseIf rcvdMsgData IsNot Nothing AndAlso rcvdMsgData.Length > 0 AndAlso rcvdMsgData(1) = ToolMsgInvalid Then
                ToolFunctionReturnCode = rcvdMsgData(3)
            End If
        End If
        Return result
    End Function

    Public Shared Function InitUsageDataIncludingTime() As Boolean

        Dim result = False
        Dim msgData() As Byte = Nothing

        If ToolFuncReqDict.TryGetValue(ToolFunctionMsg.InitUsageDataIncludingTime, msgData) Then

            ' Add serial number to byte array retrieved from dictionary.
            msgData(4) = _serialNumber(0)
            msgData(5) = _serialNumber(1)
            msgData(6) = _serialNumber(2)
            msgData(7) = _serialNumber(3)

            ' Send the EraseEepromRequest ToolFunctionMsg value.
            result = SendToolFunctionCmd(ToolFunctionMsg.EraseEepromRequest, msgData)

            ' Get the response message.
            Dim rcvdMsgData = GetToolFuncMsgReponse(ToolFunctionMsg.EraseEepromRequest)
            If rcvdMsgData IsNot Nothing AndAlso rcvdMsgData.Length = RespMsgDataLength AndAlso rcvdMsgData(1) <> ToolMsgInvalid Then
                result = True
            ElseIf rcvdMsgData IsNot Nothing AndAlso rcvdMsgData.Length > 0 AndAlso rcvdMsgData(1) = ToolMsgInvalid Then
                ToolFunctionReturnCode = rcvdMsgData(3)
            End If
        End If
        Return result
    End Function

    Public Shared Function RequestApplicationReset() As Boolean

        Dim result = False
        Dim xmtMsgData() As Byte = Nothing

        If Not ToolFuncReqDict.TryGetValue(ToolFunctionMsg.ApplicationReset, xmtMsgData) Then
            Return False
        End If
        result = SendToolFunctionCmd(ToolFunctionMsg.ApplicationReset)
        ' Get the response message.
        Dim rcvdMsgData = GetToolFuncMsgReponse(ToolFunctionMsg.ApplicationReset)
        If rcvdMsgData IsNot Nothing AndAlso rcvdMsgData.Length = RespMsgDataLength AndAlso rcvdMsgData(1) <> ToolMsgInvalid Then
            If rcvdMsgData(1) = xmtMsgData(1) Then
                ' The value of byte 1 in the corresponding command message
                ' was echoed.
                result = True
            End If
        ElseIf rcvdMsgData IsNot Nothing AndAlso rcvdMsgData.Length > 0 AndAlso rcvdMsgData(1) = ToolMsgInvalid Then
            ToolFunctionReturnCode = rcvdMsgData(3)
        End If
        Return result
    End Function

    Public Shared Function ReadDblCalsFromFlashToRam() As Boolean

        Dim result = False

        result = SendToolFunctionCmd(ToolFunctionMsg.ReadDblCalsFromFlashToRam)

        ' Get the response message.
        Dim rcvdMsgData = GetToolFuncMsgReponse(ToolFunctionMsg.ReadDblCalsFromFlashToRam)
        If rcvdMsgData IsNot Nothing AndAlso rcvdMsgData.Length = RespMsgDataLength AndAlso rcvdMsgData(1) <> ToolMsgInvalid Then
            If rcvdMsgData(1) = 1 Then
                result = True
            End If
        ElseIf rcvdMsgData IsNot Nothing AndAlso rcvdMsgData.Length > 0 AndAlso rcvdMsgData(1) = ToolMsgInvalid Then
            ToolFunctionReturnCode = rcvdMsgData(3)
        End If
        Return result
    End Function

    Public Shared Function WriteDblCalRamArrayToFlash() As Boolean

        Dim result = False

        result = SendToolFunctionCmd(ToolFunctionMsg.WriteDblCalRamArrayToFlash)

        ' Get the response message.
        Dim rcvdMsgData = GetToolFuncMsgReponse(ToolFunctionMsg.ReadDblCalsFromFlashToRam)
        If rcvdMsgData IsNot Nothing AndAlso rcvdMsgData.Length = RespMsgDataLength AndAlso rcvdMsgData(1) <> ToolMsgInvalid Then
            If rcvdMsgData(1) = 1 Then
                result = True
            End If
        ElseIf rcvdMsgData IsNot Nothing AndAlso rcvdMsgData.Length > 0 AndAlso rcvdMsgData(1) = ToolMsgInvalid Then
            ToolFunctionReturnCode = rcvdMsgData(3)
        End If
        Return result
    End Function

    Public Shared Function DisableEepromWrite() As Boolean

        Dim result = False

        result = SendToolFunctionCmd(ToolFunctionMsg.DisableEepromDataWrite)

        ' Get the response message.
        Dim rcvdMsgData = GetToolFuncMsgReponse(ToolFunctionMsg.DisableEepromDataWrite)
        If rcvdMsgData IsNot Nothing AndAlso rcvdMsgData.Length = RespMsgDataLength AndAlso rcvdMsgData(1) <> ToolMsgInvalid Then
            If rcvdMsgData(1) = 1 Then
                result = True
            End If
        ElseIf rcvdMsgData IsNot Nothing AndAlso rcvdMsgData.Length > 0 AndAlso rcvdMsgData(1) = ToolMsgInvalid Then
            ToolFunctionReturnCode = rcvdMsgData(3)
        End If
        Return result
    End Function

    Public Shared Function EnableEepromWrite() As Boolean

        Dim result = False
        Dim msgData() As Byte = Nothing

        If ToolFuncReqDict.TryGetValue(ToolFunctionMsg.EnableEepromDataWrite, msgData) Then

            ' Add serial number to byte array retrieved from dictionary.
            msgData(4) = _serialNumber(0)
            msgData(5) = _serialNumber(1)
            msgData(6) = _serialNumber(2)
            msgData(7) = _serialNumber(3)
            result = SendToolFunctionCmd(ToolFunctionMsg.EnableEepromDataWrite, msgData)

            ' Get the response message.
            Dim rcvdMsgData = GetToolFuncMsgReponse(ToolFunctionMsg.EnableEepromDataWrite)
            If rcvdMsgData IsNot Nothing AndAlso rcvdMsgData.Length = RespMsgDataLength AndAlso rcvdMsgData(1) <> ToolMsgInvalid Then
                If rcvdMsgData(1) = 1 Then
                    result = True
                End If
            ElseIf rcvdMsgData IsNot Nothing AndAlso rcvdMsgData.Length > 0 AndAlso rcvdMsgData(1) = ToolMsgInvalid Then
                ToolFunctionReturnCode = rcvdMsgData(3)
            End If
        End If
        Return result
    End Function

    Public Shared Function WriteSerialNumberToFlash(ByVal serialNum() As Byte) As Boolean

        Dim result = False
        Dim xmtMsgData() As Byte = Nothing

        If serialNum Is Nothing OrElse serialNum.Length <> _serialNumber.Length Then
            Return result
        End If

        If ToolFuncReqDict.TryGetValue(ToolFunctionMsg.UpdateSerialNumberInFlash, xmtMsgData) Then

            ' Add serial number to byte array retrieved from dictionary.
            xmtMsgData(4) = serialNum(0)
            xmtMsgData(5) = serialNum(1)
            xmtMsgData(6) = serialNum(2)
            xmtMsgData(7) = serialNum(3)
            If SendToolFunctionCmd(ToolFunctionMsg.UpdateSerialNumberInFlash, xmtMsgData) Then

                ' Get the response message.
                Dim rcvdMsgData = GetToolFuncMsgReponse(ToolFunctionMsg.UpdateSerialNumberInFlash)
                If rcvdMsgData IsNot Nothing AndAlso rcvdMsgData.Length = RespMsgDataLength AndAlso rcvdMsgData(1) <> ToolMsgInvalid Then
                    ' Success.
                    _serialNumber(0) = rcvdMsgData(4)
                    _serialNumber(1) = rcvdMsgData(5)
                    _serialNumber(2) = rcvdMsgData(6)
                    _serialNumber(3) = rcvdMsgData(7)
                    If rcvdMsgData(1) = 1 Then
                        result = True
                    End If
                ElseIf rcvdMsgData IsNot Nothing AndAlso rcvdMsgData.Length > 0 AndAlso rcvdMsgData(1) = ToolMsgInvalid Then
                    ToolFunctionReturnCode = rcvdMsgData(3)
                End If
            End If
        End If
        Return result
    End Function

    Public Shared Function WritePartNumberLsbToFlash() As Boolean

        Dim result = False

        result = SendToolFunctionCmd(ToolFunctionMsg.WritePartNumberLsbToFlash)

        ' Get the response message.
        Dim rcvdMsgData = GetToolFuncMsgReponse(ToolFunctionMsg.WritePartNumberLsbToFlash)
        If rcvdMsgData IsNot Nothing AndAlso rcvdMsgData.Length = RespMsgDataLength AndAlso rcvdMsgData(1) <> ToolMsgInvalid Then
            If rcvdMsgData(1) = 1 Then
                result = True
            End If
        ElseIf rcvdMsgData IsNot Nothing AndAlso rcvdMsgData.Length > 0 AndAlso rcvdMsgData(1) = ToolMsgInvalid Then
            ToolFunctionReturnCode = rcvdMsgData(3)
        End If
        Return result
    End Function

    Public Shared Function WritePartNumberMsbToFlash() As Boolean

        Dim result = False

        result = SendToolFunctionCmd(ToolFunctionMsg.WritePartNumberMsbToFlash)

        ' Get the response message.
        Dim rcvdMsgData = GetToolFuncMsgReponse(ToolFunctionMsg.WritePartNumberMsbToFlash)
        If rcvdMsgData IsNot Nothing AndAlso rcvdMsgData.Length = RespMsgDataLength AndAlso rcvdMsgData(1) <> ToolMsgInvalid Then
            If rcvdMsgData(1) = 1 Then
                result = True
            End If
        ElseIf rcvdMsgData IsNot Nothing AndAlso rcvdMsgData.Length > 0 AndAlso rcvdMsgData(1) = ToolMsgInvalid Then
            ToolFunctionReturnCode = rcvdMsgData(3)
        End If
        Return result
    End Function

    Public Shared Function UpdateRamWithDefaultCalVals(ByVal target As Byte) As Boolean

        Dim result = False
        Dim xmtMsgData() As Byte = Nothing

        If Not ToolFuncReqDict.TryGetValue(ToolFunctionMsg.UpdateRamWithDefaultCalVals, xmtMsgData) Then
            Return False
        End If
        ' Add the App/DBL selection to the command message./
        xmtMsgData(3) = target
        result = SendToolFunctionCmd(ToolFunctionMsg.UpdateRamWithDefaultCalVals, xmtMsgData)

        ' Get the response message.
        Dim rcvdMsgData = GetToolFuncMsgReponse(ToolFunctionMsg.UpdateRamWithDefaultCalVals)
        If rcvdMsgData IsNot Nothing AndAlso rcvdMsgData.Length = RespMsgDataLength AndAlso rcvdMsgData(1) <> ToolMsgInvalid Then
            If rcvdMsgData(1) = 1 AndAlso rcvdMsgData(3) = xmtMsgData(3) Then

                ' The response message was formatted correctly.
                ' The value of byte 3 (app/DBL) in the corresponding command
                ' message was echoed.
                result = True
            End If
        ElseIf rcvdMsgData IsNot Nothing AndAlso rcvdMsgData.Length > 0 AndAlso rcvdMsgData(1) = ToolMsgInvalid Then
            ToolFunctionReturnCode = rcvdMsgData(3)
        End If
        Return result
    End Function

    Public Shared Function WriteAppCalsFromRamToFlash() As Boolean

        Dim result = False

        result = SendToolFunctionCmd(ToolFunctionMsg.WriteAppCalsFromRamToFlash)

        ' Get the response message.
        Dim rcvdMsgData = GetToolFuncMsgReponse(ToolFunctionMsg.WriteAppCalsFromRamToFlash)
        If rcvdMsgData IsNot Nothing AndAlso rcvdMsgData.Length = RespMsgDataLength AndAlso rcvdMsgData(1) <> ToolMsgInvalid Then
            If rcvdMsgData(1) = 1 Then
                result = True
            End If
        ElseIf rcvdMsgData IsNot Nothing AndAlso rcvdMsgData.Length > 0 AndAlso rcvdMsgData(1) = ToolMsgInvalid Then
            ToolFunctionReturnCode = rcvdMsgData(3)
        End If
        Return result
    End Function

    Public Shared Function SendCalScanDataRequest(ByVal dataType As CalDataTypes, ByVal calScanDataMsgRate As Byte) As Boolean

        Dim result = False
        Dim xmtMsgData() As Byte = Nothing

        If ToolFuncReqDict.TryGetValue(ToolFunctionMsg.SendCalScanData, xmtMsgData) Then
            xmtMsgData(3) = CByte(dataType)
            xmtMsgData(4) = calScanDataMsgRate
            If SendToolFunctionCmd(ToolFunctionMsg.SendCalScanData, xmtMsgData) Then
                ' Get the response message.
                Dim rcvdMsgData = GetToolFuncMsgReponse(ToolFunctionMsg.SendCalScanData)
                If rcvdMsgData IsNot Nothing AndAlso
                   rcvdMsgData.Length = RespMsgDataLength AndAlso
                   rcvdMsgData(1) <> ToolMsgInvalid Then

                    RfSensorConfiguration.CalibrationEntries = rcvdMsgData(3)
                    RfSensorConfiguration.CalibrationEntries = CUInt(rcvdMsgData(4)) << 8
                    RfSensorConfiguration.OperatingMode = rcvdMsgData(5)

                    ' Range = -55 to 200 deg. C
                    RfSensorConfiguration.BoardTemp = rcvdMsgData(7)
                    RfSensorConfiguration.BoardTemp -= 55
                    If rcvdMsgData(1) = 5 Then
                        result = True
                    End If
                ElseIf rcvdMsgData IsNot Nothing AndAlso rcvdMsgData.Length > 0 AndAlso rcvdMsgData(1) = ToolMsgInvalid Then
                    ToolFunctionReturnCode = rcvdMsgData(3)
                End If
            End If
        End If
        Return result
    End Function

    ' Send DBL Tool Message functions.

    Public Shared Function SendDblWriteInit(ByVal targetMem As WriteTargetMem,
                                                ByVal startAddr As UInteger,
                                                ByVal endAddr As UInteger,
                                                ByRef rcvdMsgData() As Byte) As Boolean
        Dim result = False
        Dim canMsg = New TPCANMsg()

        ' Verify that the targetMem and address values are valid.
        If (Not [Enum].IsDefined(GetType(WriteTargetMem), targetMem)) OrElse startAddr >= endAddr Then
            ' An argument is invalid, so return an empty array.
            rcvdMsgData = New Byte() {}
            Return result
        End If
        ' Configure and send the DBL initiate write command.
        DblErrorCode = 0
        canMsg.ID = DblToolCmdID
        canMsg.LEN = DblMsgDataLenDict(DblSendOpCodes.DblInitateWrite)
        canMsg.MSGTYPE = TPCANMessageType.PCAN_MESSAGE_EXTENDED
        canMsg.DATA = New Byte() {CByte(DblSendOpCodes.DblInitateWrite),
                                       CByte(targetMem), CByte(startAddr),
                                       CByte(startAddr >> 8), CByte(startAddr >> 16),
                                       CByte(endAddr), CByte(endAddr >> 8), CByte(endAddr >> 16)}
        LoadCanMsgQueue(canMsg)
        ' Get the DBL response message, which contains an opcode in the first byte.
        ' If the call returns false (failed), and the data array != null, check the
        ' data array length. If it equals 0, then the DBL failed to respond and
        ' a timeout resulted. If the data array length != 0, then check the
        ' DblErrorCode to see why it failed. If DblErrorCode == 0, then the 
        ' number of data bytes returned was wrong.
        Return GetDblToolMsgReponse(DblRcvOpCodes.DblInitWriteAck, rcvdMsgData)
    End Function

    Public Shared Function SendDblWriteData(ByVal data() As Byte, ByRef rcvdMsgData() As Byte) As Boolean

        Dim result = False
        Dim canMsg = New TPCANMsg()

        ' Validate the data array parameter.
        If data Is Nothing OrElse data.Length = 0 Then

            ' The data array is invalid, so return an empty array.
            rcvdMsgData = New Byte() {}
            Return result
        End If

        ' Configure and send the DBL write memory data command.
        DblErrorCode = 0
        canMsg.ID = DblToolCmdID
        canMsg.LEN = DblMsgDataLenDict(DblSendOpCodes.DblMemWriteData)
        canMsg.MSGTYPE = TPCANMessageType.PCAN_MESSAGE_EXTENDED
        If data.Length <= 7 Then
            canMsg.DATA = New Byte(data.Length) {}
            canMsg.DATA(0) = CByte(DblSendOpCodes.DblMemWriteData)
            For idx As Byte = 0 To data.Length - 1
                canMsg.DATA(idx + 1) = data(idx)
            Next idx
            LoadCanMsgQueue(canMsg)
        End If
        ' Get the DBL response message.
        Return GetDblToolMsgReponse(DblRcvOpCodes.DblMemWriteDataAck, rcvdMsgData)
    End Function

    Public Shared Function SendDblReadInit(ByVal targetMem As ReadTargetMem,
                                               ByVal startAddr As UInteger,
                                               ByVal endAddr As UInteger,
                                               ByRef rcvdMsgData() As Byte) As Boolean
        Dim result = False
        Dim canMsg = New TPCANMsg()

        rcvdMsgData = New Byte() {}
        ' Verify that the targetMem and address values are valid.
        If (Not [Enum].IsDefined(GetType(ReadTargetMem), targetMem)) OrElse startAddr >= endAddr Then

            ' An argument is invalid, so return false.
            Return result
        End If
        ' Configure and send the DBL initiate write command.
        DblErrorCode = 0
        canMsg.ID = DblToolCmdID
        canMsg.LEN = DblMsgDataLenDict(DblSendOpCodes.DblInitiateRead)
        canMsg.MSGTYPE = TPCANMessageType.PCAN_MESSAGE_EXTENDED
        canMsg.DATA = New Byte() {CByte(DblSendOpCodes.DblInitiateRead),
                                      CByte(targetMem), CByte(startAddr),
                                      CByte(startAddr >> 8), CByte(startAddr >> 16),
                                      CByte(endAddr), CByte(endAddr >> 8), CByte(endAddr >> 16)}
        LoadCanMsgQueue(canMsg)

        ' Get the DBL response message.
        If targetMem <> ReadTargetMem.Ram Then
            result = GetDblToolMsgReponse(DblRcvOpCodes.DblInitReadAck, rcvdMsgData)
        Else
            result = True ' There is no Read Init Ack when initiating RAM read.
        End If
        Return result
    End Function

    ' Get response message functions ==============================================================================

    Public Shared Function GetDblReadData(ByRef bytesRemain As UInteger, ByRef rcvdMsgData() As Byte) As Boolean

        Dim result = False
        Dim canMsg = New TPCANMsg()

        rcvdMsgData = New Byte() {}

        ' Get the DBL read data message.
        result = GetDblToolMsgReponse(DblRcvOpCodes.DblMemReadData, rcvdMsgData)
        If Not result Then
            Return result
        End If
        ' Configure and send the DBL read data Ack message.
        If rcvdMsgData.Length <= bytesRemain Then

            ' Adjust the bytes remaining count by the bytes received count
            ' and include it in the read data Ack message.
            bytesRemain -= CUInt(rcvdMsgData.Length)
            canMsg.ID = DblToolCmdID
            canMsg.LEN = DblMsgDataLenDict(DblSendOpCodes.DblMemReadDataAck)
            canMsg.MSGTYPE = TPCANMessageType.PCAN_MESSAGE_EXTENDED
            canMsg.DATA = New Byte() {CByte(DblSendOpCodes.DblMemReadDataAck),
                                           CByte(bytesRemain), CByte(bytesRemain >> 8),
                                           CByte(bytesRemain >> 16)}
            LoadCanMsgQueue(canMsg)
        End If
        Return result
    End Function

    Private Shared Function GetToolFuncMsgReponse(ByVal funcID As ToolFunctionMsg) As Byte()

        Dim rcvdMsgData() As Byte = {}
        Dim _startTime   = Common.HighResTime
        Dim _currentTime = _startTime

        If Not [Enum].IsDefined(GetType(ToolFunctionMsg), funcID) Then
            Return rcvdMsgData
        End If
        Do
            Dim rcvdMsgStruct As CanMsgRcvd = Nothing
            If GetToolFunctionMsg(funcID, rcvdMsgStruct) Then
                rcvdMsgData = New Byte(rcvdMsgStruct.CanMsg.LEN - 1) {}
                Array.Copy(rcvdMsgStruct.CanMsg.DATA, rcvdMsgData, rcvdMsgData.Length)
                Exit Do
            End If
            _currentTime = Common.HighResTime
        Loop While ((_currentTime - _startTime) < RcvMsgTimeout)

        Return rcvdMsgData
    End Function

    Public Shared Function GetToolParamReponseData(ByVal msgID As UInteger) As Byte()

        Dim rcvdMsgData() As Byte = {}
        Dim rcvdMsgStruct As CanMsgRcvd = Nothing
        Dim _startTime   = Common.HighResTime
        Dim _currentTime = _startTime

        Do
            If GetToolParamRespMsg(msgID, rcvdMsgStruct) Then
                rcvdMsgData = New Byte(rcvdMsgStruct.CanMsg.LEN - 1) {}
                Array.Copy(rcvdMsgStruct.CanMsg.DATA, rcvdMsgData, rcvdMsgStruct.CanMsg.LEN)
                Exit Do
            End If
            _currentTime = Common.HighResTime
        Loop While (_currentTime - _startTime) < RcvMsgTimeout

        Return rcvdMsgData
    End Function

    Private Shared Function GetToolAppCalReponse(ByVal msgID As UInteger) As Byte()

        Dim rcvdMsgData() As Byte = {}
        Dim _startTime   = Common.HighResTime
        Dim _currentTime = _startTime
        Do
            Dim rcvdMsgStruct As CanMsgRcvd = Nothing
            If GetToolAppCalRespMsg(msgID, rcvdMsgStruct) Then
                rcvdMsgData = New Byte(rcvdMsgStruct.CanMsg.LEN - 1) {}
                Array.Copy(rcvdMsgStruct.CanMsg.DATA, rcvdMsgData, rcvdMsgStruct.CanMsg.LEN)
                Exit Do
            End If
            _currentTime = Common.HighResTime
        Loop While (_currentTime - _startTime < RcvMsgTimeout)

        Return rcvdMsgData
    End Function

    Private Shared Function GetDblToolMsgReponse(ByVal opCode As DblRcvOpCodes, ByRef rcvdMsgData() As Byte) As Boolean

        Dim result       = False
        Dim _startTime   = Common.HighResTime
        Dim _currentTime = _startTime

        rcvdMsgData      = New Byte() {}
        ' Validate the opCode value.
        If Not [Enum].IsDefined(GetType(DblRcvOpCodes), opCode) Then
            Return False
        End If
        Do
            Dim rcvdMsgStruct As CanMsgRcvd = Nothing
            If GetDblToolResponseMsg(CByte(opCode), rcvdMsgStruct) Then
                rcvdMsgData = New Byte(rcvdMsgStruct.CanMsg.LEN - 1) {}
                Array.Copy(rcvdMsgStruct.CanMsg.DATA, rcvdMsgData, rcvdMsgData.Length)
                Exit Do
            End If
            _currentTime = Common.HighResTime
        Loop While (_currentTime - _startTime < RcvMsgTimeout)
        If rcvdMsgData IsNot Nothing AndAlso rcvdMsgData.Length > 0 Then
            If rcvdMsgData(0) <> CByte(DblRcvOpCodes.DblFaultResp) Then
                If rcvdMsgData.Length = DblRespDataLenDict(opCode) Then

                    ' We've received the correct number of bytes.
                    result = True
                End If
            Else
                ' Save the DBL error code.
                DblErrorCode = rcvdMsgData(1)
            End If
        End If
        Return result
    End Function
End Class
